//
//  OIHEmptyView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>


@class OIHEmptyView;

@protocol OIHEmptyViewDelegate <NSObject>

-(void)emptyViewDidPressReloadButton: (OIHEmptyView *)emptyView;

@end


@interface OIHEmptyView : UIView

@property (nonatomic, weak) id<OIHEmptyViewDelegate> delegate;
@property (nonatomic) UIButton *reloadButton;
@property (nonatomic) NSString *reloadButtonTitle;
@property (nonatomic) NSString *descriptionText;

-(id)initWithImage: (UIImage *)image descriptionText: (NSString *)descriptionText;

@end
