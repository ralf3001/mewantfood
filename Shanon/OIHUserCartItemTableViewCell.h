//
//  OIHUserCartItemTableViewCell.h
//  Shanon
//
//  Created by Ralf Cheung on 23/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHMealModel.h"
#import "OIHFloatingTableViewCell.h"

@interface OIHUserCartItemTableViewCell : OIHFloatingTableViewCell
@property (nonatomic) UILabel *nameLabel;
@property (nonatomic) UILabel *secondaryLabel;
@property (nonatomic) UILabel *thirdLabel;
@property (nonatomic) UILabel *coloredLabel;

@property (nonatomic) UILabel *rightLabel;

@property (nonatomic) UIImageView *itemImageView;

@property (nonatomic) OIHMealModel *itemInfo;

@end
