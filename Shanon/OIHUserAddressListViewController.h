//
//  OIHUserAddressListViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 15/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OIHAddressModel.h"


@protocol OIHUserAddressViewControllerDelegate <NSObject>

@required
-(void)userDidSelectAddress: (OIHAddressModel *)address;

@end

@interface OIHUserAddressListViewController : UIViewController

@property (nonatomic, weak) id<OIHUserAddressViewControllerDelegate> delegate;

@end
