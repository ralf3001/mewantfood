//
//  OIHFeedSearchTableViewCell.h
//  Shanon
//
//  Created by Ralf Cheung on 1/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHFeedSearchTableViewCell : UITableViewCell
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *detailLabel;

@end
