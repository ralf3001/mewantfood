//
//  OIHCalendarInfiniteScrollView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCalendarInfiniteScrollView.h"

@interface OIHCalendarInfiniteScrollView ()

@property(nonatomic) UIView *labelContainerView;
@property(nonatomic) NSMutableArray<OIHCalendarDay *> *visibleLabels;
@property(nonatomic) UIView *todayViewBar;

@end

@implementation OIHCalendarInfiniteScrollView
#pragma mark - Layout

- (id)init {

  if ((self = [super init])) {
    
    _visibleLabels = [NSMutableArray new];
    _labelContainerView = [[UIView alloc] init];
    [self addSubview:self.labelContainerView];

//    [self.labelContainerView setUserInteractionEnabled:NO];

    // hide horizontal scroll indicator so our recentering trick is not revealed
    [self setShowsHorizontalScrollIndicator:NO];

    

  }
  return self;
}

-(void)setDataSource:(id<OIHMealSummaryViewDelegate>)dataSource{
  
  _dataSource = dataSource;
  __weak typeof(self) weakSelf = self;
  
  if ([self.dataSource
       respondsToSelector:@selector(mealSummaryFetchFutureEvents:date:)] &&
      self.isFetchingFuture == NO) {
    self.isFetchingFuture = YES;
    [self.dataSource mealSummaryFetchFutureEvents:weakSelf
                                             date:[NSDate date]];
  }

  
}


// From Apple's Stree Scrolling Demo Code
// recenter content periodically to achieve impression of infinite scrolling
- (void)recenterIfNecessary {
  
  CGPoint currentOffset = [self contentOffset];
  CGFloat contentWidth = [self contentSize].width;
  CGFloat centerOffsetX = (contentWidth - [self bounds].size.width) / 2.0;
  CGFloat distanceFromCenter = fabs(currentOffset.x - centerOffsetX);

  if (distanceFromCenter > (contentWidth / 4.0)) {
    self.contentOffset = CGPointMake(centerOffsetX, currentOffset.y);

    // move content by the same amount so it appears to stay still
    for (OIHCalendarDay *label in self.visibleLabels) {
      CGPoint center =
          [self.labelContainerView convertPoint:label.center toView:self];
      center.x += (centerOffsetX - currentOffset.x);
      label.center = [self convertPoint:center toView:self.labelContainerView];
      
      for (UIView *view in label.mealViewList) {
        CGPoint center =
        [self.labelContainerView convertPoint:view.center toView:self];
        center.x += (centerOffsetX - currentOffset.x);
        view.center = [self convertPoint:center toView:self.labelContainerView];

      }
      
    }

  }
}

- (void)layoutSubviews {

  [super layoutSubviews];

  self.contentSize = CGSizeMake(5000, self.frame.size.height);
  self.labelContainerView.frame =
      CGRectMake(0, 0, self.contentSize.width, self.contentSize.height);

  [self recenterIfNecessary];

  // tile content in visible bounds
  CGRect visibleBounds =
      [self convertRect:[self bounds] toView:self.labelContainerView];
  CGFloat minimumVisibleX = CGRectGetMinX(visibleBounds);
  CGFloat maximumVisibleX = CGRectGetMaxX(visibleBounds);

  [self tileLabelsFromMinX:minimumVisibleX toMaxX:maximumVisibleX];
}

- (void)reloadView {
  [self setNeedsLayout];
}

#pragma mark - Meal Tiling

- (OIHCalendarMealView *)insertMealView:(OIHMealModel *)meal {

  OIHCalendarMealView *mealView =
      [[OIHCalendarMealView alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
  mealView.meal = meal;

  return mealView;
}

#pragma mark - Label Tiling

- (OIHCalendarDay *)insertLabel:(NSDate *)date {

  OIHCalendarDay *label =
      [[OIHCalendarDay alloc] initWithFrame:CGRectMake(0, 0, 100, 80)];
  label.date = date;

  [self.labelContainerView addSubview:label];

  return label;
}

- (CGFloat)placeNewLabelOnRight:(CGFloat)rightEdge withDate:(NSDate *)date {

  OIHCalendarDay *label = [self insertLabel:date];
  [self.visibleLabels
      addObject:label]; // add rightmost label at the end of the array

  CGRect frame = [label frame];
  frame.origin.x = rightEdge;
  frame.origin.y =
      [self.labelContainerView bounds].size.height - frame.size.height;

  [label setFrame:frame];

  [self todayView:date dayView:label];
  
  [self mealsForDay:label];

  return CGRectGetMaxX(frame);
}

- (CGFloat)placeNewLabelOnLeft:(CGFloat)leftEdge withDate:(NSDate *)date {

  OIHCalendarDay *label = [self insertLabel:date];
  [self.visibleLabels
      insertObject:label
           atIndex:0]; // add leftmost label at the beginning of the array

  CGRect frame = [label frame];
  frame.origin.x = leftEdge - frame.size.width;
  frame.origin.y =
      [self.labelContainerView bounds].size.height - frame.size.height;
  [label setFrame:frame];

  [self todayView:date dayView:label];
  [self mealsForDay:label];

  return CGRectGetMinX(frame);
}

- (void)tileLabelsFromMinX:(CGFloat)minimumVisibleX
                    toMaxX:(CGFloat)maximumVisibleX {

  // the upcoming tiling logic depends on there already being at least one label
  // in the visibleLabels array, so
  // to kick off the tiling we need to make sure there's at least one label

  if ([self.visibleLabels count] == 0) {
    [self placeNewLabelOnRight:minimumVisibleX withDate:[NSDate date]];
  }

  [self addCalendarDays:minimumVisibleX toMaxX:maximumVisibleX];

  // add labels that are missing on right side
}


//Current Issue: View isn't updated when data is updated

- (void)addCalendarDays:(CGFloat)minimumVisibleX
                 toMaxX:(CGFloat)maximumVisibleX {

  OIHCalendarDay *lastLabel = [self.visibleLabels lastObject];
  CGFloat rightEdge = CGRectGetMaxX([lastLabel frame]);
  NSDate *tmr = nil;
  
  
  //because scrollview allocates x more cells off-screen. so after initial fetching, this loop will not be called as the maximum number of cells have been allocated
  
  while (rightEdge < (maximumVisibleX + 600)) {

    lastLabel = [self.visibleLabels lastObject]; // deal with initial condition
    NSDate *lastDay = lastLabel.date;
    tmr = [lastDay dateByAddingTimeInterval:24 * 60 * 60]; // increase by a day
    rightEdge = [self placeNewLabelOnRight:rightEdge withDate:tmr]; //something needs to be done here with recycling views
    
  }

  __weak typeof(self) weakSelf = self;

  if ([self.dataSource
          respondsToSelector:@selector(mealSummaryFetchFutureEvents:date:)] &&
      _isFetchingFuture == NO) {
    self.isFetchingFuture = YES;
    [self.dataSource mealSummaryFetchFutureEvents:weakSelf date:tmr];
  }

  // add labels that are missing on left side
  OIHCalendarDay *firstLabel = self.visibleLabels[0];
  CGFloat leftEdge = CGRectGetMinX([firstLabel frame]);
  NSDate *ytd = nil;
  while (leftEdge > (minimumVisibleX - 600)) {

    firstLabel = self.visibleLabels[0];
    ytd = [firstLabel.date
        dateByAddingTimeInterval:-24 * 60 * 60]; // decrease by a day
    leftEdge = [self placeNewLabelOnLeft:leftEdge withDate:ytd];
  }

  if ([self.dataSource
          respondsToSelector:@selector(mealSummaryFetchPastEvents:date:)] &&
      _isFetchingPast == NO) {
    self.isFetchingPast = YES;
    [self.dataSource mealSummaryFetchPastEvents:weakSelf date:ytd];
  }
  
  
//=================================================================
  // remove labels that have fallen off right edge
  lastLabel = [self.visibleLabels lastObject];

  while ([lastLabel frame].origin.x > maximumVisibleX + 600) {

    for (OIHCalendarMealView *meal in lastLabel.mealViewList) {
      [meal removeFromSuperview];
    }
    
    if ([lastLabel.date onSameDay:[NSDate date]]) {
      [_todayViewBar removeFromSuperview];
    }
    
    [lastLabel removeFromSuperview];
    [self.visibleLabels removeLastObject];

    lastLabel = [self.visibleLabels lastObject];
  }

  // remove labels that have fallen off left edge
  firstLabel = self.visibleLabels[0];
  while (CGRectGetMaxX([firstLabel frame]) < minimumVisibleX - 600) {

    for (OIHCalendarMealView *meal in firstLabel.mealViewList) {
      [meal removeFromSuperview];
    }

    if ([lastLabel.date onSameDay:[NSDate date]]) {
      [_todayViewBar removeFromSuperview];
    }
    
    [firstLabel removeFromSuperview];
    [self.visibleLabels removeObjectAtIndex:0];
    firstLabel = self.visibleLabels[0];
  }
}


-(void)initialFetch{
  
  for (OIHCalendarDay *day in _visibleLabels) {
    [self mealsForDay:day];
  }
  
}

-(void)todayView: (NSDate *)date dayView: (OIHCalendarDay *)dayView{
  
  if ([date onSameDay:[NSDate date]]) {
    _todayViewBar = [[UIView alloc] initWithFrame:CGRectMake(dayView.frame.origin.x, 0, dayView.frame.size.width, self.labelContainerView.frame.size.height - dayView.frame.size.height)];
    _todayViewBar.backgroundColor = [UIColor highlightColor];
    [self.labelContainerView insertSubview:_todayViewBar belowSubview:dayView];
  }

}


- (CGFloat)mealsForDay:(OIHCalendarDay *)dayView {

  __weak typeof(self) weakSelf = self;

  [dayView.mealViewList addObjectsFromArray:[self.dataSource mealSummaryCellForCalendar:weakSelf onDate:dayView.date]];

  for (OIHCalendarMealView *mealView in dayView.mealViewList) {
    
    [self.labelContainerView addSubview:mealView];

    mealView.date = dayView.date;
    
    CGRect mealFrame = dayView.frame;
    mealFrame.size.height = 80;
    
    switch (mealView.meal.mealType) {
    case Breakfast:
      mealFrame.origin.y = 20;
      break;
    case Lunch:
      mealFrame.origin.y = 110;
      break;
    case Dinner:
      mealFrame.origin.y = 200;
      break;
    case Snack:
      mealFrame.origin.y = 290;
      break;
    default:
      mealFrame.origin.y = 380;
      break;
    }

    mealView.frame = mealFrame;
  }
  
  return CGRectGetMaxX(dayView.frame);
}


@end
