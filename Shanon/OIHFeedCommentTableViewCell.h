//
//  OIHFeedCommentTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHActivityFeed.h"


@interface OIHFeedCommentTableViewCell : UITableViewCell
@property (nonatomic) UIImageView *profileImageView;
@property (nonatomic) UILabel *userLabel;
@property (nonatomic) UILabel *contentLabel;
@property (nonatomic) UILabel *timeLabel;

@property (nonatomic) OIHActivityFeed *comment;

@end
