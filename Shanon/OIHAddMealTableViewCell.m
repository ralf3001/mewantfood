//
//  OIHAddMealTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHAddMealTableViewCell.h"

@implementation OIHAddMealTableViewCell
@synthesize descriptionContentTextField;
@synthesize descriptionLabel;
@synthesize tableView = _tableView;

- (void)awakeFromNib {
  // Initialization code
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {

  [super setHighlighted:highlighted animated:animated];
  [_tableView endEditing:highlighted];
}

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {

  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {

    descriptionLabel = [UILabel new];
    descriptionLabel.numberOfLines = 0;
    descriptionLabel.adjustsFontSizeToFitWidth = YES;
    descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:descriptionLabel];

    descriptionContentTextField = [UITextView new];
    descriptionContentTextField.font =
        [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    descriptionContentTextField.scrollEnabled = NO;
    descriptionContentTextField.delegate = self;
    descriptionContentTextField.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:descriptionContentTextField];

    [self.contentView
        addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"H:|-[descriptionLabel(==70)]-["
                                            @"descriptionContentTextField]-|"
                                    options:NSLayoutFormatAlignAllCenterY
                                    metrics:0
                                      views:NSDictionaryOfVariableBindings(
                                                descriptionContentTextField,
                                                descriptionLabel)]];

    [self.contentView
        addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"V:|-[descriptionLabel]-|"
                                    options:0
                                    metrics:0
                                      views:NSDictionaryOfVariableBindings(
                                                descriptionLabel)]];

    [self.contentView
        addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:
                    @"V:|-[descriptionContentTextField]-|"
                                    options:0
                                    metrics:0
                                      views:NSDictionaryOfVariableBindings(
                                                descriptionContentTextField)]];
  }
  return self;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {

  if ([self.delegate respondsToSelector:@selector(textViewDidBeginEditing:)]) {
    [self.delegate textViewDidBeginEditing:self];
  }
}

- (void)textViewDidEndEditing:(UITextView *)textView {

  if (![textView.text isEqualToString:@""] &&
      [self.delegate
          respondsToSelector:@selector(textViewFinishedEditing:cell:)]) {
    [self.delegate textViewFinishedEditing:textView cell:self];
  }
}

- (void)textViewDidChange:(UITextView *)textView {

  if (![textView.text isEqualToString:@""]) {

    [self searchPlace:textView.text];

    CGSize size = textView.bounds.size;
    CGSize newSize =
        [textView sizeThatFits:CGSizeMake(size.width, CGFLOAT_MAX)];

    if (size.height <= newSize.height) {
      [_tableView beginUpdates];
      [_tableView endUpdates];

      [_tableView scrollToRowAtIndexPath:[_tableView indexPathForCell:self]
                        atScrollPosition:UITableViewScrollPositionNone
                                animated:NO];
    }
  }
}

- (void)searchPlace:(NSString *)place {

  //    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
  //
  //    [geocoder geocodeAddressString:place
  //    completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks,
  //    NSError * _Nullable error) {
  //        NSLog(@"================");
  //
  //        for (CLPlacemark *placemark in placemarks) {
  //            NSLog(@"%@", placemark.name);
  //        }
  //        NSLog(@"================");
  //
  //    }];

  //    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
  //    request.naturalLanguageQuery = place;
  //
  //    MKLocalSearch *search = [[MKLocalSearch alloc]initWithRequest:request];
  //
  //    [search startWithCompletionHandler:^(MKLocalSearchResponse
  //                                         *response, NSError *error) {
  //        NSLog(@"================");
  //
  //        if (response.mapItems.count == 0)
  //            NSLog(@"No Matches");
  //        else{
  //
  //            for (MKMapItem *item in response.mapItems){
  //                NSLog(@"%@", item.name);
  //            }
  //        }
  //        NSLog(@"================");
  //
  //    }];
  //
}

@end
