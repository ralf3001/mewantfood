//
//  OIHRestaurantListViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 13/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantListViewController.h"

#import "OIHRestaurantInfoTableViewCell.h"
#import "OIHMealInfoViewController.h"
#import "OIHEmptyView.h"
#import "OIHMealModel.h"
#import "OIHRestaurantMealDetailViewController.h"
#import "StretchyHeaderCollectionViewLayout.h"
#import "OIHUserProfileHeaderCollectionReusableView.h"
#import "OIHRestaurantListHeaderCollectionReusableView.h"
#import "OIHRestaurantMealInfoCollectionViewCell.h"
#import "OIHSearchResultsModel.h"
#import "OIHLocationManager.h"

@interface OIHRestaurantListViewController () <CLLocationManagerDelegate, OIHEmptyViewDelegate, OIHMealDetailDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, OIHRestaurantMenuListHeaderDelegate, UINavigationControllerDelegate, OIHLocationManagerDelegate, UIViewControllerPreviewingDelegate>

@property (nonatomic) CLLocationManager *locationManager;

@property (nonatomic) BOOL isFetching;
@property (nonatomic) OIHSearchResultsModel *searchResult;

//@property (nonatomic) NSMutableArray <OIHSearchResultsModel *> *foodArray;
@property (nonatomic) OIHEmptyView *emptyView;
@property (nonatomic) NSArray <CLLocation *>* locations;

@property (nonatomic) UIButton *popNavigationControllerButton;
@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) NSDictionary *mealTypeInfo;

//Refresh control and refresh animation
@property (nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) UIView *refreshLoadingView;
@property (nonatomic) UIView *refreshColorView;
@property (nonatomic) UIImageView *refreshJesusImageView;
@property (nonatomic) UIImageView *refreshAdamImageView;
@property (assign) BOOL isRefreshIconsOverlap;


@end

@implementation OIHRestaurantListViewController
@synthesize locationManager;
//@synthesize tableView = _tableView;
@synthesize searchResult;
@synthesize isFetching;
@synthesize userId;
@synthesize emptyView;
@synthesize friendProfile;
@synthesize popNavigationControllerButton;
@synthesize mealTypeInfo;

//objection_requires(@"emptyView");


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if ([self.traitCollection respondsToSelector:@selector(forceTouchCapability)] &&
        (self.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable)){
        [self registerForPreviewingWithDelegate:self sourceView:self.view];
    }
    [self refresh];
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
    
}




-(void)setupCollectionView{
    
    if (!_collectionView) {
        
        StretchyHeaderCollectionViewLayout *stretchyLayout;
        stretchyLayout = [[StretchyHeaderCollectionViewLayout alloc] init];
        [stretchyLayout setSectionInset:UIEdgeInsetsMake(0, 10.0, 10.0, 10.0)];
        [stretchyLayout setHeaderReferenceSize:CGSizeMake(320.0, self.view.frame.size.height)];
        [stretchyLayout setFooterReferenceSize:CGSizeMake(320.0, 0.0)];

        stretchyLayout.minimumInteritemSpacing = 0;
        stretchyLayout.minimumLineSpacing = 0;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:stretchyLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView setAlwaysBounceVertical:YES];
        [_collectionView setShowsVerticalScrollIndicator:NO];
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        [_collectionView registerClass:[OIHRestaurantMealInfoCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        
        [self.view addSubview:_collectionView];
        
        
        [_collectionView registerClass:[OIHRestaurantListHeaderCollectionReusableView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:@"header"];
        
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
        
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
        
        
        [self setupRefreshControl];

        
        if (self.navigationController.viewControllers.count != 1) {
            
            
        }
        
    }else{
        
        [self.collectionView reloadData];
        
    }
    
    
    
    
    
}


-(void)setupRefreshControl{
    
    CGRect frame = CGRectMake(0, 0, CGRectGetWidth(self.collectionView.frame), 500);
    
    _refreshControl = [[UIRefreshControl alloc] initWithFrame:frame];
    _refreshControl.tintColor = [UIColor clearColor];
    
    _refreshLoadingView = [[UIView alloc] init];
    _refreshLoadingView.clipsToBounds = YES;
    _refreshLoadingView.backgroundColor = [UIColor whiteColor];
    [_refreshControl addSubview:_refreshLoadingView];
    
    _refreshColorView = [[UIView alloc] initWithFrame:_refreshControl.frame];
    
    _refreshJesusImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jesus"]];
    [_refreshLoadingView addSubview:_refreshJesusImageView];
    
    _refreshAdamImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"adam"]];
    [_refreshLoadingView addSubview:_refreshAdamImageView];
    
    [_refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [_collectionView addSubview:_refreshControl];
    
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *view;
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        OIHRestaurantListHeaderCollectionReusableView *header = (OIHRestaurantListHeaderCollectionReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"header" forIndexPath:indexPath];
        
        [header.imageView sd_setImageWithURL:[NSURL URLWithString:mealTypeInfo[@"imageURL"]] placeholderImage:[UIImage imageFromColor:[UIColor colorWithWhite:0.9 alpha:1.0f]]];
        
        header.delegate = self;
        header.descriptionTextView.text = NSLocalizedString(mealTypeInfo[@"mealType"], @"");
        
        view = header;
        
    }else{
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"footer" forIndexPath:indexPath];
        view = footer;
    }
    
    
    return view;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.frame.size.width, 160);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv {
    return 1;
}

- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section {
    return searchResult.meals.count;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    OIHRestaurantMealDetailViewController *vc = [OIHRestaurantMealDetailViewController new];
    vc.mealInfo = searchResult.meals[indexPath.row];
    
    vc.delegate = self;
    
    [self presentViewController:vc animated:YES completion:nil];
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHRestaurantMealInfoCollectionViewCell *cell = (OIHRestaurantMealInfoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    OIHMealModel *meal = searchResult.meals[indexPath.row];
    
    cell.mealModel = meal;
    
    return cell;
}


-(void)createEmptyView: (UIImage *)image message: (NSString *)message reloadButtonAction: (SEL)action reloadButtonTitle: (NSString *)title{
    
    if (!emptyView) {
        [_collectionView removeFromSuperview];
        _collectionView = nil;
        
        emptyView = [[OIHEmptyView alloc] initWithImage:image descriptionText:message];
        [emptyView.reloadButton addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
        emptyView.reloadButtonTitle = title;
        emptyView.translatesAutoresizingMaskIntoConstraints = NO;
        emptyView.delegate = self;
        
        [self.view addSubview:emptyView];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
        
    }
    
}


#pragma mark - <OIHMealDetailDelegate>
-(void)userDidCancelOrder: (OIHRestaurantMealDetailViewController *)viewController{
    
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)userDidPressConfirmOrder:(OIHMealModel *)meal viewController:(OIHRestaurantMealDetailViewController *)viewController{
 
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    OIHMealInfoViewController *vc = [[OIHMealInfoViewController alloc] initWithResults:searchResult];
    vc.friendProfile = self.friendProfile;
    searchResult.purchaseItem = meal;
    
    
    [self.navigationController pushViewController:vc animated:YES];

}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    BOOL isHeaderViewHidden = scrollView.contentOffset.y > 40;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"flipImageView" object:@{@"isHeaderViewHidden": [NSNumber numberWithBool:isHeaderViewHidden]}];
    
    if (scrollView.contentOffset.y < 0) {
        
        CGRect refreshBounds = _refreshControl.bounds;
        CGFloat pullDistance = MAX(0.0, -self.refreshControl.frame.origin.y);
        
        CGFloat midX = self.collectionView.center.x;
        
        CGFloat jesusHeight = CGRectGetHeight(self.refreshJesusImageView.bounds);
        CGFloat jesusHeightHalf = jesusHeight / 2.0f;
        
        CGFloat jesusWidth = CGRectGetWidth(self.refreshJesusImageView.bounds);
        
        CGFloat adamHeight = CGRectGetHeight(self.refreshAdamImageView.bounds);
        CGFloat adamHeightHalf = adamHeight / 2.0f;
        
        CGFloat adamWidth = CGRectGetWidth(self.refreshAdamImageView.bounds);
        CGFloat adamWidthHalf = adamWidth / 2.0f;
        
        CGFloat pullRatio = MIN( MAX(pullDistance, 0.0), 100.0) / 100.0;

        CGFloat adamYOffset = 0.04 * adamHeight;
        //if there's a hamburger/or some other food in god's hand, the offset can be ignored
        
        CGFloat jesusY = pullDistance / 2.0 - jesusHeightHalf;
        CGFloat adamY = pullDistance / 2.0f - adamHeightHalf - adamYOffset;

        CGFloat adamXOffset = 0.13 * adamWidth;
        
        CGFloat jesusX = (midX + jesusWidth) - (jesusWidth * pullRatio);
        CGFloat adamX = (midX - adamWidth - adamWidthHalf) + (adamWidthHalf * pullRatio) + adamXOffset;

        self.isRefreshIconsOverlap = fabs(jesusX - adamX) < 1.0f;

        if (self.isRefreshIconsOverlap || self.refreshControl.isRefreshing) {
            jesusX = midX;
            adamX = midX - adamWidth + adamXOffset;
        }
        
        CGRect jesusFrame = self.refreshJesusImageView.frame;
        jesusFrame.origin.x = jesusX;
        jesusFrame.origin.y = jesusY;
        
        CGRect adamFrame = self.refreshAdamImageView.frame;
        adamFrame.origin.x = adamX;
        adamFrame.origin.y = adamY;
        
        self.refreshJesusImageView.frame = jesusFrame;
        self.refreshAdamImageView.frame = adamFrame;
        
        refreshBounds.size.height = pullDistance;
        
        self.refreshLoadingView.frame = refreshBounds;
        
        if (self.refreshControl.isRefreshing) {
//            [self animateRefreshView];
        }

    }

}


-(void)refresh{
    
    if (!self.friendProfile) {
        [OIHLocationManager sharedInstance].delegate = self;
        [[OIHLocationManager sharedInstance] requestLocation];
    }else{
        [self whatToEat:kCLLocationCoordinate2DInvalid];
    }
    
}


-(void)whatToEat: (CLLocationCoordinate2D )coordinates{
    
    [SVProgressHUD show];

    NSMutableDictionary *postData = [NSMutableDictionary new];
    
    if (CLLocationCoordinate2DIsValid(coordinates)) {
        postData[@"lng"] = [NSNumber numberWithDouble:coordinates.longitude];
        postData[@"lat"] = [NSNumber numberWithDouble:coordinates.latitude];
    }else{
        postData[@"userId"] = friendProfile._id;
    }
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"whatToEat" httpMethod:HTTP_GET];
    setting.requestData = postData;
    setting.cachePolicy = IgnoreCache;
    
    __weak __typeof__(self) weakSelf = self;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {

        [SVProgressHUD dismiss];
        [_refreshControl endRefreshing];
        
        if (!error) {

            searchResult = [[OIHSearchResultsModel alloc] initWithInfo:data];
            
            if (searchResult.meals.count > 0) {

                [self setupCollectionView];

                NSString *mealType = [searchResult.meals firstObject].mealTypeString;
                
                typeof(weakSelf) strongSelf = weakSelf;

                if (strongSelf) {
                    
                    NSArray *images;
                    
                    if ([mealType isEqualToString:@"breakfast"]) {
                        images = [NSArray arrayWithObject:@"https://s3-us-west-2.amazonaws.com/oih-location-time-of-day-image/breakfast-1.jpg"];
                    }else if ([mealType isEqualToString:@"lunch"]){
                        images = [NSArray arrayWithObjects:@"https://s3-us-west-2.amazonaws.com/oih-location-time-of-day-image/lunch_1.jpg",
                                  @"https://s3-us-west-2.amazonaws.com/oih-location-time-of-day-image/lunch_2.jpg",
                                  nil];
                    }else if([mealType isEqualToString:@"dinner"]){
                        images = [NSArray arrayWithObjects:@"https://s3-us-west-2.amazonaws.com/oih-location-time-of-day-image/dinner_1.jpg",
                                  @"https://s3-us-west-2.amazonaws.com/oih-location-time-of-day-image/dinner_2.jpg",
                                  @"https://s3-us-west-2.amazonaws.com/oih-location-time-of-day-image/dinner_3.jpg",
                                  @"https://s3-us-west-2.amazonaws.com/oih-location-time-of-day-image/dinner_4.jpg",
                                  nil];
                        
                    }else{
                        images = [NSArray arrayWithObject:@"https://s3-us-west-2.amazonaws.com/oih-location-time-of-day-image/breakfast-1.jpg"];
                        
                    }
                    
                    
                    NSInteger randomNumber = arc4random() % images.count;
                    
                    mealTypeInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                    images[randomNumber], @"imageURL",
                                    mealType, @"mealType",
                                    nil];
                    
                }


            }else{
                
                [self createEmptyView:[IonIcons imageWithIcon:ion_ios_list_outline size:90 color:[UIColor lightGrayColor]] message:@"Either we haven't reached your city yet or no one cares about you. Don't worry, we're working on it." reloadButtonAction:@selector(headerDidPressPopNavigationButton) reloadButtonTitle:NSLocalizedString(@"BACK", @"Back")];

            }
            
            
            
        }else{
            
            NSString *errorMsg;
            
            if (data[@"error"]) {
                errorMsg = NSLocalizedString(data[@"error"], @"");
            }else{
                errorMsg = [error localizedDescription];
            }
            
            [self createEmptyView:[UIImage imageNamed:@"sadface_icon"] message:errorMsg reloadButtonAction:@selector(headerDidPressPopNavigationButton) reloadButtonTitle:NSLocalizedString(@"BACK", @"Back")];
            
        }
    }];
    
}

- (nullable UIViewController *)previewingContext:(id <UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location{

    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    previewingContext.sourceRect = attributes.frame;
    
    OIHRestaurantMealDetailViewController *vc = [OIHRestaurantMealDetailViewController new];
    vc.delegate = self;
    vc.mealInfo = searchResult.meals[indexPath.row];
//    vc.restaurantInfo = foodArray[indexPath.row];
    
    return vc;
}


- (void)previewingContext:(id <UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit {
    
    [self presentViewController:viewControllerToCommit animated:YES completion:nil];
}


- (NSArray <id <UIPreviewActionItem>> *)previewActionItems{
    
    UIPreviewAction *buy = [UIPreviewAction actionWithTitle:NSLocalizedString(@"BUY", @"Buy") style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        
        
//        OIHRestaurantMealDetailViewController *detailVC = (OIHRestaurantMealDetailViewController *)previewViewController;
        
//        OIHMealInfoViewController *vc = [[OIHMealInfoViewController alloc] initWithRestaurant:detailVC.restaurantInfo];
//        vc.friendProfile = self.friendProfile;
//        vc.mealsList = foodArray;
        
        [previewViewController dismissViewControllerAnimated:YES completion:nil];
        
//        [self.navigationController pushViewController:vc animated:YES];

    }];
    
    UIPreviewAction *cancel = [UIPreviewAction actionWithTitle:NSLocalizedString(@"CANCEL", @"Cancel") style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        
    }];
    
    return @[buy, cancel];
    
}

#pragma mark - <OIHEmptyViewDelegate>

-(void)emptyViewDidPressReloadButton:(OIHEmptyView *)emptyView{
    
    CLLocation* location = [self.locations lastObject];
    [self whatToEat:location.coordinate];
    
}

-(NSString *)coordinatesToString: (CLLocationCoordinate2D)coordinate{
    return [NSString stringWithFormat:@"%f, %f", coordinate.longitude, coordinate.latitude];
}



#pragma mark - <OIHRestaurantMenuListHeaderDelegate>

-(void)headerScrollDownButtonDidPress{
    
    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(void)headerDidPressPopNavigationButton{

    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)openSettings{
    
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:url];
    
}

-(void)locationManagerDidUpdateLocation:(CLLocation *)location{
    
    [self whatToEat:location.coordinate];
}


-(void)locationManagerDidFailWithError:(CLError)error{
    
    [SVProgressHUD dismiss];
    
    NSString *errorMsg = nil;
    SEL reloadAction;
    NSString *reloadButtonTitle = nil;
    
    switch (error) {
        case kCLErrorLocationUnknown:
            errorMsg = NSLocalizedString(@"LOCATION_MANAGER_ERROR_UNKOWN_LOCATION", @"Unknown location");
            reloadAction = @selector(headerDidPressPopNavigationButton);
            reloadButtonTitle = NSLocalizedString(@"BACK", @"Back");
            break;
        case kCLErrorDenied:
            errorMsg = [NSString stringWithFormat:NSLocalizedString(@"LOCATION_MANAGER_ERROR_PERMISSION_%@", @" [app name] permission has no permission"), [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleExecutable"]];
            reloadAction = @selector(openSettings);
            reloadButtonTitle = NSLocalizedString(@"LOCATION_MANAGER_SETTINGS", @"Settings");
            //need to bold the words '位置' and '使用 App 期間'
            break;
        case kCLErrorNetwork:
            errorMsg = NSLocalizedString(@"LOCATION_MANAGER_ERROR_NETWORK_ERROR", @"network error");
            reloadAction = @selector(openSettings);
            break;
            
        default:
            break;
    }
    
    [self createEmptyView:[UIImage imageNamed:@"sadface_icon"] message:errorMsg reloadButtonAction:reloadAction reloadButtonTitle: reloadButtonTitle];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
