//
//  OIHFriendsListTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHFriendsListTableViewCell.h"
#import "UIImage+Color.h"
#import <SDWebImage/UIImageView+WebCache.h>



@interface OIHFriendsListTableViewCell ()

@property (nonatomic) UIImageView *profileImageView;
@property (nonatomic) UILabel *personNameLabel;
@end

@implementation OIHFriendsListTableViewCell
@synthesize profileImageView;
@synthesize personNameLabel;
@synthesize actionButton;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        profileImageView = [[UIImageView alloc] init];
        profileImageView.translatesAutoresizingMaskIntoConstraints = NO;
        profileImageView.layer.cornerRadius = 30.0f;
        profileImageView.contentMode = UIViewContentModeScaleAspectFill;
        profileImageView.layer.borderWidth = 0.6f;
        profileImageView.layer.masksToBounds = YES;
        profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;

        [self.contentView addSubview:profileImageView];
        
        personNameLabel = [UILabel new];
        personNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:personNameLabel];
        
        actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        actionButton.translatesAutoresizingMaskIntoConstraints = NO;
        actionButton.layer.borderWidth = 0.5f;
        actionButton.layer.cornerRadius = 5.0f;

        actionButton.contentEdgeInsets = UIEdgeInsetsMake(5, 10, 5, 10);
        actionButton.layer.borderColor = [UIColor redColor].CGColor;
        [actionButton setTitle:@"" forState:UIControlStateNormal];
        [actionButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [actionButton addTarget:self action:@selector(actionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:actionButton];
        
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:profileImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:60]];

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:profileImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:profileImageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[profileImageView]-15-[personNameLabel]" options:NSLayoutFormatAlignAllTop metrics:0 views:NSDictionaryOfVariableBindings(profileImageView, personNameLabel)]];

        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:actionButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0f constant:-12.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:actionButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[profileImageView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(profileImageView)]];
        
    }
    return self;
}

-(void)setUser:(OIHUserModel *)user{
    
    _user = user;
    personNameLabel.text = user.userName;
    
    if (user.profilePicURL) {
        [profileImageView sd_setImageWithURL: [NSURL URLWithString:user.profilePicURL] placeholderImage:[UIImage imageFromColor:[UIColor highlightColor]]];
    }else{
        profileImageView.image = [UIImage imageFromColor:[UIColor highlightColor]];
    }
    
    actionButton.hidden = NO;

    if ([user isUserType:UserFriend]) {
        [actionButton setTitle:@"Buy" forState:UIControlStateNormal];
    }else if ([user isUserType:UserPendingRequest]){
        [actionButton setTitle:@"Remove" forState:UIControlStateNormal];
        
    }else if ([user isUserType:UserFriendRequest]){
        [actionButton setTitle:@"Confirm" forState:UIControlStateNormal];
        
    }else{
        actionButton.hidden = YES;
        //[actionButton removeFromSuperview];
    }

    
}


-(void)actionButtonPressed: (UIButton *)button{
    
    if ([self.delegate respondsToSelector:@selector(tableViewCellDidPressActionButton:)]) {
        [self.delegate tableViewCellDidPressActionButton:self];
    }
    
}



@end
