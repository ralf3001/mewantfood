//
//  OIHFeedCommentListViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHActivityFeed.h"

@interface OIHFeedCommentListViewController : UIViewController

-(id)initWithPost: (OIHActivityFeed *)feed;

@end
