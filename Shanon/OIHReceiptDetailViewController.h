//
//  OIHReceiptDetailViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 18/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHOrderModel.h"

@interface OIHReceiptDetailViewController : UIViewController

@property (nonatomic) OIHOrderModel *orderInfo;

-(id)initWithOrder: (NSDictionary *)order;
@end
