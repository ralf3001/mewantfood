//
//  OIHFriendsListTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"


@class OIHFriendsListTableViewCell;

@protocol FriendTableViewCellDelegate <NSObject>

-(void)tableViewCellDidPressActionButton: (OIHFriendsListTableViewCell *)cell;

@end


@interface OIHFriendsListTableViewCell : UITableViewCell

@property (nonatomic, weak) id<FriendTableViewCellDelegate> delegate;
@property (nonatomic) UIButton *actionButton;
@property (nonatomic) OIHUserModel *user;

@end
