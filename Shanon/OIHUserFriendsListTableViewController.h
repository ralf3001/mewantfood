//
//  OIHUserFriendsListTableViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 5/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, FriendList) {
    
    FetchFriends = 0,
    FetchPendingRequests = 1,
    FetchFriendRequests = 2,
    FetchFollowing = 3,
    FetchFollower = 4
};


@interface OIHUserFriendsListTableViewController : UIViewController
@property (nonatomic) FriendList tableType;
@end
