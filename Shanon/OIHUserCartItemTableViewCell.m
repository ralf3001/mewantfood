//
//  OIHUserCartItemTableViewCell.m
//  Shanon
//
//  Created by Ralf Cheung on 23/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserCartItemTableViewCell.h"
#define kImageViewHeight 180

@interface OIHUserCartItemTableViewCell ()
@property (nonatomic) UIView *textContainerView;

@end


@implementation OIHUserCartItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _textContainerView = [UIView new];
        _textContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.floatingView addSubview:_textContainerView];
        
        _nameLabel = [UILabel new];
        _nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _nameLabel.numberOfLines = 0;
        _nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:28];
        [_textContainerView addSubview:_nameLabel];
        
        
        _secondaryLabel = [UILabel new];
        _secondaryLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [_textContainerView addSubview:_secondaryLabel];
        
        _thirdLabel = [UILabel new];
        _thirdLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_textContainerView addSubview:_thirdLabel];
        
        _rightLabel = [UILabel new];
        _rightLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _rightLabel.textAlignment = NSTextAlignmentRight;
        _rightLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        [_textContainerView addSubview:_rightLabel];
        
        _coloredLabel = [UILabel new];
        _coloredLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _coloredLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        [_textContainerView addSubview:_coloredLabel];

        
        _itemImageView = [UIImageView new];
        _itemImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _itemImageView.layer.masksToBounds = YES;
        _itemImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.floatingView addSubview:_itemImageView];
        
        
        [self.floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_itemImageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_itemImageView)]];
        [self.floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_textContainerView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_textContainerView)]];


        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_nameLabel]-[_rightLabel(<=rightLabelWidth)]-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"rightLabelWidth": @80} views:NSDictionaryOfVariableBindings(_nameLabel, _rightLabel)]];
        
        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_coloredLabel]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_coloredLabel)]];

        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_secondaryLabel]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_secondaryLabel)]];

        
        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_nameLabel]-[_secondaryLabel]-[_coloredLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_nameLabel, _secondaryLabel, _coloredLabel)]];

        
        [self.floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_itemImageView(==kImageViewHeight)][_textContainerView]|" options:0 metrics:@{@"kImageViewHeight": @(kImageViewHeight)} views:NSDictionaryOfVariableBindings(_itemImageView, _textContainerView)]];
        
        
    }
    
    return self;
}


-(void)setItemInfo:(OIHMealModel *)itemInfo{
    
    _itemInfo = itemInfo;
    
    [_itemImageView sd_setImageWithURL:[NSURL URLWithString:itemInfo.productImage] placeholderImage:[UIImage imageFromColor:[UIColor highlightColor]]];
    _nameLabel.text = itemInfo.name;
    _secondaryLabel.text = itemInfo.restaurant.restaurantName;
    
    NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
    [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currency setCurrencyCode:itemInfo.restaurant.restaurantCurrency];
    _rightLabel.text = [currency stringFromNumber: itemInfo.priceInLocalCurrency];
    
    if (![NSDate date:[NSDate date] isGTE:_itemInfo.availableDate LTE:_itemInfo.availableEndDate]) {
        _coloredLabel.textColor = [UIColor orderCanceledColor];
        _coloredLabel.text = @"No longer available";
    }
    
    
}




@end
