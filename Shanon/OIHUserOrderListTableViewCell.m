//
//  OIHUserOrderListTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 25/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserOrderListTableViewCell.h"
#import "NSDate+JSONExtension.h"

#define kImageViewRadius 20


@implementation OIHUserOrderListTableViewCell
@synthesize orderStatusLabel, orderDateLabel, orderItemNameLabel, orderRestaurantLabel;
@synthesize orderIdLabel, orderRecipientLabel;
@synthesize orderReadLabel;
@synthesize orderStatusColorLabel;

@synthesize orderQuantityLabel;
@synthesize orderImageView;



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        orderIdLabel = [UILabel new];
        orderIdLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderIdLabel];
        
        orderItemNameLabel = [UILabel new];
        orderItemNameLabel.numberOfLines = 0;
        orderItemNameLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
        orderItemNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderItemNameLabel];
        

        orderRestaurantLabel = [UILabel new];
        orderRestaurantLabel.numberOfLines = 0;
        orderRestaurantLabel.translatesAutoresizingMaskIntoConstraints = NO;
        orderRestaurantLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        orderRestaurantLabel.textColor = [UIColor lightGrayColor];
        
        [self.contentView addSubview:orderRestaurantLabel];
        
        orderStatusLabel = [UILabel new];
        orderStatusLabel.textColor = [UIColor grayColor];
        orderStatusLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        orderStatusLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderStatusLabel];
        
        orderDateLabel = [UILabel new];
        orderDateLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
        orderDateLabel.textColor = [UIColor grayColor];
        orderDateLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderDateLabel];

        
        _orderRecipientAddressLabel = [UILabel new];
        _orderRecipientAddressLabel.numberOfLines = 0;
        _orderRecipientAddressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _orderRecipientAddressLabel.textColor = [UIColor darkGrayColor];
        _orderRecipientAddressLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        [self.contentView addSubview:_orderRecipientAddressLabel];
        

        orderImageView = [UIImageView new];
        orderImageView.translatesAutoresizingMaskIntoConstraints = NO;
        orderImageView.layer.cornerRadius = kImageViewRadius;
        orderImageView.layer.masksToBounds = YES;
        orderImageView.image = [UIImage imageFromColor:[UIColor lightGrayColor]];
        orderImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:orderImageView];
        
        orderQuantityLabel = [UILabel new];
        orderQuantityLabel.translatesAutoresizingMaskIntoConstraints = NO;
        orderQuantityLabel.layer.cornerRadius = 15;
        orderQuantityLabel.layer.masksToBounds = YES;
        orderQuantityLabel.backgroundColor = [UIColor redColor];
        orderQuantityLabel.textAlignment = NSTextAlignmentCenter;
        orderQuantityLabel.textColor = [UIColor whiteColor];
        [self.contentView addSubview:orderQuantityLabel];
        
        _orderPriceLabel = [UILabel new];
        _orderPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _orderPriceLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        _orderPriceLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_orderPriceLabel];

        orderStatusColorLabel = [UIView new];
        orderStatusColorLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderStatusColorLabel];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[orderImageView(==imageViewRadius)]-[orderItemNameLabel]-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"imageViewRadius": @(kImageViewRadius * 2)} views:NSDictionaryOfVariableBindings(orderItemNameLabel, orderImageView)]];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[orderStatusLabel]-[orderItemNameLabel]-[orderRestaurantLabel]-[_orderRecipientAddressLabel]-[orderDateLabel]-|" options:NSLayoutFormatAlignAllLeading metrics:0 views:NSDictionaryOfVariableBindings(orderDateLabel, orderItemNameLabel,orderStatusLabel,orderRestaurantLabel,_orderRecipientAddressLabel)]];

        
        NSLayoutConstraint *restaurantDateConstraint = [NSLayoutConstraint constraintWithItem:orderDateLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:orderRestaurantLabel attribute:NSLayoutAttributeBottom multiplier:1.0f constant:10.0f];
        restaurantDateConstraint.priority = 500;
        [self.contentView addConstraint:restaurantDateConstraint];
        

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[orderStatusLabel]-[_orderPriceLabel(<=80)]-|" options:NSLayoutFormatAlignAllCenterY | NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:@{@"quantityLabelWidth": @30} views:NSDictionaryOfVariableBindings(orderStatusLabel, _orderPriceLabel)]];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[orderRestaurantLabel]-[orderQuantityLabel(==quantityLabelWidth)]-|" options: NSLayoutFormatAlignAllTop metrics:@{@"quantityLabelWidth": @30} views:NSDictionaryOfVariableBindings(orderRestaurantLabel, orderQuantityLabel)]];
        
        
        
        NSLayoutConstraint *dateConstraint = [NSLayoutConstraint constraintWithItem:orderDateLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:orderRestaurantLabel attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0];

        dateConstraint.priority = 500;
        
        [self.contentView addConstraint:dateConstraint];

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:orderImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:kImageViewRadius * 2]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[orderStatusColorLabel]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(orderStatusColorLabel)]];

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:orderStatusColorLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:orderStatusColorLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0 constant:8.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:orderQuantityLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:orderQuantityLabel attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
        

    }
    return self;
}


-(void)setStatusLabel{
    
    orderStatusLabel.text = [self.order orderStatus];
    
    orderStatusColorLabel.backgroundColor = [self.order statusColor];

}


-(void)setOrder:(OIHOrderModel *)order{
    
    _order = order;
    
    self.orderDateLabel.text = [NSString stringWithFormat:@"%@", [NSDate daysBetweenDate:_order.orderDate andDate:[NSDate date]]];
    
    if (![_order.orderRecipient._id isEqualToString:[UserProfile currentUser]._id]) {

        self.orderItemNameLabel.text = _order.orderMeal.name;
        self.orderRestaurantLabel.text = _order.restaurant.restaurantName;
        
        NSMutableAttributedString *recipientAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"%@_DEFAULT_ADDRESS", @"[person]'s default address"), _order.orderRecipient.userName]];
  
        [recipientAttributedString setAttributes:@{NSForegroundColorAttributeName: (id)[UIColor blackColor],
                                                   NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                                   } range:NSMakeRange(0, _order.orderRecipient.userName.length + 1)];
        
        
        self.orderRecipientAddressLabel.hidden = NO;
        self.orderRecipientAddressLabel.attributedText = recipientAttributedString;
        
    }else{
        
        self.orderItemNameLabel.text = _order.orderMeal.name;
        self.orderRestaurantLabel.text = _order.restaurant.restaurantName;
        self.orderRecipientAddressLabel.hidden = YES;
    
    }

    NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
    [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currency setCurrencyCode:self.order.restaurant.restaurantCurrency];
    self.orderPriceLabel.text = [currency stringFromNumber: self.order.totalAmount];

    self.orderQuantityLabel.text = [NSString stringWithFormat:@"%ld", (long)_order.quantity];

    [orderImageView sd_setImageWithURL:[NSURL URLWithString:_order.orderMeal.productImage] placeholderImage:[UIImage imageFromColor:[UIColor lightGrayColor]]];

}

-(void)setAckBubble: (BOOL) isAcked{
    
//    self.orderReadLabel.backgroundColor = isAcked ? [UIColor clearColor] : [UIColor blueColor];
    
}


@end
