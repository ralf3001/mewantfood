//
//  OIHMapLocationDetailViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHMapViewAddressPin.h"


@class OIHMapLocationDetailViewController;

@protocol OIHMapLocationDetailProtocol <NSObject>

@optional
-(void)mapLocationDetailControllerWillDismiss: (OIHMapLocationDetailViewController *)mapDetail;
-(void)mapLocationDetailControllerDidDismiss: (OIHMapLocationDetailViewController *)mapDetail;

-(void)mapLocationDetailControllerDidPressContentView;

@end

@interface OIHMapLocationDetailViewController : UIViewController <UIViewControllerTransitioningDelegate>

@property (nonatomic) OIHMapViewAddressPin *locationDetail;
@property (nonatomic, weak) id<OIHMapLocationDetailProtocol> delegate;

-(id)initWithLocationDetail: (OIHMapViewAddressPin *)location;

@end


@interface MapLocationAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (assign) BOOL presenting;
@end
