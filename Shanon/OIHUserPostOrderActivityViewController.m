//
//  OIHUserPostOrderActivityViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserPostOrderActivityViewController.h"
#import "OIHMapLocationDetailViewController.h"
#import "OIHSocialManager.h"
#import "OIHAudioRecordingActionSheet.h"
#import "OIHRatingView.h"
#import "OIHActivityFeed.h"
#import "OIHUserOrderReviewViewController.h"

#import "UIImage+Color.h"

@interface OIHUserPostOrderActivityViewController () <UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, OIHSocialManagerDelegate, OIHAudioRecordingActionSheetDelegate, RatingViewDelegate>


@property (nonatomic) UITextView *textView;
@property (nonatomic) UIToolbar *toolBar;
@property (nonatomic) UIButton *doneButton;
@property (nonatomic) UIButton *audioNoteButton;

@property (nonatomic) UIView *contentView;

@property (nonatomic) UIImage *chosenImage;
@property (nonatomic) UIImagePickerController *imagePickr;
@property (nonatomic) UIProgressView *progressViewBar;

@property (nonatomic) NSURL *recordingURL;

//for pan gesture
@property (nonatomic) CGPoint _originalCenter;
@property (nonatomic) bool willDismiss;
@property (nonatomic) CGRect originalFrame;

@property (nonatomic) BOOL dismissUp;

@property (nonatomic) OIHActivityFeed *review;


@end

@implementation OIHUserPostOrderActivityViewController
@synthesize toolBar;
@synthesize doneButton;
@synthesize contentView;
@synthesize chosenImage;
@synthesize imagePickr;

@synthesize progressViewBar;

@synthesize willDismiss;
@synthesize _originalCenter;
@synthesize originalFrame;

-(id)init{
    
    self = [super init];
    if (self) {
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        
    }
    return self;
}




- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self fetchExistingComment];
    
    [self setupViews];
    [self createProgressViewBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socialManagerUpload:) name:OIHSocialManagerUploadDidCompleteNotification object:nil];
    
}


-(void)setupViews{
    
    contentView = [UIView new];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    contentView.layer.shadowColor = [UIColor blackColor].CGColor;
    contentView.layer.shadowRadius = 30.0f;
    contentView.layer.shadowOpacity = 0.4f;
    contentView.layer.borderColor = [UIColor colorWithWhite:0.8 alpha:1.0f].CGColor;
    contentView.layer.borderWidth = 0.5f;
    contentView.layer.cornerRadius = 8.0f;
    contentView.layer.shadowOffset = CGSizeMake(-15, 20);
    contentView.layer.masksToBounds = NO;
    
    [self.view addSubview:contentView];

    [contentView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPanGesture:)]];

    UIToolbar *bottomToolBar = [UIToolbar new];
    bottomToolBar.translatesAutoresizingMaskIntoConstraints = NO;
    bottomToolBar.layer.masksToBounds = YES;
    bottomToolBar.layer.cornerRadius = 8.0f;
    [contentView addSubview:bottomToolBar];
    
    
    toolBar = [UIToolbar new];
    toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    toolBar.layer.masksToBounds = YES;
    toolBar.layer.cornerRadius = 8.0f;

    [contentView addSubview:toolBar];
    
    UIButton *photoAlbum = [UIButton buttonWithType:UIButtonTypeCustom];
    [photoAlbum setImage:[IonIcons imageWithIcon:ion_images size:25.0f color:self.view.tintColor] forState:UIControlStateNormal];
    [photoAlbum addTarget:self action:@selector(uploadPictureFromAlbum) forControlEvents:UIControlEventTouchUpInside];
    photoAlbum.translatesAutoresizingMaskIntoConstraints = NO;
    [bottomToolBar addSubview:photoAlbum];

    [bottomToolBar addConstraint:[NSLayoutConstraint constraintWithItem:photoAlbum attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:bottomToolBar attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0f]];

    _audioNoteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_audioNoteButton setImage:[IonIcons imageWithIcon:ion_ios_mic size:25.0f color:self.view.tintColor] forState:UIControlStateNormal];
    [_audioNoteButton addTarget:self action:@selector(audioNote) forControlEvents:UIControlEventTouchUpInside];
    _audioNoteButton.translatesAutoresizingMaskIntoConstraints = NO;
    _audioNoteButton.enabled = NO;
    [bottomToolBar addSubview:_audioNoteButton];

    
    UIButton *ratingButton = [UIButton buttonWithType:UIButtonTypeSystem];
    ratingButton.translatesAutoresizingMaskIntoConstraints = NO;
    [ratingButton setTitle:@"Rating" forState:UIControlStateNormal];
    [ratingButton addTarget:self action:@selector(ratingView) forControlEvents:UIControlEventTouchUpInside];
    [bottomToolBar addSubview:ratingButton];
    
    
    UIButton *picture = nil;
    
    if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {

        //add camera button
        picture = [UIButton buttonWithType:UIButtonTypeCustom];
        [picture setImage:[IonIcons imageWithIcon:ion_ios_camera_outline size:25.0f color:self.view.tintColor] forState:UIControlStateNormal];
        [picture addTarget:self action:@selector(uploadPicture) forControlEvents:UIControlEventTouchUpInside];
        picture.translatesAutoresizingMaskIntoConstraints = NO;
        [bottomToolBar addSubview:picture];

        [bottomToolBar addConstraint:[NSLayoutConstraint constraintWithItem:picture attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:bottomToolBar attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0f]];

        [bottomToolBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[picture(==25)]-[photoAlbum(==25)]-[_audioNoteButton(==25)]-[ratingButton]" options:NSLayoutFormatAlignAllCenterY metrics:0 views:NSDictionaryOfVariableBindings(picture, _audioNoteButton, photoAlbum, ratingButton)]];

    }else{
        
        [bottomToolBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[photoAlbum(==25)]-[_audioNoteButton(==25)]-[ratingButton]" options:NSLayoutFormatAlignAllCenterY metrics:0 views:NSDictionaryOfVariableBindings(_audioNoteButton, photoAlbum, ratingButton)]];

    }
    
    doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    [doneButton addTarget:self action:@selector(postOrderComment) forControlEvents:UIControlEventTouchUpInside];
    [doneButton setTitle:NSLocalizedString(@"REVIEW_POST_BUTTON", @"") forState:UIControlStateNormal];
    doneButton.enabled = NO;
    [toolBar addSubview:doneButton];
    
    [toolBar addConstraint:[NSLayoutConstraint constraintWithItem:doneButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:toolBar attribute:NSLayoutAttributeRight multiplier:1.0f constant:-10.0f]];
    
    [toolBar addConstraint:[NSLayoutConstraint constraintWithItem:doneButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:toolBar attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0f]];
    
    
    _textView = [UITextView new];
    _textView.translatesAutoresizingMaskIntoConstraints = NO;
    _textView.delegate = self;
    _textView.text = NSLocalizedString(@"ACTIVITY_NEW_POST_PLACEHOLDER", @"Placeholder string");
    _textView.textColor = [UIColor colorWithWhite:0.8 alpha:1.0f];
    _textView.layer.cornerRadius = 8.0f;

    [contentView addSubview:_textView];
    
    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_textView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_textView)]];
    
    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[toolBar(==40)][_textView][bottomToolBar(==toolBar)]|" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:0 views:NSDictionaryOfVariableBindings(_textView, toolBar, bottomToolBar)]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[contentView]-15-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(contentView)]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:contentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:40.0f]];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0 constant:200.0f]];

    
}

-(void)fetchExistingComment{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"activity" httpMethod:HTTP_GET];
    setting.requestData = @{@"orderId": _order._id};
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error && data) {
            _review = [[OIHActivityFeed alloc] initWithActivity:data];
        }
    }];
}



-(void)ratingView{
    
    OIHRatingView *vc = [OIHRatingView new];
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
    
}

#pragma mark - RatingViewDelegate

-(void)ratingViewDidRate: (NSInteger)score{

    
    
}

#pragma mark - UITextViewDelegate

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    textView.textColor = [UIColor darkGrayColor];
    textView.text = @"";
    doneButton.enabled = YES;
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView{

    if(textView.text.length == 0){
        doneButton.enabled = NO;
    }else{
        doneButton.enabled = YES;
    }
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if(textView.text.length == 0){
        textView.textColor = [UIColor colorWithWhite:0.8 alpha:1.0f];
        textView.text = NSLocalizedString(@"ACTIVITY_NEW_POST_PLACEHOLDER", @"Placeholder string");
    
    }
    
}


-(void)dismissPanGesture: (UIPanGestureRecognizer *)gesture{
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            _originalCenter = contentView.center;
            originalFrame = contentView.frame;
            
            break;
        case UIGestureRecognizerStateChanged:{
            
            CGPoint translation = [gesture translationInView:contentView];
            contentView.center = CGPointMake(_originalCenter.x, _originalCenter.y + translation.y);
            
            CGPoint velocity = [gesture velocityInView:contentView];
            
            willDismiss = (fabs(contentView.frame.origin.y) > contentView.frame.size.height / 3) || fabs(velocity.y) > 1000;
            if (willDismiss) {
                self.dismissUp = -velocity.y > 0;
            }
        }
            break;
        case UIGestureRecognizerStateEnded:{
            
            if (willDismiss) {
                [self dismissView];
            }else{
                
                [UIView animateWithDuration:0.4f animations:^{
                    contentView.frame = originalFrame;
                }];
            }
            
        }
            break;
        default:
            break;
    }
    
    
}


-(void)dismissView{

    if ([self.delegate respondsToSelector:@selector(activityOrderViewControllerWillDismiss:)]) {
        [self.delegate activityOrderViewControllerWillDismiss:self];
    }
    
    __weak typeof(self) weakSelf = self;
    
    [self dismissViewControllerAnimated:YES completion:^{
        if ([weakSelf.delegate respondsToSelector:@selector(activityOrderViewControllerDidDismiss:)]) {
            [weakSelf.delegate activityOrderViewControllerDidDismiss:weakSelf];
        }
    }];

}


-(void)postOrderComment{
    
    [SVProgressHUD showProgress:0.0 status:NSLocalizedString(@"LABEL_SENDING", @"Sending")];
    
    if (chosenImage) {

        [OIHSocialManager sharedInstance].delegate = self;
        [[OIHSocialManager sharedInstance] uploadActivityImage:[chosenImage resizeImage] completionHandler:^(id result, NSError *error) {
            
            if (!error) {
                [self uploadComment:result];
            }else{
                NSLog(@"%@", [error localizedDescription]);
            }
            
        }];
 
    }else{
        [self uploadComment:nil];
    }
    
}


-(void)uploadComment: (id)result{
    
    if (_review) {
        
        if (result) {
            _review.postPicture = result[@"url"];
        }
        
        _review.content = _textView.text;
        [_review save:^(id data, NSError *error) {
            if (error) {
                NSLog(@"[ERROR]: %@", [error localizedDescription]);
            }else{
                [self dismissView];
            }
        }];
        
        
    }else{

        NSMutableDictionary *data = [NSMutableDictionary new];
        data[@"comment"] = _textView.text;
        data[@"orderId"] = self.order._id;
        
        if (result) {
            data[@"image_url"] = result[@"url"];
        }
        
        JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"ordercomment" httpMethod:HTTP_POST];
        setting.requestData = data;
        
        [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            if (!error) {
                [self dismissView];
            }else{
                NSLog(@"[ERROR]: %@", [error localizedDescription]);
            }
        }];

    }
    
    
}


#pragma mark - <OIHSocialManagerDelegate>

-(void)socialManagerUploadProgress:(float)progress{
    
    //handled in main thread
    [SVProgressHUD showProgress:progress];
    [self.progressViewBar setProgress:progress animated:YES];
    
}

-(void)socialManagerUpload: (NSNotification *)notification{
    
    
    
}


-(void)uploadPictureFromAlbum{
    
    imagePickr = [[UIImagePickerController alloc] init];
    imagePickr.delegate = self;
    imagePickr.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    [self presentViewController:imagePickr animated:YES completion:NULL];
    
}

-(void)audioNote{
    
    OIHAudioRecordingActionSheet *aS = [OIHAudioRecordingActionSheet new];
    aS.delegate = self;
    [self presentViewController:aS animated:YES completion:nil];

}

-(void)uploadPicture{
    
    imagePickr = [[UIImagePickerController alloc] init];
    imagePickr.delegate = self;
    imagePickr.sourceType = UIImagePickerControllerSourceTypeCamera;

    
    [self presentViewController:imagePickr animated:YES completion:NULL];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    chosenImage = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}



#pragma mark - <OIHAudioRecordingActionSheetDelegate>

-(void)audioActionSheetWillDismiss:(NSURL *)fileURL{
    
    _recordingURL = [fileURL copy];

}



-(void)uploadAudio{
    
    NSData *data = [NSData dataWithContentsOfURL:_recordingURL];
//    __weak typeof(self) weakSelf = self;
  
    if (data) {
        
        [OIHSocialManager sharedInstance].delegate = self;
        [[OIHSocialManager sharedInstance] uploadOrderAudioFile:data completionHandler:^(id result, NSError *error) {
            
            if (!error) {
                
                [SVProgressHUD dismiss];
                
                JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:@"orderaudionote" httpMethod:HTTP_POST];
                settings.requestData = @{@"audioURL": result[@"url"], @"orderId": _order._id};
                
//                [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
//                    
//                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"DONE", @"Done")];
//                    [[NSFileManager defaultManager] removeItemAtPath: weakSelf.recordingURL.path error: &error];
//                
//                }];
                
            }
        }];
        
    }

    
}



#pragma mark - <UIViewControllerTransitioningDelegate>

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    OrderReviewAnimatedTransitioning * transitioning = [OrderReviewAnimatedTransitioning new];
    transitioning.dismissUp = [self dismissUp];
    transitioning.presenting = YES;

    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    OrderReviewAnimatedTransitioning * transitioning = [OrderReviewAnimatedTransitioning new];
    transitioning.dismissUp = [self dismissUp];
    transitioning.presenting = NO;
    return transitioning;
}


-(void)createProgressViewBar{
    
    progressViewBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    progressViewBar.progressTintColor = [UIColor colorWithRed:216/256.0f green:106/256.0f blue:106/256.0f alpha:1.0f];
    progressViewBar.trackTintColor = [UIColor whiteColor];
    progressViewBar.translatesAutoresizingMaskIntoConstraints = NO;
    [contentView addSubview:progressViewBar];
    
    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[toolBar][progressViewBar(==2)]" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:nil views:NSDictionaryOfVariableBindings(toolBar, progressViewBar)]];
    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[progressViewBar]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(progressViewBar)]];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
