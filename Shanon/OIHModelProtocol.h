//
//  OIHModelProtocol.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 9/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OIHModelProtocol <NSObject>

@optional
-(void)save:(void (^)(id data, NSError *error))completionHandler;
-(void)save;

-(void)fetchMore: (NSString *)fields completionHandler :(void (^)(id data, NSError *error))completionHandler;

@required
-(NSDictionary *)toDictionary;

@end
