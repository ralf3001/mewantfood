//
//  OIHMessageBubbleTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHMessageFooterLabel.h"
#import "OIHMessageModel.h"

typedef NS_ENUM(NSUInteger, AuthorType) {
    MessageFromSender = 0,
    MessageFromReceiver
};


@interface OIHMessageBubbleTableViewCell : UITableViewCell

@property (nonatomic) OIHMessageFooterLabel *contentLabel;
@property (nonatomic) UILabel *senderLabel;
@property (nonatomic) UILabel *dateLabel;

@property (nonatomic, assign) AuthorType authorType;

@property (nonatomic, weak) OIHMessageModel *message;

@property (nonatomic, weak) UITableView *tableView;

@end
