//
//  OIHUserHomeScreenViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserHomeScreenViewController.h"
#import "OIHRestaurantListViewController.h"
#import "OIHUserRestaurantListTableViewController.h"
#import "OIHUserFriendsListTableViewController.h"
#import "OIHUserFriendsRestaurantViewController.h"
#import "OIHHomeScreenTopScrollView.h"
#import "NSMutableArray+extraBits.h"
#import "OIHActivityFeed.h"

#import "OIHUserOrderReviewViewController.h"

#import "OIHHomeScreenOrderTableViewCell.h"
#import "OIHUserActivityTableViewCell.h"
#import "OIHHomeScreenReviewTableViewCell.h"
#import "UserProfile.h"
#import "OIHFeedCommentListViewController.h"

#import "OIHHomeScreenSearchResultViewController.h"
#import "OIHSearchResultViewController.h"
#import "OIHCompetitionInfoTableViewCell.h"


#import "OIHCompetitionInfo.h"
#import "OIHCompetitionApplicationViewController.h"



#define kStatusBarHeight 20
#define kEnableTopScrollView NO


@interface OIHUserHomeScreenViewController () <UITableViewDataSource, UITableViewDelegate, OIHActionSheetProtocol, OIHActivityCellButtonDelegate, OIHEmptyViewDelegate, UISearchControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, OIHCompetitionInfoTableViewCellDelegate>


@property (nonatomic) UIButton *findRestaurants;
@property (nonatomic) UITableView *tableView;
@property (nonatomic) UIButton *buyFoodForFriendButton;
@property (nonatomic) UIRefreshControl *refreshControl;
@property (assign) BOOL isFetching;
@property (nonatomic) NSMutableArray *postInfo;
@property (nonatomic) OIHEmptyView *emptyView;
@property (nonatomic) UISearchController *searchController;
@property (nonatomic) OIHHomeScreenTopScrollView *topScroller;
@property (assign) BOOL hasMoreStories;

@property (nonatomic) NSMutableArray *activityData;
@property (nonatomic) NSMutableArray <OIHCompetitionInfo *> *competitionInfo;

@property (nonatomic) NSMutableSet *thankDataSet;

@end

@implementation OIHUserHomeScreenViewController
@synthesize findRestaurants;
@synthesize tableView = _tableView;
@synthesize buyFoodForFriendButton;
@synthesize isFetching;
@synthesize refreshControl;
@synthesize emptyView;
@synthesize searchController;
@synthesize topScroller;
@synthesize hasMoreStories;

@synthesize activityData;
@synthesize thankDataSet;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    findRestaurants = [UIButton buttonWithType:UIButtonTypeSystem];
    [findRestaurants setTitle:NSLocalizedString(@"FIND_RESTAURANTS", @"") forState:UIControlStateNormal];
    findRestaurants.translatesAutoresizingMaskIntoConstraints = NO;
    [findRestaurants addTarget:self action:@selector(find) forControlEvents:UIControlEventTouchUpInside];
    findRestaurants.backgroundColor = [UIColor colorWithRed:46/256.0f green:65/256.0f blue:114/256.0f alpha:1.0f];
    [findRestaurants setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:findRestaurants];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[findRestaurants]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(findRestaurants)]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:findRestaurants attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:findRestaurants attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:60]];
    
    hasMoreStories = YES;
    activityData = [NSMutableArray new];
    thankDataSet = [NSMutableSet set];
    
    [self createSearchController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentModal:) name:OIHViewControllerPresentModalViewControllerNotification object:nil];
    
    [self fetchCompetition];
    
}


-(void)createTopScrollView{
    
    topScroller = [[OIHHomeScreenTopScrollView alloc] init];
    topScroller.backgroundColor = [UIColor whiteColor];
    topScroller.pagingEnabled = YES;
    topScroller.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    [topScroller layoutIfNeeded];
    topScroller.contentSize = CGSizeMake(topScroller.frame.size.width * 3, 150);
    
}


-(void)createSearchController{
    
    OIHHomeScreenSearchResultViewController *searchVC = [[OIHHomeScreenSearchResultViewController alloc] init];
    
    searchController = [[UISearchController alloc] initWithSearchResultsController:searchVC];
    searchController.searchResultsUpdater = searchVC;
    searchController.searchBar.delegate = searchVC;
    searchController.delegate = searchVC;
    searchController.dimsBackgroundDuringPresentation = NO;
    searchController.hidesNavigationBarDuringPresentation = NO;
    self.navigationItem.titleView = searchController.searchBar;
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self fetchActivities: nil];
}



-(void)fetchActivities: (NSString *)date{
    
    NSMutableDictionary *getData = [NSMutableDictionary new];
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"userActivity" httpMethod:HTTP_GET];
    
    if (date) {
        getData[@"feedDate"] = date;
        setting.cachePolicy = IgnoreCache;
    }else{
        setting.cachePolicy = CacheFirstThenLoadData;
    }
    
    setting.requestData = getData;
    
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        [refreshControl endRefreshing];
        isFetching = NO;
        if (!error) {
            [self populateData:data isCacheData:isCachedResult];
        }
    }];
    
}


-(void)fetchActivities{
    [self fetchActivities:nil];
}

-(void)fetchMoreActivities{
    
    OIHActivityFeed *lastActivity = [activityData lastObject];
    [self fetchActivities:[lastActivity.createdAt JSONDateFromDate]];
    
}


-(void)populateData: (NSArray *)data isCacheData: (BOOL) isCache{
    
    NSArray *feedArray = [OIHActivityFeed activitiesFromArray:data];
    
    if(isCache){
        
        //initial fetch
        activityData = [NSMutableArray arrayWithArray:feedArray];
        activityData.isDataDirty = [NSNumber numberWithBool:isCache];
        
    }else{  //new data, flush cache data in the array
        
        if (activityData.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            //new data from network
            if (data.count > 0) {
                activityData = [NSMutableArray arrayWithArray:feedArray];
                activityData.isDataDirty = [NSNumber numberWithBool:NO];
            }else{
                return [self createEmptyView];
            }
            
        }else{
            
            //new data from network, load another page
            if (data.count > 0) {
                [activityData addObjectsFromArray:feedArray];
                
            }else{
                hasMoreStories = NO;
            }
        }
        
    }
    
    [self createTableView];
}


#pragma mark - <UITableViewDelegate>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_competitionInfo) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_competitionInfo.count > 0) {
        switch (section) {
            case 0:
                return _competitionInfo.count;
            case 1:{
                return hasMoreStories ? activityData.count : (activityData.count + 1);
            }
            default:
                break;
        }
    }else{
        return hasMoreStories ? activityData.count : (activityData.count + 1);
    }
    
    return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView competitionInfoTableViewCellAtIndexPath:  (NSIndexPath *)indexPath{
    
    OIHCompetitionInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"competition" forIndexPath:indexPath];
    cell.competitionInfo = [_competitionInfo firstObject];
    cell.delegate = self;
    
    return cell;
}

-(UITableViewCell *)tableView: (UITableView *)tableView activityInfoTableViewCellAtIndexPath: (NSIndexPath *)indexPath{
    
    if (indexPath.row == activityData.count) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoMoreDataCell" forIndexPath:indexPath];
        
        cell.textLabel.text = NSLocalizedString(@"ACTIVITY_NO_MORE_STORIES", @"no more stories");
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1.0f];
        
        return cell;
        
    }else{
        
        OIHActivityFeed *activity = activityData[indexPath.row];
        
        if (activity.type == UserReview) {
            
            OIHHomeScreenReviewTableViewCell *cell = (OIHHomeScreenReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"reviewCell" forIndexPath:indexPath];
            
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:activity.postPicture] placeholderImage:[UIImage imageFromColor:[UIColor colorWithWhite:0.8 alpha:1.0f]]];
            cell.feed = activity;
            cell.delegate = self;
            cell.tableView = tableView;
            
            return cell;
            
        }else if (activity.type == NewItem || (activity.type == UserOrder && [activity.fromUser._id isEqualToString:activity.toUser._id])){
            
            OIHHomeScreenOrderTableViewCell *cell = (OIHHomeScreenOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"orderCell" forIndexPath:indexPath];
            cell.feed = activity;
            cell.delegate = self;
            
            return cell;
            
        }
        else{
            
            OIHUserActivityTableViewCell *cell = (OIHUserActivityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            
            cell.activityInfo = activity;
            cell.delegate = self;
            
            return cell;
            
        }
        
        
    }

    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_competitionInfo.count > 0) {
        
        switch (indexPath.section) {
            case 0:
                return [self tableView: tableView competitionInfoTableViewCellAtIndexPath:indexPath];
                break;
            case 1:
                return [self tableView: tableView activityInfoTableViewCellAtIndexPath:indexPath];
            default:
                break;
        }
        
        
    }else{
        return [self tableView: tableView activityInfoTableViewCellAtIndexPath:indexPath];
        
    }

    return [self tableView: tableView activityInfoTableViewCellAtIndexPath:indexPath];

}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    AudioServicesPlaySystemSound (1105);

    if (_competitionInfo.count > 0) {
        
        switch (indexPath.section) {
            case 0:{
                OIHCompetitionInfo *competition = [_competitionInfo firstObject];
                OIHCompetitionApplicationViewController *vc = [[OIHCompetitionApplicationViewController alloc] initWithCompetition:competition];
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 1:{

                OIHActivityFeed *activity = activityData[indexPath.row];
                
                if (activity.type == UserReview) {
                    
                    OIHUserOrderReviewViewController *vc = [[OIHUserOrderReviewViewController alloc] initWithReview:activity];
                    [self presentViewController:vc animated:YES completion:nil];
                    
                }
                
                break;
            }
            default:
                break;
        }
        
        
    }else{
        
        OIHActivityFeed *activity = activityData[indexPath.row];
        
        if (activity.type == UserReview) {
            OIHUserOrderReviewViewController *vc = [[OIHUserOrderReviewViewController alloc] initWithReview:activity];
            [self presentViewController:vc animated:YES completion:nil];
            
        }

        
    }

    
}


#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat actualPosition = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height - (1500);
    
    if (scrollView == _tableView && isFetching == NO && actualPosition >= contentHeight && contentHeight > scrollView.frame.size.height) {
        isFetching = YES;
        [self fetchMoreActivities];
    }
    
}



#pragma mark - OIHUserActivityTableViewCellDelegate

-(void)userDidTapThanksButton: (OIHActivityFeed *) activity isPostThanked: (BOOL) isPostThanked{
    
    if (activity.isPostThanked) {
        [activity unthankActivity:nil];
    }else{
        [activity thankActivity:nil];
    }
    
}

-(void)userDidTapCommentButton:(OIHActivityFeed *)activity{
    
    OIHFeedCommentListViewController *vc = [[OIHFeedCommentListViewController alloc] initWithPost:activity];
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)buyForFriend{
    
    OIHUserFriendsRestaurantViewController *vc = [OIHUserFriendsRestaurantViewController new];
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)find{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOCAITON_MANAGER_CURRENT_LOCATION_OR_DEFAULT_ADDRESS", @"current locatio or default address") message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"LOCATION_MANAGER_CURRENT_LOCATION", @"Current Location") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        OIHRestaurantListViewController *vc = [OIHRestaurantListViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        
    }]];
    
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"LOCATION_MANAGER_DEFAULT_ADDRESS", @"Default Address") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        OIHUserModel *user = [OIHUserModel new];
        user._id = [UserProfile currentUser]._id;
        
        OIHRestaurantListViewController *vc = [OIHRestaurantListViewController new];
        vc.friendProfile = user;
        [self.navigationController pushViewController:vc animated:YES];
        
    }]];
    
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"CANCEL", @"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)createEmptyView{
    
    if (emptyView) {
        [emptyView removeFromSuperview];
        emptyView = nil;
    }
    
    [_tableView removeFromSuperview];
    _tableView = nil;
    
    emptyView = [[OIHEmptyView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_list_outline size:90 color:[UIColor lightGrayColor]] descriptionText:NSLocalizedString(@"HOME_SCREEN_HAS_NO_STORIES", @"")];
    
    emptyView.translatesAutoresizingMaskIntoConstraints = NO;
    emptyView.delegate = self;
    [self.view addSubview:emptyView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
    
    
}

-(void)createTableView{
    
    if (!_tableView) {
        
        if (emptyView) {
            [emptyView removeFromSuperview];
        }
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        [_tableView registerClass:[OIHUserActivityTableViewCell class] forCellReuseIdentifier:@"cell"];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"NoMoreDataCell"];
        [_tableView registerClass:[OIHHomeScreenReviewTableViewCell class] forCellReuseIdentifier:@"reviewCell"];
        [_tableView registerClass:[OIHHomeScreenOrderTableViewCell class] forCellReuseIdentifier:@"orderCell"];
        [_tableView registerClass:[OIHCompetitionInfoTableViewCell class] forCellReuseIdentifier:@"competition"];
        
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.contentInset = UIEdgeInsetsZero;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        _tableView.estimatedRowHeight = 300;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [self.view addSubview:_tableView];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:findRestaurants attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]];
        
        
        refreshControl = [UIRefreshControl new];
        [refreshControl addTarget:self action:@selector(fetchActivities) forControlEvents:UIControlEventValueChanged];
        [_tableView addSubview:refreshControl];
        
        if (kEnableTopScrollView) {
            topScroller = [[OIHHomeScreenTopScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
            topScroller.backgroundColor = [UIColor whiteColor];
            topScroller.pagingEnabled = YES;
            topScroller.contentSize = CGSizeMake(topScroller.frame.size.width * 3, 150);
            _tableView.tableHeaderView = topScroller;
        }
        
    }
    
    [_tableView reloadData];
    
    
}


-(void)presentModal: (NSNotification *)notification{
    
    UIViewController *vc = notification.object;
    
    UINavigationController *nav = self.navigationController;
    [nav presentViewController:vc animated:YES completion:nil];
}


#pragma mark - <OIHEmptyViewDelegate>

-(void)emptyViewDidPressReloadButton:(OIHEmptyView *)emptyView{
    
    [self fetchActivities:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchCompetition{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"fetchUnjoinedCompetition" httpMethod:HTTP_GET];
    setting.cachePolicy = IgnoreCache;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error && data) {
            OIHCompetitionInfo *competition = [[OIHCompetitionInfo alloc] initWithCompetitionInfo:data];
            _competitionInfo = [NSMutableArray arrayWithObject:competition];
            [_tableView reloadData];
        }
    }];
}

-(void)userDidDismissCompetition: (OIHCompetitionInfo *)competition{
    
    [_competitionInfo removeObject:competition];
    [_tableView reloadData];

}


@end
