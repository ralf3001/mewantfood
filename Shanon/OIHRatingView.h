//
//  OIHRatingView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OIHRatingView;

@protocol RatingViewDelegate <NSObject>

@required
-(void)ratingViewDidRate: (NSInteger)score;

@optional
-(void)ratingViewDidDisappear: (OIHRatingView *)ratingView;
-(void)ratingViewWillDisappear: (OIHRatingView *)ratingView;

@end

@interface OIHRatingView : UIViewController <UIViewControllerTransitioningDelegate>

@property (nonatomic) NSInteger score;
@property (nonatomic, weak) id <RatingViewDelegate> delegate;


@end
