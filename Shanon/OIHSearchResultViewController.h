//
//  OIHSearchResultViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 21/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"

@protocol SearchFriendResultViewControllerDelegate <NSObject>

-(void)searchResultViewControllerDidSelectUser: (OIHUserModel *)user;

@end

@interface OIHSearchResultViewController : UIViewController <UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate>
@property (nonatomic, weak) id<SearchFriendResultViewControllerDelegate>delegate;

@end
