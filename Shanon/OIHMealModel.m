//
//  OIHMealModel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "NSDate+JSONExtension.h"
#import "OIHMealModel.h"
#import <objc/runtime.h>

@implementation OIHMealModel
@synthesize _id, name, availableDate, availableEndDate;
@synthesize restaurantId, restaurant;
@synthesize priceInLocalCurrency;

- (id)initWithInfoDictionary:(NSDictionary *)info {
    
    self = [super init];
    if (self) {
        
        _id = info[@"_id"];
        name = info[kOIHMealName];
        
        availableDate = [NSDate dateFromJSONDate:info[kOIHMealAvailableDate]];
        availableEndDate = [NSDate dateFromJSONDate:info[@"availableEndDate"]];
        
        if (availableDate) {
            
            NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *component =
            [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |
                                  NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute)
                        fromDate:availableDate];
            
            component.hour = 0;
            component.minute = 0;
            availableDate = [calendar dateFromComponents:component];
            
            if (![NSDate dateFromJSONDate:info[@"availableEndDate"]]) {
                
                NSCalendar *calendar =
                [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
                component.day += 7;
                component.hour = 23;
                component.minute = 59;
                availableEndDate = [calendar dateFromComponents:component];
                
            }
            
        }
        
        if ([info[kOIHMealRestaurant] isKindOfClass:[NSDictionary class]]) {
            restaurantId = info[kOIHMealRestaurant][kOIHRestaurantId];
        }
        
        priceInLocalCurrency = info[kOIHMealPriceInLocalCurrency];
        _productImage = info[@"productImage"];
        _mealType = [self typeEnumFromString:info[@"type"]];
        _chargeRate = info[@"rate"];
        _priceInString = info[@"priceInLocalCurrencyString"];
        
        _productNote = info[@"productNote"];
        _productIngredients = info[@"productIngredients"];
        _productNetWeight = info[@"productNetWeight"];
        _productQuantity = info[@"productQuantity"];
        
        if ([info[kOIHMealRestaurant] isKindOfClass:[NSDictionary class]]) {
            restaurant = [[OIHRestaurantModel alloc]
                          initWithDictionary:info[kOIHMealRestaurant]];
        }
        _isFavourited = [info[@"isFavourited"] boolValue];
        
        self.infoDictionary = info;
    }
    
    return self;
}

+(instancetype)meal{
    return [[self alloc] init];
}

- (id)init {
    
    self = [super init];
    if (self) {
        _id = @"";
        name = @"";
        availableDate = [NSDate date];
        
        NSCalendar *calendar =
        [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *component =
        [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |
                              NSCalendarUnitDay)
                    fromDate:availableDate];
        component.day++;
        availableEndDate = [component date];
        
        _mealType = [self typeEnumFromString:@"breakfast"];
    }
    return self;
}

+ (NSMutableArray<OIHMealModel *> *)mealsFromArray:(id)data {
    
    NSMutableArray *array = [NSMutableArray new];
    for (NSDictionary *info in data) {
        OIHMealModel *meal = [[OIHMealModel alloc] initWithInfoDictionary:info];
        [array addObject:meal];
    }
    return array;
}

- (void)setAttribute:(NSString *)attribute data:(id)data {
    
    if ([attribute isEqualToString:kOIHMealType]) {
        _mealType = [self typeEnumFromString:data];
        
    } else {
        [self setValue:data forKeyPath:attribute];
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    // override it just to prevent an exception
}

- (MealType)typeEnumFromString:(NSString *)type {
    
    NSDictionary<NSString *, NSNumber *> *types = @{
                                                    @"breakfast" : @(Breakfast),
                                                    @"lunch" : @(Lunch),
                                                    @"dinner" : @(Dinner),
                                                    @"snack" : @(Snack),
                                                    @"nil" : @(MealInvalid)
                                                    };
    
    return types[type].integerValue;
}

+ (NSDictionary *)typeDisplayNames {
    return @{
             @(Breakfast) : @"Breakfast",
             @(Lunch) : @"Lunch",
             @(Dinner) : @"Dinner",
             @(Snack) : @"Snack"
             };
}

- (NSString *)mealTypeString {
    return [[self class] typeDisplayNames][@(self.mealType)];
}

-(BOOL)isEqual:(id)object{
    if ([object isKindOfClass:[OIHMealModel class]]) {
        return [self._id isEqualToString:((OIHMealModel *)object)._id];
    }
    return NO;
}

-(void)setIsFavourited:(BOOL)isFavourited{
    
    _isFavourited = isFavourited;
    
    JSONRequestSettings *setting;
    
    if (isFavourited) {
        setting = [[JSONRequestSettings alloc] initWithRoute:@"favourites" httpMethod:HTTP_POST];
        
    }else{
        setting = [[JSONRequestSettings alloc] initWithRoute:@"favourites" httpMethod:HTTP_DELETE];
    }
    
    setting.requestData = @{@"mealId": self._id};
    
    [[JSONRequest sharedInstance] fetchBackground:setting];
    
    
}



- (NSDictionary *)toDictionary {
    
    if (self.infoDictionary) {
        return self.infoDictionary;
    }
    
    return @{
             kOIHMealName : name,
             kOIHMealPriceInLocalCurrency :
                 priceInLocalCurrency ? priceInLocalCurrency : [NSNull null],
             kOIHRestaurantId : restaurantId ? restaurantId : [NSNull null],
             kOIHMealAvailableDate : [availableDate JSONDateFromDate],
             @"productImage" : _productImage ? _productImage : [NSNull null],
             kOIHMealType : [NSNumber numberWithInteger:_mealType],
             @"productNetWeightInString" : _productNetWeight,
             @"productQuantity" : _productQuantity,
             @"productIngredients" : _productIngredients,
             @"productNote" : _productNote
             };
}

@end
