//
//  OIHUserFriendsListTableViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 5/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserFriendsListTableViewController.h"
#import "OIHUserProfileViewController.h"
#import "OIHFriendsListTableViewCell.h"
#import "OIHEmptyView.h"
#import "OIHDropDownMenuViewController.h"
#import "OIHRestaurantListViewController.h"
#import "OIHAddFriendsViewController.h"
#import "OIHUserModel.h"
#import "OIHSearchResultViewController.h"

#define kFriendButtonHeight 60

@interface OIHUserFriendsListTableViewController () <UITableViewDataSource, UITableViewDelegate, OIHDropDownMenuDelegate, FriendTableViewCellDelegate, OIHEmptyViewDelegate, SearchFriendResultViewControllerDelegate>


@property (nonatomic) NSMutableArray *friendsArray;
@property (nonatomic) UISearchController *searchController;
@property (nonatomic) UIButton *friendsButton;
@property (nonatomic) UIButton *pendingRequestsButton;
@property (nonatomic) UIButton *friendRequestsButton;
@property (nonatomic) UITableView *tableView;
@property (nonatomic) OIHEmptyView *emptyView;

@property (nonatomic) UIView *tableContentView;

@end

@implementation OIHUserFriendsListTableViewController
@synthesize friendsArray;
@synthesize searchController;
@synthesize friendRequestsButton, friendsButton, pendingRequestsButton;
@synthesize tableView = _tableView;
@synthesize emptyView;
@synthesize tableType;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    friendsArray = [NSMutableArray new];
    
    tableType = FetchFriends;
    
    friendsButton = [self createButtons];
    friendsButton.backgroundColor = [UIColor colorWithRed:224/256.0f green:80/256.0f blue:76/256.0f alpha:1.0f];
    [friendsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [friendsButton setTitle:NSLocalizedString(@"TAB_BAR_FRIENDS", @"Friends") forState:UIControlStateNormal];
    [friendsButton addTarget:self action:@selector(dropDownMenu) forControlEvents:UIControlEventTouchUpInside];
//    [self.navigationController.view addSubview:friendsButton];
    [self.view addSubview:friendsButton];

    _tableContentView = [UIView new];
    _tableContentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_tableContentView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[friendsButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(friendsButton)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableContentView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableContentView)]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[friendsButton(==kFriendButtonHeight)][_tableContentView]|" options:0 metrics:@{@"kFriendButtonHeight": @(kFriendButtonHeight)} views:NSDictionaryOfVariableBindings(friendsButton, _tableContentView)]];

    [self fetchFriends];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_plus_empty size:35 color:self.navigationController.navigationBar.tintColor] style:UIBarButtonItemStylePlain target:self action:@selector(addFriend)];
    
    OIHSearchResultViewController *searchVC = [[OIHSearchResultViewController alloc] init];
    searchVC.delegate = self;
    searchController = [[UISearchController alloc] initWithSearchResultsController:searchVC];
    searchController.searchResultsUpdater = searchVC;
    searchController.searchBar.delegate = searchVC;
    searchController.dimsBackgroundDuringPresentation = false;
    searchController.hidesNavigationBarDuringPresentation = false;
    self.navigationItem.titleView = searchController.searchBar;
    
    
}


-(void)createTableView{
    
    if (_tableView) {
        
        [emptyView removeFromSuperview];
        
        [_tableView reloadData];
        
    }else{
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        [_tableView registerClass:[OIHFriendsListTableViewCell class] forCellReuseIdentifier:@"cell"];
        
//        _tableView.contentInset = UIEdgeInsetsMake(kFriendButtonHeight, 0, 0, 0);
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 60.0f;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        [_tableContentView addSubview:_tableView];
        
        [_tableContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        [_tableContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        
        
        self.definesPresentationContext = YES;
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return friendsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHFriendsListTableViewCell *cell = (OIHFriendsListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    OIHUserModel *user = friendsArray[indexPath.row];
    
    cell.user = user;
    cell.delegate = self;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self pushToUserProfileViewController: friendsArray[indexPath.row]];
}


-(void)createEmptyView: (FriendList) type{
    
    if (_tableView) {
        [_tableView removeFromSuperview];
        _tableView = nil;
    }
    
    NSString *descriptionText;
    NSString *reloadButtonText;
    
    switch (type) {
        case FetchFriends:
            descriptionText = NSLocalizedString(@"NO_FRIENDS_TEXT", "No friends");
            reloadButtonText = NSLocalizedString(@"ADD_FRIENDS", @"Add friends");
            break;
        case FetchFriendRequests:
            descriptionText = NSLocalizedString(@"NO_FRIENDS_REQUESTS_TEXT", @"");
            break;
        case FetchPendingRequests:
            descriptionText = NSLocalizedString(@"NO_PENDING_REQUESTS_TEXT", @"");
            break;
        case FetchFollower:
            descriptionText = NSLocalizedString(@"NO_FOLLOWERS_TEXT", @"");
            break;
        case FetchFollowing:
            descriptionText = NSLocalizedString(@"NO_FOLLOWING_TEXT", @"");
            break;
        default:
            break;
    }
    
    if (emptyView) {
        [emptyView removeFromSuperview];
        emptyView = nil;
    }
    
    emptyView = [[OIHEmptyView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_people_outline size:90 color:[UIColor lightGrayColor]] descriptionText:descriptionText];
    
    if (type == FetchFriends) {
        [emptyView.reloadButton setTitle:reloadButtonText forState:UIControlStateNormal];
        
    }
    
    emptyView.translatesAutoresizingMaskIntoConstraints = NO;
    emptyView.delegate = self;
    
    [_tableContentView addSubview:emptyView];
    
    [_tableContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
    [_tableContentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
    
    
    
}

-(UIButton *)createButtons{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    return button;
    
}

-(void)dropDownMenu{
    
    OIHDropDownMenuViewController *dropDownMenu = [[OIHDropDownMenuViewController alloc] initWithMenuItems:@[
                                                                                                             NSLocalizedString(@"TAB_BAR_FRIENDS", @"Friends"),
                                                                                                             NSLocalizedString(@"USER_PENDING_REQUESTS", @"Pending Requests"),
                                                                                                             NSLocalizedString(@"USER_FRIEND_REQUEST_SENT", @"Friend Requests"),
                                                                                                             NSLocalizedString(@"USER_FOLLLOWING", @"Following"),
                                                                                                             NSLocalizedString(@"USER_FOLLOWERS", @"Followers")
                                                                                                             ]];
    
    
    dropDownMenu.delegate = self;
    // Why not '[self presentViewController:]': http://stackoverflow.com/questions/19890761/warning-presenting-view-controllers-on-detached-view-controllers-is-discourage
    [self presentViewController:dropDownMenu animated:YES completion:nil];
    
}

#pragma mark - <OIHDropDownMenuDelegate>

-(void)dropDownMenuWillAppear:(OIHDropDownMenuViewController *)dropDownMenu{
    
    JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:@"friendsinfo" httpMethod:HTTP_GET];
    settings.cachePolicy = CacheFirstThenLoadData;
    
    [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            dropDownMenu.menuMiscInfo = data;
            [dropDownMenu updateTableViewNotifications];
        }
    }];
    
}


-(void)dropDownMenu:(OIHDropDownMenuViewController *)dropDownMenu didSelectIndx:(NSInteger)selectedIndex{
    
    
    if (selectedIndex != tableType) {
        switch (selectedIndex) {
            case 0:
                [friendsButton setTitle:NSLocalizedString(@"TAB_BAR_FRIENDS", @"Friends") forState:UIControlStateNormal];
                [self fetchData:FetchFriends];
                break;
            case 1:
                [friendsButton setTitle:NSLocalizedString(@"USER_PENDING_REQUESTS", @"Pending Requests") forState:UIControlStateNormal];
                [self fetchData:FetchPendingRequests];
                break;
            case 2:
                [friendsButton setTitle:NSLocalizedString(@"USER_FRIEND_REQUEST_SENT", @"Friend Requests") forState:UIControlStateNormal];
                [self fetchData:FetchFriendRequests];
                break;
            case 3:
                [friendsButton setTitle:NSLocalizedString(@"USER_FOLLLOWING", @"Following") forState:UIControlStateNormal];
                [self fetchData:FetchFollowing];
                break;
            case 4:
                [friendsButton setTitle:NSLocalizedString(@"USER_FOLLOWERS", @"Followers") forState:UIControlStateNormal];
                [self fetchData:FetchFollower];
                break;
            default:
                break;
        }
    }
    
}


#pragma mark - Retrieve Different data lists

-(void)fetchData: (FriendList)list{
    
    switch (list) {
        case FetchFriends:
            [self fetchFriends];
            break;
        case FetchFriendRequests:
            [self retrieveFriendRequests];
            break;
        case FetchPendingRequests:
            [self retrievePending];
            break;
        case FetchFollowing:
            [self fetchFollowings];
            break;
        case FetchFollower:
            [self fetchFollowers];
            break;
        default:
            break;
    }
    
    tableType = list;
}

-(void)retrievePending{
    
    if (tableType != FetchPendingRequests) {
        
        JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"pendingRequests" httpMethod:HTTP_GET];
        setting.cachePolicy = CacheFirstThenLoadData;
        
        [friendsArray removeAllObjects];
        
//        [SVProgressHUD showWithStatus:NSLocalizedString(@"FRIENDS_FETCHING_PENDING_REQUESTS", @"")];
        
        [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            [SVProgressHUD dismiss];
            
            if (!error) {
                [self populateData:data list:FetchPendingRequests isCache:isCachedResult];
            }
        }];
        
    }
    
}


-(void)retrieveFriendRequests{
    
    if (tableType != FetchFriendRequests) {
        
        JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"friendRequests" httpMethod:HTTP_GET];
        setting.cachePolicy = CacheFirstThenLoadData;
        
        [friendsArray removeAllObjects];
        
//        [SVProgressHUD showWithStatus:NSLocalizedString(@"FRIENDS_FETCHING_FRIENDS_REQUESTS", @"")];
        
        [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            
            [SVProgressHUD dismiss];
            
            if (!error) {
                [self populateData:data list:FetchPendingRequests isCache:isCachedResult];
            }
            
        }];
        
    }
    
}


-(void)fetchFriends{
    
    [friendsArray removeAllObjects];
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"friend" httpMethod:HTTP_GET];
    setting.cachePolicy = CacheFirstThenLoadData;
    
//    [SVProgressHUD showWithStatus:NSLocalizedString(@"FRIENDS_FETCHING", @"")];
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        [SVProgressHUD dismiss];
        if (!error) {
            [self populateData:data list:FetchFriends isCache:isCachedResult];
        }
        
    }];
    
}

-(void)fetchFollowers{
    
    [friendsArray removeAllObjects];
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"follower" httpMethod:HTTP_GET];
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        [SVProgressHUD dismiss];
        if (!error) {
            [self populateData:data list:FetchFollower isCache:isCachedResult];
        }
        
    }];
    
    
    
}


-(void)fetchFollowings{
    
    [friendsArray removeAllObjects];
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"following" httpMethod:HTTP_GET];
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        [SVProgressHUD dismiss];
        if (!error) {
            [self populateData:data list:FetchFollowing isCache:isCachedResult];
        }
        
    }];
    
    
    
    
}

-(void)populateData: (id)data list: (FriendList)list isCache: (BOOL) isCache{
    
    //    [self createEmptyView:list];
    if ([data isKindOfClass:[NSArray class]]) {
        if (((NSArray *)data).count > 0) {
            
            NSMutableArray *usersArray = [OIHUserModel profilesFromArray:data];
            
            if (list != tableType) {
            }
            
            if (isCache) {
                friendsArray = [NSMutableArray arrayWithArray:usersArray];
                friendsArray.isDataDirty = [NSNumber numberWithBool:YES];
            }else{
                if ([friendsArray.isDataDirty boolValue]) {
                    friendsArray = [NSMutableArray arrayWithArray:usersArray];
                    friendsArray.isDataDirty = [NSNumber numberWithBool:NO];
                }else{
                    [friendsArray addObjectsFromArray:usersArray];
                }
            }
            
            [self createTableView];
            
        }else{
            [self createEmptyView: list];
        }
        
    }else{
        [self createEmptyView: list];
    }
    
}


#pragma mark - <FriendTableViewCellDelegate>

-(void)tableViewCellDidPressActionButton: (OIHFriendsListTableViewCell *)cell{
    
    
    OIHUserModel *user = cell.user;
    
    if ([user isUserType:UserFriend]) {
        NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
        OIHRestaurantListViewController *vc = [OIHRestaurantListViewController new];
        vc.friendProfile = friendsArray[indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if ([user isUserType:UserPendingRequest]){
        [user cancelRequest:nil];
        
    }else if ([user isUserType:UserFriendRequest]){
        [user confirmFriend:nil];
        
    }
    
    
}
#pragma mark - <OIHEmptyViewDelegate>

-(void)emptyViewDidPressReloadButton:(OIHEmptyView *)emptyView{
    
    switch (tableType) {
            
        case FetchFriends:
            [self addFriend];
            break;
            
        case FetchFriendRequests:
            
            break;
        case FetchPendingRequests:
            
            break;
        default:
            break;
            
    }
    
}

-(void)confirmFriend: (OIHUserModel *)user{
    
    [user resetUserType];
    [user setUserType:UserFriend];
    
    [user confirmFriend:^(id data, NSError *error) {
        if (!error) {
            
            [_tableView reloadData];
            
        }else{
            [user resetUserType];
            [user setUserType:UserFriendRequest];
        }
    }];
    
    
}


-(void)cancelRequest: (OIHUserModel *)user{
    
    [user cancelRequest:^(id data, NSError *error) {
        if (!error) {
            
            [user resetUserType];
            [_tableView reloadData];
            
        }else{
            [user resetUserType];
            [user setUserType:UserPendingRequest];
        }
    }];
    
}


-(void)addFriend{
    
    OIHAddFriendsViewController *vc = [OIHAddFriendsViewController new];
    [self.navigationController pushViewController:vc animated:YES];
    
}


#pragma mark - <SearchFriendResultViewControllerDelegate>
-(void)searchResultViewControllerDidSelectUser:(OIHUserModel *)user{
    
    [self pushToUserProfileViewController:user];
    
}

-(void)pushToUserProfileViewController: (OIHUserModel *)user{
    
    OIHUserProfileViewController *vc = [[OIHUserProfileViewController alloc] initWithFriendProfile:user];
    
    [self.navigationController pushViewController:vc animated:YES];
}



@end
