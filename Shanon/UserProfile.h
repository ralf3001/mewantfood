//
//  UserProfile.h
//  Ferrle
//
//  Created by Ralf Cheung on 12/29/14.
//  Copyright (c) 2014 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHAddressModel.h"

@class UserProfile;

@protocol OIHLoginProtocol <NSObject>

-(void)userDidLogin: (UserProfile *)profile;
-(void)userLoginFailed: (NSString *)reason;

@end


@protocol OIHSignupProtocol <NSObject>

-(void)userDidSignUp: (UserProfile *)profile;
-(void)userSignupFailed: (NSString *)reason;

@end

@interface UserProfile : NSObject
@property (nonatomic) NSDictionary *userDictionary;
@property (assign) BOOL isMerchant;
@property (nonatomic, weak) id<OIHLoginProtocol> loginDelegate;
@property (nonatomic, weak) id<OIHSignupProtocol> signupDelegate;
@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *lastName;

@property (nonatomic) NSMutableArray *addresses;
@property (nonatomic) NSArray *phoneNumber;
@property (nonatomic) NSArray *profilePics;

+(UserProfile *)currentUser;

-(void)setUserDictionary: (NSDictionary *)userDict;

-(void)logout;
-(void)login: (NSDictionary *)loginInfo;
-(void)signup: (NSDictionary *)signupInfo;

-(void)updateProfile;
-(void)updateProfile :(void (^)(NSError *error))completionHandler;

-(NSDictionary *) getProfile;

@end
