//
//  OIHCompetitionInfoTableViewCell.m
//  Shanon
//
//  Created by Ralf Cheung on 14/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCompetitionInfoTableViewCell.h"


@interface OIHCompetitionInfoTableViewCell ()
@property (nonatomic) UIButton *dismissButton;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *descriptionLabel;


@end

@implementation OIHCompetitionInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    
    [_floatingView setHighlight:highlighted];
    
}
-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _floatingView = [OIHFloatingView new];
        _floatingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_floatingView];

        
        _dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_dismissButton setImage:[IonIcons imageWithIcon:ion_ios_close_empty size:50 color:[UIColor whiteColor]] forState:UIControlStateNormal];
        [_dismissButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [_floatingView addSubview:_dismissButton];
        
        _floatingView.backgroundColor = [UIColor colorWithRed:35/255.0f green:106/255.0f blue:98/255.0f alpha:1.0f];
        
        _titleLabel = [UILabel new];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.numberOfLines = 0;
        _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:25];
        [_floatingView addSubview:_titleLabel];
        
        _descriptionLabel = [UILabel new];
        _descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _descriptionLabel.textColor = [UIColor whiteColor];
        _descriptionLabel.numberOfLines = 0;
        [_floatingView addSubview:_descriptionLabel];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_floatingView]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_floatingView)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_floatingView]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_floatingView)]];

        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_dismissButton]" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_dismissButton)]];

        [_floatingView addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_dismissButton attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[_dismissButton(==40)]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_dismissButton)]];

        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_titleLabel]-[_descriptionLabel]-|" options:NSLayoutFormatAlignAllLeft metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel, _descriptionLabel)]];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_descriptionLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_descriptionLabel)]];
        
        
        [_floatingView addConstraint:[NSLayoutConstraint constraintWithItem:_titleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:_dismissButton attribute:NSLayoutAttributeLeft multiplier:1.0f constant:-8.0f]];
        

        
    }
    
    return self;

}

-(void)setCompetitionInfo:(OIHCompetitionInfo *)competitionInfo{
    
    _competitionInfo = competitionInfo;
    
    _titleLabel.text = _competitionInfo.title;
    _descriptionLabel.text = _competitionInfo.competitionDescription;
    
}

-(void)dismiss{
    
    [_competitionInfo dismissCompetition];
    [self.delegate userDidDismissCompetition:_competitionInfo];
    
}



@end
