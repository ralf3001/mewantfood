//
//  EinerUserTracking.h
//  Einer
//
//  Created by Ralf Cheung on 5/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewControllerTrackingInfo.h"
#import <UIKit/UIKit.h>


@interface EinerUserTracking : NSObject <UITabBarControllerDelegate, UINavigationControllerDelegate>


@property (nonatomic, weak) id<UITabBarControllerDelegate> tabBarDelegate;
@property (nonatomic) NSMutableArray <ViewControllerTrackingInfo *> *VCsInThisSection;
@property (nonatomic) NSString *userId;



+(EinerUserTracking *)sharedInstance;


-(void)applicationDidEnterBackground;
-(void)applicationDidBecomeActive;
-(void)addEvent: (NSString *)eventName;
-(void)openedPushNotification: (NSDictionary *)notification;


-(void)generateCrashAnalyticReport;
-(void)handleException;



-(void)addTabBarControllerToMonitor: (UITabBarController *)tabBar;
-(void)addNavigationControllerToMonitor: (UINavigationController *)nav;

@end
