//
//  OIHDropDownMenuTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHDropDownMenuTableViewCell : UITableViewCell

@property (nonatomic) UILabel *menuLabel;
@property (nonatomic) UIView *notificationDot;
@property (nonatomic) UILabel *notificationCount;

-(void)setNotification: (NSInteger)count notificationDot: (BOOL)displayDot;

@end
