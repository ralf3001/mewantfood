//
//  OIHUserOrderReviewViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 19/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserOrderReviewViewController.h"
#import "OIHUserRatingView.h"


@interface OIHUserOrderReviewViewController ()

@property (nonatomic) UITextView *textView;
@property (nonatomic) UIImageView *imageView;
//@property (nonatomic) OIHUserRatingView *ratingView;
@property (nonatomic) UILabel *ratingLabel;

@property (nonatomic) UIView *containerView;

@property (nonatomic) UIButton *dismissButton;

//for pan gesture
@property (nonatomic) CGPoint _originalCenter;
@property (nonatomic) bool willDismiss;
@property (nonatomic) CGRect originalFrame;

@property (nonatomic) BOOL dismissUp;

@property (nonatomic) UIScrollView *scrollView;

@end

@implementation OIHUserOrderReviewViewController
@synthesize textView;
@synthesize imageView;

@synthesize willDismiss;
@synthesize _originalCenter;
@synthesize originalFrame;

@synthesize containerView;


-(id)initWithReview: (OIHActivityFeed *)feed{
    
    self = [super init];
    if (self) {
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        _feed = feed;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    containerView = [UIView new];
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    containerView.backgroundColor = [UIColor blackColor];
    [containerView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleTextView)]];

    [self.view addSubview:containerView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[containerView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(containerView)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[containerView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(containerView)]];

    imageView = [UIImageView new];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [containerView addSubview:imageView];
    
    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];

    if (_feed.postPicture) {
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:_feed.postPicture] placeholderImage:[UIImage imageFromColor:[UIColor blackColor]]options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
            [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
            [SVProgressHUD setForegroundColor:[UIColor whiteColor]];

            [SVProgressHUD showProgress:(float)receivedSize/expectedSize];
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];

        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            [SVProgressHUD dismiss];
            [SVProgressHUD setForegroundColor:[UIColor blackColor]];
            [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
        
        }];
        
        
    }
    
    _dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_dismissButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    [_dismissButton setImage:[IonIcons imageWithIcon:ion_ios_close_empty size:60 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    [containerView insertSubview:_dismissButton aboveSubview:imageView];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:containerView attribute:NSLayoutAttributeTop multiplier:1.0f constant:10.0f]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:containerView attribute:NSLayoutAttributeLeft multiplier:1.0f constant:10.0f]];


    _scrollView = [UIScrollView new];
    _scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [_scrollView setContentInset:UIEdgeInsetsMake(CGRectGetHeight(self.view.frame) * 0.6, 0, 0, 0)];
    [_scrollView setContentOffset:CGPointMake(0, -(CGRectGetHeight(self.view.frame) * 0.6))];
    _scrollView.showsVerticalScrollIndicator = NO;
    
    [containerView addSubview:_scrollView];
    
    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_scrollView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_scrollView)]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:_scrollView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_dismissButton attribute:NSLayoutAttributeBottom multiplier:1.0f constant:10]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:_scrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:containerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0f]];

    [self setupTextView];
    
    [containerView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)]];

    
}


-(void)setupTextView{
    

    
    textView = [UITextView new];
    textView.backgroundColor = [UIColor clearColor];
    textView.translatesAutoresizingMaskIntoConstraints = NO;
    textView.attributedText = [self textViewText:_feed];
    textView.editable = NO;
//    textView.text = [LoremIpsum wordsWithNumber:100];
//    textView.textColor = [UIColor whiteColor];

    UIEdgeInsets inset = textView.textContainerInset;
    inset.top = 20.0f;
    inset.bottom = 20.0f;
    
    textView.textContainerInset = inset;
    textView.scrollEnabled = NO;
    
    [_scrollView addSubview:textView];

    [_scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[textView(==_scrollView)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(textView, _scrollView)]];

    [_scrollView addConstraint:[NSLayoutConstraint constraintWithItem:textView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:0 multiplier:1.0f constant:CGRectGetHeight(self.view.frame) * 0.4]];
    
    [_scrollView addConstraint:[NSLayoutConstraint constraintWithItem:textView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_scrollView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]];
    
    [_scrollView addConstraint:[NSLayoutConstraint constraintWithItem:textView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_scrollView attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f]];
    

}


-(NSAttributedString *)textViewText: (OIHActivityFeed *)feed{
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterShortStyle;
    NSString *dateString = [NSString stringWithFormat:NSLocalizedString(@"POSTED_ON_%@", @"Posted on %@"), [NSDate daysBetweenDate:_feed.createdAt andDate:[NSDate date]]];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:_feed.fromUser.userName attributes:@{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleTitle1], NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setParagraphSpacing:10];
    
    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:dateString attributes:@{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleBody], NSParagraphStyleAttributeName: paragraphStyle, NSForegroundColorAttributeName: (id)[UIColor colorWithWhite:0.8 alpha:1.0f]}]];
    
    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
    
    NSMutableParagraphStyle *reviewParagraph = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    [reviewParagraph setParagraphSpacing:20];
    
    [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"USER_REVIEW_%@", @""), [self emojiAtIndex:_feed.rating]] attributes:@{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleTitle3], NSForegroundColorAttributeName: [UIColor whiteColor], NSParagraphStyleAttributeName: reviewParagraph}]];
    
    
    if (_feed.content) {
        
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [attrString appendAttributedString:[[NSAttributedString alloc] initWithString:_feed.content attributes:@{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleBody], NSForegroundColorAttributeName: [UIColor whiteColor]}]];
        
    }

    return attrString;
    
}



-(void)panGesture: (UIPanGestureRecognizer *)gesture{
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            _originalCenter = containerView.center;
            originalFrame = containerView.frame;

            break;
        case UIGestureRecognizerStateChanged:{
            
            CGPoint translation = [gesture translationInView:containerView];
            containerView.center = CGPointMake(_originalCenter.x, _originalCenter.y + translation.y);

            CGPoint velocity = [gesture velocityInView:containerView];
            
            willDismiss = (fabs(containerView.frame.origin.y) > containerView.frame.size.height / 3.6) || fabs(velocity.y) > 1000;
            if (willDismiss) {
                self.dismissUp = -velocity.y > 0;
            }
        }
            break;
        case UIGestureRecognizerStateEnded:{
            
            if (willDismiss) {
                [self dismissView];
            }else{
                
                [UIView animateWithDuration:0.4f animations:^{
                    containerView.frame = originalFrame;
                }];
            }

        }
            break;
        default:
            break;
    }
    
}


-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    dispatch_async(dispatch_get_main_queue(), ^{    //it somehow needs to be run on the main thread, no idea why
        //the gradient is still debatable
        
        if (imageView.image) {
            CAGradientLayer *gradient = [CAGradientLayer layer];
            gradient.frame = containerView.frame;
            
            gradient.colors = [NSArray arrayWithObjects:
                               (id)[[UIColor clearColor] CGColor],
                               (id)[[UIColor colorWithWhite:0.0 alpha:0.3f] CGColor],
                               nil];
            gradient.locations = [NSArray arrayWithObjects:
                                  [NSNumber numberWithFloat:0.9],
                                  [NSNumber numberWithInt:1],
                                  nil];
            
            [containerView.layer insertSublayer:gradient above:imageView.layer];
            
        }
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = textView.frame;
        
        gradient.colors = [NSArray arrayWithObjects:
                           (id)[[UIColor clearColor] CGColor],
                           (id)[[UIColor colorWithWhite:0.0 alpha:0.3] CGColor],
                           (id)[[UIColor colorWithWhite:0.0 alpha:0.3] CGColor],
                           (id)[[UIColor colorWithWhite:0.0 alpha:0.3f] CGColor],
                           (id)[[UIColor clearColor] CGColor],
                           nil];
        
        gradient.locations = [NSArray arrayWithObjects:
                              [NSNumber numberWithFloat:0.0],
                              [NSNumber numberWithFloat:0.05],
                              [NSNumber numberWithFloat:0.5],
                              [NSNumber numberWithFloat:0.5],
                              [NSNumber numberWithInt:1],
                              nil];
        
        [textView.layer insertSublayer:gradient atIndex:0];
        
    });
    
}


-(NSString *)emojiAtIndex: (NSInteger)index{
    
    switch (index) {
        case 0:
            //angry face
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\xA1"];
        case 1:
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x9E"];
        case 2:
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\xB7"];
        case 3:
            //delicious face
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x8B"];
        case 4:
            //throw a kiss face, love it
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x8D"];
        default:
            break;
    }
    
    return nil;
}

-(void)toggleTextView{
    
    if (self.textView.layer.opacity == 0.0) {
        [UIView animateWithDuration:0.2 animations:^{
            self.textView.layer.opacity = 1.0f;
        }];
    }else{
        [UIView animateWithDuration:0.2 animations:^{
            self.textView.layer.opacity = 0.0f;
        }];
    }
}

-(void)dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - <UIViewControllerTransitioningDelegate>

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    OrderReviewAnimatedTransitioning *transitioning = [OrderReviewAnimatedTransitioning new];
    transitioning.presenting = YES;
    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    OrderReviewAnimatedTransitioning * transitioning = [OrderReviewAnimatedTransitioning new];
    transitioning.dismissUp = [self dismissUp];
    transitioning.presenting = NO;
    return transitioning;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

@implementation OrderReviewAnimatedTransitioning
@synthesize snapShot;

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext{
    return 0.45;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
    
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *fromView = fromViewController.view;
    UIView *toView = toViewController.view;
    UIView *containerView = [transitionContext containerView];

    
    if (self.presenting) {
        
        _backgroundView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
        _backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        [containerView addSubview:_backgroundView];
        
        [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_backgroundView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_backgroundView)]];
        [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_backgroundView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_backgroundView)]];
        
        [containerView insertSubview:toViewController.view aboveSubview:fromViewController.view];
        
        toView.alpha = 0.0f;
        toView.transform = CGAffineTransformMakeScale(0.6, 0.6);
        [containerView addSubview:toView];
        
        _indexOfBackgroundView = [[containerView subviews] indexOfObject:_backgroundView];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 usingSpringWithDamping:0.9 initialSpringVelocity:0.3 options:0 animations:^{
            toView.alpha = 1.0f;
            toView.transform = CGAffineTransformIdentity;

        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];
        
    }else{

        _backgroundView = [[containerView subviews] objectAtIndex:_indexOfBackgroundView];
        
        if (self.dismissUp) {
            fromView.transform = CGAffineTransformMakeTranslation(0, -1000);
        }else{
            fromView.transform = CGAffineTransformMakeTranslation(0, 1000);
        }
        
        containerView.backgroundColor = [UIColor clearColor];

        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.05 usingSpringWithDamping:0.9 initialSpringVelocity:0.3 options:0 animations:^{

            toView.transform = CGAffineTransformIdentity; //home screen
            _backgroundView.layer.opacity = 0.0f;
            
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];

    }

    
}

@end