//
//  OIHUserModel.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 10/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHModelProtocol.h"

typedef NS_OPTIONS(NSInteger, UserType) {
    
    UserFriend = 1 << 0,
    UserIsFollowing = 1 << 1,
    UserIsFollower = 1 << 2,
    UserPendingRequest = 1 << 3,
    UserFriendRequest = 1 << 4,
    UserIsMerchant = 1 << 5
};


@interface OIHUserModel : NSObject <OIHModelProtocol>

@property (nonatomic) NSString *userName;
@property (nonatomic) NSString *userLastName;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *_id;
@property (nonatomic) NSDictionary *userInfo;
@property (nonatomic) UserType userType;
@property (nonatomic) NSString *profilePicURL;
@property (nonatomic) BOOL isUserAllowToBeAddedAsFriend;

-(id)initWithUserInfo: (NSDictionary *)info;
+(NSMutableArray *) profilesFromArray: (NSArray *)data;

-(BOOL)isUserType: (UserType) type;
-(void)removeUserType: (UserType) type;
-(void)addUserType: (UserType) type;
-(void)resetUserType;

//friend requeset
-(void)confirmFriend :(void (^)(id data, NSError *error))completionHandler;
-(void)cancelRequest :(void (^)(id data, NSError *error))completionHandler;

-(void)removeFromFriendsList :(void (^)(id data, NSError *error))completionHandler;
-(void)sendFriendRequest :(void (^)(id data, NSError *error))completionHandler;

-(void)followPerson;
-(void)unfollowPerson;

-(void)userLastOrder :(void (^)(id data, NSError *error))completionHandler;

//-(BOOL) isUserFriend;
//-(BOOL) isUserPendingRequest;
//-(BOOL) isUserFriendRequest;
//-(BOOL) isUserMerchant;

@end
