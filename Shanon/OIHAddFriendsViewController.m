//
//  OIHAddFriendsViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 5/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHAddFriendsViewController.h"
#import "OIHFriendsListTableViewCell.h"
#import "OIHUserProfileViewController.h"
#import "OIHFriendRequestTableViewCell.h"



@interface OIHAddFriendsViewController () <UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchBarDelegate, FriendTableViewCellDelegate>

@property (nonatomic) UITableView *tableView;
@property (nonatomic) UISearchController *searchController;
@property (nonatomic) NSMutableArray *friendsArray;


@end

@implementation OIHAddFriendsViewController
@synthesize tableView = _tableView;
@synthesize friendsArray;
@synthesize searchController;



- (void)viewDidLoad {
    [super viewDidLoad];

    friendsArray = [NSMutableArray new];
    
    [self createTableView];

}


-(void)createTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [_tableView registerClass:[OIHFriendsListTableViewCell class] forCellReuseIdentifier:@"cell"];
    [_tableView registerClass:[OIHFriendRequestTableViewCell class] forCellReuseIdentifier:@"requestCell"];

    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 60.0f;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:_tableView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];

    
    //    self.clearsSelectionOnViewWillAppear = NO;
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;

    
}



-(void) updateSearchResultsForSearchController:(UISearchController *)searchController{}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchFriend: searchBar.text];
}


-(void)searchFriend: (NSString *)name{
    
    [[JSONRequest sharedInstance] fetch:@"user" method:@"GET" data:@{@"username": name} completionHandler:^(id data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                
                [friendsArray removeAllObjects];
                for (NSDictionary *user in data) {
                    [friendsArray addObject:user];
                }
                [self.tableView reloadData];
            }
        });
        
    }];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return friendsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHFriendsListTableViewCell *cell = (OIHFriendsListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    OIHUserModel *user = friendsArray[indexPath.row];

    cell.user = user;
    cell.delegate = self;

    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    OIHUserProfileViewController *vc = [[OIHUserProfileViewController alloc] initWithFriendProfile:friendsArray[indexPath.row]];
    [self.navigationController pushViewController:vc animated:YES];

    
}



#pragma mark - <OIHFriendsListTableViewCell>

-(void)tableViewCellDidPressActionButton: (OIHFriendsListTableViewCell *)cell{
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
