//
//  OIHUserFBFriendsTableViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"

@interface OIHUserFBFriendsTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) NSArray <OIHUserModel *> *friendsList;

@end
