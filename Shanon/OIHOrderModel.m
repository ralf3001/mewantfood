//
//  OIHOrderModel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHOrderModel.h"
#import "NSDate+JSONExtension.h"


@implementation OIHOrderModel
@synthesize _id;
@synthesize sellerRestaurantId;
@synthesize orderUser, orderRecipient;
@synthesize orderDate;
@synthesize orderMeal, orderMealId;
@synthesize status;
@synthesize quantity;
@synthesize restaurant;


-(id)initWithOrderInfo: (NSDictionary *)info{
    
    self = [super init];
    if (self) {
        
        self._id = info[kOIHOrderId];
        
        if ([info[kOIHOrderRestaurant] isKindOfClass:[NSDictionary class]]) {
            self.restaurant = [[OIHRestaurantModel alloc] initWithDictionary:info[kOIHOrderRestaurant]];
        }
        
        self.orderDate = [NSDate dateFromJSONDate:info[kOIHOrderDate]];
        self.sellerRestaurantId = info[kOIHOrderRestaurant][kOIHRestaurantId];
        
        if ([info[kOIHOrderMeal] isKindOfClass:[NSDictionary class]]) {
            self.orderMeal = [[OIHMealModel alloc] initWithInfoDictionary:info[kOIHOrderMeal]];
        }
        self.quantity = [info[kOIHOrderQuantity] integerValue];
        
        if ([info[kOIHOrderUser] isKindOfClass:[NSDictionary class]]) {

            OIHUserModel *user = [[OIHUserModel alloc] initWithUserInfo:info[kOIHOrderUser]];
            self.orderUser = user;
            if ([user._id isEqualToString:info[kOIHOrderRecipient][kOIHUserId]]) {
                self.orderRecipient = user;
            }

        }

        if ([info[kOIHOrderRecipient] isKindOfClass:[NSDictionary class]]) {
            self.orderRecipient = [[OIHUserModel alloc] initWithUserInfo:info[kOIHOrderRecipient]];
        }
        
        if ([info[@"recipientAddress"] isKindOfClass:[NSDictionary class]]) {
            self.recipientAddress = [[OIHAddressModel alloc] initWithAddressInfo:info[@"recipientAddress"]];
        }
        
        self.audioNoteURL = info[@"audioReview"];
        self.orderReceipt = info[kOIHOrderReceipt];
        self.status = [self typeEnumFromString:info[kOIHOrderStatus]];
        
        if ([info[@"orderReceipt"] isKindOfClass:[NSDictionary class]]) {
            self.orderReceipt = [[OIHReceiptModel alloc] initWithInfo:info[@"orderReceipt"]];
        }
        
        if ([info[@"orderReceipt"] isKindOfClass:[NSDictionary class]]) {
            self.totalAmount = info[@"orderReceipt"][@"totalAmount"];
        }
        
        self.infoDictionary = [info copy];
        self.isAcknowledged = [info[@"isAcknowledgedByRecipient"] boolValue];
    
    }
    
    return self;
    
}


+(NSMutableArray *)initFromArray: (NSArray *)orders{
    
    NSMutableArray *newOrders = [NSMutableArray new];
    
    for (NSDictionary *info in orders) {
        OIHOrderModel *order = [[OIHOrderModel alloc] initWithOrderInfo:info];
        [newOrders addObject:order];
    }
    return newOrders;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self._id forKey:kOIHOrderId];
    [encoder encodeObject:self.orderDate forKey:kOIHOrderDate];
    [encoder encodeObject:self.sellerRestaurantId forKey:kOIHOrderRestaurant];
    [encoder encodeObject:self.orderMeal forKey:kOIHOrderMeal];
    [encoder encodeObject:self.orderReceipt forKey:kOIHOrderReceipt];
    [encoder encodeObject:self.orderUser forKey:kOIHOrderUser];
    [encoder encodeObject:self.infoDictionary forKey:@"info"];
    
}

-(OrderStatus )typeEnumFromString: (NSString *)orderStatus{
    
    NSDictionary<NSString *, NSNumber *> *types = @{
                                                    @"ordered": @(Ordered),
                                                    @"accepted": @(OrderAccepted),
                                                    @"delivered": @(OrderDelivered),
                                                    @"canceled": @(OrderCanceled)
                                                    };
    
    return types[orderStatus].integerValue;
}


-(void)updateOrderStatus: (OrderStatus) updatedStatus{
    
    if (self.status != updatedStatus) {
        self.status = updatedStatus;
        
        NSString *statusString;
        
        switch (updatedStatus) {
                
            case OrderAccepted:
                statusString = kOIHOrderStatusAccepted;
                break;
            case OrderDelivered:
                statusString = kOIHOrderStatusDelivered;
                break;
            case OrderCanceled:
                statusString = kOIHOrderStatusCanceled;
                break;
            default:
                break;
        }
        
        
        [[JSONRequest sharedInstance] fetch:@"orderStatus" method:@"POST" data:@{@"_id": self._id, @"status": statusString} completionHandler:^(id data, NSError *error) {
            if (error) {
                //TODO: i haven't taken care when the request fails;
            }
        }];
 
    }

}


+ (NSDictionary *)typeDisplayNames{
    
    return @{@(Ordered) : NSLocalizedString(@"ORDER_ORDERED", "Ordered"),
             @(OrderAccepted) : NSLocalizedString(@"ORDER_ACCEPTED", @"Order Accepted"),
             @(OrderDelivered) : NSLocalizedString(@"ORDER_DELIVERED", @"Order Delivered"),
             @(OrderCanceled) : NSLocalizedString(@"ORDER_CANCELED", @"Order Canceled")
             };


}

-(NSString *)orderStatus{
    return [[self class] typeDisplayNames][@(self.status)];
}


-(UIColor *)statusColor{
    
    UIColor *color;
    
    switch (self.status) {
        case Ordered:
            color = [UIColor orderOrderedColor];
            break;
        case OrderAccepted:
            color = [UIColor orderAccptedColor];
            //brown
            break;
        case OrderDelivered:
            color = [UIColor orderDeliveredColor];
            //green color
            break;
        case OrderCanceled:
            color = [UIColor orderCanceledColor];
            break;
        default:
            break;
    }
    
    return color;

}



-(NSDictionary *)toDictionary{
    
    return self.infoDictionary;
}


@end
