//
//  UIFont+OIHSystemFonts.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 18/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "UIFont+OIHSystemFonts.h"

@implementation UIFont (OIHSystemFonts)

+(UIFont *)tableViewCellDescriptionFont{
    
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
    
}

+(UIFont *)tableViewCellContentFont{
    
    return [UIFont fontWithName:@"HelveticaNeue-Thin" size:16];
    
}

+(UIFont *)buttonTextBold{
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:[UIFont buttonFontSize]];
}

+(UIFont *)buttonTextNormal{
    return [UIFont fontWithName:@"HelveticaNeue" size:[UIFont buttonFontSize]];
}

+(UIFont *)activityButtonNormal{
    return [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
}


@end
