//
//  OIHRestaurantMealListViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantModel.h"
#import <UIKit/UIKit.h>

@interface OIHRestaurantMealListViewController : UIViewController
@property(nonatomic) OIHRestaurantModel *restaurantInfo;

@end
