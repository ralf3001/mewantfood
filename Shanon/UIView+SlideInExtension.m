//
//  UIView+SlideInExtension.m
//  slidingtest
//
//  Created by Ralf Cheung on 24/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "UIView+SlideInExtension.h"

@implementation UIView (SlideInExtension)



-(void)slideInFromDirection: (SlideInDirection) direction{

    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.fillMode = kCAFillModeRemoved;
    transition.subtype = [self direction:direction];
    
    CABasicAnimation *fadeIn = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeIn.fromValue = [NSNumber numberWithDouble:1.0f];
    fadeIn.toValue = [NSNumber numberWithDouble:0.4f];
    
    
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.duration = 1.0f;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    group.animations = @[transition, fadeIn];
    
    [self.layer addAnimation:transition forKey:@"slideInTransition"];
    
}

+(NSDictionary *)typeDisplayNames{
    return @{
             @(UIViewSlideInFromTop): kCATransitionFromTop,
             @(UIViewSlideInFromRight): kCATransitionFromRight,
             @(UIViewSlideInFromBottom):kCATransitionFromBottom,
             @(UIViewSlideInFromLeft): kCATransitionFromLeft
             };
}


-(NSString *)direction: (SlideInDirection) direction{
    return [[self class] typeDisplayNames][@(direction)];
}



@end
