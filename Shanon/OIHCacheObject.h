//
//  OIHCacheObject.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHCacheObject : NSObject <NSCoding>
@property (nonatomic) id object;
@property (nonatomic) NSString *key;
@property (nonatomic) NSDate *createdDate;

@end
