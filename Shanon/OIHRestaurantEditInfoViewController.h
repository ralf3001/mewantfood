//
//  OIHRestaurantEditInfoViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantModel.h"
#import <UIKit/UIKit.h>

@interface OIHRestaurantEditInfoViewController : UIViewController
@property(nonatomic) OIHRestaurantModel *restaurantModel;
@end
