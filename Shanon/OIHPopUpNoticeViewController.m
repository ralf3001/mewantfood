//
//  OIHPopUpNoticeViewController.m
//  Shanon
//
//  Created by Ralf Cheung on 13/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHPopUpNoticeViewController.h"
#import "OIHModalPresentationController.h"
#import "OIHMapLocationDetailViewController.h"


@interface OIHPopUpNoticeViewController ()
@property (nonatomic) UIView *containerView;

@property (nonatomic) UIButton *dismissButton;
@property (nonatomic) UIButton *actionButton;

@end

@implementation OIHPopUpNoticeViewController

-(id)initWithTitle: (NSString *)title description: (NSString *)description{
    
    if (self = [super init]) {
        _descriptionText = description;
        _titleText = title;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    _containerView = [UIView new];
    _containerView.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (!_backgroundColor) {
        _backgroundColor = [UIColor colorWithRed:35/255.0f green:106/255.0f blue:98/255.0f alpha:1.0f];
    }
    
    _containerView.backgroundColor = _backgroundColor;
    
    [self.view addSubview:_containerView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_containerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_containerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    
    if (IS_IPAD) {
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_containerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:CGRectGetWidth(self.view.frame) * 0.5]];
        
    }else{
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_containerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:CGRectGetWidth(self.view.frame) * 0.8]];
        
    }

    
    _dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_dismissButton setImage:[IonIcons imageWithIcon:ion_ios_close_empty size:50 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    [_dismissButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [_containerView addSubview:_dismissButton];
    
    [_containerView addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_dismissButton attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[_dismissButton(==40)]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_dismissButton)]];

    _actionButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _actionButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_actionButton setTitle:_buttonTitle forState:UIControlStateNormal];
    [_actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_actionButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:24]];
    [_actionButton addTarget:self action:@selector(actionButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    _actionButton.backgroundColor = [_backgroundColor darkerColorForColor];
    
    [_containerView addSubview:_actionButton];
    
    
    UILabel *congratzLabel = [UILabel new];
    congratzLabel.translatesAutoresizingMaskIntoConstraints = NO;
    congratzLabel.numberOfLines = 0;
    congratzLabel.textColor = [UIColor whiteColor];
    congratzLabel.text = _descriptionText;
    [_containerView addSubview:congratzLabel];
    
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.numberOfLines = 0;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = _titleText;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:28];
    [_containerView addSubview:titleLabel];
    
    
    
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[titleLabel]-15-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(titleLabel)]];
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[congratzLabel]-15-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(congratzLabel)]];

    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-40-[_actionButton]-50-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_actionButton)]];

    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[_dismissButton(==40)]-[titleLabel]-[congratzLabel]-25-[_actionButton]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(congratzLabel, _dismissButton, titleLabel, _actionButton)]];
    
}

-(void)dismiss{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)actionButtonPressed{
    
    __weak typeof(self) weakSelf = self;
    _buttonAction(weakSelf);
    
}


#pragma mark - UIViewControllerTransitioningDelegate

- (nullable UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    if (presented == self) {
        return [[OIHModalPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    }
    return nil;
}


- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    if (presented == self) {
        MapLocationAnimatedTransitioning *transition = [MapLocationAnimatedTransitioning new];
        transition.presenting = YES;
        return transition;
    }else return nil;
    
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    if (dismissed == self) {
        MapLocationAnimatedTransitioning *transition = [MapLocationAnimatedTransitioning new];
        transition.presenting = NO;
        return transition;
    }else return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
