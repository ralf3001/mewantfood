//
//  OIHActivityCellButtonDelegate.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#ifndef OIHActivityCellButtonDelegate_h
#define OIHActivityCellButtonDelegate_h


#endif /* OIHActivityCellButtonDelegate_h */

#import <Foundation/Foundation.h>

@protocol OIHActivityCellButtonDelegate <NSObject>

-(void)userDidTapThanksButton: (OIHActivityFeed *) activity isPostThanked: (BOOL) isPostThanked;

@optional
-(void)userDidTapLikeButton: (OIHActivityFeed *) activity isPostLiked: (BOOL) isPostLiked;
-(void)userDidTapCommentButton: (OIHActivityFeed *) activity;

@end