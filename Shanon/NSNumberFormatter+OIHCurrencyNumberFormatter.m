//
//  NSNumberFormatter+OIHCurrencyNumberFormatter.m
//  Shanon
//
//  Created by Ralf Cheung on 8/5/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "NSNumberFormatter+OIHCurrencyNumberFormatter.h"

@implementation NSNumberFormatter (OIHCurrencyNumberFormatter)

+(id)formatterWithCurrency: (NSString *)currency{
    
    NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: currency}];

    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:localeIde]];
    [formatter setMinimumFractionDigits:2];
    [formatter setMaximumFractionDigits:2];

    return formatter;
}

@end
