//
//  OIHUserOrderInformationTableViewCell.m
//  Shanon
//
//  Created by Ralf Cheung on 12/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserOrderInformationTableViewCell.h"
#import "OIHFloatingView.h"

#define kImageViewHeight 180


@interface OIHUserOrderInformationTableViewCell ()
@property (nonatomic) UIView *stateBackgroundView;
@property (nonatomic) OIHFloatingView *floatingView;
@property (nonatomic) UIView *textContainerView;

@end


@implementation OIHUserOrderInformationTableViewCell

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    
    [_floatingView setHighlight:highlighted];
    
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
    
}



-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _floatingView = [OIHFloatingView new];
        _floatingView.translatesAutoresizingMaskIntoConstraints = NO;
        _floatingView.cornerRadius = 0.0f;
        [self.contentView addSubview:_floatingView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_floatingView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_floatingView)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_floatingView]-15-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_floatingView)]];
        
        
        _textContainerView = [UIView new];
        _textContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        [_floatingView addSubview:_textContainerView];
        
        _orderImageView = [UIImageView new];
        _orderImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _orderImageView.contentMode = UIViewContentModeScaleAspectFill;
        _orderImageView.layer.masksToBounds = YES;
        [_floatingView addSubview:_orderImageView];

        _stateBackgroundView = [UIView new];
        _stateBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        [_textContainerView addSubview:_stateBackgroundView];
        
        [self allocLabels:_textContainerView];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_orderImageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_orderImageView)]];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_textContainerView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_textContainerView)]];

        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_orderImageView(==imageViewHeight)][_textContainerView]|" options:0 metrics:@{@"imageViewHeight": @(kImageViewHeight)} views:NSDictionaryOfVariableBindings(_orderImageView, _textContainerView)]];
        
        
        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_stateBackgroundView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_stateBackgroundView)]];
        
        [_textContainerView addConstraint:[NSLayoutConstraint constraintWithItem:_stateBackgroundView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:_textContainerView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0f]];
        
        [_textContainerView addConstraint:[NSLayoutConstraint constraintWithItem:_stateBackgroundView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0 constant:8.0f]];

        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_stateBackgroundView(==8)]-[_orderItemNameLabel]-[_orderPriceLabel(<=priceLabelWidth)]-|" options:0 metrics:@{@"priceLabelWidth": @80} views:NSDictionaryOfVariableBindings(_orderItemNameLabel, _orderPriceLabel, _stateBackgroundView)]];

        [_textContainerView addConstraint:[NSLayoutConstraint constraintWithItem:_orderPriceLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_orderItemNameLabel attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
        
        
        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[_orderStateLabel]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_orderStateLabel)]];
        
        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[_orderRestaurantLabel]-[_orderQuantityLabel(==quantityLabelWidth)]-|" options:NSLayoutFormatAlignAllTop metrics:@{@"quantityLabelWidth": @30} views:NSDictionaryOfVariableBindings(_orderRestaurantLabel, _orderQuantityLabel)]];
        
        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[_orderDateLabel]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_orderDateLabel)]];

        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[_orderRecipientAddressLabel]" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_orderRecipientAddressLabel)]];

        [_textContainerView addConstraint:[NSLayoutConstraint constraintWithItem:_orderRecipientAddressLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:_orderRestaurantLabel attribute:NSLayoutAttributeRight multiplier:1.0f constant:0.0f]];
        
        
        [_textContainerView addConstraint:[NSLayoutConstraint constraintWithItem:_orderQuantityLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_orderQuantityLabel attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];

        
        
        
        
        [_textContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_orderItemNameLabel]-[_orderRestaurantLabel]-[_orderRecipientAddressLabel]-[_orderDateLabel]-[_orderStateLabel]-|" options:NSLayoutFormatAlignAllLeft metrics:0 views:NSDictionaryOfVariableBindings(_orderStateLabel, _orderItemNameLabel, _orderRestaurantLabel, _orderDateLabel, _orderRecipientAddressLabel)]];

        
        
        
        
    }
    return self;
}



-(void)allocLabels: (UIView *)superView{
    
    _orderItemNameLabel = [self createLabel];
    _orderItemNameLabel.numberOfLines = 0;
    _orderItemNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:28];
    [superView addSubview:_orderItemNameLabel];
    
    _orderRestaurantLabel = [self createLabel];
    _orderRestaurantLabel.numberOfLines = 0;
    _orderRestaurantLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
    _orderRestaurantLabel.textColor = [UIColor lightGrayColor];
    [superView addSubview:_orderRestaurantLabel];
    
    _orderRecipientNameLabel = [self createLabel];
    [superView addSubview:_orderRecipientNameLabel];
    
    _orderDateLabel = [self createLabel];
    _orderDateLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    _orderDateLabel.textColor = [UIColor grayColor];

    [superView addSubview:_orderDateLabel];
    
    _orderPriceLabel = [self createLabel];
    _orderPriceLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
    _orderPriceLabel.textAlignment = NSTextAlignmentRight;

    [superView addSubview:_orderPriceLabel];
    
    _orderStateLabel = [self createLabel];
    _orderStateLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
    [superView addSubview:_orderStateLabel];
    
    _orderRecipientAddressLabel = [self createLabel];
    _orderRecipientAddressLabel.numberOfLines = 0;
    _orderRecipientAddressLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _orderRecipientAddressLabel.textColor = [UIColor darkGrayColor];
    _orderRecipientAddressLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];

    [superView addSubview:_orderRecipientAddressLabel];
    
    _orderQuantityLabel = [self createLabel];
    _orderQuantityLabel.layer.cornerRadius = 15;
    _orderQuantityLabel.layer.masksToBounds = YES;
    _orderQuantityLabel.backgroundColor = [UIColor redColor];
    _orderQuantityLabel.textAlignment = NSTextAlignmentCenter;
    _orderQuantityLabel.textColor = [UIColor whiteColor];

    [superView addSubview:_orderQuantityLabel];

    
}

-(UILabel *)createLabel{
    
    UILabel *label = [UILabel new];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    return label;
    
}

-(void)setOrder:(OIHOrderModel *)order{
    
    _order = order;
    
    self.orderDateLabel.text = [NSString stringWithFormat:@"%@", [NSDate daysBetweenDate:_order.orderDate andDate:[NSDate date]]];
    
    if (![_order.orderRecipient._id isEqualToString:[UserProfile currentUser]._id]) {
        
        self.orderItemNameLabel.text = _order.orderMeal.name;
        self.orderRestaurantLabel.text = _order.restaurant.restaurantName;
        
        NSMutableAttributedString *recipientAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"%@_DEFAULT_ADDRESS", @"[person]'s default address"), _order.orderRecipient.userName]];
        
        [recipientAttributedString setAttributes:@{NSForegroundColorAttributeName: (id)[UIColor blackColor],
                                                   NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                                   } range:NSMakeRange(0, _order.orderRecipient.userName.length + 1)];
        
        
        self.orderRecipientAddressLabel.attributedText = recipientAttributedString;
        
    }else{
        
        self.orderItemNameLabel.text = _order.orderMeal.name;
        self.orderRestaurantLabel.text = _order.restaurant.restaurantName;
        self.orderRecipientAddressLabel.text = _order.recipientAddress.userEnteredAddress;
        
    }
    
    NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
    [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currency setCurrencyCode:self.order.restaurant.restaurantCurrency];
    self.orderPriceLabel.text = [currency stringFromNumber: self.order.totalAmount];
    
    self.orderQuantityLabel.text = [NSString stringWithFormat:@"%ld", (long)_order.quantity];
    
    
    [_orderImageView sd_setImageWithURL:[NSURL URLWithString:_order.orderMeal.productImage] placeholderImage:[UIImage imageFromColor:[UIColor lightGrayColor]]];
    
    
    _orderStateLabel.text = [self.order orderStatus];
    _orderStateLabel.textColor = [self.order statusColor];
    _stateBackgroundView.backgroundColor = [self.order statusColor];

}

@end
