//
//  OIHCompetitionApplicationViewController.m
//  Shanon
//
//  Created by Ralf Cheung on 14/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCompetitionApplicationViewController.h"
#import "OIHSocialManager.h"


@interface OIHCompetitionApplicationViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UIButton *uploadButton;
@property (nonatomic) UITextView *textView;

@end

@implementation OIHCompetitionApplicationViewController

-(id)initWithCompetition: (OIHCompetitionInfo *)competition{
    
    if (self = [super init]) {
        _competitionInfo = competition;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    
    _imageView = [UIImageView new];
    _imageView.translatesAutoresizingMaskIntoConstraints = NO;
    _imageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _imageView.layer.borderWidth = 0.5f;
    _imageView.userInteractionEnabled = YES;
    [_imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImage)]];
    
    [self.view addSubview:_imageView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_imageView attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
    
    _textView = [UITextView new];
    _textView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_textView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_textView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_textView)]];
    
    
    
    _uploadButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _uploadButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_uploadButton setTitle:@"Upload" forState:UIControlStateNormal];
    [_uploadButton addTarget:self action:@selector(sendCompetition) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_uploadButton];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_uploadButton]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_uploadButton)]];


    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_imageView]-[_textView(==90)]-[_uploadButton]-|" options:NSLayoutFormatAlignAllCenterX metrics:0 views:NSDictionaryOfVariableBindings(_imageView, _textView, _uploadButton)]];
    
    
}

-(void)sendCompetition{
    
    [self joinCompetition:_competitionInfo];
    
}


-(void)joinCompetition: (OIHCompetitionInfo *) competition{
    
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(22.401265, 114.199131);
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"competitionEntry/%@", competition._id] httpMethod:HTTP_POST];
    setting.requestData = @{@"imageURL": @"http://cdn-3.motorsport.com/images/amp/6OxwqX50/s6/f1-bahrain-gp-2016-lewis-hamilton-mercedes-amg-f1-team.jpg", @"content": [LoremIpsum wordsWithNumber:30], @"coordinates": [self coordinatesToString:coordinate]};
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (error) {
            NSLog(@"[Error]: %@", [error localizedDescription]);
        }
    }];
    
}

-(NSString *)coordinatesToString: (CLLocationCoordinate2D)coordinate{
    return [NSString stringWithFormat:@"%f,%f", coordinate.longitude, coordinate.latitude];
}

//22.401265, 114.199131

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [[OIHSocialManager sharedInstance] uploadActivityImage:[chosenImage resizeImage] completionHandler:^(id result, NSError *error) {
        NSString *url = result[@"url"];
        
    }];
    
}


-(void)selectImage{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
