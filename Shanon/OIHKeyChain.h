//
//  OIHKeyChain.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHKeyChain : NSObject

+ (void)clearCache;
+ (void)deleteProfile;
+ (void)saveItem:(NSDictionary *)item;
+ (NSDictionary *)profile;

@end
