//
//  OIHConversationListTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 15/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHConversation.h"

@interface OIHConversationListTableViewCell : UITableViewCell
@property (nonatomic) UILabel *userLabel;
@property (nonatomic) UILabel *lastMessageLabel;
@property (nonatomic) UIImageView *userProfileImageView;
@property (nonatomic) OIHConversation *conversation;

@end
