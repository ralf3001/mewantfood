//
//  OIHMessageInputView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMessageInputView.h"

@implementation OIHMessageInputView


-(id)init{
    
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor highlightColor];
        
        
        UIView *topBorder = [UIView new];
        topBorder.translatesAutoresizingMaskIntoConstraints = NO;
        topBorder.layer.borderColor = [UIColor lightGrayColor].CGColor;
        topBorder.layer.borderWidth = 0.5f;
        [self addSubview:topBorder];
        
        
        _inputView = [UITextView new];
        _inputView.delegate = self;
        _inputView.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0f].CGColor;
        _inputView.layer.borderWidth = 1.0f;
        _inputView.translatesAutoresizingMaskIntoConstraints = NO;
        [self setPlaceholderText];
        [self addSubview:_inputView];
        
        _sendButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _sendButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_sendButton setTitle:NSLocalizedString(@"SEND_BUTTON", @"Send") forState:UIControlStateNormal];
        [_sendButton addTarget:self action:@selector(sendButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_sendButton];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[topBorder]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(topBorder)]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_inputView]-[_sendButton(==buttonWidth)]-|" options: NSLayoutFormatAlignAllBottom metrics:@{@"buttonWidth":@(50)} views:NSDictionaryOfVariableBindings(_inputView, _sendButton)]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[topBorder(==1)]-[_inputView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_inputView, topBorder)]];
        
    }
    
    return self;
}

-(void)sendButtonPressed{
    
    if ([self.delegate respondsToSelector:@selector(messageInputViewDidPressSendButton:)]) {
        [self.delegate messageInputViewDidPressSendButton:_inputView.text];
    }
    
    _inputView.text = @"";
    [_inputView resignFirstResponder];
}


-(void)textViewDidChange:(UITextView *)textView{
    
    if (textView.text.length > 0) {
        _inputView.textColor = [UIColor blackColor];
//        NSLog(@"%f", [self appropriateHeight]);
//        
        [[self notificationCenter] postNotificationName:OIHMessageUserIsTypingNotification object:nil];
        
    }
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{

    if ([textView.text isEqualToString:@""]) {
        
        [self setPlaceholderText];
        
    }

}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:NSLocalizedString(@"MESSAGE_INPUT_TEXT_PLACEHOLDER", @"Placeholder text")]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

-(void)setPlaceholderText{
    
    _inputView.text = NSLocalizedString(@"MESSAGE_INPUT_TEXT_PLACEHOLDER", @"Placeholder text");
    _inputView.textColor = [UIColor lightGrayColor];
    
}


- (NSUInteger)numberOfLines{
    
    CGSize contentSize = _inputView.contentSize;
    
    CGFloat contentHeight = contentSize.height;
    contentHeight -= _inputView.textContainerInset.top + _inputView.textContainerInset.bottom;
    
    NSUInteger lines = fabs(contentHeight/_inputView.font.lineHeight);
    
    // This helps preventing the content's height to be larger that the bounds' height
    // Avoiding this way to have unnecessary scrolling in the text view when there is only 1 line of content
    if (lines == 1 && contentSize.height > _inputView.bounds.size.height) {
        contentSize.height = _inputView.bounds.size.height;
        _inputView.contentSize = contentSize;
    }
    
    // Let's fallback to the minimum line count
    if (lines == 0) {
        lines = 1;
    }
    
    return lines;
}

- (NSUInteger)maxNumberOfLines{
    
    NSUInteger numberOfLines = _maxNumberOfLines;
    
    if (SLK_IS_LANDSCAPE) {
        if ((SLK_IS_IPHONE4 || SLK_IS_IPHONE5)) {
            numberOfLines = 2.0; // 2 lines max on smaller iPhones
        }
        else if (SLK_IS_IPHONE) {
            numberOfLines /= 2.0; // Half size on larger iPhone
        }
    }

    return numberOfLines;
}



- (CGFloat)appropriateHeight{
    
    CGFloat height = 0.0;
    CGFloat minimumHeight = 80;
    
    if (self.numberOfLines == 1) {
        height = minimumHeight;
    }
    else if (self.numberOfLines < self.maxNumberOfLines) {
        height = [self slk_inputBarHeightForLines:self.numberOfLines];
    }
    else {
        height = [self slk_inputBarHeightForLines:self.maxNumberOfLines];
    }
    
    if (height < minimumHeight) {
        height = minimumHeight;
    }
    
//    if (self.isEditing) {
//        height += self.editorContentViewHeight;
//    }
    
    return roundf(height);
}


- (CGFloat)slk_inputBarHeightForLines:(NSUInteger)numberOfLines{
    
    CGFloat height = _inputView.intrinsicContentSize.height;
    height -= self.inputView.font.lineHeight;
    height += roundf(self.inputView.font.lineHeight*numberOfLines);
    height += 20;
    
    return height;
}



-(NSNotificationCenter *)notificationCenter{
    
    return [NSNotificationCenter defaultCenter];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
