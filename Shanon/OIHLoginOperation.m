//
//  OIHLoginOperation.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHLoginOperation.h"

@interface OIHLoginOperation ()
@property (nonatomic) NSMutableURLRequest *request;
@property (nonatomic) NSURLSession *session;
@property (nonatomic) NSURLSessionDataTask *task;

@end


@implementation OIHLoginOperation

-(id)initWithRequest: (NSMutableURLRequest *)request session: (NSURLSession *)session completionHandler:(void (^)(id data, NSError *error))completionHandler{
    
    self = [super init];
    if (self) {
        
        executing = NO;
        finished = NO;
        
        [request setValue:@"testing" forHTTPHeaderField:@"authorization"];
        
        _task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            [self setFinishStatus];
            if (!error) {
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                
                if ([httpResponse statusCode] == 200) {
                    
                    NSDictionary *userDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                    
                    if (completionHandler) {
                        completionHandler(userDictionary, nil);
                    }
                    
                }else{
                    
                    completionHandler(nil, error);
                    
                }
                
            }else{
                completionHandler(nil, error);
                
            }
            
            
            
        }];

        
    }
    return self;
}

-(void)setFinishStatus{
    
    [self willChangeValueForKey:@"isExecuting"];
    executing = NO;
    [self didChangeValueForKey:@"isExecuting"];
    
    [self willChangeValueForKey:@"isFinished"];
    finished = YES;
    [self didChangeValueForKey:@"isFinished"];

    
}



-(void)start{
    
    if ([self isCancelled]){
        // Must move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"finished"];
        finished = YES;
        [self didChangeValueForKey:@"finished"];
        return;
    }
    
    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"executing"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    executing = YES;
    [self didChangeValueForKey:@"executing"];
    
}

-(void)main{
    
    [_task resume];
    
}


-(BOOL)isConcurrent{
    return YES;    //Default is NO so overriding it to return YES;
}

-(BOOL)isExecuting{
    return executing;
}

-(BOOL)isFinished{
    return finished;
}

@end
