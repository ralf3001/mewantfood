//
//  JSONRequest.m
//  Ferrle
//
//  Created by Ralf Cheung on 2/18/15.
//  Copyright (c) 2015 Ralf Cheung. All rights reserved.
//

#import "JSONRequest.h"
#import "UserProfile.h"
#import "NSData+AES.h"
#import "AppDelegate.h"
#import "OIHLoginOperation.h"
#import "OIHNetworkOperation.h"
#import "OIHKeyChain.h"


@interface JSONRequest () <NSURLSessionDataDelegate>

@property (nonatomic) NSOperationQueue *operationQueue;
@property (nonatomic) OIHLoginOperation *loginOperation;
@end


@implementation JSONRequest
@synthesize operationQueue;
@synthesize loginOperation;


+(JSONRequest *)sharedInstance{
    
    static JSONRequest *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[JSONRequest alloc] init];
        
        
        NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:40 * 1024 * 1024 diskCapacity:300 * 1024 * 1024 diskPath:@"nsurlcache"];
        [NSURLCache setSharedURLCache:cache];
        //        sleep(1);
        //http://stackoverflow.com/questions/21957378/how-to-cache-using-nsurlsession-and-nsurlcache-not-working
    });
    return _sharedInstance;
}

-(void) setupNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login:) name:@"loginRetry" object:nil];
    
}


-(id)init{
    
    self = [super init];
    if (self) {
        operationQueue = [NSOperationQueue new];
    }
    return self;
}

-(NSNotificationCenter *)notificationCenter{
    return [NSNotificationCenter defaultCenter];
}



-(NSDictionary *)decryptLoginInfo{
    return [OIHKeyChain profile];
}

-(NSString *)connectionURL{
    
#ifdef DEBUG
    return @"http://10.0.1.4:3000/";
#else
    return @"https://api.getshanon.com/";
#endif

}


-(void)loginWithToken:(void (^)(id data, NSError *error))completionHandler{
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSDictionary *accountInfo = [self decryptLoginInfo];
        
        if (accountInfo) {
            NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[accountInfo objectForKey:@"accessToken"], @"custom-token", accountInfo[@"email"], @"custom-username", nil];
            
            NSMutableURLRequest *request = [self createMutableURLRequest:@"loginToken" method:@"POST" dictionary:dictionary];
            
            NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession* session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
            
            loginOperation = [[OIHLoginOperation alloc] initWithRequest:request session:session completionHandler:completionHandler];
            [operationQueue addOperation:loginOperation];
            
        }
        
    });
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        
        NSLog(@"Ignoring SSL");
        SecTrustRef trust = challenge.protectionSpace.serverTrust;
        NSURLCredential *cred;
        cred = [NSURLCredential credentialForTrust:trust];
        [challenge.sender useCredential:cred forAuthenticationChallenge:challenge];
        return;
    }
}


-(void) fetch: (NSString*)requestMethod method: (NSString *)method dictionary: (NSDictionary *)dictionary {
    
    NSMutableURLRequest *request = [self createMutableURLRequest:requestMethod method:method dictionary:dictionary];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.URLCache = nil;
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Something's gone wrong"
                                          message:@"Maybe try again later"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"OK")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
    [alertController addAction:cancelAction];
    
    NSURLSessionDataTask *t = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self dataTaskReply:data response:response error:error ];
    }];
    
    [t resume];
    
}


-(void) dataTaskReply: (NSData *)data response:(NSURLResponse *)response error:(NSError *)error{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Something's gone wrong"
                                          message:@"Maybe try again later"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"OK")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
    [alertController addAction:cancelAction];
    
    
    if (!error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if ([httpResponse statusCode] == 200) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            [[self notificationCenter] postNotificationName:@"fetchNotification" object:dict];
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController presentViewController:alertController animated:YES completion:nil];
            });
            
        }
    }else{
        switch (error.code) {
                
            case -1004: NSLog(@"Cannot connect to the server");
                break;
            default: NSLog(@"%@", [error localizedDescription]);
                break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alertController.message = [error localizedDescription];
            [[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController presentViewController:alertController animated:YES completion:nil];
        });
        
        
    }
    
    
}

+(void)fetchBackground: (JSONRequestSettings *)setting{

    id data = setting.requestData;
    NSString *requestMethod = setting.route;
    NSString *method = [setting httpMethod];
    
//    NSMutableURLRequest *request;
//    if ([data isKindOfClass:[NSDictionary class]]) {
//        request = [self createMutableURLRequest:requestMethod method:method dictionary:data];
//    }else{
//        request = [self createMutableURLRequest:requestMethod method:method array:data];
//    }
//    
//    OIHNetworkOperation *operation = [[OIHNetworkOperation alloc] initWithRequest:request setting:setting completionHandler:nil];
//    [operation addDependency:loginOperation];
//    [operationQueue addOperation:operation];

    
}

-(void)fetchBackground: (JSONRequestSettings *)setting{
    
    id data = setting.requestData;
    NSString *requestMethod = setting.route;
    NSString *method = [setting httpMethod];
    
    NSMutableURLRequest *request;
    if ([data isKindOfClass:[NSDictionary class]]) {
        request = [self createMutableURLRequest:requestMethod method:method dictionary:data];
    }else{
        request = [self createMutableURLRequest:requestMethod method:method array:data];
    }
    
    OIHNetworkOperation *operation = [[OIHNetworkOperation alloc] initWithRequest:request setting:setting completionHandler:nil];
    [operation addDependency:loginOperation];
    [operationQueue addOperation:operation];
    
}


-(void)fetch: (JSONRequestSettings *)setting completionHandler:(void (^)(id data, BOOL isCachedResult, NSError *error))completionHandler{
    
    NSString *region = [[NSUserDefaults standardUserDefaults] objectForKey:@"region"];
    
    if (region) {
        setting.requestData[@"region"] = region;
    }
    
    id data = setting.requestData;
    NSString *requestMethod = setting.route;
    NSString *method = [setting httpMethod];
    
    NSMutableURLRequest *request;
    
    if ([data isKindOfClass:[NSDictionary class]]) {
        request = [self createMutableURLRequest:requestMethod method:method dictionary:data];
    }else{
        request = [self createMutableURLRequest:requestMethod method:method array:data];
    }
    
    OIHNetworkOperation *operation = [[OIHNetworkOperation alloc] initWithRequest:request setting:setting completionHandler:completionHandler];
    if (loginOperation) {
        [operation addDependency:loginOperation];
    }
    [operationQueue addOperation:operation];
    
}


-(void)userTimeout: (NSTimer *)timer{
    
    NSDictionary *userInfo = timer.userInfo;
    [self returnCache:userInfo];
    
}

-(void)returnCache: (NSDictionary *)info{
    
    void (^completionHandler)(id data, BOOL isCachedResult, NSError *error);
    completionHandler = [info[@"completionHandler"] copy];
    
    id cache = [[OIHOnDiskCache sharedCache] loadObjectFromCache:info[@"path"]];
    completionHandler(cache, YES, nil);
    
}



-(void) fetch: (NSString*)requestMethod method: (NSString *)method data: (id)data completionHandler:(void (^)(id data, NSError *error))completionHandler{
    
    
    NSMutableURLRequest *request;
    if ([data isKindOfClass:[NSDictionary class]]) {
        request = [self createMutableURLRequest:requestMethod method:method dictionary:data];
    }else{
        request = [self createMutableURLRequest:requestMethod method:method array:data];
    }
    
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.URLCache = nil;
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
    
    
    NSURLSessionDataTask *t = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            
            
            if ([httpResponse statusCode] == 200) {
                
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                
                if (completionHandler) {
                    completionHandler(dict, NULL);
                }
                if ([method isEqualToString:@"GET"] && dict) {
                    [[OIHOnDiskCache sharedCache] cacheObject:dict withKey:request.URL.path toDisk:YES];
                }
                if (!dict) {
                    [[OIHOnDiskCache sharedCache] deleteCache:request.URL.path];
                }
                
            }else{
                
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                
                error = [NSError errorWithDomain:dict[@"message"] ? dict[@"message"] : @"Error" code:[httpResponse statusCode] userInfo:dict];
                
                if (completionHandler)
                    completionHandler(dict, error);
            }
        }else{
            switch (error.code) {
                    
                case -1004: NSLog(@"Cannot connect to the server");
                    break;
                default: NSLog(@"%@", [error localizedDescription]);
                    break;
            }
            
            if (completionHandler)
                completionHandler(nil, error);
            
            
        }
        
        
    }];
    
    
    __weak NSURLSessionDataTask *task = t;
    NSBlockOperation *operation = [NSBlockOperation new];
    if (loginOperation) {
        [operation addDependency:loginOperation];
        
    }
    
    [operation addExecutionBlock:^{
        [task resume];
    }];
    
    [operationQueue addOperation:operation];
    
    
    
}


-(NSMutableURLRequest *)createMutableURLRequest: (NSString*)requestMethod method: (NSString *)method dictionary: (NSDictionary *)dictionary{
    
    //    UserProfile *profile = [UserProfile currentUser];
    NSMutableURLRequest* request;
    
    if([method isEqualToString:@"GET"] || [method isEqualToString:@"DELETE"]){
        NSString *urlString = [NSString stringWithFormat:@"%@%@", [self connectionURL], requestMethod];
        
        if (dictionary) {
            urlString = [urlString stringByAppendingString:@"?"];
        }
        
        //NSMutableArray *queryItems = [NSMutableArray new];
        
        for (NSString *string in [dictionary allKeys]) {
            //[queryItems addObject:[NSURLQueryItem queryItemWithName:string value:dictionary[string]]];
            urlString  = [urlString stringByAppendingString:[NSString stringWithFormat:@"%@=%@&", string, [dictionary objectForKey:string]]];
        }
        if (dictionary) {
            urlString = [urlString substringWithRange:NSMakeRange(0, [urlString length] - 1)];
            
        }
        
        NSURL *url = [NSURL URLWithString:urlString];
        request = [NSMutableURLRequest requestWithURL:url];
        
        
    }else if([method isEqualToString:@"POST"] || [method isEqualToString:@"PUT"]){
        
        NSData *jsonRequest;
        if (dictionary) {
            NSMutableDictionary *jsonDict = [NSMutableDictionary new];
            for (NSString *key in [dictionary allKeys]) {
                [jsonDict setObject:[dictionary objectForKey:key] forKey:key];
            }
            jsonRequest = [NSJSONSerialization dataWithJSONObject:jsonDict options:0 error:nil];
            
        }else{
            
        }
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@", [self connectionURL], requestMethod];
        NSURL *url = [NSURL URLWithString:urlString];
        request = [NSMutableURLRequest requestWithURL:url];
        NSString *length = [NSString stringWithFormat:@"%ld", (unsigned long)[jsonRequest length]];
        [request setValue:length forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:jsonRequest];
        
    }
    
    
    request.HTTPMethod = method;
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    return request;
    
}


-(NSMutableURLRequest *)createMutableURLRequest: (NSString*)requestMethod method: (NSString *)method array: (NSArray *)array{
    
    NSMutableURLRequest* request;
    
    if([method isEqualToString:@"GET"] || [method isEqualToString:@"DELETE"]){
        NSString *urlString = [NSString stringWithFormat:@"%@%@/", [self connectionURL], requestMethod];
        
        NSURL *url = [NSURL URLWithString:urlString];
        request = [NSMutableURLRequest requestWithURL:url];
        
        
    }else if([method isEqualToString:@"POST"] || [method isEqualToString:@"PUT"] ){
        
        NSData *jsonRequest;
        
        if (array) {
            jsonRequest = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil];
        }
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@", [self connectionURL], requestMethod];
        NSURL *url = [NSURL URLWithString:urlString];
        request = [NSMutableURLRequest requestWithURL:url];
        NSString *length = [NSString stringWithFormat:@"%ld", (unsigned long)[jsonRequest length]];
        [request setValue:length forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:jsonRequest];
        
    }
    request.HTTPMethod = method;
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    return request;
    
}


-(void) addNotificationToken: (NSString *)token key: (NSString *)key{
    
    NSMutableURLRequest *request = [self createMutableURLRequest:@"deviceToken" method:@"POST" dictionary:@{@"deviceToken": token, @"device": @"ios", @"userId": key}];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.URLCache = nil;
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
    
    NSURLSessionDataTask *t = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!error) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            
            if ([httpResponse statusCode] != 200) {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                error = [NSError errorWithDomain:dict[@"message"] ? dict[@"message"] : @"Error" code:[httpResponse statusCode] userInfo:dict];
                
            }
            
        }else{
            switch (error.code) {
                    
                case -1004: NSLog(@"Cannot connect to the server");
                    break;
                default: NSLog(@"%@", [error localizedDescription]);
                    break;
            }
            
            
            
        }
        
        
    }];
    
    [t resume];
    
    
    
}



-(void) removeNotificationToken: (NSString *)token key: (NSString *)key{
    
    
    NSMutableURLRequest *request = [self createMutableURLRequest:@"deviceToken" method:@"PUT" dictionary:@{@"deviceToken": token, @"device": @"ios", @"userId": key}];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.URLCache = nil;
    NSURLSession* session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
    
    NSURLSessionDataTask *t = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!error) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            
            if ([httpResponse statusCode] != 200) {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                error = [NSError errorWithDomain:dict[@"message"] ? dict[@"message"] : @"Error" code:[httpResponse statusCode] userInfo:dict];
                
            }
            
        }else{
            switch (error.code) {
                    
                case -1004: NSLog(@"Cannot connect to the server");
                    break;
                default: NSLog(@"%@", [error localizedDescription]);
                    break;
            }
            
            
            
        }
        
        
    }];
    
    [t resume];
    
    
    
}




@end
