//
//  OIHRestaurantMealInfoCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHRestaurantModel.h"
#import "OIHMealModel.h"

@interface OIHRestaurantMealInfoCollectionViewCell : UICollectionViewCell

@property (nonatomic) UIImageView *backgroundImageView;
@property (nonatomic) UILabel *dishLabel;
@property (nonatomic) UILabel *restaurantLabel;
@property (nonatomic) UILabel *priceLabel;

@property (nonatomic, weak) OIHRestaurantModel *restaurantModel;
@property (nonatomic, weak) OIHMealModel *mealModel;

@end
