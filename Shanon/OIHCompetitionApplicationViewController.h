//
//  OIHCompetitionApplicationViewController.h
//  Shanon
//
//  Created by Ralf Cheung on 14/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHCompetitionInfo.h"

@interface OIHCompetitionApplicationViewController : UIViewController
@property (nonatomic) OIHCompetitionInfo *competitionInfo;

-(id)initWithCompetition: (OIHCompetitionInfo *)competition;

@end
