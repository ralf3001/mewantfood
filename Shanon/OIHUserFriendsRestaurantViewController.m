//
//  OIHUserFriendsRestaurantViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserFriendsRestaurantViewController.h"
#import "OIHRestaurantListViewController.h"
#import "OIHUserProfileViewController.h"


@interface OIHUserFriendsRestaurantViewController () <UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchBarDelegate>
@property (nonatomic) NSMutableArray *friendsArray;
@property (nonatomic) UISearchController *searchController;
@property (nonatomic) UITableView *tableView;
@end

@implementation OIHUserFriendsRestaurantViewController
@synthesize friendsArray;
@synthesize tableView = _tableView;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    friendsArray = [NSMutableArray new];
    [self setupTableView];
    [self fetchFriends];
    
}


-(void)setupTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 90;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    [self.view addSubview:_tableView];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    [self.searchController.searchBar sizeToFit];
    
}



-(void)fetchFriends{
    
    [[JSONRequest sharedInstance] fetch:@"friend" method:@"GET" data:nil completionHandler:^(id data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error && data) {
                friendsArray = [NSMutableArray arrayWithArray:data];
                [self.tableView reloadData];
            }
        });
        
    }];
    
}



-(void) updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    //    NSString *text = searchController.searchBar.text;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    
    // Return the number of rows in the section.
    return friendsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary *user = friendsArray[indexPath.row];
    
    cell.textLabel.text = user[kOIHUserName];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OIHUserProfileViewController *vc = [[OIHUserProfileViewController alloc] initWithFriendProfile:friendsArray[indexPath.row]];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
