//
//  OIHUserOrderConfirmedViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 27/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserOrderConfirmedViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "EZAudio.h"
#import "OIHAudioRecordingActionSheet.h"
#import "OIHSocialManager.h"
#import "OIHModalPresentationController.h"
#import "OIHMapLocationDetailViewController.h"



@interface OIHUserOrderConfirmedViewController () <OIHAudioRecordingActionSheetDelegate, OIHSocialManagerDelegate, UIViewControllerTransitioningDelegate>

@property (nonatomic) UIButton *recordAudioButton;
@property (nonatomic) UIButton *playButton;
@property (nonatomic) UIButton *dismissButton;

@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UIButton *recordButton;

@property (nonatomic) NSURL *recordingURL;
@property (nonatomic) AVPlayer *player;

@property (nonatomic) UIView *containerView;

@property (nonatomic) OIHOrderModel *orderInfo;

@end

@implementation OIHUserOrderConfirmedViewController

-(id)initWithOrder: (OIHOrderModel *)order{
    
    if (self = [super init]) {
        self.orderInfo = order;
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    self.modalPresentationStyle = UIModalPresentationCustom;
    self.transitioningDelegate = self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.view.backgroundColor = [UIColor colorWithRed:218/255.0f green:40/255.0f blue:40/255.0f alpha:1.0f];
    
    _containerView = [UIView new];
    _containerView.translatesAutoresizingMaskIntoConstraints = NO;
    _containerView.backgroundColor = [UIColor colorWithRed:35/255.0f green:106/255.0f blue:98/255.0f alpha:1.0f];
    [self.view addSubview:_containerView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_containerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_containerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    
    if (IS_IPAD) {

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_containerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:CGRectGetWidth(self.view.frame) * 0.5]];

    }else{

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_containerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:CGRectGetWidth(self.view.frame) * 0.8]];
        
    }
    
    _dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_dismissButton setImage:[IonIcons imageWithIcon:ion_ios_close_empty size:50 color:[UIColor whiteColor]] forState:UIControlStateNormal];
    [_dismissButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [_containerView addSubview:_dismissButton];
    
    [_containerView addConstraint:[NSLayoutConstraint constraintWithItem:_dismissButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_dismissButton attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[_dismissButton(==40)]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_dismissButton)]];
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_dismissButton]" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_dismissButton)]];

    _imageView = [[UIImageView alloc] init];
    [_imageView sd_setImageWithURL:[NSURL URLWithString:_mealInfo.productImage] placeholderImage:[UIImage imageFromColor: [UIColor colorWithWhite:0.9 alpha:1.0f]]];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.layer.borderWidth = 1.5f;
    _imageView.layer.masksToBounds = YES;
    _imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [_containerView addSubview:_imageView];
    
    
    UILabel *congratzLabel = [UILabel new];
    congratzLabel.translatesAutoresizingMaskIntoConstraints = NO;
    congratzLabel.numberOfLines = 0;
    congratzLabel.textColor = [UIColor whiteColor];
    
    if (_friendProfile && ![_friendProfile._id isEqualToString:[UserProfile currentUser]._id]) {
        congratzLabel.text = [NSString stringWithFormat:NSLocalizedString(@"ORDER_CONFIRMED_DESCRIPTION_TEXT_%@_TO_FRIEND_%@_%@_%@", @""), _restaurantInfo.restaurantName, _friendProfile.userName, _friendProfile.userName, _friendProfile.userName];

    }else{
        congratzLabel.text = [NSString stringWithFormat:NSLocalizedString(@"ORDER_CONFIRMED_DESCRIPTION_TEXT_%@", @""), _restaurantInfo.restaurantName];

    }

    [_containerView addSubview:congratzLabel];
    
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.numberOfLines = 0;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = NSLocalizedString(@"ORDER_CONFIRMED_TITLE", @"");
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:28];
    [_containerView addSubview:titleLabel];
    
    _recordButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _recordButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_recordButton addTarget:self action:@selector(recordActionSheet) forControlEvents:UIControlEventTouchUpInside];
    _recordButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _recordButton.layer.borderWidth = 2.0f;
    [_recordButton setImage:[IonIcons imageWithIcon:ion_ios_mic_outline size:30.0f color:[UIColor whiteColor]] forState:UIControlStateNormal];
    [_containerView addSubview:_recordButton];
    
    
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[titleLabel]-[_dismissButton(==40)]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(titleLabel, _dismissButton)]];
    
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[_imageView(==70)]-[titleLabel]" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(_imageView, titleLabel)]];
    
    [_containerView addConstraint:[NSLayoutConstraint constraintWithItem:congratzLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:_containerView attribute:NSLayoutAttributeLeft multiplier:1.0f constant:15.0f]];

    [_containerView addConstraint:[NSLayoutConstraint constraintWithItem:congratzLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:_containerView attribute:NSLayoutAttributeRight multiplier:1.0f constant:-15.0f]];

    [_containerView addConstraint:[NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:congratzLabel attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0.0f]];
    
    [_containerView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_recordButton attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];

    [_containerView addConstraint:[NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_imageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    
    [_containerView addConstraint:[NSLayoutConstraint constraintWithItem:_recordButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_containerView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    
    [_containerView addConstraint:[NSLayoutConstraint constraintWithItem:congratzLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_containerView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];

    
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[_imageView]-15-[congratzLabel]-20-[_recordButton(==60)]-15-|" options:0 metrics:@{@"recordButtonHeight": @(80)} views:NSDictionaryOfVariableBindings(_imageView, congratzLabel, _recordButton, _dismissButton)]];
    
}


-(void)recordActionSheet{
    
    OIHAudioRecordingActionSheet *aS = [OIHAudioRecordingActionSheet new];
    aS.delegate = self;
    [self presentViewController:aS animated:YES completion:nil];
    
}

-(void)viewWillLayoutSubviews{
    
    _imageView.layer.cornerRadius = CGRectGetHeight(_imageView.frame) / 2;
    _recordButton.layer.cornerRadius = CGRectGetHeight(_recordButton.frame) / 2;
    
}



#pragma mark - <OIHAudioRecordingActionSheetDelegate>

-(void)audioActionSheetWillDismiss:(NSURL *)fileURL{
    
    _recordingURL = [fileURL copy];
    NSData *data = [NSData dataWithContentsOfURL:_recordingURL];
    __weak typeof(self) weakSelf = self;
    
    if (data) {
        
        [OIHSocialManager sharedInstance].delegate = self;
        [[OIHSocialManager sharedInstance] uploadOrderAudioFile:data completionHandler:^(id result, NSError *error) {
            
            if (!error) {
                
                [SVProgressHUD dismiss];
                
                JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:@"orderaudionote" httpMethod:HTTP_POST];
                settings.requestData = @{@"audioURL": result[@"url"], @"orderId": _orderInfo._id};
                
                [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
                    
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"DONE", @"Done")];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        NSError *error = nil;
                        [[NSFileManager defaultManager] removeItemAtPath: weakSelf.recordingURL.path error: &error];
                        
                        [self dismissViewControllerAnimated:YES completion:nil];
                    });
                    
                }];
                
            }
        }];
        
    }
    
    
}




-(void)socialManagerUploadProgress: (float)progress{
    
    //handled in main thread
    [SVProgressHUD showProgress:progress status:NSLocalizedString(@"LABEL_SENDING", @"Sending...")];
    
}


-(BOOL)prefersStatusBarHidden{
    return YES;
}


-(void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UIViewControllerTransitioningDelegate

- (nullable UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    if (presented == self) {
        return [[OIHModalPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    }
    return nil;
}


- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    if (presented == self) {
        MapLocationAnimatedTransitioning *transition = [MapLocationAnimatedTransitioning new];
        transition.presenting = YES;
        return transition;
    }else return nil;
    
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    if (dismissed == self) {
        MapLocationAnimatedTransitioning *transition = [MapLocationAnimatedTransitioning new];
        transition.presenting = NO;
        return transition;
    }else return nil;

    
}


@end
