//
//  OIHFeedSearchTableViewCell.m
//  Shanon
//
//  Created by Ralf Cheung on 1/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHFeedSearchTableViewCell.h"

@implementation OIHFeedSearchTableViewCell


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        _titleLabel = [UILabel new];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
        _titleLabel.textColor = [UIColor grayColor];

        [self.contentView addSubview:_titleLabel];
        
        _detailLabel = [UILabel new];
        _detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _detailLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        
        [self.contentView addSubview:_detailLabel];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_titleLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_titleLabel]-[_detailLabel]-|" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel, _detailLabel)]];
    
    }
    
    return self;
}


@end
