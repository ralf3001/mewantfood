//
//  OIHRestaurantManagementStatisticsViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 15/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHRestaurantModel.h"


@interface OIHRestaurantManagementStatisticsViewController : UIViewController

@property (nonatomic) OIHRestaurantModel *restaurantInfo;

@end
