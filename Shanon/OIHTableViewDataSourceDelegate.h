//
//  OIHTableViewDataSourceDelegate.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 2/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FeedDataDelegate <NSObject>

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface OIHTableViewDataSourceDelegate
    : NSObject <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) id<FeedDataDelegate> delegate;

- (id)initWithQuery:(NSDictionary *)data;
@end
