//
//  OIHRestaurantManagementStatisticsViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 15/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantManagementStatisticsViewController.h"
#import "JSONRequestSettings.h"
#import "OIHPriceWithAnimationLabel.h"
#import "StretchyHeaderCollectionViewLayout.h"
#import "OIHUserProfileHeaderCollectionReusableView.h"
#import "OIHMonthlySalesCollectionViewCell.h"
#import "OIHRestaurantSalesMapReduceResult.h"
#import "OIHRestaurantSalesGraphCollectionViewCell.h"
#import "OIHSalesGraphViewController.h"



@interface OIHRestaurantManagementStatisticsViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic) OIHPriceWithAnimationLabel *totalSalesAmountLabel;
@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UICollectionView *collectionView;


@property (nonatomic) NSMutableArray <OIHRestaurantSalesMapReduceResult *> *salesData;

@property (nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) double totalSales;

@end


//NSString * const kHeaderIdent = @"Header";

@implementation OIHRestaurantManagementStatisticsViewController

@synthesize scrollView;
@synthesize totalSalesAmountLabel;
@synthesize salesData;



- (void)viewDidLoad {
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Get Paid" style:UIBarButtonItemStylePlain target:self action:@selector(getPaid)];
    
    [self fetchStatistics];
    _totalSales = 0.0f;
    
}


-(void)setupCollectionView{
    
    
    if (!_collectionView) {
        
        StretchyHeaderCollectionViewLayout *stretchyLayout;
        stretchyLayout = [[StretchyHeaderCollectionViewLayout alloc] init];
        [stretchyLayout setSectionInset:UIEdgeInsetsMake(0, 10.0, 10.0, 10.0)];
        [stretchyLayout setHeaderReferenceSize:CGSizeMake(320.0, 160.0)];
        [stretchyLayout setFooterReferenceSize:CGSizeMake(320.0, 80.0)];
        
        stretchyLayout.minimumInteritemSpacing = 0;
        stretchyLayout.minimumLineSpacing = 0;
        
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:stretchyLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView setAlwaysBounceVertical:YES];
        [_collectionView setShowsVerticalScrollIndicator:NO];
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        
        _refreshControl = [[UIRefreshControl alloc] init];
        [_refreshControl addTarget:self action:@selector(fetchStatistics) forControlEvents:UIControlEventValueChanged];
        _collectionView.alwaysBounceVertical = YES;
        
        [_collectionView addSubview:_refreshControl];
        
        [_collectionView registerClass:[OIHMonthlySalesCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        [_collectionView registerClass:[OIHRestaurantSalesGraphCollectionViewCell class] forCellWithReuseIdentifier:@"graph"];
        
        
        
        [self.view addSubview:_collectionView];
        
        
        
        [_collectionView registerClass:[OIHUserProfileHeaderCollectionReusableView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:@"header"];
        
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
        
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];

    }else{
        [_collectionView reloadData];
    }
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *view;

    if (kind == UICollectionElementKindSectionHeader) {

        OIHUserProfileHeaderCollectionReusableView *header = (OIHUserProfileHeaderCollectionReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"header" forIndexPath:indexPath];
        [header.personUtilityButton removeFromSuperview];

        if (_restaurantInfo) {
         
            [header setNameLabelText:_restaurantInfo.restaurantName];

            if (_restaurantInfo.companyLogos.count > 0) {
                
                [header.imageView sd_setImageWithURL: [NSURL URLWithString:_restaurantInfo.companyLogos[0]] placeholderImage:[UIImage imageFromColor:[UIColor colorWithWhite:0.8 alpha:1.0f]]];
                
            }
        
        }
        view = header;

    }else{
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"footer" forIndexPath:indexPath];
        view = footer;
    }

    
    return view;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    CGSize size = CGSizeMake(collectionView.frame.size.width, 80);
    
    switch (indexPath.row) {
        case 0:{

            size.height = 150;
            break;
        }
        case 1:
            size.height = 230;
            break;
            
        default:
            break;
    }
    
    return size;

}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv {
    
    return 1;
}

- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section {
    return 2;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 1) {
        OIHRestaurantSalesMapReduceResult *sales = [salesData firstObject];
        
        if (sales) {

            OIHSalesGraphViewController *vc = [OIHSalesGraphViewController new];
            vc.data = sales.salesInRange;
            [self.navigationController pushViewController:vc animated:YES];
        
        }
        
    }
    
}


- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    switch(indexPath.row){
            
        case 0:{
            OIHMonthlySalesCollectionViewCell *salesCell = (OIHMonthlySalesCollectionViewCell *)cell;
            
            NSString *currencyCode = _restaurantInfo.restaurantCurrency ? _restaurantInfo.restaurantCurrency : @"HKD";
            
            NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: currencyCode}];
            
            NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
            [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
            [currency setLocale:[NSLocale localeWithLocaleIdentifier:localeIde]];
            
            salesCell.salesAmountLabel.currencySymbol = [currency.locale objectForKey:NSLocaleCurrencySymbol];
            
            
            _totalSales = 0;
            
            for (OIHRestaurantSalesMapReduceResult *sales in salesData) {
                _totalSales += sales.value;
            }
            

            salesCell.salesAmountLabel.animateLoop = ^NSString*(float value){
                return [currency stringFromNumber: [NSNumber numberWithDouble: value]];
            };
            
            [salesCell.salesAmountLabel startCounter:0 end:_totalSales];
   
            break;
        }
            
        default:
            break;
            
            
    }
    

}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    UICollectionViewCell *cell;
    
    if (_restaurantInfo) {
        cell = [self cellConfigForIndividualRestaurant:collectionView atIndexPath:indexPath];
    }else{
        cell = [self cellConfigForMultipleRestaunrats:collectionView atIndexPath:indexPath];
    }
    
    return cell;
}


-(UICollectionViewCell *)cellConfigForMultipleRestaunrats: (UICollectionView *)collectionView atIndexPath: (NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{

            OIHMonthlySalesCollectionViewCell *cell = (OIHMonthlySalesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateStyle:NSDateFormatterShortStyle];
            [formatter setTimeStyle:NSDateFormatterNoStyle];
            [formatter setDateFormat:@"MMM YYYY"];
            
            cell.monthLabel.text = [NSString stringWithFormat:@"Sales of %@ to Now", [formatter stringFromDate:[NSDate date]]];
            return cell;
        }
        case 1:{
            OIHMonthlySalesCollectionViewCell *cell = (OIHMonthlySalesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
            return cell;
            //bar chart for the x highest sales of restaurants;
        }
        default:
            break;
    }

    return nil;
    
}


-(UICollectionViewCell *)cellConfigForIndividualRestaurant: (UICollectionView *)collectionView atIndexPath: (NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{
            
            OIHMonthlySalesCollectionViewCell *cell = (OIHMonthlySalesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];

            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateStyle:NSDateFormatterShortStyle];
            [formatter setTimeStyle:NSDateFormatterNoStyle];
            [formatter setDateFormat:@"MMM YYYY"];
            
            cell.monthLabel.text = [NSString stringWithFormat:@"Sales of %@ to Now", [formatter stringFromDate:[NSDate date]]];
            
            return cell;
        }
        case 1:{
            
            OIHRestaurantSalesGraphCollectionViewCell *cell = (OIHRestaurantSalesGraphCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"graph" forIndexPath:indexPath];
            
            if ([salesData firstObject]) {
                OIHRestaurantSalesMapReduceResult *sales = [salesData firstObject];
                
                if (sales) {
                    cell.periodDataSource = sales.salesInRange;
                    
                }
            }
            
            return cell;
            //graph for the last 30 days sales
        }
        default:
            break;
    }

    
    return nil;
}



-(void)fetchStatistics{
    

    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"statistics" httpMethod:HTTP_GET];
    setting.cachePolicy = IgnoreCache;
    
    if (_restaurantInfo) {
        setting.requestData = @{@"restaurantId": _restaurantInfo._id};
    }
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        [_refreshControl endRefreshing];
        
        if (!error) {
            salesData = [NSMutableArray arrayWithArray:[OIHRestaurantSalesMapReduceResult salesResultsFromArray:data]];
            [self setupCollectionView];
            
        }
        
    }];
    

}


-(void)getPaid{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"monthlysales" httpMethod:HTTP_GET];
    setting.cachePolicy = IgnoreCache;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }else{
            NSLog(@"done");
        }
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
