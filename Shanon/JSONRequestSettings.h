//
//  JSONRequestSettings.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_OPTIONS(NSInteger, RequestCachePolicy) {
    
    CacheFirstThenLoadData = 1 << 0,
    IgnoreCache = 1 << 1,
    CacheOnly = 1 << 2,
    UseCacheIfTimeout = 1 << 3
};

typedef NS_OPTIONS(NSInteger, RequestHTTPMethod) {
    
    HTTP_GET = 1 << 0,
    HTTP_POST = 1 << 1,
    HTTP_DELETE = 1 << 2,
    HTTP_PUT = 1 << 3
};


@interface JSONRequestSettings : NSObject

//@property (nonatomic) NSString *HTTPMethod;
@property (nonatomic) NSString *route;
@property (assign) BOOL cacheQuery;
@property (assign) BOOL isDataTimely;
@property (nonatomic) RequestCachePolicy cachePolicy;
@property (nonatomic) RequestHTTPMethod HTTPMethod;
@property (nonatomic) id requestData;

-(NSString *)httpMethod;
-(id)initWithRoute: (NSString *)route httpMethod: (RequestHTTPMethod) method;


@end
