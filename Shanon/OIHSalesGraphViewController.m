//
//  OIHSalesGraphViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 31/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSalesGraphViewController.h"
//#import <CorePlot/ios/CorePlot.h>

@interface OIHSalesGraphViewController ()
//@property (nonatomic, strong) CPTGraphHostingView *hostView;

@end

@implementation OIHSalesGraphViewController
@synthesize data;

//
//-(id)init{
//    
//    self =[super init];
//    if (self) {
//        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
//        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
//
//    }
//    return self;
//}


- (void)viewDidLoad {
    [super viewDidLoad];

    [self initPlot];

    UIButton *popNavigationControllerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    popNavigationControllerButton.translatesAutoresizingMaskIntoConstraints = NO;
    [popNavigationControllerButton setImage:[IonIcons imageWithIcon:ion_ios_close_empty size:50 color:[UIColor blackColor]] forState:UIControlStateNormal];
    [popNavigationControllerButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:popNavigationControllerButton];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:popNavigationControllerButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:popNavigationControllerButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0f constant:5.0f]];

    

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    

    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
    
}


-(void)initPlot {
    /*
    [self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
    */
}


/*
-(void)configureHost {
    
    CGRect parentRect = self.view.bounds;
    parentRect = CGRectMake(parentRect.origin.x,
                            (parentRect.origin.y + 10),
                            parentRect.size.width,
                            (parentRect.size.height - 50));
    
    self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:parentRect];
    self.hostView.backgroundColor = [UIColor redColor];
    self.hostView.allowPinchScaling = YES;
    [self.view addSubview:self.hostView];
}


-(void)configureGraph {
    
    // 1 - Create the graph
    
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
    graph.backgroundColor = [UIColor greenColor].CGColor;
    [graph applyTheme:[CPTTheme themeNamed:kCPTPlainWhiteTheme]];
    self.hostView.hostedGraph = graph;
    
    // 2 - Set graph title
    NSString *title = @"Portfolio Prices: April 2012";
    graph.title = title;
    
    // 3 - Create and set text style
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color = [CPTColor grayColor];
    titleStyle.fontName = @"Helvetica-Bold";
    titleStyle.fontSize = 16.0f;
    graph.titleTextStyle = titleStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, -10.0f);
    
    // 4 - Set padding for plot area
    [graph.plotAreaFrame setPaddingLeft:10.0f];
    [graph.plotAreaFrame setPaddingBottom:10.0f];
    
    
    // 5 - Enable user interactions for plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = YES;
    
    
}

-(void)configurePlots {
    // 1 - Get graph and plot
    
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    // 2 - Create the three plots
    
    CPTScatterPlot *aaplPlot = [[CPTScatterPlot alloc] init];
    aaplPlot.dataSource = self;
    //    aaplPlot.identifier = CPDTickerSymbolAAPL;
    CPTColor *aaplColor = [CPTColor redColor];
    [graph addPlot:aaplPlot toPlotSpace:plotSpace];
    
    
    // 3 - Set up plot space
    [plotSpace scaleToFitPlots:[NSArray arrayWithObjects:aaplPlot, nil]];
    CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
    [xRange expandRangeByFactor:[NSNumber numberWithFloat:1.1f]];
    plotSpace.xRange = xRange;
    
    CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
    [yRange expandRangeByFactor:[NSNumber numberWithFloat:1.3f]];
    plotSpace.yRange = yRange;
    
    
    // 4 - Create styles and symbols
    CPTMutableLineStyle *aaplLineStyle = [aaplPlot.dataLineStyle mutableCopy];
    aaplLineStyle.lineWidth = 2.5;
    aaplLineStyle.lineColor = aaplColor;
    aaplPlot.dataLineStyle = aaplLineStyle;
    
    CPTMutableLineStyle *aaplSymbolLineStyle = [CPTMutableLineStyle lineStyle];
    aaplSymbolLineStyle.lineColor = aaplColor;
    
    CPTPlotSymbol *aaplSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    aaplSymbol.symbolType = CPTPlotSymbolTypeNone;
    aaplSymbol.fill = [CPTFill fillWithColor:aaplColor];
    aaplSymbol.lineStyle = aaplSymbolLineStyle;
    aaplSymbol.size = CGSizeMake(6.0f, 6.0f);
    aaplPlot.plotSymbol = aaplSymbol;
    
}


-(void)configureAxes {
    
    // 1 - Create styles
    CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
    axisTitleStyle.color = [CPTColor whiteColor];
    axisTitleStyle.fontName = @"Helvetica-Bold";
    axisTitleStyle.fontSize = 12.0f;
    
    CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
    axisLineStyle.lineWidth = 2.0f;
    axisLineStyle.lineColor = [CPTColor whiteColor];
    
    CPTMutableTextStyle *axisTextStyle = [[CPTMutableTextStyle alloc] init];
    axisTextStyle.color = [CPTColor whiteColor];
    axisTextStyle.fontName = @"Helvetica-Bold";
    axisTextStyle.fontSize = 11.0f;
    
    CPTMutableLineStyle *tickLineStyle = [CPTMutableLineStyle lineStyle];
    //    tickLineStyle.lineColor = [CPTColor whiteColor];
    //    tickLineStyle.lineWidth = 2.0f;
    
    CPTMutableLineStyle *gridLineStyle = [CPTMutableLineStyle lineStyle];
    tickLineStyle.lineColor = [CPTColor blackColor];
    tickLineStyle.lineWidth = 1.0f;
    
    // 2 - Get axis set
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    
    // 3 - Configure x-axis
    CPTAxis *x = axisSet.xAxis;
    x.title = @"Day of Month";
    x.titleTextStyle = axisTitleStyle;
    x.titleOffset = 15.0f;
    x.axisLineStyle = axisLineStyle;
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    x.labelTextStyle = axisTextStyle;
    x.majorTickLineStyle = axisLineStyle;
    x.majorTickLength = 4.0f;
    x.tickDirection = CPTSignNegative;
    
    //    CGFloat dateCount = [[[CPDStockPriceStore sharedInstance] datesInMonth] count];
    //    CGFloat dateCount = 30;
    //
    //    NSMutableSet *xLabels = [NSMutableSet setWithCapacity:dateCount];
    //    NSMutableSet *xLocations = [NSMutableSet setWithCapacity:dateCount];
    //
    //    for (int i = 0; i < 31; i++) {
    //
    ////    for (NSString *date in [[CPDStockPriceStore sharedInstance] datesInMonth]) {
    //
    //        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%d", i + 1] textStyle:x.labelTextStyle];
    //        CGFloat location = i++;
    ////        label.tickLocation = CPTDecimalFromCGFloat(i);
    //        label.offset = x.majorTickLength;
    //        if (label) {
    //            [xLabels addObject:label];
    //            [xLocations addObject:[NSNumber numberWithFloat:location]];
    //        }
    //    }
    
    //    x.axisLabels = xLabels;
    //    x.majorTickLocations = xLocations;
    
    // 4 - Configure y-axis
    CPTAxis *y = axisSet.yAxis;
    y.title = @"Price";
    y.titleTextStyle = axisTitleStyle;
    y.titleOffset = -40.0f;
    y.axisLineStyle = axisLineStyle;
    y.majorGridLineStyle = gridLineStyle;
    y.labelingPolicy = CPTAxisLabelingPolicyNone;
    y.labelTextStyle = axisTextStyle;
    y.labelOffset = 16.0f;
    y.majorTickLineStyle = axisLineStyle;
    y.majorTickLength = 4.0f;
    y.minorTickLength = 2.0f;
    y.tickDirection = CPTSignPositive;
    
    //    NSInteger majorIncrement = 100;
    //    NSInteger minorIncrement = 50;
    //    CGFloat yMax = 100;  // should determine dynamically based on max price
    //    NSMutableSet *yLabels = [NSMutableSet set];
    //    NSMutableSet *yMajorLocations = [NSMutableSet set];
    //    NSMutableSet *yMinorLocations = [NSMutableSet set];
    //
    //    for (NSInteger j = minorIncrement; j <= yMax; j += minorIncrement) {
    //        NSUInteger mod = j % majorIncrement;
    //        if (mod == 0) {
    //            CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%li", (long)j] textStyle:y.labelTextStyle];
    //            NSDecimal location = CPTDecimalFromInteger(j);
    ////            label.tickLocation = location;
    //            label.offset = -y.majorTickLength - y.labelOffset;
    //            if (label) {
    //                [yLabels addObject:label];
    //            }
    //            [yMajorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:location]];
    //        } else {
    //            [yMinorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:CPTDecimalFromInteger(j)]];
    //        }
    //    }
    //    y.axisLabels = yLabels;
    //    y.majorTickLocations = yMajorLocations;
    //    y.minorTickLocations = yMinorLocations;
    
    
}


-(NSUInteger)numberOfRecordsForPlot:(nonnull CPTPlot *)plot{
    return 30;
}


-(id)numberForPlot:(nonnull CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)idx{
    
    switch (fieldEnum) {
        case CPTScatterPlotFieldX:
            return [NSNumber numberWithUnsignedInteger:idx];
            
        case CPTScatterPlotFieldY:{
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF['_id']['day'] == %ld", idx];
            NSArray *res = [data filteredArrayUsingPredicate:predicate];
            
            NSDictionary *dict = [res firstObject];
            
            if (dict[@"value"]) {
                return [NSNumber numberWithInteger:[dict[@"value"] integerValue]];
            }
            
            return [NSNumber numberWithInteger:0];
        }
        default:
            break;
    }
    
    return [NSNumber numberWithInteger:0];
    
}

-(void)dismiss{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}



-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    switch (orientation) {
        case 1:
        case 2:
            //NSLog(@"portrait");
            break;
            
        case 3:
        case 4:
            //NSLog(@"landscape");
            break;
        default:
            //NSLog(@"other");
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
