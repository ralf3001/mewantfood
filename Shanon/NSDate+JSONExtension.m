//
//  NSDate+JSONExtension.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 23/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "NSDate+JSONExtension.h"

@implementation NSDate (JSONExtension)

- (NSString *)JSONDateFromDate {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    
    return [dateFormatter stringFromDate:self];
}

+ (NSDate *)dateFromJSONDate:(NSString *)date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSz"];
    return [formatter dateFromString:date];
}

- (BOOL)onSameDay:(NSDate *)date {
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *compareDate = [gregorian
                                     components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)
                                     fromDate:date];
    NSDateComponents *selfComponent = [gregorian
                                       components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)
                                       fromDate:self];
    
    return (selfComponent.day == compareDate.day &&
            selfComponent.month == compareDate.month &&
            selfComponent.year == compareDate.year);
}

// http://stackoverflow.com/questions/1072848/how-to-check-if-an-nsdate-occurs-between-two-other-nsdates
//+ (BOOL)date:(NSDate *)dateisBetweenDate:(NSDate *)beginDate andDate:(NSDate *)endDate {
//    
//      if ([date compare:beginDate] == NSOrderedAscending)
//        return NO;
//    
//      if ([date compare:endDate] == NSOrderedDescending)
//        return NO;
//    
//    return NO;
//}

+(BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate{
    
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}


+ (BOOL)date:(NSDate *)date isGTE:(NSDate *)beginDate LTE:(NSDate *)endDate {
    
    return [self date:date isBetweenDate:beginDate andDate:endDate];
}

+ (NSString *)daysBetweenDate:(NSDate *)fromDateTime
                      andDate:(NSDate *)toDateTime {
    
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned int flags = NSCalendarUnitDay | NSCalendarUnitMonth |
    NSCalendarUnitYear | NSCalendarUnitMinute |
    NSCalendarUnitHour | NSCalendarUnitWeekOfYear;
    
    [calendar rangeOfUnit:flags
                startDate:&fromDate
                 interval:NULL
                  forDate:fromDateTime];
    [calendar rangeOfUnit:flags
                startDate:&toDate
                 interval:NULL
                  forDate:toDateTime];
    
    NSDateComponents *difference =
    [calendar components:flags fromDate:fromDate toDate:toDate options:0];
    
    if (difference.year > 0 || difference.month > 0) {
        NSDateFormatter *formatter = [NSDateFormatter new];
        formatter.dateStyle = kCFDateFormatterShortStyle;
        return [formatter stringFromDate:fromDateTime];
    } else if (difference.weekOfYear > 0) {
        
        if ((long)difference.weekOfYear == 1) {
            return NSLocalizedString(@"TIME_LAST_WEEK", @"Last week");
        } else {
            return [NSString stringWithFormat:NSLocalizedString(@"TIME_%ld_WEEKS_AGO",
                                                                @"[week] weeks ago"),
                    (long)difference.weekOfYear];
        }
    } else if (difference.day > 0) {
        if (difference.day > 1) {
            return [NSString stringWithFormat:NSLocalizedString(@"TIME_%ld_DAYS_AGO",
                                                                @"[day] days ago"),
                    (long)difference.day];
        } else {
            return NSLocalizedString(@"TIME_YESTERDAY", @"Yesterday");
        }
    } else if (difference.hour > 0){
        if (difference.hour == 1) {
            return NSLocalizedString(@"TIME_ONE_HOUR_AGO",
                                     @"[1] hour ago"); // x min ago
        } else
            return [NSString stringWithFormat:NSLocalizedString(@"TIME_%ld_HOURS_AGO",
                                                                @"[hour] hours ago"),
                    (long)difference.hour];
    
    }else if (difference.minute > 0){
        
        if (difference.minute == 1) {
            return NSLocalizedString(@"TIME_ONE_MIN_AGO",
                                     @"[1] min ago"); // x min ago

        }else{
            return [NSString stringWithFormat:NSLocalizedString(@"TIME_%ld_MINS_AGO",
                                                                @"[hour] mins ago"),
                    (long)difference.minute];

        }
    }else{
        return NSLocalizedString(@"TIME_JUST_NOW", @"Just now");

    }
    
    return nil;
}

@end
