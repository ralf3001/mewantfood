//
//  OIHMealInfoTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 27/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHMealInfoTableViewCell.h"

@interface OIHMealInfoTableViewCell () <UITextViewDelegate>

@end

@implementation OIHMealInfoTableViewCell
@synthesize descriptionContentLabel, descriptionTitleLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
   
    [super setHighlighted:highlighted animated:animated];

    if (_tableView) {
        [_tableView endEditing:highlighted];
    }
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        descriptionTitleLabel = [UILabel new];
        descriptionTitleLabel.textColor = [UIColor grayColor];
        descriptionTitleLabel.numberOfLines = 0;
        descriptionTitleLabel.font = [UIFont tableViewCellDescriptionFont];
        descriptionTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:descriptionTitleLabel];
    
    
        descriptionContentLabel = [UITextView new];
        descriptionContentLabel.editable = NO;
        descriptionContentLabel.font = [UIFont tableViewCellContentFont];
        descriptionContentLabel.scrollEnabled = NO;
        descriptionContentLabel.translatesAutoresizingMaskIntoConstraints = NO;
        descriptionContentLabel.delegate = self;
        UIEdgeInsets insets = descriptionContentLabel.contentInset;
        insets.left = -4;
        descriptionContentLabel.contentInset = insets;
        [self.contentView addSubview:descriptionContentLabel];
      
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descriptionTitleLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionTitleLabel)]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descriptionContentLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionContentLabel)]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[descriptionTitleLabel]-[descriptionContentLabel(>=30)]-|" options:NSLayoutFormatAlignAllLeading metrics:0 views:NSDictionaryOfVariableBindings(descriptionTitleLabel, descriptionContentLabel)]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:0 multiplier:1.0f constant:60.0f]];
        
        
    }

    return self;
}

-(void)createNavButton{
    
    if (!_navButton) {
        _navButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _navButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self activateNavButton:NO];
        
        [self.contentView insertSubview:_navButton aboveSubview:self.descriptionContentLabel];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_navButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0f constant:-10.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_navButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.descriptionContentLabel attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
        
    }
}

-(void)activateNavButton: (BOOL)isActive{
    if (isActive) {
        [_navButton setImage:[IonIcons imageWithIcon:ion_ios_navigate_outline size:30.0f color:[UIColor blueColor]] forState:UIControlStateNormal];
    }else{
        [_navButton setImage:[IonIcons imageWithIcon:ion_ios_navigate_outline size:30.0f color:[UIColor lightGrayColor]] forState:UIControlStateNormal];

    }
}


-(void)setWarningForContentLabel: (NSString *) warningMessage{
    
    descriptionContentLabel.textColor = [UIColor redColor];
    NSString *originalText = descriptionContentLabel.text;
    descriptionContentLabel.text = warningMessage;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        descriptionContentLabel.textColor = [UIColor blackColor];
        descriptionContentLabel.text = originalText;
    });
    
    
}

-(void)setPlaceHolderText:(NSString *)placeHolderText{
    
    _placeHolderText = placeHolderText;
    
    descriptionContentLabel.text = _placeHolderText;
    descriptionContentLabel.textColor = [UIColor lightGrayColor];
}

-(BOOL)textViewShouldBeginEditing: (UITextView *)textView{
    
    if ([textView.text isEqualToString:_placeHolderText]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}


-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if (![textView.text isEqualToString:@""] && [self.delegate respondsToSelector:@selector(textViewFinishedEditing:cell:)]) {
        [self.delegate textViewFinishedEditing:textView cell:self];
    }
    
    if ([textView.text isEqualToString:@""]) {

        textView.textColor = [UIColor lightGrayColor];
        textView.text = _placeHolderText;

    }
}

-(void)textViewDidChange:(UITextView *)textView{
    
    if (![textView.text isEqualToString:@""] && _tableView) {
        CGSize size = textView.bounds.size;
        CGSize newSize = [textView sizeThatFits:CGSizeMake(size.width, CGFLOAT_MAX)];
        
        if (size.height <= newSize.height) {
            
            [_tableView beginUpdates];
            [_tableView endUpdates];
            
            [_tableView scrollToRowAtIndexPath:[_tableView indexPathForCell:self]  atScrollPosition: UITableViewScrollPositionNone animated:NO];
            
        }
        
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end
