//
//  OIHPopupContentView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHPopupContentView.h"

@implementation OIHPopupContentView

-(id)initWithTitleText: (NSString *)title descriptionText: (NSString *)descriptionText withImage: (UIImage *)image{
    
    self = [super init];
    if (self) {
        [self commonInit:title descriptionText:descriptionText withImage: image];
    }
    
    return self;
}

-(void)setTitle:(NSString *)title{
    _title = title;
    _titleLabel.text = title;
}

-(void)setDescriptionText:(NSString *)descriptionText{
    
    _descriptionText = descriptionText;
    _descriptionLabel.text = descriptionText;
    
}

-(void)commonInit: (NSString *)title descriptionText: (NSString *)descriptionText withImage: (UIImage *)image{
    
    
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 10.0f;

    
    _titleLabel = [UILabel new];
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _titleLabel.text = title;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_titleLabel];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_titleLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel)]];
    
    _descriptionLabel = [UILabel new];
    _descriptionLabel.numberOfLines = 0;
    _descriptionLabel.text = descriptionText;
    _descriptionLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
    _descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _descriptionLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_descriptionLabel];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_descriptionLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_descriptionLabel)]];
    
    
    
    UIView *buttonTopDivider = [UIView new];
    buttonTopDivider.translatesAutoresizingMaskIntoConstraints = NO;
    buttonTopDivider.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
    [self addSubview:buttonTopDivider];
    
    UIView *sideDivider = [UIView new];
    sideDivider.translatesAutoresizingMaskIntoConstraints = NO;
    sideDivider.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
    [self addSubview:sideDivider];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[buttonTopDivider]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(buttonTopDivider)]];
    
    
    
    _doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_doneButton setTitle:NSLocalizedString(@"DONE", @"Cancel") forState:UIControlStateNormal];
    _doneButton.layer.masksToBounds = YES;
    [self addSubview:_doneButton];
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_cancelButton setTitle:NSLocalizedString(@"CANCEL", @"Cancel") forState:UIControlStateNormal];
    _cancelButton.layer.masksToBounds = YES;
    
    [self addSubview:_cancelButton];
    
    
    _imageViewBackground = [UIView new];
    _imageViewBackground.translatesAutoresizingMaskIntoConstraints = NO;
    _imageViewBackground.layer.borderWidth = 3.0f;
    _imageViewBackground.layer.cornerRadius = 35.0f;
    _imageViewBackground.layer.borderColor = [UIColor whiteColor].CGColor;
    _imageViewBackground.layer.masksToBounds = YES;
    [self addSubview:_imageViewBackground];
    
    
    UIImageView *imageView = [UIImageView new];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    imageView.image = image;
    [_imageViewBackground addSubview:imageView];
    
    [_imageViewBackground addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[imageView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
    [_imageViewBackground addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[imageView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
    
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_imageViewBackground attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_imageViewBackground attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f]];
    
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_imageViewBackground attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:70.0f]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_imageViewBackground attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_imageViewBackground attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
    
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_cancelButton][sideDivider(==1)][_doneButton(==_cancelButton)]|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(_cancelButton, _doneButton, sideDivider)]];
    
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-45-[_titleLabel]-15-[_descriptionLabel]-10-[buttonTopDivider(==1)][_doneButton(==40)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel, _doneButton, buttonTopDivider, _descriptionLabel)]];
    

    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
