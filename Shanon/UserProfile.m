
//
//  UserProfile.m
//  Ferrle
//
//  Created by Ralf Cheung on 12/29/14.
//  Copyright (c) 2014 Ralf Cheung. All rights reserved.
//

#import "UserProfile.h"
#import "NSData+AES.h"
#import "AppDelegate.h"
#import "AWSS3/AWSS3.h"
#import "AFMInfoBanner.h"
#import "OIHKeyChain.h"

@implementation UserProfile
@synthesize isMerchant;

-(id) initWithUserProfile: (NSDictionary *)userDict{
    
    self = [super init];
    if (self) {
        self.userDictionary = userDict;

    }
    return self;
}

-(id) init{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProfile) name:OIHUserDidUpdateProfileNotification object:nil];
    }
    return self;
}

+(UserProfile *)currentUser{
    
    static UserProfile *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[UserProfile alloc] init];
    });
    
    return _sharedInstance;
}



-(void)logout{
    
    
    [[JSONRequest sharedInstance] fetch:@"api/logout" method:@"GET" data:nil completionHandler:^(id data, NSError *error) {
        if (error) {
            
        }else{
            [OIHKeyChain deleteProfile];
            self.userDictionary = nil;
            [[OIHOnDiskCache sharedCache] clearAllCache];
        }
        
    }];

    
}

-(void)login:(NSDictionary *)loginInfo{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"api/login" httpMethod:HTTP_POST];
    setting.requestData = loginInfo;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            
            if (data) {
                
                [self setUserData:data];
                NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"PushNotificationDeviceToken"];
                if (deviceToken) {
                    [[JSONRequest sharedInstance] addNotificationToken:deviceToken key:self._id];
                }
                
                [self.loginDelegate userDidLogin:self];
                
            }else{
                [self.loginDelegate userLoginFailed: data[@"message"]];
            }
            
        }else{
            if (data) {
                [self.loginDelegate userLoginFailed: data[@"message"]];
            }else{
                [self.loginDelegate userLoginFailed:@"SERVER_ERROR"];
            }
        }

    }];
    

}


-(void)signup:(NSDictionary *)signupInfo{
    
    [[JSONRequest sharedInstance] fetch:@"api/signup" method:@"POST" data:signupInfo completionHandler:^(id data, NSError *error) {
      dispatch_async(dispatch_get_main_queue(), ^{
        if (!error) {
          
          if (data) {
            [self setUserData:data];
            
            NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"PushNotificationDeviceToken"];
            if (deviceToken) {
              [[JSONRequest sharedInstance] addNotificationToken:deviceToken key:self._id];
            }
            
            [self.signupDelegate userDidSignUp:self];
            
          }else{
            [self.signupDelegate userSignupFailed: data[@"message"]];
          }
          
          
        }else{
          if (data) {
            [self.signupDelegate userSignupFailed: data[@"message"]];
          }else{
            [self.signupDelegate userSignupFailed:@"SERVER_ERROR"];
            
          }
        }

      });

    }];
    
    
}


-(void)encryptLoginInfo: (NSDictionary *)info{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        [OIHKeyChain saveItem:info];
        
    });
    
}


-(NSDictionary *)decryptLoginInfo{

    return [OIHKeyChain profile];
    
}

-(void)updateProfile{
    
    [self updateProfile: nil];
}


-(void)updateProfile :(void (^)(NSError *error))completionHandler{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"api/profile" httpMethod:HTTP_GET];
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            if (data) {
                [self setUserData:data];
                if (completionHandler) {
                    completionHandler(error);
                }else{
                    NSLog(@"[ERROR]: %@", [error localizedDescription]);
                }
            }
        }else{
            if (completionHandler) {
                completionHandler(error);
            }else{
                NSLog(@"[ERROR]: %@", [error localizedDescription]);
            }
        }
    }];

}


-(void)signInWithToken{
    
    [[JSONRequest sharedInstance] loginWithToken:^(id data, NSError *error) {
        
        if (!error) {
            if (data) {
                
                [self setUserData:data];
                
                NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"PushNotificationDeviceToken"];
                if (deviceToken) {
                    [[JSONRequest sharedInstance] addNotificationToken:deviceToken key:self._id];
                }

            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [AFMInfoBanner showWithText:@"Server offline" style:AFMInfoBannerStyleError andHideAfter:2.0f];
                });
            }
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [AFMInfoBanner showWithText:@"Server offline" style:AFMInfoBannerStyleError andHideAfter:2.0f];
                
            });

        }
    }];
}


-(void)setUserData: (NSDictionary *)data{
    
    
    [self encryptLoginInfo:data];
    [self setUserDictionary:data];
    
//    if (data[@"isMerchant"]) {
//        isMerchant = [data[@"isMerchant"] boolValue];
//    }
//    
    [self setAttributes:data];
    [self fetchUnAckOrders];
    [self fetchFriendRequestsCount];
    
}

-(void)fetchUnAckOrders{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"unackordersCount" httpMethod:HTTP_GET];
    setting.requestData = @{@"userId": [UserProfile currentUser]._id};
    setting.cachePolicy = CacheFirstThenLoadData;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            
            if ([data[@"count"] integerValue] > 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:OIHTabBarWillCreateNotificationIcon object:@{@"tab": @4}];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:OIHTabBarWillDismissNotificationIcon object:@{@"tab": @4}];
                
            }
            
        }
    }];
    
}

-(void)fetchFriendRequestsCount{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"friendRequests" httpMethod:HTTP_GET];
    setting.requestData = @{@"count": @1};
    setting.cachePolicy = IgnoreCache;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (error) {
            
            if ([data[@"count"] integerValue] > 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:OIHTabBarWillCreateNotificationIcon object:@{@"tab": @1}];

                [[NSNotificationCenter defaultCenter] postNotificationName:OIHUserDidReceiveFriendRequestsNotification object:nil];

            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:OIHTabBarWillDismissNotificationIcon object:@{@"tab": @1}];

            }
            
        }
    }];
    
}


-(void)setAttributes: (NSDictionary *)data{
    
    if (data) {
        self.email = data[kOIHUserEmail];
        self.name = data[kOIHUserName];
        self.lastName = data[kOIHUserLastName];
        self._id = data[kOIHUserId];
        self.profilePics = data[@"profilePic"];
        
        self.addresses = [NSMutableArray new];
        for (NSDictionary *add in data[kOIHUserAddress]) {
            OIHAddressModel *address = [[OIHAddressModel alloc] initWithAddressInfo:add];
            [self.addresses addObject: address];
        }
        self.phoneNumber = data[kOIHUserPhoneNumber];
    }

}


-(NSDictionary *)getProfile{

    NSDictionary *initialProfile = [self decryptLoginInfo];

    [self signInWithToken];
    
    if ([self userDictionary]) {
        return [self userDictionary];
        
    }else if(initialProfile){
        isMerchant = NO;
        [self setAttributes:initialProfile];
        return initialProfile;
    
    }else{
        
        return nil;
    }
    
}



@end
