//
//  OIHLoginViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHLoginViewController.h"
#import "OIHUserHomeScreenViewController.h"
#import "OIHPopUpTextAlertController.h"
#import "UITextField+HighlightFrame.h"

@interface OIHLoginViewController () <OIHLoginProtocol,  UITextFieldDelegate, PopUpTextDelegate>
@property (nonatomic) UIButton *loginButton;
@property (nonatomic) UITextField *usernameField;
@property (nonatomic) UITextField *passwordField;
@property (nonatomic) UIButton *signupButton;
@property CGFloat KEYBOARD_HEIGHT;
@property (nonatomic) UILabel *warningMessageLabel;
@property (nonatomic) UIScrollView *scrollView;

@property (nonnull) UIButton *forgotPasswordButton;

@end

@implementation OIHLoginViewController
@synthesize loginButton, signupButton;
@synthesize usernameField, passwordField;
@synthesize KEYBOARD_HEIGHT;
@synthesize warningMessageLabel;
@synthesize signupViewController;
@synthesize scrollView;


-(id)init{
    
    self = [super init];
    if (self) {
        signupViewController = [OIHSignUpViewController new];
    }
    return self;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        signupViewController = [OIHSignUpViewController new];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        signupViewController = [OIHSignUpViewController new];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}






- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UserProfile currentUser].loginDelegate = self;
    
    scrollView = [UIScrollView new];
    scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:scrollView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scrollView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(scrollView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(scrollView)]];
    
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:40];
    [scrollView addSubview:titleLabel];
    
    
    usernameField = [UITextField new];
    usernameField.delegate = self;
    usernameField.placeholder = NSLocalizedString(@"LOGIN_EMAIL_PLACEHOLDER", @"");
    usernameField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    usernameField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    usernameField.translatesAutoresizingMaskIntoConstraints = NO;
    usernameField.layer.borderWidth = 0.6f;
    usernameField.returnKeyType = UIReturnKeyNext;
    usernameField.keyboardType = UIKeyboardTypeEmailAddress;
    [scrollView addSubview:usernameField];
    
    
    passwordField = [UITextField new];
    passwordField.placeholder = NSLocalizedString(@"LOGIN_PASSWORD_PLACEHOLDER", @"");
    passwordField.delegate = self;
    passwordField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    passwordField.translatesAutoresizingMaskIntoConstraints = NO;
    passwordField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    passwordField.layer.borderWidth = 0.6f;
    passwordField.secureTextEntry = YES;
    passwordField.returnKeyType = UIReturnKeyDone;
    
    [scrollView addSubview:passwordField];
    
    loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [loginButton setTitle:NSLocalizedString(@"LOGIN", @"") forState:UIControlStateNormal];
    loginButton.translatesAutoresizingMaskIntoConstraints = NO;
    [loginButton addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginButton.backgroundColor = [UIColor colorWithRed:46/256.0f green:65/256.0f blue:114/256.0f alpha:1.0f];
    loginButton.enabled = false;
    [scrollView addSubview:loginButton];
    
    
    signupButton = [UIButton buttonWithType:UIButtonTypeSystem];
    signupButton.translatesAutoresizingMaskIntoConstraints = NO;
    [signupButton setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"SIGNUP", @"Sign up") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle), NSForegroundColorAttributeName: [UIColor colorWithWhite: 0.8 alpha: 1.0f]}] forState:UIControlStateNormal];
    //    [signupButton setTitle:NSLocalizedString(@"SIGNUP", @"") forState:UIControlStateNormal];
    [signupButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signupButton addTarget:self action:@selector(signup) forControlEvents:UIControlEventTouchUpInside];
    //    signupButton.backgroundColor = [UIColor colorWithRed:224/256.0f green:80/256.0f blue:76/256.0f alpha:1.0f];
    
    [scrollView addSubview:signupButton];
    
    
    [scrollView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)]];
    
    
    _forgotPasswordButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _forgotPasswordButton.translatesAutoresizingMaskIntoConstraints = NO;
    _forgotPasswordButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    NSAttributedString *forgotPWText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"FORGOT_PASSWORD", @"Forgot password?") attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle), NSForegroundColorAttributeName: [UIColor colorWithWhite: 0.8 alpha: 1.0f]}];
    [_forgotPasswordButton setAttributedTitle:forgotPWText forState:UIControlStateNormal];
    [_forgotPasswordButton addTarget:self action:@selector(didPressForgotPassword) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:_forgotPasswordButton];
    
    
    warningMessageLabel = [UILabel new];
    warningMessageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    warningMessageLabel.alpha = 0.0f;
    warningMessageLabel.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    warningMessageLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    warningMessageLabel.textColor = [UIColor redColor];
    [scrollView addSubview:warningMessageLabel];
    
    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[titleLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(titleLabel)]];
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[usernameField]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(usernameField)]];
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[passwordField]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(passwordField)]];
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_forgotPasswordButton]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_forgotPasswordButton)]];
    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[loginButton]-15-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(loginButton)]];
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[signupButton]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(signupButton)]];
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[warningMessageLabel]-10-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(warningMessageLabel)]];
    
    
    UIView *view = [UIView new];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [scrollView addSubview:view];
    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view(==scrollView)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(scrollView, view)]];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:signupButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]];
    
    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==titleLabelTop)-[titleLabel]-50-[usernameField(==60)][passwordField(==60)]-[warningMessageLabel]-[_forgotPasswordButton(==20)]-[view]-[loginButton(==loginHeight)]-[signupButton(==loginHeight)]|" options:0 metrics:@{@"titleLabelTop": @(self.view.frame.size.height / 5 ), @"loginHeight": @(60)} views:NSDictionaryOfVariableBindings(titleLabel, usernameField, passwordField, warningMessageLabel, _forgotPasswordButton, loginButton, signupButton, view)]];
    
    
    
    RACSignal *formSignal = [RACSignal combineLatest:@[
                                                       usernameField.rac_textSignal,
                                                       passwordField.rac_textSignal
                                                       
                                                       ] reduce:^(NSString *username, NSString *password){
                                                           return @([username isValidEmail] && (password.length > 6));
                                                       }];
    
    
    RAC(loginButton, enabled) = formSignal;
    
}


-(void)dismiss{
    
    [usernameField resignFirstResponder];
    [passwordField resignFirstResponder];
    
}




-(void)signup{
    
    [self presentViewController:signupViewController animated:YES completion:nil];
    
}


-(void) login{
    
    AudioServicesPlaySystemSound (1105);
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"SIGN_IN", @"Signing in")];
    [[UserProfile currentUser] login:@{@"email": usernameField.text, @"password": passwordField.text}];
    
}



-(void)userDidLogin:(UserProfile *)profile{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        __weak typeof(self) weakSelf = self;
        [SVProgressHUD dismiss];
        [weakSelf.delegate userDidLogin:profile withLoginController:weakSelf];
    });
    
}


-(void)userLoginFailed: (NSString *)message{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        [self setWarningText:NSLocalizedString(message, @"")];
        
    });
    
}



-(void)setWarningText: (NSString *)reason{
    
    warningMessageLabel.alpha = 0.0f;
    
    warningMessageLabel.text = reason;
    [UIView animateWithDuration:0.2 animations:^{
        warningMessageLabel.alpha = 1.0f;
    }];
    
}




- (void)keyboardWillHide:(__unused NSNotification *)inputViewNotification {
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentOffset = CGPointMake(0.0, 0.0);
    }];
}

- (void)keyboardWillShow:(__unused NSNotification *)inputViewNotification {
    
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentOffset = CGPointMake(0.0, intersection.size.height - 80);
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if(textField == usernameField && textField.text.length > 0){
        
        if (![textField.text isValidEmail]) {
            
            if (![textField.text hasDomain]) {
                
                textField.text = [textField.text stringByAppendingString:@".com"];
                if (![textField.text isValidEmail]) {
                    
                    [textField hightlightBorder:[UIColor redColor]];
                    [self setWarningText:NSLocalizedString(@"NOT_AN_EMAIL", @"")];
                    
                }
            }else{
                
                [textField hightlightBorder:[UIColor redColor]];
                [self setWarningText:NSLocalizedString(@"NOT_AN_EMAIL", @"")];
                
            }
            
        }
        
        [passwordField becomeFirstResponder];
    }
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    if (textField == passwordField) {
        if (textField.text.length < 7 ) {
            [self userLoginFailed:@"PASSWORD_LENGTH_FAIL"];
        }
        [self login];
    }else if(textField == usernameField){
        
        if (![textField.text isValidEmail]) {
            [textField hightlightBorder:[UIColor redColor]];
            
            [self setWarningText:NSLocalizedString(@"NOT_AN_EMAIL", @"")];
        }
    }
    
    
    return YES;
}

-(void)didPressForgotPassword{
    
    OIHPopUpTextAlertController *alert = [[OIHPopUpTextAlertController alloc] init];
    alert.alertViewIcon = [IonIcons imageWithIcon:ion_ios_email size:60 color:[UIColor whiteColor]];
    alert.labelText = NSLocalizedString(@"FORGOT_PASSWORD_TITLE", @"Forgot password");
    alert.descriptionText = NSLocalizedString(@"FORGOT_PASSWORD_DETAILED_TEXT", @"forgot password detail");
    alert.iconBackgroundColor = [UIColor colorWithRed:216/256.0f green:33/256.0f blue:33/256.0f alpha:1.0f];
    alert.delegate = self;
    
    [alert addDoneAction:^(NSString *text, OIHPopUpTextAlertController *alert) {
        
        JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute: @"forgot" httpMethod: HTTP_POST];
        settings.requestData = @{@"email": text};
        
        [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            if (!error) {
                alert.doneContentView.title = NSLocalizedString(@"", @"");
                alert.doneContentView.descriptionText = NSLocalizedString(@"PASSWORD_RESET_DESCRIPTION", @"[message]");
                [alert switchToCompleteView];
            }else{
                alert.cancelContentView.title = NSLocalizedString(@"ERROR_BOOM", @"Boom");
                alert.cancelContentView.descriptionText = NSLocalizedString([error domain], @"");
                [alert switchToFailView];
            }
        }];
        
        
    }];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


#pragma mark - PopUpTextDelegate

-(BOOL)popupCheckValidity: (NSString *)text{
    
    return [text isValidEmail];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
