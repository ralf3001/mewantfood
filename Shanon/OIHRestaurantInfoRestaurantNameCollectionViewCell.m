//
//  OIHRestaurantInfoRestaurantNameCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantInfoRestaurantNameCollectionViewCell.h"



@implementation OIHRestaurantInfoRestaurantNameCollectionViewCell

-(id)initWithFrame:(CGRect)frame{
  
  self = [super initWithFrame:frame];
  
  if (self) {
    
    _titleLabel = [UILabel new];
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _titleLabel.numberOfLines = 0;
    _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:28.0f];
    [self.contentView addSubview:_titleLabel];
    
    _nameLabel = [UILabel new];
    _nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:28.0f];
    _nameLabel.numberOfLines = 0;
    [self.contentView addSubview:_nameLabel];
    
    UIView *separator = [UIView new];
    separator.translatesAutoresizingMaskIntoConstraints = NO;
    separator.backgroundColor = [UIColor highlightColor];
    
    [self.contentView addSubview:separator];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[separator]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(separator)]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_nameLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_nameLabel)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_titleLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel)]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_titleLabel]-[_nameLabel]-[separator(==1)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_nameLabel, separator, _titleLabel)]];
    
  }
  
  return self;
  
}

-(void)setLabelFont: (UIFont *)font{
  
  _nameLabel.font = font;
  
}


@end
