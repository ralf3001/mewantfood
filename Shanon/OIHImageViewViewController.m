//
//  OIHImageViewViewController.m
//  Shanon
//
//  Created by Ralf Cheung on 23/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHImageViewViewController.h"
#import "OIHUserOrderReviewViewController.h"

@interface OIHImageViewViewController ()
@property (nonatomic) UIImage *image;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UIButton *cancelButton;
@property (nonatomic) UIButton *doneButton;

@end

@implementation OIHImageViewViewController
@synthesize imageView;


-(id)initWithImage: (UIImage *)image{
    if (self = [super init]) {
        _image = image;
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    imageView = [UIImageView new];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    imageView.image = _image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imageView];
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_cancelButton setTitle:NSLocalizedString(@"CANCEL", @"Cancel") forState:UIControlStateNormal];
    [_cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_cancelButton];
    
    _doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_doneButton setTitle:NSLocalizedString(@"CONFIRM", @"Confirm") forState:UIControlStateNormal];
    [_doneButton addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:_doneButton];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_cancelButton][_doneButton(==_cancelButton)]|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(_cancelButton, _doneButton)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imageView][_doneButton(==80)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView, _doneButton)]];

}

-(void)cancel{
    
    if ([self.delegate respondsToSelector:@selector(modalViewControllerWillDismissWithCancelOption)]) {
        [self.delegate modalViewControllerWillDismissWithCancelOption];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(void)done{
    
    if ([self.delegate respondsToSelector:@selector(modalViewControllerWillDismissWithDoneOption)]) {
        [self.delegate modalViewControllerWillDismissWithDoneOption];
    }

    [self dismissViewControllerAnimated:YES completion:nil];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - <UIViewControllerTransitioningDelegate>

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    OrderReviewAnimatedTransitioning *transitioning = [OrderReviewAnimatedTransitioning new];
    transitioning.presenting = YES;
    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    OrderReviewAnimatedTransitioning * transitioning = [OrderReviewAnimatedTransitioning new];
    transitioning.presenting = NO;
    return transitioning;
    
}


@end
