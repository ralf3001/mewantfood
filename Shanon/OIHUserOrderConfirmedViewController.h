//
//  OIHUserOrderConfirmedViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 27/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"
#import "OIHMealModel.h"

#import "OIHOrderModel.h"


@interface OIHUserOrderConfirmedViewController : UIViewController
@property (nonatomic) OIHUserModel *friendProfile;
@property (nonatomic) OIHMealModel *mealInfo;

@property (nonatomic) OIHRestaurantModel *restaurantInfo;

-(id)initWithOrder: (OIHOrderModel *)order;
@end
