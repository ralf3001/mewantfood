//
//  OIHMessageModel.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHConversation.h"
#import "OIHModelProtocol.h"

typedef NS_ENUM(NSInteger, MessageType) {
  
  String = 1 << 0,
  Picture = 1 << 1,
  Audio = 1 << 2
};

@interface OIHMessageModel : NSObject <OIHModelProtocol>

@property (nonatomic) NSString *content;
@property (nonatomic) NSDate *createdDate;
@property (nonatomic) OIHConversation *conversation;
@property (nonatomic) NSString *url;
@property (nonatomic) MessageType messageType;
@property (nonatomic) BOOL isSentFromUser;


-(OIHMessageModel *)initWithDictionary: (id)info;

+(NSArray *)initWithArray: (id)list;
-(void)sendMessage :(void (^)(id data, NSError *error))completionHandler;
-(NSString *)messageTypeString;

@end
