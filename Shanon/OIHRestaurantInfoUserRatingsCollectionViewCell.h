//
//  OIHRestaurantInfoUserRatingsCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHRestaurantModel.h"

@interface OIHRestaurantInfoUserRatingsCollectionViewCell : UICollectionViewCell
@property (nonatomic) UILabel *angryLabel;
@property (nonatomic) UILabel *notSatisfiedLabel;
@property (nonatomic) UILabel *sickLabel;
@property (nonatomic) UILabel *tastyLabel;
@property (nonatomic) UILabel *loveItLabel;

@property (nonatomic) OIHRestaurantModel *restaurantInfo;

@end
