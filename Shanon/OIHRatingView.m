//
//  OIHRatingView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRatingView.h"
#import "OIHPopUpTextAlertController.h"
#import "OIHItemRatingView.h"

@interface OIHRatingView () <OIHRatingContentViewDelegate>

@property (nonatomic) UIView *contentView;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *descriptionLabel;
@property (nonatomic) UIButton *cancelButton;
@property (nonatomic) UIButton *doneButton;
@property (nonatomic) OIHItemRatingView *ratingView;
@end

@implementation OIHRatingView

-(id)init{
    
    self = [super init];
    if (self) {
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];

    
    _contentView = [UIView new];
    _contentView.translatesAutoresizingMaskIntoConstraints = NO;
    _contentView.backgroundColor = [UIColor whiteColor];
    _contentView.layer.cornerRadius = 10.0f;
    _contentView.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0f].CGColor;
    _contentView.layer.borderWidth = 0.8f;
    
//    [_contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTapped)]];
    [self.view addSubview:_contentView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:-30.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:self.view.frame.size.width * 0.8]];

//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:140]];

    
    [self setupContentView];
    
    

    
}


-(void)setupContentView{

    

    _titleLabel = [UILabel new];
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _titleLabel.text = NSLocalizedString(@"ORDER_DETAIL_REVIEW_RATE_IT_LABEL", @"Rate It!");
    _titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleTitle2];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [_contentView addSubview:_titleLabel];

    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_titleLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel)]];
    
    _descriptionLabel = [UILabel new];
    _descriptionLabel.numberOfLines = 0;
    _descriptionLabel.text = NSLocalizedString(@"ORDER_DETAIL_REVIEW_DESCRIPTION_LABEL", @"Tell people how you feel about this item");
    _descriptionLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
    _descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _descriptionLabel.textAlignment = NSTextAlignmentCenter;
    [_contentView addSubview:_descriptionLabel];
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_descriptionLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_descriptionLabel)]];

    _ratingView = [[OIHItemRatingView alloc] init];
    _ratingView.translatesAutoresizingMaskIntoConstraints = NO;
    _ratingView.delegate = self;
    [_contentView addSubview:_ratingView];
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_ratingView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_ratingView)]];
    
    _doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_doneButton addTarget:self action:@selector(contentDoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [_doneButton setTitle:NSLocalizedString(@"DONE", @"Cancel") forState:UIControlStateNormal];
    _doneButton.layer.masksToBounds = YES;
    [_contentView addSubview:_doneButton];
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_cancelButton setTitle:NSLocalizedString(@"CANCEL", @"Cancel") forState:UIControlStateNormal];
    _cancelButton.layer.masksToBounds = YES;
    [_cancelButton addTarget:self action:@selector(contentCancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [_contentView addSubview:_cancelButton];

    UIView *buttonTopDivider = [UIView new];
    buttonTopDivider.translatesAutoresizingMaskIntoConstraints = NO;
    buttonTopDivider.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
    [_contentView addSubview:buttonTopDivider];
    
    UIView *sideDivider = [UIView new];
    sideDivider.translatesAutoresizingMaskIntoConstraints = NO;
    sideDivider.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
    [_contentView addSubview:sideDivider];
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[buttonTopDivider]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(buttonTopDivider)]];

    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_cancelButton][sideDivider(==1)][_doneButton(==_cancelButton)]|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(_cancelButton, _doneButton, sideDivider)]];
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[_titleLabel]-[_descriptionLabel]-15-[_ratingView(==50)]-10-[buttonTopDivider(==1)][_doneButton(==40)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel, _ratingView, _doneButton, buttonTopDivider, _descriptionLabel)]];


}


-(void)contentDoneButtonPressed{
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


-(void)contentCancelButtonPressed{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)ratingViewDidChooseEmoji:(NSInteger)emoji{
    
    
    
}

-(void)setScore:(NSInteger)score{
    
    [_ratingView setChosenEmoji:score];

}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    PopUpTextAnimatedTransitioning *transitioning = [PopUpTextAnimatedTransitioning new];
    transitioning.isPresenting = YES;
    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    PopUpTextAnimatedTransitioning * transitioning = [PopUpTextAnimatedTransitioning new];
    transitioning.isPresenting = NO;
    return transitioning;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
