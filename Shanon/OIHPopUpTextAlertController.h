//
//  OIHPopUpTextAlertController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHPopupContentView.h"


@class OIHPopUpTextAlertController;


@protocol PopUpTextDelegate <NSObject>

@required
-(BOOL)popupCheckValidity: (NSString *)text;

@end

typedef void (^OIHActionHandler)(NSString * text, OIHPopUpTextAlertController *alert);
typedef void (^OIHCancelHandler)();

@interface OIHPopUpTextAlertController : UIViewController <UIViewControllerTransitioningDelegate>

@property (nonatomic, weak) OIHActionHandler doneAction;
@property (nonatomic, weak) OIHCancelHandler cancelAction;
@property (nonatomic, weak) id <PopUpTextDelegate> delegate;

@property (nonatomic) NSString *labelText;
@property (nonatomic) NSString *descriptionText;

@property (nonatomic) OIHPopupContentView *doneContentView;
@property (nonatomic) OIHPopupContentView *cancelContentView;

@property (nonatomic) UIImage *alertViewIcon;

@property (nonatomic) UIColor *iconBackgroundColor;

-(void) addDoneAction: (OIHActionHandler) action;
-(void) addCancelAction: (OIHCancelHandler) action;

-(void)switchToCompleteView;
-(void)switchToFailView;

@end

@interface PopUpTextAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (assign) BOOL isPresenting;
@property (nonatomic) NSInteger indexOfBackgroundView;

@end
