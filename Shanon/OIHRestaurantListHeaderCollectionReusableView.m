//
//  OIHRestaurantListHeaderCollectionReusableView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantListHeaderCollectionReusableView.h"

@interface OIHRestaurantListHeaderCollectionReusableView ()
@property (assign) BOOL downArrowFliped;

@end


@implementation OIHRestaurantListHeaderCollectionReusableView

@synthesize imageView;
@synthesize descriptionTextView;
@synthesize downArrowImageView;
@synthesize popNavigationButton;


-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {

        CGRect bounds;
        bounds = [self bounds];

        imageView = [[UIImageView alloc] initWithFrame:bounds];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
        [imageView setClipsToBounds:YES];
        [self addSubview:imageView];
        
        descriptionTextView = [UITextView new];
        descriptionTextView.editable = NO;
        descriptionTextView.translatesAutoresizingMaskIntoConstraints = NO;
        descriptionTextView.textColor = [UIColor whiteColor];
        descriptionTextView.scrollEnabled = NO;
        descriptionTextView.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:30];
        descriptionTextView.backgroundColor = [UIColor clearColor];
        [self addSubview:descriptionTextView];
        
        
        downArrowImageView = [UIButton buttonWithType:UIButtonTypeCustom];
        downArrowImageView.translatesAutoresizingMaskIntoConstraints = NO;
        downArrowImageView.contentMode = UIViewContentModeScaleAspectFit;
        [downArrowImageView setImage:[IonIcons imageWithIcon:ion_ios_arrow_down size:30 color:[UIColor whiteColor]] forState:UIControlStateNormal];
        _downArrowFliped = NO;
        [downArrowImageView addTarget:self action:@selector(arrowButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:downArrowImageView];
        
        popNavigationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        popNavigationButton.translatesAutoresizingMaskIntoConstraints = NO;
        [popNavigationButton setImage:[IonIcons imageWithIcon:ion_ios_close_empty size:50 color:[UIColor whiteColor]] forState:UIControlStateNormal];
        [popNavigationButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:popNavigationButton];

        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [animation setDuration:1.3f];
        [animation setRepeatCount:100];
        [animation setAutoreverses:YES];
        [animation setFromValue:[NSNumber numberWithFloat:0.0f]];
        [animation setToValue:[NSNumber numberWithFloat:1.0f]];
        [downArrowImageView.layer addAnimation:animation forKey:@"opacity"];

        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:descriptionTextView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:downArrowImageView attribute:NSLayoutAttributeTop multiplier:1.0f constant:-10.0f]];

        [self addConstraint:[NSLayoutConstraint constraintWithItem:downArrowImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-10.0f]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[downArrowImageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(downArrowImageView)]];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[descriptionTextView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionTextView)]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:popNavigationButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0f constant:20.0f]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:popNavigationButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0f constant:10.0f]];

        
        
        UIView *overlay = [UIView new];
        overlay.translatesAutoresizingMaskIntoConstraints = NO;
        overlay.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        [self insertSubview:overlay belowSubview:descriptionTextView];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[overlay]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(overlay)]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[overlay]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(overlay)]];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(flipDownArrowImageView:) name:@"flipImageView" object:nil];

    }
    
    
    return self;
}




-(void)arrowButtonTapped{
    
    if ([self.delegate respondsToSelector:@selector(headerScrollDownButtonDidPress)]) {
        [self.delegate headerScrollDownButtonDidPress];
    }

}

-(void)dismiss{
    
    if ([self.delegate respondsToSelector:@selector(headerDidPressPopNavigationButton)]) {
        [self.delegate headerDidPressPopNavigationButton];
    }
}

-(void)flipDownArrowImageView: (NSNotification *)notification{

    @synchronized (self) {
    
        BOOL isHeaderViewHidden = [notification.object[@"isHeaderViewHidden"] boolValue];
                
        if (_downArrowFliped != isHeaderViewHidden) {

            _downArrowFliped = isHeaderViewHidden;
            
            [UIView animateWithDuration:0.3 animations:^{
                if (_downArrowFliped) {
                    downArrowImageView.imageView.transform = CGAffineTransformMakeRotation(M_PI); // flip the image view
                }else{
                    downArrowImageView.imageView.transform = CGAffineTransformMakeRotation(0); // flip the image view
                }
            }];
            
        }
        
    }
    
}




@end
