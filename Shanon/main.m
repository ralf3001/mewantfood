//
//  main.m
//  Shanon
//
//  Created by Ralf Cheung on 23/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
