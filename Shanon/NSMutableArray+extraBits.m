//
//  NSMutableArray+extraBits.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 10/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "NSMutableArray+extraBits.h"


static void * isDataDirtyKey = &isDataDirtyKey;

@implementation NSMutableArray (extraBits)
@dynamic isDataDirty;

-(NSNumber *)isDataDirty{
    return objc_getAssociatedObject(self, isDataDirtyKey);
}

-(void)setIsDataDirty:(NSNumber *)isDataDirty{
    objc_setAssociatedObject(self, isDataDirtyKey, isDataDirty, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
