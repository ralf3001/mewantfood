//
//  OIHMapViewAddressPin.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHMapViewAddressPin.h"

@implementation OIHMapViewAddressPin

- (id)initWithCoordinates:(CLLocationCoordinate2D)location placeName:(NSString *)placeName formattedAddress: (NSString *)formattedAddress{

    self = [super init];
    if (self) {
        
        self.coordinate = location;
        self.title = placeName;
        self.formattedAddress = formattedAddress;
        
    }
    return self;
}


@end
