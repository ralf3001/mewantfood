//
//  OIHAudioPlayer.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 27/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHAudioPlayer : NSObject

@property(assign, nonatomic) BOOL isPlaying;
@property(nonatomic) NSURL *currentlyPlaying;
@property(nonatomic) NSString *currentPlaying;

+ (OIHAudioPlayer *)audioPlayer;

- (void)playAudioWithURLString:(NSString *)urlString;
- (void)playAudioWithURL:(NSURL *)url;

- (void)stop;

@end
