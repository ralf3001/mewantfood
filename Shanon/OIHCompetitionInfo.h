//
//  OIHCompetitionInfo.h
//  Shanon
//
//  Created by Ralf Cheung on 14/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHCompetitionInfo : NSObject

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *competitionDescription;
@property (nonatomic) NSDate *createdDate;
@property (nonatomic) NSDate *endDate;

@property (nonatomic) NSDictionary *info;

-(id)initWithCompetitionInfo: (NSDictionary *)info;
-(void)dismissCompetition;

@end
