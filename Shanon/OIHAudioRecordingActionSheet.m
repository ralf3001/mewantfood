//
//  OIHAudioRecordingActionSheet.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 31/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHAudioRecordingActionSheet.h"
#import "OIHActionSheet.h"
#include "EZAudio.h"


@interface OIHAudioRecordingActionSheet () <EZMicrophoneDelegate, EZAudioPlayerDelegate, EZRecorderDelegate>
@property (nonatomic) EZAudioPlotGL *recordingAudioPlot;

@property (nonatomic, assign) BOOL isRecording;
@property (nonatomic, assign) BOOL isPlaying;

@property (nonatomic) EZMicrophone *microphone;
@property (nonatomic) EZAudioPlayer *player;
@property (nonatomic) EZRecorder *recorder;
@property (nonatomic) EZAudioPlot *playingAudioPlot;

@property (nonatomic) UIView *contentView;
@property (nonatomic) UIView *dismissView;

@property (nonatomic) AVAudioRecorder *audioRecorder;
@property (nonatomic) AVPlayer *audioPlayer;

@property (nonatomic) UIButton *recordAudioButton;
@property (nonatomic) UIButton *playButton;

@end

@implementation OIHAudioRecordingActionSheet


-(id)init{
    
    self = [super init];
    if (self) {
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}



- (void)viewDidLoad {
    
    [super viewDidLoad];

    
    _dismissView = [UIView new];
    _dismissView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_dismissView];
    
    [_dismissView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)]];
    
    _contentView = [UIView new];
    _contentView.backgroundColor = [UIColor lightGrayColor];
    _contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_contentView];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_dismissView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:0]];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_dismissView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeTop multiplier:1.0f constant:0]];

    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_dismissView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_dismissView)]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_contentView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_contentView)]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:30.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0]];

    
    [self prepareAudioSession];

}



-(void)prepareAudioSession{
    
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if (error){
        NSLog(@"Error setting up audio session category: %@", error.localizedDescription);
    }
    
    [session setActive:YES error:&error];
    
    if (error){
        NSLog(@"Error setting up audio session active: %@", error.localizedDescription);
    }
    
    
    self.recordingAudioPlot = [EZAudioPlotGL new];
    self.recordingAudioPlot.translatesAutoresizingMaskIntoConstraints = NO;
    
    //
    // Customizing the audio plot that'll show the current microphone input/recording
    //

    self.recordingAudioPlot.backgroundColor = [UIColor lightGrayColor];

    self.recordingAudioPlot.color           = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    self.recordingAudioPlot.plotType        = EZPlotTypeRolling;
    self.recordingAudioPlot.shouldFill      = YES;
    self.recordingAudioPlot.shouldMirror    = YES;
    
    [_contentView addSubview:self.recordingAudioPlot];
    
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_recordingAudioPlot]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_recordingAudioPlot)]];
    
    
    // Create an instance of the microphone and tell it to use this view controller instance as the delegate
    self.microphone = [EZMicrophone microphoneWithDelegate:self];
    self.player = [EZAudioPlayer audioPlayerWithDelegate:self];
    
    //
    // Override the output to the speaker. Do this after creating the EZAudioPlayer
    // to make sure the EZAudioDevice does not reset this.
    //
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
    if (error){
        NSLog(@"Error overriding output to the speaker: %@", error.localizedDescription);
    }
    

    _recordAudioButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [_recordAudioButton setImage:[IonIcons imageWithIcon:ion_ios_mic_outline size:30.0f color:[UIColor whiteColor]] forState:UIControlStateNormal];
//    [_recordAudioButton addTarget:self action:@selector(toggleRecording) forControlEvents:UIControlEventTouchUpInside];
    [_recordAudioButton addTarget:self action:@selector(startRecording) forControlEvents:UIControlEventTouchUpInside];
    
    _recordAudioButton.translatesAutoresizingMaskIntoConstraints = NO;
    _recordAudioButton.layer.cornerRadius = 30;
    _recordAudioButton.backgroundColor = [UIColor redColor];
    _recordAudioButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _recordAudioButton.layer.borderWidth = 2.0f;
    _recordAudioButton.layer.masksToBounds = NO;
    [_recordAudioButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_contentView insertSubview:_recordAudioButton aboveSubview:_recordingAudioPlot];

    
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_recordingAudioPlot]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_recordingAudioPlot, _recordAudioButton)]];
    
    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:_recordAudioButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];

    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:_recordAudioButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:60.0f]];
    
    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:_recordAudioButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_recordAudioButton attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    
    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:_recordAudioButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-15.0f]];
    
    

    _playButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [_playButton addTarget:self action:@selector(playFile:) forControlEvents:UIControlEventTouchUpInside];
    _playButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_playButton setImage:[IonIcons imageWithIcon:ion_ios_play_outline size:30.0f color:[UIColor whiteColor]] forState:UIControlStateNormal];
    _playButton.backgroundColor = [UIColor clearColor];
    [_contentView addSubview:_playButton];
    
    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:_playButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-15.0f]];

    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:_playButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_recordAudioButton attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];

    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:_playButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:60.0f]];

    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:_playButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeRight multiplier:1.0f constant:-10.0f]];


    self.isRecording = NO;

    [self setupNotifications];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"File written to application sandbox's documents directory: %@", [self testFilePathURL]);
    });
    
    [self addObserver:self forKeyPath:@"isRecording" options:NSKeyValueObservingOptionNew context:nil];

    
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if([keyPath isEqualToString:@"isRecording"]){
        
        dispatch_async(dispatch_get_main_queue(), ^{

            BOOL newC = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];

            if (newC) {
                _recordAudioButton.layer.borderColor = [UIColor colorWithRed:152/255.0f green:193/255.0f blue: 193/255.0f alpha:1.0f].CGColor;
                _recordAudioButton.layer.shadowColor = [UIColor whiteColor].CGColor;
                _recordAudioButton.layer.shadowOffset = CGSizeMake(0.0, 0.0);
                _recordAudioButton.layer.shadowRadius = 10.0;
                _recordAudioButton.layer.shadowOpacity = 0.8;
                _recordAudioButton.layer.masksToBounds = NO;
                [_recordAudioButton setImage:[IonIcons imageWithIcon:ion_ios_mic size:30.0f color:[UIColor whiteColor]] forState:UIControlStateNormal];

                
            }else{
                [_recordAudioButton setImage:[IonIcons imageWithIcon:ion_ios_mic_outline size:30.0f color:[UIColor whiteColor]] forState:UIControlStateNormal];

                _recordAudioButton.layer.borderColor = [UIColor whiteColor].CGColor;
                _recordAudioButton.layer.shadowOpacity = 0.0;
                
            }
            
        });

    }
    
    
}


- (void)setupNotifications{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDidChangePlayState:)
                                                 name:EZAudioPlayerDidChangePlayStateNotification
                                               object:self.player];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDidReachEndOfFile:)
                                                 name:EZAudioPlayerDidReachEndOfFileNotification
                                               object:self.player];
}



- (void)playerDidChangePlayState:(NSNotification *)notification{
    
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        EZAudioPlayer *player = [notification object];
        BOOL isPlaying = [player isPlaying];
        if (isPlaying){
            [_playButton setImage:[IonIcons imageWithIcon:ion_ios_play size:30.0f color:[UIColor whiteColor]] forState:UIControlStateNormal];
            weakSelf.recorder.delegate = nil;
        }else{
            [_playButton setImage:[IonIcons imageWithIcon:ion_ios_play_outline size:30.0f color:[UIColor whiteColor]] forState:UIControlStateNormal];

        }
        self.isPlaying = isPlaying;
        
        weakSelf.playingAudioPlot.hidden = !isPlaying;
    });
}

//------------------------------------------------------------------------------

- (void)playerDidReachEndOfFile:(NSNotification *)notification{
    
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.playingAudioPlot clear];
    });
}


-(void)toggleRecording{
    
    [self.player pause];
    
    if (self.isRecording) {
        //stop recording;
        self.isRecording = NO;
        [self.microphone stopFetchingAudio];
    }else{
        
        [self.recordingAudioPlot clear];
        [self.microphone startFetchingAudio];
        self.recorder = [EZRecorder recorderWithURL:[self testFilePathURL]
                                       clientFormat:[self.microphone audioStreamBasicDescription]
                                           fileType:EZRecorderFileTypeM4A
                                           delegate:self];
        
        self.isRecording = YES;
        
    }
    
    
}



-(void) startRecording{
    
    
    if (_audioRecorder.isRecording) {
        self.isRecording = NO;
        [_audioRecorder stop];
        [self.microphone stopFetchingAudio];
    }else{
        // Init audio with record capability
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *err = nil;
        [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
        if(err){
            NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
            return;
        }
        [audioSession setActive:YES error:&err];
        err = nil;
        if(err){
            NSLog(@"audioSession: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
            return;
        }
        
        NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
        
        [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
        [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
        [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
        [recordSetting setValue:@(kAudioFormatMPEG4AAC) forKey:AVFormatIDKey];
        [recordSetting setValue :[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
        [recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
        [recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
        
        
        
        NSError *error = nil;
        _audioRecorder = [[AVAudioRecorder alloc] initWithURL:[self testFilePathURL] settings:recordSetting error:&error];
        
        if ([_audioRecorder prepareToRecord] == YES){
            [_audioRecorder record];
            [self.recordingAudioPlot clear];
            [self.microphone startFetchingAudio];
            self.isRecording = YES;
        }
        
    }

}


- (void)playFile:(id)sender{
    
    [self.microphone stopFetchingAudio];
    
    self.isRecording = NO;
    
    if (self.recorder){
        [self.recorder closeAudioFile];
    }
    
    NSError *error = nil;
    
    if ([[self testFilePathURL] checkResourceIsReachableAndReturnError:&error]) {
        EZAudioFile *audioFile = [EZAudioFile audioFileWithURL:[self testFilePathURL]];
        [self.player playAudioFile:audioFile];
    }
    
}




//#warning Thread Safety
//
// Note that any callback that provides streamed audio data (like streaming
// microphone input) happens on a separate audio thread that should not be
// blocked. When we feed audio data into any of the UI components we need to
// explicity create a GCD block on the main thread to properly get the UI to
// work.
- (void)   microphone:(EZMicrophone *)microphone
     hasAudioReceived:(float **)buffer
       withBufferSize:(UInt32)bufferSize
 withNumberOfChannels:(UInt32)numberOfChannels{
    
    // Getting audio data as an array of float buffer arrays. What does that
    // mean? Because the audio is coming in as a stereo signal the data is split
    // into a left and right channel. So buffer[0] corresponds to the float* data
    // for the left channel while buffer[1] corresponds to the float* data for
    // the right channel.
    
    //
    // See the Thread Safety warning above, but in a nutshell these callbacks
    // happen on a separate audio thread. We wrap any UI updating in a GCD block
    // on the main thread to avoid blocking that audio flow.
    //
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        //
        // All the audio plot needs is the buffer data (float*) and the size.
        // Internally the audio plot will handle all the drawing related code,
        // history management, and freeing its own resources. Hence, one badass
        // line of code gets you a pretty plot :)
        //
        [weakSelf.recordingAudioPlot updateBuffer:buffer[0]
                                   withBufferSize:bufferSize];
    });
}

//------------------------------------------------------------------------------

- (void)   microphone:(EZMicrophone *)microphone
        hasBufferList:(AudioBufferList *)bufferList
       withBufferSize:(UInt32)bufferSize
 withNumberOfChannels:(UInt32)numberOfChannels{
    
    //
    // Getting audio data as a buffer list that can be directly fed into the
    // EZRecorder. This is happening on the audio thread - any UI updating needs
    // a GCD main queue block. This will keep appending data to the tail of the
    // audio file.
    //
    if (self.isRecording){
        [self.recorder appendDataFromBufferList:bufferList
                                 withBufferSize:bufferSize];
    }
}

//------------------------------------------------------------------------------
#pragma mark - EZRecorderDelegate
//------------------------------------------------------------------------------

- (void)recorderDidClose:(EZRecorder *)recorder{
    
    recorder.delegate = nil;
}


//------------------------------------------------------------------------------
#pragma mark - Utility
//------------------------------------------------------------------------------

- (NSArray *)applicationDocuments{
    
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
}

//------------------------------------------------------------------------------

- (NSString *)applicationDocumentsDirectory{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

//------------------------------------------------------------------------------

- (NSURL *)testFilePathURL{
    
    return [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",
                                   [self applicationDocumentsDirectory],
                                   @"test.aac"]];
}


-(void)dismiss{
    
    if ([self.delegate respondsToSelector:@selector(audioActionSheetWillDismiss:)]) {
        [self.delegate audioActionSheetWillDismiss:[self testFilePathURL]];
    }
    
    __weak typeof (self) weakSelf = self;

    [self dismissViewControllerAnimated:YES completion:^{
        
        if ([weakSelf.delegate respondsToSelector:@selector(audioActionSheetDidDismiss:)]) {
            [weakSelf.delegate audioActionSheetDidDismiss:[weakSelf testFilePathURL]];
        }

    }];
    
}


#pragma mark - <Transition Delegate>

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    CustomAnimatedTransitioning *transitioning = [CustomAnimatedTransitioning new];
    transitioning.presenting = YES;
    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    CustomAnimatedTransitioning * transitioning = [CustomAnimatedTransitioning new];
    transitioning.presenting = NO;
    return transitioning;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [self removeObserver:self forKeyPath:@"isRecording"];

}

@end
