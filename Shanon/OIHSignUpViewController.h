//
//  OIHSignUpViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>


@class OIHSignUpViewController;

@protocol SignupViewProtocol <NSObject>

-(void)userDidSignUp:(UserProfile *)profile withSignUpController: (OIHSignUpViewController *)signupViewController;;

@end

@interface OIHSignUpViewController : UIViewController
@property (nonatomic, weak) id<SignupViewProtocol>delegate;
@end

