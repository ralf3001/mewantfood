//
//  OIHMapAnnotationView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMapAnnotationView.h"

@interface OIHMapAnnotationView ()
@property (nonatomic) UIView *annotationView;

@end

@implementation OIHMapAnnotationView

- (id)initWithAnnotation:(id)annotation reuseIdentifier:(NSString *)reuseIdentifier {
  
  self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
  if (self) {
    _annotationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
    _annotationView.layer.cornerRadius = 8.0f;
    _annotationView.backgroundColor = [UIColor colorWithRed:1.0f green:0.0 blue:0.0 alpha:0.4];
    _annotationView.layer.masksToBounds = YES;
    [self addSubview:_annotationView];

  }
  return self;
}

@end
