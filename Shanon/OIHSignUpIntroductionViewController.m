//
//  OIHSignUpIntroductionViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 25/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSignUpIntroductionViewController.h"
#import "AppDelegate.h"


@interface MessageWrapper : NSObject
@property (nonatomic) NSString *message;
@property (nonatomic) double delay;
@end

@implementation MessageWrapper
@end

@interface OIHSignUpIntroductionViewController ()
@property (nonatomic) UILabel *haveAStoryLabel;

@property (nonatomic) NSArray *stringsArray;
@property (nonatomic, assign) NSInteger index;
@end

@implementation OIHSignUpIntroductionViewController
@synthesize haveAStoryLabel;
@synthesize stringsArray;
@synthesize index;



- (void)viewDidLoad {
    [super viewDidLoad];

    
    MessageWrapper *haveAStoryMessage = [[MessageWrapper alloc] init];
    haveAStoryMessage.message = @"I have a story to tell";
    haveAStoryMessage.delay = 1.5;
    
    MessageWrapper *beganString = [[MessageWrapper alloc] init];
    beganString.message = @"It all began when, one day, the person who inspired me the idea of this platform teased me if I would really buy her a late dinner.";
    beganString.delay = 3;
    
    MessageWrapper *yesString = [MessageWrapper new];
    yesString.message = @"I said \"Yes, of course.\"";
    yesString.delay = 2;
    
    MessageWrapper *butString = [MessageWrapper new];
    butString.message = @"But there's a problem.";
    butString.delay = 1.5f;
    
    MessageWrapper *problemString = [MessageWrapper new];
    problemString.message = @"I live an hour from where she lives and there is no way I could go home that night if I go to her place.";
    problemString.delay = 2.5f;
    
    MessageWrapper *soISaidString = [MessageWrapper new];
    soISaidString.message = @"So I thought, one day, I would buy you dinner even though I'm across the Pacific Ocean.";
    soISaidString.delay = 1.8f;
    
    MessageWrapper *hereItIsString = [MessageWrapper new];
    hereItIsString.message = @"And here it is";
    hereItIsString.delay = 1.5f;
    
    MessageWrapper *last = [MessageWrapper new];
    last.message = @"Anyone Unveils Oh";
    last.delay = 2.0f;
    
    
    stringsArray = [NSArray arrayWithObjects:haveAStoryMessage, beganString, yesString, butString, problemString, soISaidString, hereItIsString, last, nil];
    
    index = 0;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    haveAStoryLabel = [UILabel new];
    haveAStoryLabel.numberOfLines = 0;
    haveAStoryLabel.translatesAutoresizingMaskIntoConstraints = NO;
    haveAStoryLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20.0f];
    haveAStoryLabel.layer.opacity = 0.0f;
    haveAStoryLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:haveAStoryLabel];


    UIButton *skipButton = [UIButton buttonWithType:UIButtonTypeSystem];
    skipButton.translatesAutoresizingMaskIntoConstraints = NO;
    [skipButton setTintColor:[UIColor blackColor]];
    skipButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:14.0f];
    [skipButton setTitle:@"Skip" forState:UIControlStateNormal];
    [skipButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:skipButton];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[skipButton]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(skipButton)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[haveAStoryLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(haveAStoryLabel)]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:haveAStoryLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:skipButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-15]];
    

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self setStoryLabelAnimation:stringsArray[index]];
    
}

-(void)setStoryLabelAnimation: (MessageWrapper *)message{

    haveAStoryLabel.text = message.message;
    
    [CATransaction begin];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.duration = 0.5;
    animation.fromValue = [NSNumber numberWithFloat:0.0f];
    animation.toValue = [NSNumber numberWithFloat:1.0f];
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeBoth;
    
    
    [CATransaction setCompletionBlock:^{
        
        CABasicAnimation *animation2 = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animation2.duration = 0.5;
        animation2.beginTime = CACurrentMediaTime() + message.delay;
        animation2.fromValue = [NSNumber numberWithFloat:1.0f];
        animation2.toValue = [NSNumber numberWithFloat:0.0f];
        animation2.removedOnCompletion = NO;
        animation2.fillMode = kCAFillModeBoth;
        animation2.delegate = self;
        [haveAStoryLabel.layer addAnimation:animation2 forKey:@"fadeOut"];

        
    }];
    
    [haveAStoryLabel.layer addAnimation:animation forKey:@"fadeIn"];    
    [CATransaction commit];

}


-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    
    if (anim == [haveAStoryLabel.layer animationForKey:@"fadeOut"]) {

        if (++index < stringsArray.count) {
            [self setStoryLabelAnimation:stringsArray[index]];
        }else{

            if ([self.delegate respondsToSelector:@selector(introWillDismiss:)]) {
                [self.delegate introWillDismiss: self];
            }
            
        }
        
    }

}

-(void)dismiss{
    
    if ([self.delegate respondsToSelector:@selector(introWillDismiss:)]) {
        [self.delegate introWillDismiss: self];
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)prefersStatusBarHidden{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
