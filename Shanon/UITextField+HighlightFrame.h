//
//  UITextField+HighlightFrame.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (HighlightFrame)
@property (nonatomic, assign) BOOL isModifying;

-(void)hightlightBorder: (UIColor *)highlightColor;

@end
