//
//  OIHActivityFeedViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHActivityFeedViewController.h"
#import "OIHActivityFeedDataSource.h"
#import "OIHHomeScreenReviewTableViewCell.h"
#import "OIHUserOrderReviewViewController.h"


@interface OIHActivityFeedViewController () <ActivityFeedDataSourceDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) OIHActivityFeedDataSource *dataSource;

@end

@implementation OIHActivityFeedViewController


-(id)initWithRestaurant: (OIHRestaurantModel *)restaurantInfo{
  self = [super init];
  if (self) {
    _restaurantInfo = restaurantInfo;
  }
  return self;
}

- (void)viewDidLoad {
  
  [super viewDidLoad];

  [self createTableView];

}

-(void)viewWillAppear:(BOOL)animated{
  [_dataSource fetchActivities:nil];
}


- (void)createTableView {
  
  _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                            style:UITableViewStyleGrouped];
  _tableView.translatesAutoresizingMaskIntoConstraints = NO;
  [_tableView registerClass:[OIHHomeScreenReviewTableViewCell class] forCellReuseIdentifier:@"reviewCell"];
  [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"NoMoreDataCell"];
  
  _tableView.backgroundColor = [UIColor whiteColor];
  _tableView.contentInset = UIEdgeInsetsZero;
  _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
  
  _tableView.estimatedRowHeight = 130;
  _tableView.rowHeight = UITableViewAutomaticDimension;
  
  _dataSource = [[OIHActivityFeedDataSource alloc] initWithRestaurant: _restaurantInfo];
  _dataSource.tableView = _tableView;
  _tableView.delegate = _dataSource;
  _tableView.dataSource = _dataSource;
  
  _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  
  [self.view addSubview:_tableView];
  
  [self.view addConstraints:
   [NSLayoutConstraint
    constraintsWithVisualFormat:@"H:|[_tableView]|"
    options:0
    metrics:0
    views:NSDictionaryOfVariableBindings(
                                         _tableView)]];
  
  [self.view addConstraints:
   [NSLayoutConstraint
    constraintsWithVisualFormat:@"V:|[_tableView]|"
    options:0
    metrics:0
    views:NSDictionaryOfVariableBindings(
                                         _tableView)]];
  
  
}


#pragma mark - Data source delegate

-(void)tableViewDidSelectRowForReview:(OIHActivityFeed *)feed{
  
  AudioServicesPlaySystemSound (1105);
  
  OIHUserOrderReviewViewController *vc = [[OIHUserOrderReviewViewController alloc] initWithReview:feed];
  [self presentViewController:vc animated:YES completion:nil];

}


@end
