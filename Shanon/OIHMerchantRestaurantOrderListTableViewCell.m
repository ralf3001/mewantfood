//
//  OIHMerchantRestaurantOrderListTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHMerchantRestaurantOrderListTableViewCell.h"
#import "OIHOrderModel.h"


@implementation OIHMerchantRestaurantOrderListTableViewCell
@synthesize orderIdLabel, orderItemNameLabel, orderRecipientAddressLabel, orderRestaurantLabel, orderStatusLabel;
@synthesize orderDateLabel;
@synthesize orderStatusColorLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];
 
        orderIdLabel = [UILabel new];
        orderIdLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderIdLabel];
        
        orderItemNameLabel = [UILabel new];
        orderItemNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderItemNameLabel];
        
        orderRecipientAddressLabel = [UILabel new];
        orderRecipientAddressLabel.numberOfLines = 0;
        orderRecipientAddressLabel.textColor = [UIColor darkGrayColor];
        orderRecipientAddressLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        orderRecipientAddressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderRecipientAddressLabel];
        
        orderRestaurantLabel = [UILabel new];
        orderRestaurantLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderRestaurantLabel];
        
        orderStatusLabel = [UILabel new];
        orderStatusLabel.textColor = [UIColor grayColor];
        orderStatusLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
        orderStatusLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderStatusLabel];
        
        orderDateLabel = [UILabel new];
        orderDateLabel.translatesAutoresizingMaskIntoConstraints = NO;
        orderDateLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
        orderDateLabel.textColor = [UIColor darkGrayColor];
        [self.contentView addSubview:orderDateLabel];
        
        _orderRecipientLabel = [UILabel new];
        _orderRecipientLabel.numberOfLines = 0;
        _orderRecipientLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        _orderRecipientLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_orderRecipientLabel];
        
        _orderPriceLabel = [UILabel new];
        _orderPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _orderPriceLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        _orderPriceLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_orderPriceLabel];
        
        
        orderStatusColorLabel = [UIView new];
        orderStatusColorLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:orderStatusColorLabel];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[orderItemNameLabel]-[_orderPriceLabel(<=80)]-|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllCenterY metrics:0 views:NSDictionaryOfVariableBindings(orderItemNameLabel, _orderPriceLabel)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_orderRecipientLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_orderRecipientLabel)]];

        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[orderRecipientAddressLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(orderRecipientAddressLabel)]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[orderStatusColorLabel]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(orderStatusColorLabel)]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:orderStatusColorLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0f]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:orderStatusColorLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0 constant:8.0f]];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[orderStatusLabel]-[orderItemNameLabel]-[orderRecipientAddressLabel]-[_orderRecipientLabel]-15-[orderDateLabel]-|" options:NSLayoutFormatAlignAllLeading metrics:0 views:NSDictionaryOfVariableBindings(orderDateLabel, orderItemNameLabel,orderStatusLabel,orderRecipientAddressLabel, _orderRecipientLabel)]];
        
    
    }
    
    return self;
}

-(void)setOrderStatus{
    
    orderStatusLabel.text = [self.orderModel orderStatus];
    
    orderStatusColorLabel.backgroundColor = [self.orderModel statusColor];
    orderStatusLabel.textColor = [self.orderModel statusColor];

}


-(void)setOrderModel:(OIHOrderModel *)orderModel{
    
    if (orderModel != _orderModel){
        _orderModel = orderModel;
        // custom code here
    }
    
    self.orderItemNameLabel.text = _orderModel.orderMeal.name;

    self.orderDateLabel.text = [NSString stringWithFormat:@"Ordered %@", [NSDate daysBetweenDate:_orderModel.orderDate andDate:[NSDate date]]];

    self.orderItemNameLabel.text = self.orderModel.orderMeal.name;

    
    NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
    [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currency setCurrencyCode:self.orderModel.restaurant.restaurantCurrency];
    self.orderPriceLabel.text = [currency stringFromNumber: self.orderModel.totalAmount];

    
    [self setOrderStatus];
    
    [self setSwipeLabels];
}



-(void)setSwipeLabels{
    
    switch (_orderModel.status) {
        case Ordered:
            [self deliveredLabel:MCSwipeTableViewCellState2];
            [self acceptedLabel:MCSwipeTableViewCellState1];
            [self canceledLabel:MCSwipeTableViewCellState3];
            break;
        case OrderAccepted:
            [self deliveredLabel:MCSwipeTableViewCellState1];
            [self canceledLabel:MCSwipeTableViewCellState3];
            break;
        case OrderDelivered:
        case OrderCanceled:
            break;
        default:
            break;
    }
    
    
}



-(void)deliveredLabel: (MCSwipeTableViewCellState)state{
 
    UILabel *doneLabel = [UILabel new];
    doneLabel.textColor = [UIColor whiteColor];
    doneLabel.text = NSLocalizedString(@"ORDER_DELIVERED", @"Delivered");
    [doneLabel sizeToFit];
    
    __weak typeof(self) weakSelf = self;
    
    [self setSwipeGestureWithView:doneLabel color:[UIColor orderDeliveredColor] mode:MCSwipeTableViewCellModeSwitch state:state completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
        
        [weakSelf.orderModel updateOrderStatus:OrderDelivered];
        [weakSelf setOrderStatus];
        
    }];


}

-(void)acceptedLabel: (MCSwipeTableViewCellState) state{
    
    UILabel *acceptedLabel = [UILabel new];
    acceptedLabel.text = NSLocalizedString(@"ORDER_ACCEPTED", @"Accepted");
    acceptedLabel.textColor = [UIColor whiteColor];
    [acceptedLabel sizeToFit];
    
    __weak typeof(self) weakSelf = self;

    [self setSwipeGestureWithView:acceptedLabel color:[UIColor orderAccptedColor] mode:MCSwipeTableViewCellModeSwitch state:state completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
        
        [weakSelf.orderModel updateOrderStatus:OrderAccepted];
        [weakSelf setOrderStatus];
        
    }];

    
}

-(void)canceledLabel: (MCSwipeTableViewCellState) state{
    
    UILabel *canceledLabel = [UILabel new];
    canceledLabel.textColor = [UIColor whiteColor];
    canceledLabel.text = NSLocalizedString(@"ORDER_CANCELED", @"Canceled");
    [canceledLabel sizeToFit];
    
    __weak typeof(self) weakSelf = self;
    
    [self setSwipeGestureWithView:canceledLabel color:[UIColor orderCanceledColor] mode:MCSwipeTableViewCellModeSwitch state:state completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
        
        [weakSelf.orderModel updateOrderStatus:OrderCanceled];
        [weakSelf setOrderStatus];
        
    }];

    
}
@end
