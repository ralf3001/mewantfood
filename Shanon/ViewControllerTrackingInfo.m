//
//  ViewControllerTrackingInfo.m
//  Einer
//
//  Created by Ralf Cheung on 5/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "ViewControllerTrackingInfo.h"

@implementation ViewControllerTrackingInfo


-(id)initWithViewControllerString: (NSString *)vcString type: (ViewControllerTrackingType)type{
    
    self = [super self];
    
    if (self) {
        
        self.viewControllerInString = vcString;
        self.timeElapsed = 0;
        self.pageType = type;
        
    }
    
    return self;
    
}


@end
