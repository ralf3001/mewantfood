//
//  AppDelegate.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "EinerUserTracking.h"
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "OIHTabBarViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;

@property(readonly, strong, nonatomic)
    NSManagedObjectContext *managedObjectContext;
@property(readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property(readonly, strong, nonatomic)
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic, readonly) int networkStatus;

@property(nonatomic) OIHTabBarViewController *tabBar;


- (BOOL)isNetworkReachable;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)switchToMerchant:(BOOL)isMerchant;
- (void)logout;

@end
