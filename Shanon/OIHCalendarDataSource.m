//
//  OIHCalendarDataSource.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 5/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCalendarDataSource.h"
#import "OIHCalendarMealView.h"

@interface OIHCalendarDataSource () <CalendarMealViewDelegate>

@end


@implementation OIHCalendarDataSource

-(id)initWithRestaurant: (OIHRestaurantModel *)restaurant{

  self = [super init];
  if (self) {

    _restaurantInfo = restaurant;
    _mealList = [[NSMutableSet alloc] init];
    _mealViews = [NSMutableArray new];

  }
  return self;
}


- (void)mealSummaryFetchFutureEvents:(OIHCalendarInfiniteScrollView *)calendar
                                date:(NSDate *)date {
  
  if ([self fetchFutureData:date]) {
    
    JSONRequestSettings *setting =
    [[JSONRequestSettings alloc] initWithRoute:@"meal" httpMethod:HTTP_GET];
    NSMutableDictionary *data = [NSMutableDictionary new];
    
    if (date) {
      data[@"availableDate"] = [date JSONDateFromDate];
    }
    
    data[@"restaurant"] = _restaurantInfo._id;
    data[@"forward"] = [NSNumber numberWithBool:YES];
    setting.requestData = data;
    
    [[JSONRequest sharedInstance]
     fetch:setting
     completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
       if (!error) {
         
         if (((NSArray *)data).count > 0) {
           NSArray *array = [OIHMealModel mealsFromArray:data];
           if (_mealList.count == 0) {
             [_mealList addObjectsFromArray:array];
             [calendar initialFetch];     //yes, i know this is extremely hacky, populating the cells initailly
           }else{
             [_mealList addObjectsFromArray:array];
             [calendar reloadView];

           }
         }
       }

       calendar.isFetchingFuture = NO;

     }];
    
  }else{
    calendar.isFetchingFuture = NO;

  }
  
}

- (void)mealSummaryFetchPastEvents:(OIHCalendarInfiniteScrollView *)calendar
                              date:(NSDate *)date {
  
  if ([self fetchPastData:date]) {
    
    JSONRequestSettings *setting =
    [[JSONRequestSettings alloc] initWithRoute:@"meal" httpMethod:HTTP_GET];
    NSMutableDictionary *data = [NSMutableDictionary new];
    
    if (date) {
      data[@"availableDate"] = [date JSONDateFromDate];
    }
    
    data[@"restaurant"] = _restaurantInfo._id;
    data[@"forward"] = [NSNumber numberWithBool:NO];
    setting.requestData = data;
    
    [[JSONRequest sharedInstance]
     fetch:setting
     completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
       if (!error) {
         
         if (((NSArray *)data).count > 0) {
           NSMutableArray *array = [OIHMealModel mealsFromArray:data];
           [_mealList addObjectsFromArray:array];
           [calendar reloadView];
         }
       
       }
       calendar.isFetchingPast = NO;

     }];

  }else{
    calendar.isFetchingPast = NO;

  }
  
}

- (NSArray *)mealsForDate:(NSDate *)date {
  
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%@ >= availableDate && %@ <= availableEndDate)", date, date];
  NSSet *set = [_mealList filteredSetUsingPredicate:predicate];
  
  return [set allObjects];
}


-(NSArray *)mealSummaryCellForCalendar:(OIHCalendarInfiniteScrollView *)calendar onDate:(NSDate *)date{
  
  NSMutableArray *mealViews = [NSMutableArray new];
  
  for (OIHMealModel *meal in [self mealsForDate:date]) {
    
    OIHCalendarMealView *mealView =
    [[OIHCalendarMealView alloc] initWithFrame:CGRectZero];
    mealView.meal = meal;
    mealView.date = date;
    mealView.delegate = self;

    [mealViews addObject:mealView];
  }
  
  
  return mealViews;
}


- (BOOL)fetchFutureData:(NSDate *)date {
  
  NSDate *latestMealDate = [_mealList valueForKeyPath:@"@max.availableDate"];
  if (latestMealDate) {
    return ([latestMealDate compare:date] == NSOrderedDescending);
  }
  return YES;
}

- (BOOL)fetchPastData:(NSDate *)date {
  
  NSDate *earliestMealDate = [_mealList valueForKeyPath:@"@min.availableDate"];
  if (earliestMealDate) {
    return ([earliestMealDate compare:date] == NSOrderedDescending);
  }
  return YES;
}

-(void)calendarViewDidTap: (OIHCalendarMealView *)mealView{
  
  OIHMealModel *meal = ((OIHCalendarMealView *)mealView).meal;
  
  if ([self.delegate respondsToSelector:@selector(calendarDidTapContentView:)]) {
    [self.delegate calendarDidTapContentView: meal];
  }
  
}

@end
