//
//  OIHCompetitionInfoTableViewCell.h
//  Shanon
//
//  Created by Ralf Cheung on 14/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHFloatingView.h"
#import "OIHCompetitionInfo.h"

@protocol OIHCompetitionInfoTableViewCellDelegate <NSObject>
@required;
-(void)userDidDismissCompetition:(OIHCompetitionInfo *)competition;

@end

@interface OIHCompetitionInfoTableViewCell : UITableViewCell

@property (nonatomic, weak) id<OIHCompetitionInfoTableViewCellDelegate> delegate;

@property (nonatomic) OIHFloatingView *floatingView;
@property (nonatomic) NSString *competitionTitle;
@property (nonatomic) NSString *competitionDescription;

@property (nonatomic, weak) OIHCompetitionInfo *competitionInfo;


@end
