//
//  OIHEmptyView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHEmptyView.h"


@interface OIHEmptyView ()
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *descriptionLabel;
@end

@implementation OIHEmptyView
@synthesize reloadButton;
@synthesize imageView;
@synthesize descriptionLabel;


-(id)initWithImage: (UIImage *)image descriptionText: (NSString *)descriptionText{
    
    self = [super init];
    if (self) {
        [self commonInit];
        imageView.image = image;
      NSMutableParagraphStyle *para = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
      para.lineSpacing = 3;
      para.alignment = NSTextAlignmentCenter;
      NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:@{NSParagraphStyleAttributeName: para}];
      descriptionLabel.attributedText = attrString;

    }
    
    return self;
}

-(void)commonInit{
    
    imageView = [[UIImageView alloc] init];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageView];
    
    descriptionLabel = [UILabel new];
    descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    descriptionLabel.textColor = [UIColor lightGrayColor];
    descriptionLabel.numberOfLines = 0;
    [self addSubview:descriptionLabel];
    
    reloadButton = [UIButton buttonWithType:UIButtonTypeSystem];
    reloadButton.translatesAutoresizingMaskIntoConstraints = NO;
    [reloadButton setTitle:NSLocalizedString(@"RELOAD", @"Reload") forState:UIControlStateNormal];
    reloadButton.layer.cornerRadius = 5.0f;
    reloadButton.layer.borderColor = reloadButton.tintColor.CGColor;
    reloadButton.layer.borderWidth = 0.5f;
    UIEdgeInsets insets = reloadButton.contentEdgeInsets;
    reloadButton.contentEdgeInsets = UIEdgeInsetsMake(insets.top + 15, insets.left + 15, insets.bottom + 15, insets.right + 15);
    [reloadButton addTarget:self action:@selector(reloadData:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:reloadButton];
    
    
    
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:20.0f]];

    [self addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];

    [self addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:250]];

    [self addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:descriptionLabel attribute:NSLayoutAttributeTop multiplier:1.0f constant:-50.0f]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0 constant:100.0f]];

    
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:reloadButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:descriptionLabel attribute:NSLayoutAttributeBottom multiplier:1.0f constant:20.0f]];
  
    [self addConstraint:[NSLayoutConstraint constraintWithItem:reloadButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];


}

-(void)setReloadButtonTitle:(NSString *)title{
  
  if (title) {
    [self.reloadButton setTitle:title forState:UIControlStateNormal];
    [self.reloadButton setNeedsLayout];
  }

}

-(void)reloadData: (UIButton *)button{
 
    if ([self.delegate respondsToSelector:@selector(emptyViewDidPressReloadButton:)]) {
        [self.delegate emptyViewDidPressReloadButton:self];
    }
    
}


@end
