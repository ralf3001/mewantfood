//
//  OIHRestaurantInfoOwnerCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantInfoOwnerCollectionViewCell.h"

#define kImageViewHeight 90


@implementation OIHRestaurantInfoOwnerCollectionViewCell
-(id)initWithFrame:(CGRect)frame{
  
  self = [super initWithFrame:frame];
  if (self) {
    
    _nameLabel = [UILabel new];
    _nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _nameLabel.numberOfLines = 0;
    _nameLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-Thin" size:28.0f];
    [self.contentView addSubview:_nameLabel];

    UILabel *ownerLabel = [UILabel new];
    ownerLabel.translatesAutoresizingMaskIntoConstraints = NO;
    ownerLabel.text = @"Owner";
    [self.contentView addSubview:ownerLabel];

    _imageView = [UIImageView new];
    _imageView.translatesAutoresizingMaskIntoConstraints = NO;
    _imageView.layer.cornerRadius = kImageViewHeight / 2;
    _imageView.layer.borderWidth = 2.0f;
    _imageView.layer.borderColor = [UIColor highlightColor].CGColor;

    _imageView.layer.masksToBounds = YES;
    [self.contentView addSubview:_imageView];
    
    UIView *separator = [UIView new];
    separator.translatesAutoresizingMaskIntoConstraints = NO;
    separator.backgroundColor = [UIColor highlightColor];
    [self.contentView addSubview:separator];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[separator]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(separator)]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_imageView attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_imageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];

    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:ownerLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:_nameLabel attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0.0f]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_imageView(==height)]-[_nameLabel]-|" options:0 metrics:@{@"height": @(kImageViewHeight)} views:NSDictionaryOfVariableBindings(_imageView, _nameLabel)]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[ownerLabel]-[_nameLabel]-[separator(==1)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_nameLabel, ownerLabel, separator)]];
    
    
  }
  return self;
  
}

-(void)setUser:(OIHUserModel *)user{
  
  _user = user;
  
  self.nameLabel.text = _user.userName;

  [self.imageView sd_setImageWithURL:[NSURL URLWithString:_user.profilePicURL] placeholderImage:[UIImage imageFromColor: [UIColor highlightColor]]];
  
}


@end
