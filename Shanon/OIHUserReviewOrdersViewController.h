//
//  OIHUserReviewOrdersViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"

@interface OIHUserReviewOrdersViewController : UIViewController

-(id)initWithPersonProfile: (OIHUserModel *)profile;


@end
