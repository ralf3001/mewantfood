//
//  OIHOnDiskCache.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHOnDiskCache.h"
#import "OIHCacheObject.h"
#import <CommonCrypto/CommonDigest.h>

@interface OIHOnDiskCache () <NSCacheDelegate>
@property (nonatomic) NSCache *cache;
//@property (nonatomic) NSURL
@property (nonatomic) dispatch_queue_t writeDiskQueue;
@property (nonatomic) dispatch_queue_t readDiskQueue;
@property (nonatomic) NSURL *directory;
@end

@implementation OIHOnDiskCache
@synthesize cache;
@synthesize writeDiskQueue, readDiskQueue;
@synthesize directory;


//cache policy
//
+(id)sharedCache{
    
    static OIHOnDiskCache *_sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[OIHOnDiskCache alloc] init];
    });
    return _sharedInstance;
}

-(id)init{
    
    
    self = [super init];
    if (self) {
        cache = [[NSCache alloc] init];
        cache.evictsObjectsWithDiscardedContent = YES;
        cache.delegate = self;
        writeDiskQueue = dispatch_queue_create("com.oih.cache.writeToDiskQueue", DISPATCH_QUEUE_SERIAL);
        readDiskQueue = dispatch_queue_create("com.oih.cache.readFromDiskQueue", DISPATCH_QUEUE_SERIAL);
        
        NSError *error = nil;
        
        NSString *d = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        d = [d stringByAppendingPathComponent:@"com.ralfcheung.Oh-It-s-Here"];
        d = [d stringByAppendingPathComponent:@"cache"];
        
        [[NSFileManager defaultManager] createDirectoryAtPath:d withIntermediateDirectories:YES attributes:nil error:&error];
        if (error) {
            NSLog(@"[ERROR] from Cache: %@", [error localizedDescription]);
        }
        
        directory = [NSURL URLWithString:d];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearCache) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
        
    }
    return self;
}

-(id)loadObjectFromCache: (NSString *)key{
    
    __block OIHCacheObject *obj = [cache objectForKey:key];
    
    if (!obj) {
        dispatch_sync(readDiskQueue, ^{
            obj = [NSKeyedUnarchiver unarchiveObjectWithFile:[self cacheURLFromKey:key].path];
        });
        //try load object from disk
    }
    
    return obj.object;
    
}


-(NSURL *)cacheURLFromKey: (NSString *)key{
    
    NSString *hashedKey = [self md5:key];
    return [self keyToURL:hashedKey];
    
}

-(NSURL *)isKeyCached: (NSString *)key{
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[[self cacheURLFromKey:key] absoluteString]];
    if (fileExists) {
        return [self cacheURLFromKey:key];
    }else
        return nil;
    
}


-(void)cacheObject: (id)object withKey: (NSString *)key toDisk: (BOOL) writeToDisk{
    
    
    NSString *hashedKey = [self md5:key];
    
    OIHCacheObject *cacheObject = [[OIHCacheObject alloc] init];
    cacheObject.object = object;
    cacheObject.key = hashedKey;
    
    [cache setObject:object forKey:hashedKey];
    
    dispatch_async(writeDiskQueue, ^{
        NSURL *url = [self keyToURL:hashedKey];
        [NSKeyedArchiver archiveRootObject:cacheObject toFile:url.path];
    });
    
    
}



-(void)deleteCache: (NSString *)key{
    
    NSString *hashedKey = [self md5:key];
    
    [cache removeObjectForKey:hashedKey];
    dispatch_async(writeDiskQueue, ^{
        NSURL *url = [self keyToURL:key];
        
        BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath: url.path];
        if(exists) {
            NSError *err;
            [[NSFileManager defaultManager]removeItemAtPath: url.path error:&err];
            if (err) {
                NSLog(@"%@", [err localizedDescription]);
            }
        }
        
    });
}


-(void)sortCacheByDate{
    
    //    NSError *error = nil;
    //    NSDictionary* properties = [[NSFileManager defaultManager]
    //                                attributesOfItemAtPath:NSFileModificationDate
    //                                error:&error];
    
    
    
}


-(void)cache:(NSCache *)cache willEvictObject:(id)obj{
    
#ifdef DEBUG
    NSLog(@"[MESSAGE]: cache evicting object");
#endif
    
}

-(void)clearCache{
    
#ifdef DEBUG
    NSLog(@"nscache purging");
#endif
    
    [cache removeAllObjects];
}

-(void)clearAllCache{
    
    dispatch_async(writeDiskQueue, ^{
        
        [self clearCache];
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
        NSEnumerator *e = [contents objectEnumerator];
        NSString *filename;

        while ((filename = [e nextObject])) {
            
            [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];
        
        }
        
//        d = [d stringByAppendingPathComponent:[[NSBundle mainBundle] bundleIdentifier]];
//        d = [d stringByAppendingPathComponent:@"cache"];
        
//        NSError *error = nil;
        
//        [[NSFileManager defaultManager] removeItemAtPath:d error:&error];
        
    });
}

-(NSString *)md5: (NSString *)str {
    
    const char *ptr = [str UTF8String];
    
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(ptr, (CC_LONG)strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}




-(NSURL *)keyToURL: (NSString *)key{
    
    NSString *trimmedKey = [key stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    NSURL *url = [NSURL URLWithString:trimmedKey];
    url = [[directory URLByAppendingPathComponent:trimmedKey] URLByAppendingPathExtension:@"cache"];
    
    return url;
    
}




@end
