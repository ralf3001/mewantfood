//
//  OIHHomeScreenReviewTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 19/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHHomeScreenReviewTableViewCell.h"


#define kImageViewWidth 80.0

@interface OIHHomeScreenReviewTableViewCell ()
@property (nonatomic) CAGradientLayer *gradientLayer;
@property (nonatomic) UIView *longPressedCircleView;
@property (nonatomic) NSLayoutConstraint *heightConstraint;
@property (nonatomic) NSLayoutConstraint *imageViewWidthConstraint;


@end

@implementation OIHHomeScreenReviewTableViewCell

@synthesize floatingView;

@synthesize imageView;
@synthesize textView;
@synthesize descriptionLabel;
@synthesize thanksButton;
@synthesize commentButton;

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{

  [floatingView setHighlight:highlighted];
  
}


-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
  
}



-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
                
        floatingView = [OIHFloatingView new];
        floatingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:floatingView];

        UIView *descriptionDivider = [UIView new];
        descriptionDivider.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1.0f];
        descriptionDivider.translatesAutoresizingMaskIntoConstraints = NO;
        [floatingView addSubview:descriptionDivider];
        
        
        textView = [[UITextView alloc] init];
        textView.translatesAutoresizingMaskIntoConstraints = NO;
        textView.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
      
        UIEdgeInsets inset = textView.contentInset;
        inset.top = -8;
        textView.contentInset = inset;
        textView.editable = NO;
        textView.scrollEnabled = NO;
        textView.userInteractionEnabled = NO;
        [floatingView addSubview:textView];

        descriptionLabel = [UILabel new];
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        descriptionLabel.numberOfLines = 0;
        descriptionLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];

        [floatingView addSubview:descriptionLabel];

        
        _timeLabel = [UILabel new];
        _timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _timeLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
        _timeLabel.textColor = [UIColor darkGrayColor];
        [floatingView addSubview:_timeLabel];
        
        imageView = [UIImageView new];
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = kImageViewWidth / 2;
        imageView.layer.borderColor = [UIColor colorWithWhite:0.95 alpha:1.0f].CGColor;
        imageView.layer.borderWidth = 2.0f;

        [floatingView addSubview:imageView];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[floatingView]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(floatingView)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[floatingView]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(floatingView)]];

        
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descriptionLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionLabel)]];
        
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_timeLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_timeLabel)]];

        
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descriptionDivider]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionDivider)]];

        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(textView)]];
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[descriptionLabel]-[_timeLabel]-[descriptionDivider(==1)]-[textView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionLabel, textView, descriptionDivider, _timeLabel)]];
        
        self.heightConstraint = [NSLayoutConstraint constraintWithItem:textView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:kImageViewWidth];
        
        [floatingView addConstraint:self.heightConstraint];
        
        [floatingView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];

        [floatingView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:descriptionDivider attribute:NSLayoutAttributeBottom multiplier:1.0f constant:10.0f]];

        [floatingView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:floatingView attribute:NSLayoutAttributeLeft multiplier:1.0f constant:10.0f]];
        
        [floatingView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:kImageViewWidth]];
    
    }
    return self;
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    CGRect dragViewFrame = self.imageView.frame;
    dragViewFrame.size.width += 10;
    
    CGRect exclusionRect = [self.floatingView convertRect:dragViewFrame toView:self.imageView];
    UIBezierPath *exclusion = [UIBezierPath bezierPathWithRect:exclusionRect];
    self.textView.textContainer.exclusionPaths = @[exclusion];

    if (!_gradientLayer) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
//            _gradientLayer = [CAGradientLayer layer];
//            _gradientLayer.frame = textView.frame;
//
//            _gradientLayer.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:1.0f alpha:0.0f] CGColor], (id)[[UIColor colorWithWhite:1.0 alpha:1.0] CGColor], nil];
//            _gradientLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.6],[NSNumber numberWithInt:1], nil];
            
//            [floatingView.layer insertSublayer:_gradientLayer above:textView.layer];
            
        });
    }
    

}


-(void)longPress: (UITapGestureRecognizer *)gesture{
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:{
            CGPoint center = [gesture locationInView:self];
            _longPressedCircleView = [[UIView alloc] initWithFrame:CGRectMake(center.x, center.y, 50, 50)];
            _longPressedCircleView.layer.cornerRadius = 25.0f;
            _longPressedCircleView.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0.8];
            [floatingView addSubview:_longPressedCircleView];
            break;
        }
        case UIGestureRecognizerStateChanged:{
            CGRect frame = _longPressedCircleView.frame;
            frame.size.height += 5;
            frame.size.width += 5;
            _longPressedCircleView.frame = frame;
            _longPressedCircleView.layer.cornerRadius = frame.size.width / 2;
            break;
        }
        case UIGestureRecognizerStateEnded:
            [_longPressedCircleView removeFromSuperview];
            break;
        default:
            break;
    }
    
}

-(void)setFeed:(OIHActivityFeed *)feed{
    
    _feed = feed;
    
    if (_feed.content) {
        
        if ([feed.fromUser._id isEqualToString:[UserProfile currentUser]._id]) {
            self.descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"YOU_REVIEWED_%@_BY_%@", @"you reviewed [some item] by [restaurant]"), feed.order.orderMeal.name, feed.order.restaurant.restaurantName];
        }else{
            self.descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_REVIEWED_%@_BY_%@", @"[person] reviewed [some item] by [restaurant]"), feed.fromUser.userName, feed.order.orderMeal.name, feed.order.restaurant.restaurantName];
        }
        self.textView.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];

        self.textView.text = _feed.content;
        
    }else{
        if ([feed.fromUser._id isEqualToString:[UserProfile currentUser]._id]) {
            self.descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"YOU_RATED_%@_BY_%@", @"you rated [some item] by [restaurant]"), feed.order.orderMeal.name, feed.order.restaurant.restaurantName];
        }else{
            self.descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_RATED_%@_BY_%@", @"[person] rated [some item] by [restaurant]"), feed.fromUser.userName, feed.order.orderMeal.name, feed.order.restaurant.restaurantName];
        }
        
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"USER_RATING", @"User Rating")];
        [attr appendAttributedString:[[NSAttributedString alloc] initWithString:[self emoji:_feed.rating] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:40]}]];
        self.textView.attributedText = attr;
    }
    

    CGSize newSize = [textView sizeThatFits:CGSizeMake(CGRectGetWidth(floatingView.frame) - 20, MAXFLOAT)];
    self.heightConstraint.constant = MAX(80, MIN(newSize.height, 100));
//    [_tableView beginUpdates];
//    [_tableView endUpdates];

    self.timeLabel.text = [NSDate daysBetweenDate:feed.createdAt andDate:[NSDate date]];

    if (_feed.isPostThanked) {
      thanksButton.titleLabel.font = [UIFont buttonTextBold];
    }else{
      thanksButton.titleLabel.font = [UIFont activityButtonNormal];
    }
    

}


-(void)thanksButtonPressed: (UIButton *)button{
  
  if (_feed.isPostThanked == YES) {
    
    thanksButton.titleLabel.font = [UIFont activityButtonNormal];

    if ([self.delegate respondsToSelector:@selector(userDidTapThanksButton:isPostThanked:)]) {
      [self.delegate userDidTapThanksButton:_feed isPostThanked:NO];
    }
    _feed.isPostThanked = NO;

  }else{
    
    thanksButton.titleLabel.font = [UIFont buttonTextBold];
    
    if ([self.delegate respondsToSelector:@selector(userDidTapThanksButton:isPostThanked:)]) {
      [self.delegate userDidTapThanksButton:_feed isPostThanked:YES];
    }
    _feed.isPostThanked = YES;

  }
  
}


-(void)commentButtonPressed: (UIButton *)button{
  
  if ([self.delegate respondsToSelector:@selector(userDidTapCommentButton:)]) {
    [self.delegate userDidTapCommentButton:self.feed];
  }

}

-(NSString *)emoji: (NSInteger) rating{
    
    switch (rating) {
        case 0:
            //angry face
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\xA1"];
            break;
        case 1:
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x9E"];
            // //disappointing face
            break;
            
        case 2:
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\xB7"];
            break;
        case 3:
            //delicious face
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x8B"];
            break;
        case 4:
            //throw a kiss face, love it
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x8D"];
            break;
            
        default:
            return nil;
            
}

}


@end
