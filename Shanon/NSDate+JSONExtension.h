//
//  NSDate+JSONExtension.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 23/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (JSONExtension)

-(NSString *)JSONDateFromDate;
+(NSDate *)dateFromJSONDate: (NSString *)date;
+(NSString *)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
-(BOOL)onSameDay: (NSDate *)date;

+(BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate;
+(BOOL)date:(NSDate *)date isGTE:(NSDate *)beginDate LTE:(NSDate *)endDate;

@end
