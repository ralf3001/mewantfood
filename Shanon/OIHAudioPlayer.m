//
//  OIHAudioPlayer.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 27/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHAudioPlayer.h"
#import "OIHOnDiskCache.h"
#import <AVFoundation/AVFoundation.h>

@interface OIHAudioPlayer ()
@property(nonatomic) AVPlayer *player;
@property(nonatomic) AVAssetExportSession *exportSession;

@end

@implementation OIHAudioPlayer

+ (OIHAudioPlayer *)audioPlayer {
    
    static OIHAudioPlayer *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[OIHAudioPlayer alloc] init];
        
    });
    return _sharedInstance;
}

- (id)init {
    
    if (self) {
        _player = [AVPlayer new];
        [_player addObserver:self forKeyPath:@"status" options:0 context:nil];
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(playerItemDidReachEnd:)
         name:AVPlayerItemDidPlayToEndTimeNotification
         object:[_player currentItem]];
        [self audioSession];
    }
    
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if (object == _player && [keyPath isEqualToString:@"status"]) {
        if (_player.status == AVPlayerStatusFailed) {
            [[NSNotificationCenter defaultCenter]
             postNotificationName:OIHAudioPlayerAVPlayerStatusDidFail
             object:nil];
        } else if (_player.status == AVPlayerStatusReadyToPlay) {
            
        } else if (_player.status == AVPlayerItemStatusUnknown) {
        }
    }
}

- (void)playAudioWithURLString:(NSString *)urlString {
    return [self playAudioWithURL:[NSURL URLWithString:urlString]];
}

- (void)playAudioWithURL:(NSURL *)url {
    
    NSString *filename = [[url absoluteString] lastPathComponent];
    NSURL *audioURL = nil;
    
    if ((audioURL = [[OIHOnDiskCache sharedCache] isKeyCached:filename])) {
        url = audioURL;
    }
    
    _currentlyPlaying = url;
    
    AVPlayerItem *item = [AVPlayerItem playerItemWithURL:_currentlyPlaying];
    [[self player] replaceCurrentItemWithPlayerItem:item];
    [[self player] play];
    
    return;
}

- (NSString *)currentPlaying {
    return _currentlyPlaying.absoluteString;
}

- (void)audioSession {
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error = nil;
    BOOL result = NO;
    
    if ([audioSession
         respondsToSelector:@selector(setActive:withOptions:error:)]) {
        result = [audioSession setActive:YES withOptions:0 error:&error]; // iOS6+
    }
    
    if (!result && error) {
        // deal with the error
    }
    
    error = nil;
    result =
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (!result && error) {
        // deal with the error
    }
}

- (void)stop {
    [[self player] pause];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
}

- (AVPlayer *)player {
    return _player;
}

- (NSNotificationCenter *)notificationCenter {
    return [NSNotificationCenter defaultCenter];
}

- (BOOL)isPlaying {
    return ((_player.rate != 0) && (_player.error == nil));
}

@end
