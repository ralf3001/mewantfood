//
//  OIHUserRestaurantOrdersViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHRestaurantModel.h"


@interface OIHUserRestaurantOrdersViewController : UIViewController

@property (nonatomic) OIHRestaurantModel *restaurantInfo;

@end
