//
//  OIHMerchantRestaurantCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 25/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHRestaurantModel.h"


@interface OIHMerchantRestaurantCollectionViewCell : UICollectionViewCell

@property (nonatomic) UILabel *restaurantNameLabel;
@property (nonatomic) UILabel *restaurantAddressLabel;
@property (nonatomic) UILabel *restaurantOperatingHours;


-(void)setAttributes: (OIHRestaurantModel *)restaurant;

@end
