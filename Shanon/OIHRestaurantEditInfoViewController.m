//
//  OIHRestaurantEditInfoViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMealInfoTableViewCell.h"
#import "OIHRestaurantEditInfoViewController.h"

@interface OIHRestaurantEditInfoViewController () <
UITableViewDelegate, UITableViewDataSource,
OIHMealInfoTableViewCellDelegate>

@property(nonatomic) UITableView *tableView;
@property(nonatomic) NSMutableDictionary *editedData;
@end

@implementation OIHRestaurantEditInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _editedData = [NSMutableDictionary new];
    
    [self createTableView];
}

- (void)createTableView {
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                              style:UITableViewStyleGrouped];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [_tableView registerClass:[OIHMealInfoTableViewCell class]
       forCellReuseIdentifier:@"cell"];
    
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    
    _tableView.estimatedRowHeight = 130;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    doneButton.frame = CGRectMake(0, 0, self.view.frame.size.width, 80);
    [doneButton setTitle:NSLocalizedString(@"DONE", @"Done")
                forState:UIControlStateNormal];
    doneButton.backgroundColor = [UIColor colorWithRed:46 / 256.0f
                                                 green:65 / 256.0f
                                                  blue:114 / 256.0f
                                                 alpha:1.0f];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton addTarget:self
                   action:@selector(doneDidPress)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIView *footerView = [[UIView alloc]
                          initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 80)];
    
    [footerView addSubview:doneButton];
    _tableView.tableFooterView = footerView;
    
    [self.view addSubview:_tableView];
    
    [self.view addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"H:|[_tableView]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           _tableView)]];
    
    [self.view addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"V:|[_tableView]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           _tableView)]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHMealInfoTableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:@"cell"
                                    forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"RESTAURANT_NAME", @"Restaurant Name");
            cell.descriptionContentLabel.text = _restaurantModel.restaurantName;
            cell.cellAttribute = @"restaurantName";
            break;
        case 1:
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"RESTAURANT_ADDRESS", "Restaurant Address");
            cell.descriptionContentLabel.text = _restaurantModel.addressInfo;
            cell.cellAttribute = @"addressString";
            break;
        case 2:
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"RESTAURANT_NOTES", "Restaurant notes");
            cell.cellAttribute = @"sellersNote";
            cell.descriptionContentLabel.text = _restaurantModel.sellersNote;
            break;
        case 3:
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"RESTAURANT_PHONE", "Restaurant Phone");
            cell.cellAttribute = @"phoneNumber";
            cell.descriptionContentLabel.text = _restaurantModel.phoneNumber;
            break;
        default:
            break;
    }
    
    cell.tableView = tableView;
    cell.descriptionContentLabel.editable = YES;
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)textViewFinishedEditing:(UITextView *)textView
                           cell:(OIHMealInfoTableViewCell *)cell {
    
    _editedData[cell.cellAttribute] = [textView.text
                                       stringByTrimmingCharactersInSet:[NSCharacterSet
                                                                        whitespaceAndNewlineCharacterSet]];
    
    [UIView setAnimationsEnabled:NO];
    [_tableView reloadData];
    [UIView setAnimationsEnabled:YES];
}

- (void)doneDidPress {
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc]
                                    initWithRoute:[NSString
                                                   stringWithFormat:@"restaurant/%@", _restaurantModel._id]
                                    httpMethod:HTTP_PUT];
    
    setting.requestData = _editedData;
    
    [[JSONRequest sharedInstance]
     fetch:setting
     completionHandler:^(id data, BOOL isCachedResult, NSError *error){
         
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little
 preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
