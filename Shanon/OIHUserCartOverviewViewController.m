//
//  OIHUserCartOverviewViewController.m
//  Shanon
//
//  Created by Ralf Cheung on 23/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserCartOverviewViewController.h"
#import "OIHMealModel.h"
#import "OIHUserCartItemTableViewCell.h"
#import "OIHRestaurantMealDetailViewController.h"



@interface OIHUserCartOverviewViewController () <UITableViewDelegate, UITableViewDataSource, OIHEmptyViewDelegate, OIHMealDetailDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSMutableArray <OIHMealModel *> *cartItems;
@property (nonatomic) OIHEmptyView *emptyView;
@end

@implementation OIHUserCartOverviewViewController
@synthesize emptyView;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _cartItems = [NSMutableArray new];
    
    [self fetchCart];
}

-(void)createEmptyView{
    
    if (emptyView) {
        [emptyView removeFromSuperview];
        emptyView = nil;
    }
    
    [_tableView removeFromSuperview];
    _tableView = nil;
    
    emptyView = [[OIHEmptyView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_list_outline size:90 color:[UIColor lightGrayColor]] descriptionText:NSLocalizedString(@"HOME_SCREEN_HAS_NO_STORIES", @"")];
    
    emptyView.translatesAutoresizingMaskIntoConstraints = NO;
    emptyView.delegate = self;
    [self.view addSubview:emptyView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
    
    
}


-(void)createTableView{
    
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 90;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.separatorColor = [UIColor clearColor];
        
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 40)];
        _tableView.tableFooterView = footerView;
        
        [_tableView registerClass:[OIHUserCartItemTableViewCell class] forCellReuseIdentifier:@"cell"];
        [self.view addSubview:_tableView];
        
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        
    }else{
        [_tableView reloadData];
    }
    
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _cartItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OIHUserCartItemTableViewCell *cell = (OIHUserCartItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    OIHMealModel *meal = _cartItems[indexPath.row];
    cell.itemInfo = meal;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    OIHRestaurantMealDetailViewController *vc = [OIHRestaurantMealDetailViewController new];
    vc.mealInfo = _cartItems[indexPath.row];
    vc.delegate = self;
    
    [self presentViewController:vc animated:YES completion:nil];

}


-(void)userDidPressConfirmOrder: (OIHRestaurantModel *)restaurantInfo  viewController: (OIHRestaurantMealDetailViewController *)viewController{

    [viewController dismissViewControllerAnimated:YES completion:nil];

    
}


-(void)userDidCancelOrder: (OIHRestaurantMealDetailViewController *)viewController{
    
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)fetchCart{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"cart" httpMethod:HTTP_GET];
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            [self populateData:data toArray:_cartItems isCacheData:isCachedResult];
        }
    }];
    
    
}


-(void)populateData: (NSArray *)data toArray: (NSMutableArray *)array isCacheData: (BOOL) isCache{
    
    NSArray *feedArray = [OIHMealModel mealsFromArray:data];
    
    if(isCache){
        
        //initial fetch
        array = [NSMutableArray arrayWithArray:feedArray];
        array.isDataDirty = [NSNumber numberWithBool:isCache];
        
    }else{  //new data, flush cache data in the array
        
        if (array.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            //new data from network
            if (data.count > 0) {
                array = [NSMutableArray arrayWithArray:feedArray];
                array.isDataDirty = [NSNumber numberWithBool:NO];
            }else{
                return [self createEmptyView];
            }
            
        }else{
            
            //new data from network, load another page
            if (data.count > 0) {
                [array addObjectsFromArray:feedArray];
                
            }else{
//                hasMoreStories = NO;
            }
        }
        
    }
    
    [self createTableView];
}


-(void)emptyViewDidPressReloadButton:(OIHEmptyView *)emptyView{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
