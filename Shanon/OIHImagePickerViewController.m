
//
//  OIHImagePickerViewController.m
//  Shanon
//
//  Created by Ralf Cheung on 5/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHImagePickerViewController.h"
#import <CoreGraphics/CoreGraphics.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import <ImageIO/CGImageSource.h>
#import <ImageIO/CGImageProperties.h>
#import <Photos/Photos.h>

@interface OIHImagePickerViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end

@implementation OIHImagePickerViewController


-(id)init{
    
    self = [super init];
    if (self) {
        _imagePicker = [[UIImagePickerController alloc] init];
        
        if ([UIImagePickerController availableMediaTypesForSourceType:
             UIImagePickerControllerSourceTypeCamera]
            .count > 0) {
            _imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else {
            _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        _imagePicker.delegate = self;

    }
    return self;
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
   
//    NSURL *referenceURL = [info objectForKey:UIImagePickerControllerReferenceURL];
//    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//    [library assetForURL:referenceURL resultBlock:^(ALAsset *asset) {
//        ALAssetRepresentation *rep = [asset defaultRepresentation];
//        NSDictionary *metadata = rep.metadata;
//        NSLog(@"%@", metadata);
//        
//        CGImageRef iref = [rep fullScreenImage] ;
//        
//        if (iref) {
////            self.imageView.image = [UIImage imageWithCGImage:iref];
//        }
//    } failureBlock:^(NSError *error) {
//        // error handling
//    }];

//    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
//        
//    }];
    
//    PHPhotoLibrary *pLibrary = [PHPhotoLibrary sharedPhotoLibrary];
//    pLibrary
//    PHFetchResult<PHAsset *> *asset = [PHAsset fetchAssetsWithALAssetURLs:@[referenceURL] options:nil];
    
}





@end
