//
//  OIHUserSettingObject.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHUserSettingObject : NSObject
@property (nonatomic) NSString *settingTitle;
@property (nonatomic) SEL settingMethod;
@property (nonatomic) UIImage *settingIconImage;

-(id)initWithTitle: (NSString *)title method: (SEL)method icon: (UIImage *)image;

@end
