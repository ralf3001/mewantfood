//
//  OIHTabBarViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHTabBar.h"
#import <UIKit/UIKit.h>

@interface OIHTabBarViewController : UIViewController <OIHTabBarDelegate>

@property(nonatomic) NSArray *viewControllers;
@property(nonatomic) NSInteger selectedIndex;

- (void)setSelectedTabBarIndex:(NSInteger)index;

- (void)setNotificationForTabBarItem:(NSInteger)tab;
- (void)removeNotificationForTabBarItem:(NSInteger)tab;
- (id)initWithViewControllers:(NSArray *)viewControllers;

- (NSInteger)tabBarHeight;

@end
