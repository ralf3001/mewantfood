//
//  OIHRestaurantMealInfoCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantMealInfoCollectionViewCell.h"
#import "OIHMealModel.h"
#import "NSNumberFormatter+OIHCurrencyNumberFormatter.h"


@implementation OIHRestaurantMealInfoCollectionViewCell

@synthesize backgroundImageView;
@synthesize dishLabel;
@synthesize restaurantLabel;
@synthesize priceLabel;




-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        backgroundImageView = [UIImageView new];
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
        backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        backgroundImageView.layer.masksToBounds = YES;
        [self.contentView addSubview:backgroundImageView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundImageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(backgroundImageView)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundImageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(backgroundImageView)]];

        
        UIView *overlay = [UIView new];
        overlay.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        overlay.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:overlay];
        
        dishLabel = [UILabel new];
        dishLabel.numberOfLines = 0;
        dishLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleTitle1];
        dishLabel.textColor = [UIColor whiteColor];
        dishLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [overlay addSubview:dishLabel];
        
        
        restaurantLabel = [UILabel new];
        restaurantLabel.numberOfLines = 0;
        restaurantLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1.0f];
        restaurantLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        restaurantLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [overlay addSubview:restaurantLabel];
        
        
        priceLabel = [UILabel new];
        priceLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1.0f];
        priceLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [overlay addSubview:priceLabel];
        
        
        [overlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[restaurantLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(restaurantLabel)]];
        [overlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[dishLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(dishLabel)]];
        [overlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[priceLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(priceLabel)]];
        
        
        [overlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[dishLabel(>=80)]-[restaurantLabel]-[priceLabel(==restaurantLabel)]-|" options:NSLayoutFormatAlignAllLeft metrics:0 views:NSDictionaryOfVariableBindings(restaurantLabel, dishLabel, priceLabel)]];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[overlay]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(overlay)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[overlay]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(overlay)]];

        
        
    }

    return self;
}

-(void)setMealModel:(OIHMealModel *)mealModel{
    
    _mealModel = mealModel;
    self.restaurantLabel.text = mealModel.restaurant.restaurantName;
    self.dishLabel.text = mealModel.name;

    NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: mealModel.restaurant.restaurantCurrency}];

    NSNumberFormatter *currency = [NSNumberFormatter formatterWithCurrency:mealModel.restaurant.restaurantCurrency];
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:mealModel.priceInString locale:[NSLocale localeWithLocaleIdentifier:localeIde]];
    NSDecimalNumber *surcharge = [NSDecimalNumber decimalNumberWithDecimal:[mealModel.chargeRate decimalValue]];
    
    NSDecimalNumber *total = [price decimalNumberByMultiplyingBy:surcharge];
    self.priceLabel.text = [currency stringFromNumber: total];

    
    [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:mealModel.productImage]];
    
}


-(void)setRestaurantModel:(OIHRestaurantModel *)restaurantModel{
    
    _restaurantModel = restaurantModel;
    self.restaurantLabel.text = restaurantModel.restaurantName;

    self.dishLabel.text = restaurantModel.meal.name;
    
    
    
    NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: restaurantModel.restaurantCurrency}];
    
    
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:restaurantModel.meal.priceInString locale:[NSLocale localeWithLocaleIdentifier:localeIde]];
    NSDecimalNumber *surcharge = [NSDecimalNumber decimalNumberWithDecimal:[restaurantModel.meal.chargeRate decimalValue]];
    
    NSDecimalNumber *total = [price decimalNumberByMultiplyingBy:surcharge];
    
    
    NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
    [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currency setLocale:[NSLocale localeWithLocaleIdentifier:localeIde]];

    self.priceLabel.text = [currency stringFromNumber: total];
    
    [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:restaurantModel.meal.productImage]];
    
}



@end
