//
//  OIHFriendRequestTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 13/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHFriendRequestTableViewCell.h"
#import "UIImage+Color.h"


@interface OIHFriendRequestTableViewCell ()
@property (nonatomic) UIImageView *profileImageView;

@property (nonatomic) NSLayoutConstraint *constraint;
@end


@implementation OIHFriendRequestTableViewCell
@synthesize actionButton;
@synthesize nameLabel;
@synthesize constraint;
@synthesize profileImageView;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
 
        
        profileImageView = [[UIImageView alloc] init];
        profileImageView.translatesAutoresizingMaskIntoConstraints = NO;
        profileImageView.layer.cornerRadius = 30.0f;
        profileImageView.contentMode = UIViewContentModeScaleAspectFill;
        profileImageView.layer.borderWidth = 0.6f;
        profileImageView.layer.masksToBounds = YES;
        profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        profileImageView.image = [UIImage imageFromColor:[UIColor lightGrayColor]];
        [self.contentView addSubview:profileImageView];

        
        actionButton = [UIButton buttonWithType:UIButtonTypeSystem];
        actionButton.translatesAutoresizingMaskIntoConstraints = NO;
        [actionButton setTitle:@"Done" forState:UIControlStateNormal];
        [actionButton addTarget:self action:@selector(actionPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:actionButton];
        
        nameLabel = [UILabel new];
        nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        nameLabel.numberOfLines = 0;
        [self.contentView addSubview:nameLabel];
        
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:profileImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:60]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:profileImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:profileImageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[profileImageView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(profileImageView)]];

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:actionButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[profileImageView]-15-[nameLabel]-[actionButton(==80)]-|" options:NSLayoutFormatAlignAllTop metrics:0 views:NSDictionaryOfVariableBindings(profileImageView, nameLabel, actionButton)]];

        
//        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[nameLabel]-[actionButton(==40)]-|" options:NSLayoutFormatAlignAllCenterY metrics:0 views:NSDictionaryOfVariableBindings(nameLabel, actionButton)]];

//        constraint = [NSLayoutConstraint constraintWithItem:nameLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0f constant:8.0];
//        constraint.priority = 800;
//        
//        [self.contentView addConstraint:constraint];
        
        
    }
    
    return self;
}


-(void)actionPressed: (UIButton *)button{
    
    if ([self.delegate respondsToSelector:@selector(userDidPressActionButton:cell:)]) {
        [self.delegate userDidPressActionButton:button cell:self];
    }
    
}


@end
