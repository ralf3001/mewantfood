//
//  OIHUserOrderListTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 25/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHOrderModel.h"
#import "MCSwipeTableViewCell.h"

@interface OIHUserOrderListTableViewCell : MCSwipeTableViewCell

@property (nonatomic) OIHOrderModel *order;

@property (nonatomic) UILabel *orderStatusLabel;
@property (nonatomic) UILabel *orderItemNameLabel;
@property (nonatomic) UILabel *orderRestaurantLabel;
@property (nonatomic) UILabel *orderIdLabel;
@property (nonatomic) UILabel *orderDateLabel;
@property (nonatomic) UILabel *orderRecipientLabel;
@property (nonatomic) UILabel *orderUser;
@property (nonatomic) UILabel *orderRecipientAddressLabel;
@property (nonatomic) UILabel *orderQuantityLabel;
@property (nonatomic) UILabel *orderPriceLabel;

@property (nonatomic) UIView *orderReadLabel;

@property (nonatomic) UIView *orderStatusColorLabel;

@property (nonatomic) UIImageView *orderImageView;

-(void)setStatusLabel;
-(void)setAckBubble: (BOOL) isAcked;

@end
