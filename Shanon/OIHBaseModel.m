//
//  OIHBaseModel.m
//  Shanon
//
//  Created by Ralf Cheung on 8/5/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHBaseModel.h"

@implementation OIHBaseModel

-(id)initWithInfo: (NSDictionary *)info{

    if (self = [super init]) {
        self._id = info[@"_id"];
    }
    return self;
}
@end
