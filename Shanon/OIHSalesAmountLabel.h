//
//  OIHSalesAmountLabel.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHPriceWithAnimationLabel.h"

typedef NSString* (^CurrencyLoop)(float value, NSString *currencyCode);

@interface OIHSalesAmountLabel : OIHPriceWithAnimationLabel

@property (nonatomic, copy) CurrencyLoop currencyAnimationLoop;
@property (nonatomic) NSString *currencySymbol;

@end
