//
//  OIHPopupContentView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHPopupContentView : UIView
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *descriptionLabel;
@property (assign) BOOL showCancelButton;

@property (nonatomic) UIView *imageViewBackground;

@property (nonatomic) UIButton *doneButton;
@property (nonatomic) UIButton *cancelButton;


@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descriptionText;


-(id)initWithTitleText: (NSString *)title descriptionText: (NSString *)descriptionText withImage: (UIImage *)image;

@end
