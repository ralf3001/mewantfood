//
//  UITextField+HighlightFrame.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "UITextField+HighlightFrame.h"
#import <objc/runtime.h>

static char const* const isModifiedKey = "isModified";

@implementation UITextField (HighlightFrame)

-(void)hightlightBorder: (UIColor *)highlightColor{

    if (!self.isModifying) {

        self.isModifying = YES;
        __block UIColor *originalTextColor = self.textColor;
        __block CGColorRef originalBorderColor = self.layer.borderColor;
        
        self.textColor = highlightColor;
        self.layer.borderColor = highlightColor.CGColor;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.textColor = originalTextColor;
            self.layer.borderColor = originalBorderColor;
            self.isModifying = NO;
        });
    }
    
}


-(BOOL)isModifying{
    NSNumber *number = objc_getAssociatedObject(self, isModifiedKey);
    return [number boolValue];
}

-(void)setIsModifying:(BOOL)isModifying{
    
    NSNumber *number = [NSNumber numberWithBool: isModifying];
    objc_setAssociatedObject(self, isModifiedKey, number, OBJC_ASSOCIATION_RETAIN);
    
}

@end
