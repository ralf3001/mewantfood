//
//  OIHReceiptModel.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 1/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHReceiptModel : NSObject

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *braintreeReceiptId;
@property (nonatomic) NSNumber *totalAmount;


-(id)initWithInfo: (NSDictionary *)info;

@end
