//
//  JSONRequestSettings.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "JSONRequestSettings.h"

@implementation JSONRequestSettings

-(id)initWithRoute: (NSString *)route httpMethod: (RequestHTTPMethod) method{
    
    self = [super init];
    
    if (self) {
        self.route = route;
        self.HTTPMethod = method;
        self.cachePolicy = CacheFirstThenLoadData;
    }
    
    return self;
}

-(id)init{
    
    self = [super init];
    if (self) {
        self.cachePolicy = UseCacheIfTimeout;
    }
    return self;
}

+ (NSDictionary *)typeDisplayNames{
    
    return @{@(HTTP_GET) : @"GET",
             @(HTTP_POST) : @"POST",
             @(HTTP_DELETE) : @"DELETE",
             @(HTTP_PUT) : @"PUT"};
}

-(NSString *)httpMethod{
    return [[self class] typeDisplayNames][@(self.HTTPMethod)];
}

@end
