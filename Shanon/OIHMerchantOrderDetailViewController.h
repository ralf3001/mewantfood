//
//  OIHMerchantOrderDetailViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHOrderModel.h"

@interface OIHMerchantOrderDetailViewController : UIViewController
@property (nonatomic) OIHOrderModel *orderInfo;

@end
