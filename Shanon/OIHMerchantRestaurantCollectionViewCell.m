//
//  OIHMerchantRestaurantCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 25/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHMerchantRestaurantCollectionViewCell.h"

@implementation OIHMerchantRestaurantCollectionViewCell
@synthesize restaurantAddressLabel, restaurantNameLabel, restaurantOperatingHours;

-(void)setHighlighted:(BOOL)highlighted{
  
  if (highlighted) {
    self.contentView.backgroundColor = [UIColor highlightColor];
  }else{
    self.contentView.backgroundColor = [UIColor whiteColor];
  }
  
}

-(void)setSelected:(BOOL)selected{
  
  if (selected) {
    self.contentView.backgroundColor = [UIColor highlightColor];
  }else{
    self.contentView.backgroundColor = [UIColor whiteColor];
  }
  
}


- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        restaurantAddressLabel = [UILabel new];
        restaurantAddressLabel.numberOfLines = 0;
        restaurantAddressLabel.textColor = [UIColor grayColor];
        restaurantAddressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:restaurantAddressLabel];
        
        restaurantNameLabel = [UILabel new];
        restaurantNameLabel.textColor = [UIColor blackColor];
        restaurantNameLabel.numberOfLines = 0;
        restaurantNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:restaurantNameLabel];
        
        restaurantOperatingHours = [UILabel new];
        restaurantOperatingHours.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:restaurantOperatingHours];
        
        
        UIView *bottomLine = [UIView new];
        bottomLine.translatesAutoresizingMaskIntoConstraints = NO;
        bottomLine.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
        [self.contentView addSubview:bottomLine];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[bottomLine]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(bottomLine)]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[restaurantNameLabel]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(restaurantNameLabel)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[restaurantAddressLabel]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(restaurantAddressLabel)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[restaurantNameLabel]-10-[restaurantAddressLabel]-[bottomLine(==1)]|" options:NSLayoutFormatAlignAllLeading metrics:0 views:NSDictionaryOfVariableBindings(restaurantNameLabel, restaurantAddressLabel, bottomLine)]];

    }
    return self;
}


-(void)setAttributes: (OIHRestaurantModel *)restaurant{
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                (id)[UIFont fontWithName:@"HelveticaNeue-Thin" size:25.0f], NSFontAttributeName,
                                (id)[UIColor blackColor], NSForegroundColorAttributeName, nil];
    
    NSString *correctString = restaurant.restaurantName;
    
    NSAttributedString *attrText;
    attrText = [[NSAttributedString alloc] initWithString:correctString
                                               attributes:attributes];

    self.restaurantNameLabel.attributedText = attrText;
//    self.restaurantNameLabel.text = restaurant.restaurantName;
    self.restaurantAddressLabel.text = restaurant.addressInfo;
    
}


@end
