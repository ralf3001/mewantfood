//
//  OIHTabBarItemView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHTabBarItemView : UIView

@property (nonatomic) UIView *redDotView;

-(id)initWithButton: (UIButton *)button;
-(void)setNotificationCountForView: (NSInteger)notifications;

@end
