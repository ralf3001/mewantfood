//
//  OIHCalendarDataSource.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 5/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHCalendarInfiniteScrollView.h"
#import "OIHRestaurantModel.h"
#import "OIHMealModel.h"
#import "OIHCalendarDay.h"
#import "OIHCalendarMealView.h"


@protocol CalendarDataSourceDelegate <NSObject>

@optional
-(void)calendarDidTapContentView: (OIHMealModel *)meal;

@end

@interface OIHCalendarDataSource : NSObject <OIHMealSummaryViewDelegate>
@property (nonatomic) OIHRestaurantModel *restaurantInfo;
@property (nonatomic, weak) id <CalendarDataSourceDelegate> delegate;
@property(nonatomic) NSMutableArray<OIHCalendarMealView *> *mealViews;
@property(nonatomic) NSMutableSet<OIHMealModel *> *mealList;

-(id)initWithRestaurant: (OIHRestaurantModel *)restaurant;

@end
