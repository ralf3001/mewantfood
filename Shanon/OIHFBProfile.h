//
//  OIHFBProfile.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface OIHFBProfile : NSObject <NSSecureCoding>
@property (nonatomic, strong) FBSDKProfile *profile;
@property (nonatomic, strong) FBSDKAccessToken *token;


@end
