//
//  OIHMessageInputView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SLK_IS_LANDSCAPE         ([[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeLeft || [[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeRight)
#define SLK_IS_IPAD              ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define SLK_IS_IPHONE            ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#define SLK_IS_IPHONE4           (SLK_IS_IPHONE && [UIScreen mainScreen].bounds.size.height < 568.0)
#define SLK_IS_IPHONE5           (SLK_IS_IPHONE && [UIScreen mainScreen].bounds.size.height == 568.0)
#define SLK_IS_IPHONE6           (SLK_IS_IPHONE && [UIScreen mainScreen].bounds.size.height == 667.0)
#define SLK_IS_IPHONE6PLUS       (SLK_IS_IPHONE && [UIScreen mainScreen].bounds.size.height == 736.0 || [UIScreen mainScreen].bounds.size.width == 736.0) // Both orientations
#define SLK_IS_IOS8_AND_HIGHER   ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)
#define SLK_IS_IOS9_AND_HIGHER   ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0)


@protocol OIHMessageInputDelegate <NSObject>

-(void)messageInputViewDidPressSendButton:(NSString *)text;

@end

@interface OIHMessageInputView : UIView <UITextViewDelegate>

@property (nonatomic) UITextView *inputView;
@property (nonatomic) UIButton *sendButton;

@property (nonatomic, readwrite) NSUInteger maxNumberOfLines;

/** The current displayed number of lines. */
@property (nonatomic, readonly) NSUInteger numberOfLines;

@property (nonatomic, weak) id <OIHMessageInputDelegate> delegate;
@end
