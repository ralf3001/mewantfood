//
//  OIHUserCardsListViewController.m
//  Shanon
//
//  Created by Ralf Cheung on 25/5/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserCardsListViewController.h"
#import "OIHSettingTableViewCell.h"
#import <Braintree/BraintreeCore.h>
#import "CardIO.h"
#import "BraintreeCard.h"

@interface OIHUserCardsListViewController () <UITableViewDelegate, UITableViewDataSource, CardIOPaymentViewControllerDelegate>

@property (nonatomic) UITableView *tableView;
@property (nonatomic) BTAPIClient *braintreeClient;

@end

@implementation OIHUserCardsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addCard)];

    [self fetchCards];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self fetchClientToken];
    
    //success case
    //378282246310005
    //3530111333300000
    //4012000077777777
    
    //fail case
    //5105105105105100
    //4000111111111115
    //378734493671000
    
    [CardIOUtilities preload];

}

-(void)fetchCards{
    
    JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:@"cards" httpMethod:HTTP_GET];
    
    [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            NSLog(@"%@", data);
        }else{
            NSLog(@"%@", [error localizedDescription]);
        }
        
    }];
    
}

-(void)fetchClientToken{
    
    [[JSONRequest sharedInstance] fetch:@"client_token" method:@"GET" data:nil completionHandler:^(id data, NSError *error) {
        if (!error) {
            self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:data[@"clientToken"]];
        }
    }];
    
}



-(void)addCard{
    
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:scanViewController animated:YES completion:nil];

}


-(void)setupTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 90;
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 40)];
    
    _tableView.tableFooterView = footerView;
    
    [_tableView registerClass:[OIHSettingTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    
}


- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"Scan succeeded with info: %@", info);
    // Do whatever needs to be done to deliver the purchased items.
    [self dismissViewControllerAnimated:YES completion:nil];
    
//    NSLog(@"%@", [NSString stringWithFormat:@"Received card info. Number: %@ Number: %@, expiry: %02lu/%lu, cvv: %@.", info.cardNumber, info.redactedCardNumber, (unsigned long)info.expiryMonth, (unsigned long)info.expiryYear, info.cvv]);
    
    BTCardClient *cardClient = [[BTCardClient alloc] initWithAPIClient:self.braintreeClient];
    BTCard *card = [[BTCard alloc] initWithNumber:info.cardNumber
                                  expirationMonth:[NSString stringWithFormat:@"%ld", (unsigned long)info.expiryMonth]
                                   expirationYear:[NSString stringWithFormat:@"%ld", (unsigned long)info.expiryYear]
                                              cvv:info.cvv];
    [cardClient tokenizeCard:card
                  completion:^(BTCardNonce *tokenizedCard, NSError *error) {
                      // Communicate the tokenizedCard.nonce to your server, or handle error
                      
                      if (error) {
                          NSLog(@"%@", [error localizedDescription]);
                      }else{
                          NSLog(@"%@", tokenizedCard.nonce);
                          [self sendCard:tokenizedCard.nonce];
                      }
                      
                  }];

    
    
}

-(void)sendCard: (NSString *)nonce{
    
    JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:@"cards" httpMethod:HTTP_POST];
    
    settings.requestData = @{@"nonce": nonce};
    
    [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            NSLog(@"done");
        }else{
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
    
}


- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"User cancelled scan");
    [self dismissViewControllerAnimated:YES completion:nil];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
