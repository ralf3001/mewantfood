//
//  OIHStepperTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 18/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OIHSteppTableViewCellDelegate <NSObject>

-(void)stepperValueDidChange: (UIStepper *)stepper;

@end


@interface OIHStepperTableViewCell : UITableViewCell
@property (nonatomic) UIStepper *quantityStepper;
@property (nonatomic) UILabel *cellDescriptionLabel;
@property (nonatomic) UILabel *cellContentLabel;

@property (nonatomic) UILabel *cellWarningLabel;
@property (nonatomic, weak) id<OIHSteppTableViewCellDelegate> delegate;
@end
