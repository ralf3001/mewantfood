//
//  OIHMessageFooterLabel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMessageFooterLabel.h"

@implementation OIHMessageFooterLabel

@synthesize topInset, leftInset, rightInset, bottomInset;

- (void)drawTextInRect:(CGRect)uiLabelRect {
    
    [super drawTextInRect:UIEdgeInsetsInsetRect(uiLabelRect, UIEdgeInsetsMake(10, 10, 10, 10))];

}


- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines{
    
    UIEdgeInsets insets = UIEdgeInsetsMake(10, 10, 10, 10);
    CGRect rect = [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, insets)
                    limitedToNumberOfLines:numberOfLines];
    
    rect.origin.x    -= insets.left;
    rect.origin.y    -= insets.top;
    rect.size.width  += (insets.left + insets.right);
    rect.size.height += (insets.top + insets.bottom);
    
    return rect;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
