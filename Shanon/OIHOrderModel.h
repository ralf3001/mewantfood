//
//  OIHOrderModel.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHMealModel.h"
#import "OIHRestaurantModel.h"
#import "OIHModelProtocol.h"
#import "OIHUserModel.h"
#import "OIHAddressModel.h"
#import "OIHReceiptModel.h"


typedef NS_ENUM(NSInteger, OrderStatus) {
    
    Ordered = 0,
    OrderAccepted = 1,
    OrderDelivered = 2,
    OrderCanceled = 3
    
};

@interface OIHOrderModel : NSObject <OIHModelProtocol>
@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *sellerRestaurantId;
@property (nonatomic) OIHRestaurantModel *restaurant;
@property (nonatomic) OIHUserModel *orderUser;
@property (nonatomic) OIHUserModel *orderRecipient;
@property (nonatomic) NSDate *orderDate;
@property (nonatomic) NSString *orderMealId;
@property (nonatomic) OIHMealModel *orderMeal;
@property (nonatomic) OrderStatus status;
@property (nonatomic) NSInteger quantity;
@property (nonatomic) OIHAddressModel *recipientAddress;
@property (nonatomic) NSString *audioNoteURL;

@property (nonatomic) NSNumber *totalAmount;

@property (nonatomic) OIHReceiptModel *orderReceipt;

@property (nonatomic) NSDictionary *infoDictionary;

@property (assign) BOOL isAcknowledged;

+(NSMutableArray *)initFromArray: (NSArray *)orders;
-(id)initWithOrderInfo: (NSDictionary *)info;
-(NSString *)orderStatus;
-(UIColor *)statusColor;

-(void)updateOrderStatus: (OrderStatus) updatedStatus;

@end
