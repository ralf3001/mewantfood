
//  OIHPieSliceLayer.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 18/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHPieSliceLayer.h"

@implementation OIHPieSliceLayer
@synthesize fillColor, lineWidth, strokeColor;
@dynamic startAngle, endAngle;

-(id<CAAction>)actionForKey:(NSString *)event{
  
  if ([event isEqualToString:@"startAngle"] || [event isEqualToString:@"endAngle"]) {
    return [self makeAnimationForKey: event];
  }
  
  return [super actionForKey:event];
  
}


-(id)initWithLayer:(id)layer{
  
  if (self = [super initWithLayer:layer]) {
    if ([layer isKindOfClass:[OIHPieSliceLayer class]]) {
      OIHPieSliceLayer *other = (OIHPieSliceLayer *)layer;
      self.startAngle = other.startAngle;
      self.endAngle = other.endAngle;
      self.fillColor = other.fillColor;
      
      self.strokeColor = other.strokeColor;
      self.lineWidth = other.lineWidth;

    }
  }
  return self;
}

-(CABasicAnimation *)makeAnimationForKey:(NSString *)key {
  CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:key];
  anim.fromValue = [[self presentationLayer] valueForKey:key];
  anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
  anim.duration = 0.5;
  
  return anim;
}


-(void)drawInContext:(CGContextRef)ctx{
  
  CGPoint center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
  CGFloat radius = MIN(center.x, center.y);
  
  CGContextBeginPath(ctx);
  CGContextMoveToPoint(ctx, center.x, center.y);
  
  CGPoint p1 = CGPointMake(center.x + radius * cos(self.startAngle), center.y + radius * sin(self.startAngle));
  
  CGContextAddLineToPoint(ctx, p1.x, p1.y);
  
  int clockwise = self.startAngle > self.endAngle;
  CGContextAddArc(ctx, center.x, center.y, radius, self.startAngle, self.endAngle, clockwise);
  
  CGContextClosePath(ctx);
  
  CGContextSetFillColorWithColor(ctx, self.fillColor.CGColor);
  CGContextSetStrokeColorWithColor(ctx, self.strokeColor.CGColor);
  CGContextSetLineWidth(ctx, self.lineWidth);
  
  CGContextDrawPath(ctx, kCGPathFillStroke);
  
}


+(BOOL)needsDisplayForKey:(NSString *)key{
  if ([key isEqualToString:@"startAngle"] || [key isEqualToString:@"endAngle"]) {
    return YES;
  }
  return [super needsDisplayForKey:key];
}

@end
