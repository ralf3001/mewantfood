//
//  OIHActivityFeed.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 23/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHActivityFeed.h"
#import "NSDate+JSONExtension.h"


@implementation OIHActivityFeed
@synthesize _id;
@synthesize fromUserId, toUserId;
@synthesize type;
@synthesize createdAt;

-(id)initWithActivity: (NSDictionary *)activity{
    
    self = [super init];
    if (self) {
        
        self._id = activity[kOIHActivityId];
        self.createdAt = [NSDate dateFromJSONDate:activity[kOIHActivityCreatedAt]];
        self.fromUserId = activity[kOIHActivityFromUser];
        self.toUserId = activity[kOIHActivityToUser];
        self.type = [self typeEnumFromString:activity[kOIHActivityType]];
        self.rating = [activity[@"rating"] integerValue];
        
        if ([activity[kOIHActivityFromUser] isKindOfClass:[NSDictionary class]]) {
            self.fromUser = [[OIHUserModel alloc] initWithUserInfo:activity[kOIHActivityFromUser]];
        }
        if ([activity[kOIHActivityToUser] isKindOfClass:[NSDictionary class]]) {
            self.toUser = [[OIHUserModel alloc] initWithUserInfo:activity[kOIHActivityToUser]];
        }
        if ([activity[kOIHActivityOrder] isKindOfClass:[NSDictionary class]]) {
            self.order = [[OIHOrderModel alloc] initWithOrderInfo:activity[kOIHActivityOrder]];
        }
        if ([activity[@"meal"] isKindOfClass:[NSDictionary class]]) {
            self.meal = [[OIHMealModel alloc] initWithInfoDictionary:activity[@"meal"]];
        }
        
        
        self.audioNote = activity[@"audioNote"];
        self.content = activity[@"content"];
        self.postPicture = activity[@"postPicture"];
        if (activity[@"userAlreadyThank"]) {
            self.isPostThanked = [activity[@"userAlreadyThank"] boolValue];
        }
        
        if ([activity[@"activity"] isKindOfClass:[NSDictionary class]]) {
            self.parentActivity = [[OIHActivityFeed alloc] initWithActivity:activity[@"activity"]];
        }
        
        
    }
    return self;
}

+(NSArray *) activitiesFromArray: (NSArray *)data{
    
    NSMutableArray *list = [NSMutableArray new];
    
    for (NSDictionary *info in data) {
        OIHActivityFeed *feed = [[OIHActivityFeed alloc] initWithActivity:info];
        [list addObject:feed];
    }

    return list;
    
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self._id forKey:kOIHActivityId];
    [encoder encodeObject:self.createdAt forKey:kOIHActivityCreatedAt];
    [encoder encodeObject:self.fromUserId forKey:kOIHActivityFromUser];
    [encoder encodeObject:self.toUserId forKey:kOIHActivityToUser];
    [encoder encodeObject:self.content forKey:@"content"];
    [encoder encodeObject:self.activityInfo forKey:@"activityInfo"];
    
}

-(FeedType )typeEnumFromString: (NSString *)activityType{
    
    NSDictionary<NSString *, NSNumber *> *types = @{
                                                    @"like": @(UserLike),
                                                    @"orderFood": @(UserOrder),
                                                    @"thank": @(UserThank),
                                                    @"review": @(UserReview),
                                                    @"comment": @(UserComment),
                                                    @"newItem": @(NewItem)
                                                    };
    
    return types[activityType].integerValue;
}

+(NSDictionary *)typeDisplayNames{
    return @{
             @(UserLike): @"like",
             @(UserOrder): @"orderFood",
             @(UserThank): @"thank",
             @(UserReview): @"review",
             @(UserComment): @"comment",
             @(NewItem): @"newItem"
             };
}


-(NSString *)activityType{
    return [[self class] typeDisplayNames][@(self.type)];
}


-(NSDictionary *)toDictionary{
    return self.activityInfo;
}


-(void)setAttribute: (NSString *) attribute data: (id)data{
    
    [self setValue:data forKeyPath:attribute];
    NSError *error = nil;
    
    if ([self validateValue:&data forKeyPath:attribute error:&error]) {
        
    }
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    //override it just to prevent an exception
}


-(void)save{
    return [self save:nil];
}

-(void)save:(void (^)(id, NSError *))completionHandler{
    
    JSONRequestSettings *settings = nil;
    
    NSMutableDictionary *dict = [self dictionaryWithPropertiesOfObject];
    
    [dict removeObjectForKey:@"debugDescription"];
    [dict removeObjectForKey:@"hash"];
    [dict removeObjectForKey:@"superclass"];
    [dict removeObjectForKey:@"description"];
    
    if (self._id) {

        settings = [[JSONRequestSettings alloc] initWithRoute:@"activity" httpMethod:HTTP_PUT];
        settings.requestData = dict;
        
    }else{
        
        settings = [[JSONRequestSettings alloc] initWithRoute:@"activity" httpMethod:HTTP_POST];
        settings.requestData = dict;

    }
    
    [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        if (completionHandler) {
            completionHandler(data, error);
        }
        
    }];
    
}


-(NSMutableDictionary *) dictionaryWithPropertiesOfObject{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i < count; i++) {
        
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        
        if ([self valueForKey:key]) {
            if ([key isEqualToString:@"type"]) {
                [dict setObject:[self activityType] forKey:key];
            }else{
                [dict setObject:[self valueForKey:key] forKey:key];
            }
        }
    }
    
    free(properties);
    
    return dict;
}


-(void)thankActivity :(void (^)(id data, NSError *error))completionHandler{

    self.type = UserThank;
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"activity" httpMethod:HTTP_POST];
    setting.requestData = @{@"activityId": self._id, @"activityType": @"thank"};
    
    [self executeSetting:setting completionHandler:completionHandler];
    
}

-(void)unthankActivity :(void (^)(id data, NSError *error))completionHandler{
    
    self.type = UserThank;

    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"activity" httpMethod:HTTP_DELETE];
    setting.requestData = @{@"activityId": self._id, @"activityType": @"thank"};
    
    [self executeSetting:setting completionHandler:completionHandler];

}

-(void)executeSetting: (JSONRequestSettings *)setting completionHandler: (void (^)(id data, NSError *error))completionHandler{
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (completionHandler) {
            completionHandler(data, error);
        }else{
          if (error) {
            NSLog(@"[ERROR]: %@", [error localizedDescription]);
          }
        }
    }];
    
}


@end
