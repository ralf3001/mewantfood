//
//  OIHAddressModel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHAddressModel.h"

@implementation OIHAddressModel

-(id)initWithAddressInfo: (NSDictionary *)addressInfo{
    
    self = [super init];
    if (self) {
        
        self.addressComponents = addressInfo[kOIHAddressFormattedAddress];
        
        double lng = [addressInfo[@"geometry"][@"coordinates"][0] doubleValue];
        double lat =[addressInfo[@"geometry"][@"coordinates"][1] doubleValue];
        
        self.addressTag = addressInfo[kOIHAddressTag];
        self.coordinates = CLLocationCoordinate2DMake(lat, lng);
        self._id = addressInfo[@"_id"];
        self.userEnteredAddress = addressInfo[kOIHAddressUserEnteredAddress];
        self.formattedAddress = addressInfo[kOIHAddressFormattedAddress];
        
        [self reverseGeocode:self.coordinates];
        
        self.addressDictionary = addressInfo;
        
    }
    return self;
    
}


-(NSDictionary *)toDictionary{
    return self.addressDictionary;
}


+(id)addressFromArray:(NSArray *)addresses{
    
    NSMutableArray *array = [NSMutableArray new];
    for (NSDictionary *dict in addresses) {
        [array addObject:[[OIHAddressModel alloc] initWithAddressInfo:dict]];
    }
    return array;
}


-(void)reverseGeocode: (CLLocationCoordinate2D)coordinates{
    
    CLGeocoder *geocoder = [CLGeocoder new];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinates.latitude longitude:coordinates.longitude];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (!error && placemarks.count > 0) {
            
            CLPlacemark *placemark = [placemarks firstObject];
            
            self.city = placemark.locality;
            self.postalCode = placemark.postalCode;
            self.country = placemark.country;
            self.ISOCountryCode = placemark.ISOcountryCode;
            
        }
        
    }];
    
}



@end
