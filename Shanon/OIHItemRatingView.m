//
//  OIHItemRatingView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHItemRatingView.h"
#import "OIHMessageFooterLabel.h"


#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)

@interface OIHItemRatingView ()
@property (nonatomic) NSMutableArray <UIButton *> *imageViewArray;
@property (nonatomic) NSArray <NSString *> *emojiDescription;
@property (nonatomic) UIColor *emojiLabelColor;

@property (nonatomic) OIHMessageFooterLabel *emojiLabel;
@end

@implementation OIHItemRatingView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithChosenItem: (NSInteger)item{
    
    self = [super init];
    if (self) {
        [self commonInit];
        if (item > 0 && item < _imageViewArray.count) {
            _chosenEmoji = item;
        }
    }
    return self;
}




-(id)init{
    
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    
    _imageViewArray = [NSMutableArray new];
    _chosenEmoji = 100;
    
    _emojiDescription = [NSArray arrayWithObjects:@"Angry with item", @"Meh...", @"Got sick", @"Delicious", @"Love it", nil];
    

    for (int i = 0; i < 5; i++) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:40];
        [self flashAnimationForView:button];
        
        switch (i) {
            case 0:
                //angry face
                [button setTitle:[NSString stringWithUTF8String:"\xF0\x9F\x98\xA1"] forState:UIControlStateNormal];
                
                break;
            case 1:
                [button setTitle:[NSString stringWithUTF8String:"\xF0\x9F\x98\x9E"] forState:UIControlStateNormal];
                // //disappointing face
                break;
                
            case 2:
                [button setTitle:[NSString stringWithUTF8String:"\xF0\x9F\x98\xB7"] forState:UIControlStateNormal];
                break;
            case 3:
                //delicious face
                [button setTitle:[NSString stringWithUTF8String:"\xF0\x9F\x98\x8B"] forState:UIControlStateNormal];
                break;
            case 4:
                //throw a kiss face, love it
                [button setTitle:[NSString stringWithUTF8String:"\xF0\x9F\x98\x8D"] forState:UIControlStateNormal];
                break;
                
            default:
                break;
        }
        
        [_imageViewArray addObject:button];
        
        [self addSubview:button];
        
    }

    _emojiLabelColor = [UIColor colorWithRed:194/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f];;
    
    _emojiLabel = [OIHMessageFooterLabel new];
    _emojiLabel.hidden = YES;
    _emojiLabel.layer.cornerRadius = 10.0f;
    _emojiLabel.layer.masksToBounds = YES;
    _emojiLabel.textColor = [UIColor whiteColor];
    _emojiLabel.backgroundColor = _emojiLabelColor;
    [self bringSubviewToFront:_emojiLabel];
    [self addSubview:_emojiLabel];
    
    
    UILongPressGestureRecognizer *gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    gesture.minimumPressDuration = 0.2;
    [self addGestureRecognizer:gesture];
    
    
}


-(void)longPress:(UILongPressGestureRecognizer *)gesture{
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:{

            break;
        }
        case UIGestureRecognizerStateChanged:{
            CGPoint point = [gesture locationInView: self];
            _chosenEmoji = [self overlapIcon:point];
            break;
        }
        case UIGestureRecognizerStateEnded:{
            if (_chosenEmoji < _imageViewArray.count) {
            
                CGPoint point = [gesture locationInView: self];
                UIButton *button = _imageViewArray[_chosenEmoji];
                if (point.y > button.frame.origin.y + button.frame.size.height + 50 || point.y < button.frame.origin.y - 50) {
                    [self restoreScale:button];
                    _chosenEmoji = 100;
                    
                }
            }

            _emojiLabel.hidden = YES;
            
            break;
        }
        default:
            break;
    }
    
    
}


-(NSInteger)overlapIcon: (CGPoint )point{
    
    NSInteger overlapping = 100;
    
    for (int i = 0; i < _imageViewArray.count; i++) {
        
        UIButton *button = _imageViewArray[i];
        NSString *labelString = _emojiDescription[i];
        
        if(point.x >= button.frame.origin.x && point.x <= (button.frame.size.width + button.frame.origin.x)){
            
            overlapping = i;
            
            CGRect frame = button.frame;
            frame.origin.y -= 60;
            _emojiLabel.hidden = NO;
            _emojiLabel.frame = frame;
            _emojiLabel.text = labelString;
            [_emojiLabel sizeToFit];
            
            //set the frame before scaling, otherwise the origin of the label is going to be everywhere
            [self scaleUpButton:button];
            
        }else{
            [self restoreScale:button];
            
        }

    }
    
    return overlapping;
}



-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if([keyPath isEqualToString:@"chosenEmoji"]){

        NSInteger newValue = [change[NSKeyValueChangeNewKey] integerValue];
        NSInteger oldValue = [change[NSKeyValueChangeOldKey] integerValue];
        
        if (newValue != oldValue) {
            [self restoreScale:_imageViewArray[oldValue]];
            [self scaleUpButton:_imageViewArray[newValue]];
        }else{
            if (newValue != 100) {
                [self restoreScale:_imageViewArray[oldValue]];
            }

        }
        
    }
    
}


-(void) flashAnimationForView: (UIButton *)view{
    
    CABasicAnimation *zRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    zRotation.fromValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(-15)];
    zRotation.toValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(15)];
    zRotation.autoreverses = YES;
    zRotation.removedOnCompletion = NO;
    zRotation.fillMode = kCAFillModeForwards;

    CABasicAnimation *xRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
    xRotation.fromValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(-15)];
    xRotation.toValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(15)];
    xRotation.fillMode = kCAFillModeForwards;
    xRotation.autoreverses = YES;
    xRotation.removedOnCompletion = NO;

    CABasicAnimation *yRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    yRotation.fromValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(-10)];
    yRotation.toValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(10)];
    yRotation.fillMode = kCAFillModeForwards;
    yRotation.autoreverses = YES;
    yRotation.removedOnCompletion = NO;

    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.duration = (float)((arc4random() % 4) + 7) / 10;

    group.repeatCount = FLT_MAX;
    group.autoreverses = YES;
    group.removedOnCompletion = NO;
    group.animations = @[xRotation, yRotation, zRotation ];
    
    [view.layer addAnimation:group forKey:@"allAnimations"];

}


-(void)stopFlashAnimation: (UIView *)view{
    [view.layer removeAnimationForKey:@"allAnimations"];
}

-(void)setChosenEmoji:(NSInteger)chosenEmoji{
    
    if (chosenEmoji < _imageViewArray.count) {
        if (_chosenEmoji == chosenEmoji) {
            UIButton *oldButton = _imageViewArray[_chosenEmoji];
            [self restoreScale:oldButton];
        }else{
            UIButton *button = _imageViewArray[chosenEmoji];
            [self scaleUpButton:button];
            if (_chosenEmoji < _imageViewArray.count) {
                [self restoreScale:_imageViewArray[_chosenEmoji]];
            }
        }
    }

    _chosenEmoji = chosenEmoji;

}

-(void)buttonPressed: (UIButton *)button{
    
    self.chosenEmoji = button.tag;
    
    if ([self.delegate respondsToSelector:@selector(ratingViewDidChooseEmoji:)]) {
        [self.delegate ratingViewDidChooseEmoji:button.tag];
    }
    
}

-(void)scaleUpButton: (UIButton *)button{

    CGAffineTransform t = CGAffineTransformMakeScale(1.3, 1.3);
    t = CGAffineTransformTranslate(t, button.center.x, button.center.y);
    button.transform = t;
    
}

-(void)restoreScale: (UIButton *)button{
    button.transform = CGAffineTransformIdentity;
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    CGFloat width = self.frame.size.width;
    CGFloat buttonWidth = width / _imageViewArray.count;
    CGFloat buttonTotalWidth = self.frame.size.height * _imageViewArray.count;
    CGFloat remainder = width - buttonTotalWidth;
    CGFloat xPos = (remainder / _imageViewArray.count) / 2;

    for (int i = 0; i < _imageViewArray.count; i++) {
        
        UIButton *button = _imageViewArray[i];
        button.frame = CGRectMake(xPos, 0, self.frame.size.height, self.frame.size.height);
        xPos += buttonWidth;
        
    }
    
}




@end
