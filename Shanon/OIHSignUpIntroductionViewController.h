//
//  OIHSignUpIntroductionViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 25/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OIHSignUpIntroductionViewController;

@protocol OIHIntroDelegate <NSObject>

@required
-(void)introWillDismiss: (OIHSignUpIntroductionViewController *) introViewController;

@end


@interface OIHSignUpIntroductionViewController : UIViewController

@property (nonatomic, weak) id<OIHIntroDelegate> delegate;

@end
