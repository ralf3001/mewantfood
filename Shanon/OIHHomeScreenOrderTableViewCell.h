//
//  OIHHomeScreenOrderTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHHomeScreenReviewTableViewCell.h"
#import "OIHActivityCellButtonDelegate.h"
#import "OIHFloatingView.h"

@interface OIHHomeScreenOrderTableViewCell : UITableViewCell

@property (nonatomic) UIImageView *imageView;

@property (nonatomic) UIButton *audioButton;
@property (nonatomic) UIButton *thanksButton;
@property (nonatomic) UIButton *commentButton;

@property (nonatomic) UITextView *textView;
@property (nonatomic) UILabel *descriptionLabel;
@property (nonatomic) UILabel *timeLabel;

@property (weak, nonatomic) OIHActivityFeed *feed;
@property (nonatomic) OIHFloatingView *floatingView;
@property (nonatomic, weak) id<OIHActivityCellButtonDelegate> delegate;

@end
