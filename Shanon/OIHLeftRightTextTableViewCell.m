//
//  OIHLeftRightTextTableViewCell.m
//  Shanon
//
//  Created by Ralf Cheung on 8/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHLeftRightTextTableViewCell.h"

@implementation OIHLeftRightTextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _leftLabel = [UILabel new];
        _leftLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _leftLabel.font = [UIFont preferredHelveticaNeueLightForTextStyle: @"HelveticaNeue"];

        [self.contentView addSubview:_leftLabel];
        
        _rightLabel = [UILabel new];
        _rightLabel.textAlignment = NSTextAlignmentRight;
        _rightLabel.numberOfLines = 0;
        _rightLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _rightLabel.font = [UIFont preferredHelveticaNeueLightForTextStyle:@"HelveticaNeue-Light"];

        [self.contentView addSubview:_rightLabel];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_leftLabel(>=50)]-[_rightLabel(<=120)]-|" options:NSLayoutFormatAlignAllCenterY metrics:0 views:NSDictionaryOfVariableBindings(_leftLabel, _rightLabel)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_rightLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_rightLabel)]];

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:0 multiplier:1.0f constant:60.0f]];
        
        
        
    }
    return self;
}


@end
