//
//  OIHRestaurantSalesMapReduceResult.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantSalesMapReduceResult.h"

@implementation OIHRestaurantSalesMapReduceResult

-(id)initWithInfo: (NSDictionary *)info{
    self = [super init];
    if (self) {
        self.restaurantName = info[kOIHRestaurantName];
        if (info[@"value"]) {
            self.value = [info[@"value"] doubleValue];
        }
        self.salesInRange = [NSArray arrayWithArray:info[@"salesPeriod"]];
    }
    
    return self;
}


+(NSArray *)salesResultsFromArray: (id)data{
    
    NSMutableArray *array = [NSMutableArray new];
    for (NSDictionary *info in data) {
        
        OIHRestaurantSalesMapReduceResult *sales = [[OIHRestaurantSalesMapReduceResult alloc] initWithInfo:info];
        [array addObject:sales];
    }
    return array;
    
}

@end
