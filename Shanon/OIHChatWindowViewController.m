//
//  OIHChatWindowViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHChatWindowViewController.h"
#import "OIHMessageInputView.h"
#import "OIHMessageBubbleTableViewCell.h"
#import "OIHMessageModel.h"
#import "OIHConversation.h"


@interface OIHChatWindowViewController () <UITableViewDelegate, UITableViewDataSource, OIHMessageInputDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSMutableArray *messageList;
@property (nonatomic) OIHMessageInputView *messageInput;
@property CGFloat KEYBOARD_HEIGHT;
@property (nonatomic) NSLayoutConstraint *textInputHeigtConstraint;
@property (nonatomic) OIHConversation *conversation;

@end

@implementation OIHChatWindowViewController

-(id)initWithConversation: (OIHConversation *)conversation withUser: (OIHUserModel *)user{
    
    self = [super init];
    
    if (self) {
        _conversation = conversation;
        _friendProfile = user;
    }
    
    return self;
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMessages:) name:[NSString stringWithFormat:@"%@-%@", OIHMessageUserWillFetchMessage, _conversation._id] object:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _messageList = [NSMutableArray new];
    [self createTableView];
    
    if (!_conversation) {
        [self fetchUserConversation];
    }else{
        [self fetchConversation: nil];
    }
    
    
}


-(void)createConversation{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"conversation" httpMethod:HTTP_POST];
    setting.requestData = @{@"userId": _friendProfile._id};
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            _conversation = [[OIHConversation alloc] initWithDictionary:data];
        }
    }];
    
}

-(void)fetchUserConversation{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"conversation/%@", _friendProfile._id] httpMethod:HTTP_GET];
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error && data) {
            if ([data isKindOfClass:[NSArray class]]) {
                [self populateData:data isCacheData:isCachedResult];
            }else{
                _conversation = [[OIHConversation alloc] initWithDictionary:data];
            }
            
        }
    }];
    
}


-(void)fetchConversation: (NSDate *)date{
    
    JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"message/%@", _conversation._id] httpMethod:HTTP_GET];
    
    if (date) {
        
        settings.requestData = @{@"createDate": [date JSONDateFromDate], @"forward": [NSNumber numberWithBool:YES]};
        settings.cachePolicy = IgnoreCache;
        
        [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            if (!error) {
                [self appendMessages:data];
            }
        }];
        
    }else{
        
        settings.requestData = @{@"forward": [NSNumber numberWithBool:NO]};
        
        [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            if (!error) {
                [self populateData:data isCacheData:isCachedResult];
            }
        }];
        
    }
    
}


-(void)appendMessages: (NSArray *)data{
    
    NSArray *feedArray = [OIHMessageModel initWithArray:data];
    [_messageList addObjectsFromArray:feedArray];
    [_tableView reloadData];
    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_messageList.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}


-(void)fetchConversation{
    [self fetchConversation:nil];
}

-(void)createTableView{
    
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tableView registerClass:[OIHMessageBubbleTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.contentInset = UIEdgeInsetsZero;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
        tap.cancelsTouchesInView = NO;
        [_tableView addGestureRecognizer:tap];
        
        [self.view addSubview:_tableView];
        
        _messageInput = [[OIHMessageInputView alloc] init];
        _messageInput.delegate = self;
        _messageInput.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:_messageInput];
        
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_messageInput]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_messageInput)]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView][_messageInput(==80)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView, _messageInput)]];
        
        
    }else{
        
        [_tableView reloadData];
        if (_messageList.count > 0) {
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([_messageList count] - 1) inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:NO];
            
        }
        
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return _messageList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHMessageBubbleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.message = _messageList[indexPath.row];
    cell.tableView = tableView;
    cell.userInteractionEnabled = NO;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
}



-(void)messageInputViewDidPressSendButton:(NSString *)text{
    
    if (text.length > 0) {
        
        OIHMessageModel *message = [[OIHMessageModel alloc] init];
        message.createdDate = [NSDate date];
        message.content = text;
        message.isSentFromUser = YES;
        
        if (_conversation) {
            message.conversation = _conversation;
            [message sendMessage:nil];
        }else{
            
            _conversation = [[OIHConversation alloc] init];
            [_conversation startConversationWithMessage:message user: _friendProfile completionHandler:^(id data, NSError *error) {
                if (!error) {
                    _conversation = [[OIHConversation alloc] initWithDictionary:data];
                }
            }];
        }
        
        //blast the message
        [_messageList addObject:message];
        [_tableView beginUpdates];
        NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:([_messageList count] - 1) inSection:0]];
        [_tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationRight];
        
        [_tableView endUpdates];
        [_tableView reloadData];
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([_messageList count] - 1) inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:NO];
    }
    
}


- (void)populateData:(NSArray *)data isCacheData:(BOOL)isCache {
    
    NSArray *feedArray = [OIHMessageModel initWithArray:data];
    
    if (isCache) {
        
        // initial fetch
        _messageList = [NSMutableArray arrayWithArray:feedArray];
        _messageList.isDataDirty = [NSNumber numberWithBool:isCache];
        
    } else { // new data, flush cache data in the array
        
        if (_messageList.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            // new data from network
            if (data.count > 0) {
                _messageList = [NSMutableArray arrayWithArray:feedArray];
                _messageList.isDataDirty = [NSNumber numberWithBool:NO];
            }else{
                //[self setupEmptyRestaurantView];
            }
            
        } else {
            
            // new data from network
            if (data.count > 0) {
                [_messageList addObjectsFromArray:feedArray];
                
            }else{
                // hasMoreStories = NO;
            }
            
        }
    }
    [self createTableView];
}

-(void)newMessages: (NSNotification *)notification{
    
    OIHMessageModel *message = [_messageList lastObject];
    
    [self fetchConversation:message.createdDate];
    
}


- (void)dismiss {
    
    [_messageInput.inputView resignFirstResponder];
    
}


- (void)keyboardWillHide:(__unused NSNotification *)notification {
    
    if (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0){
        
        [self setViewMovedUp:NO];
    }
    
    
}

- (void)keyboardWillShow:(__unused NSNotification *)notification {
    
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _KEYBOARD_HEIGHT = kbSize.height;
    
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0){
        [self setViewMovedUp:NO];
    }
    
}



-(void)setViewMovedUp:(BOOL)movedUp{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp){
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= _KEYBOARD_HEIGHT;
        //rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else{
        // revert back to the normal state.
        rect.origin.y += _KEYBOARD_HEIGHT;
        //rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


@end
