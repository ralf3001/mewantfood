//
//  OIHFBCache.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHFBProfile.h"

@interface OIHFBCache : NSObject

+ (void)clearCache;
+ (void)deleteProfile;
+ (void)saveItem:(OIHFBProfile *)item;
+ (OIHFBProfile *)profile;

@end
