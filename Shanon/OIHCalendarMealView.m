//
//  OIHCalendarMealView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCalendarMealView.h"

@interface OIHCalendarMealView () <UIGestureRecognizerDelegate>
@property(nonatomic) UIView *mealIndicatorView;
@property (nonatomic) UITapGestureRecognizer *tapGesture;

@end

@implementation OIHCalendarMealView

- (id)init {

  if ((self = [super init])) {
    [self commonInit];
  }

  return self;
}

- (id)initWithFrame:(CGRect)frame {

  if ((self = [super initWithFrame:frame])) {
    [self commonInit];
  }

  return self;
}

- (void)commonInit {
  
//  self.backgroundColor = [UIColor whiteColor];
  
  _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped)];
  _tapGesture.delegate = self;
  
  [self addGestureRecognizer:_tapGesture];

  _mealNameLabel = [UILabel new];
  _mealNameLabel.numberOfLines = 0;
  _mealNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
  [self addSubview:_mealNameLabel];

  _priceLabel = [UILabel new];
  _priceLabel.numberOfLines = 1;
  _priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
  [self addSubview:_priceLabel];
  
  _mealIndicatorView = [UIView new];
  _mealIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
  [self addSubview:_mealIndicatorView];

  [self addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"H:|[_mealIndicatorView]|"
                                    options:0
                                    metrics:0
                                      views:NSDictionaryOfVariableBindings(
                                                _mealIndicatorView)]];

  [self addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"H:|-[_mealNameLabel]-|"
                                    options:0
                                    metrics:0
                                      views:NSDictionaryOfVariableBindings(
                                                _mealNameLabel)]];
  [self addConstraint:[NSLayoutConstraint constraintWithItem:_priceLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0f constant:10.0f]];
//  [self addConstraints:
//           [NSLayoutConstraint
//            constraintsWithVisualFormat:@"H:|-[_priceLabel]-|"
//                                options:0
//                                metrics:0
//                                views:NSDictionaryOfVariableBindings(
//                                                                     _priceLabel)]];

  [self addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:
                    @"V:|-[_mealNameLabel]-[_priceLabel]-[_mealIndicatorView(==2)]-5-|"
                                    options:0
                                    metrics:0
                                      views:NSDictionaryOfVariableBindings(
                                                _mealNameLabel, _priceLabel,
                                                _mealIndicatorView)]];

}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch {
  return YES;
}


- (void)tapped{
  
  __weak typeof(self) weakSelf = self;
  
  if ([self.delegate respondsToSelector:@selector(calendarViewDidTap:)]) {
    [self.delegate calendarViewDidTap:weakSelf];
  }
  
}

- (void)setMeal:(OIHMealModel *)meal {

  if (!_meal) {
    _meal = meal;
    _mealNameLabel.text = _meal.name;
    [_mealNameLabel sizeToFit];
    
    NSString *currencyCode = _meal.restaurant.restaurantCurrency;
    NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: currencyCode}];
    
    NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
    [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currency setLocale:[NSLocale localeWithLocaleIdentifier:localeIde]];
    
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:_meal.priceInString locale:[NSLocale localeWithLocaleIdentifier:localeIde]];
    
    _priceLabel.text = [currency stringFromNumber: price];
    _priceLabel.adjustsFontSizeToFitWidth = YES;
    [_priceLabel sizeToFit];
    
    UIColor *backgroundColor = nil;
    
    switch (meal.mealType) {
      case Breakfast:
        backgroundColor = self.breakfastBackgroundColor;
        break;
      case Lunch:
        backgroundColor = self.lunchBackgroundColor;
        break;
      case Dinner:
        backgroundColor = self.dinnerBackgroundColor;
        break;
      case Snack:
        backgroundColor = self.snackBackgroundColor;
        break;
      default:
        backgroundColor = self.snackBackgroundColor;
        break;
    }
    
    _mealIndicatorView.backgroundColor = backgroundColor;

  }
  
}

-(void)setDate:(NSDate *)date{
  _date = date;
  if (![_date onSameDay:_meal.availableDate]) {
    _mealNameLabel.text = @"";
    _priceLabel.text = @"";
  }
  
}

- (UIColor *)breakfastBackgroundColor {
  return _breakfastBackgroundColor ? _breakfastBackgroundColor
//                                   : [UIColor colorWithRed:117 / 255.0f
//                                                     green:154 / 255.0f
//                                                      blue:69 / 255.0f
//                                                     alpha:1.0f];
                                    : [UIColor colorWithRed:170 / 255.0f
                                                      green:57 / 255.0f
                                                       blue:67 / 255.0f
                                                      alpha:1.0f];

}

- (UIColor *)lunchBackgroundColor {
  return _lunchBackgroundColor ? _lunchBackgroundColor
//                               : [UIColor colorWithRed:47 / 255.0f
//                                                 green:96 / 255.0f
//                                                  blue:103 / 255.0f
//                                                 alpha:1.0f];
                                  : [UIColor colorWithRed:170 / 255.0f
                                                    green:108 / 255.0f
                                                     blue:57 / 255.0f
                                                    alpha:1.0f];

}

- (UIColor *)dinnerBackgroundColor {
  return _dinnerBackgroundColor ? _dinnerBackgroundColor
//                                : [UIColor colorWithRed:60 / 255.0f
//                                                  green:68 / 255.0f
//                                                   blue:115 / 255.0f
//                                                  alpha:1.0f];
                                  : [UIColor colorWithRed:85 / 255.0f
                                                    green:170 / 255.0f
                                                     blue:85 / 255.0f
                                                    alpha:1.0f];

}

- (UIColor *)snackBackgroundColor {
  return _snackBackgroundColor ? _snackBackgroundColor
                                : [UIColor colorWithRed:34 / 255.0f
                                                  green:102 / 255.0f
                                                   blue:102 / 255.0f
                                                  alpha:1.0f];

  //                               : [UIColor colorWithRed:167 / 255.0f
//                                                 green:56 / 255.0f
//                                                  blue:62 / 255.0f
//                                                 alpha:1.0f];
}

- (UIColor *)mealNameLabelTextColor {
  return _mealNameLabelTextColor ? _mealNameLabelTextColor
                                 : [UIColor whiteColor];
}

- (UIColor *)dateLabelTextColor {
  return _dateLabelTextColor ? _dateLabelTextColor : [UIColor whiteColor];
}
@end
