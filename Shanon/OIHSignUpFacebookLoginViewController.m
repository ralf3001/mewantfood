//
//  OIHSignUpFacebookLoginViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 7/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSignUpFacebookLoginViewController.h"
#import "OIHSocialManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "OIHSignUpIntroductionViewController.h"
//#import "IonIcons.h"

@interface OIHSignUpFacebookLoginViewController ()
@property (nonatomic) UIButton *facebookLoginButton;
@property (nonatomic) UIButton *nextButton;
@property (nonatomic) UILabel *descriptionLabel;
@property (nonatomic) UIImageView *facebookImageView;

@property (nonatomic) UIView *overlayView;

@end

@implementation OIHSignUpFacebookLoginViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  UIColor *fbBlue = [UIColor bc_colorForBrand:@"Facebook"];
  
  self.view.backgroundColor = fbBlue;
  
  _facebookImageView = [[UIImageView alloc] initWithImage:[IonIcons imageWithIcon:ion_social_facebook size:self.view.frame.size.height color:[UIColor whiteColor]]];
  _facebookImageView.contentMode = UIViewContentModeScaleAspectFit;
  _facebookImageView.translatesAutoresizingMaskIntoConstraints = NO;
  [self.view addSubview:_facebookImageView];

  
  [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_facebookImageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_facebookImageView)]];
  
  [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_facebookImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0f constant:130.0f]];

  _overlayView = [UIView new];
  _overlayView.translatesAutoresizingMaskIntoConstraints = NO;
  _overlayView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
  [self.view addSubview:_overlayView];
  
  [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_overlayView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_overlayView)]];
  [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_overlayView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_overlayView)]];

  
  _descriptionLabel = [UILabel new];
  _descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
  _descriptionLabel.text = NSLocalizedString(@"LOGIN_FACEBOOK_DESCRIPTION", @"facebook description");
  _descriptionLabel.textColor = [UIColor whiteColor];
  _descriptionLabel.numberOfLines = 0;
  _descriptionLabel.textAlignment = NSTextAlignmentCenter;
  _descriptionLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleTitle2];
  
  [_overlayView addSubview:_descriptionLabel];
  
  [_overlayView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[_descriptionLabel]-30-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_descriptionLabel)]];

  [_overlayView addConstraint:[NSLayoutConstraint constraintWithItem:_descriptionLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_overlayView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0]];

  
  _facebookLoginButton = [UIButton buttonWithType:UIButtonTypeSystem];
  _facebookLoginButton.translatesAutoresizingMaskIntoConstraints = NO;
  [_facebookLoginButton setTitle:NSLocalizedString(@"SETTING_OPTION_CONNECT_FB", @"Login to Facebook") forState:UIControlStateNormal];
  [_facebookLoginButton addTarget:self action:@selector(facebookLogin) forControlEvents:UIControlEventTouchUpInside];
  [_facebookLoginButton setBackgroundColor:fbBlue];
  [_facebookLoginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  
  [_overlayView addSubview:_facebookLoginButton];
  
  [_overlayView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_facebookLoginButton]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_facebookLoginButton)]];
  
  _nextButton = [UIButton buttonWithType:UIButtonTypeSystem];
  _nextButton.translatesAutoresizingMaskIntoConstraints = NO;
  [_nextButton setTitle:NSLocalizedString(@"NEXT", @"Next") forState:UIControlStateNormal];
  [_nextButton addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
  [_nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  
  [_overlayView insertSubview:_nextButton aboveSubview:_overlayView];

  [_overlayView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_nextButton]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_nextButton)]];

  [_overlayView addConstraint:[NSLayoutConstraint constraintWithItem:_nextButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_overlayView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-15.0f]];

  [_overlayView addConstraint:[NSLayoutConstraint constraintWithItem:_facebookLoginButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:40.0f]];
  
  [_overlayView addConstraint:[NSLayoutConstraint constraintWithItem:_facebookLoginButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_nextButton attribute:NSLayoutAttributeTop multiplier:1.0f constant:-15.0f]];


}

-(void)viewWillAppear:(BOOL)animated{
  
  if ([FBSDKAccessToken currentAccessToken]) {

  }
  
}


-(void)next{
  
  OIHSignUpIntroductionViewController *vc = [OIHSignUpIntroductionViewController new];
  [self.navigationController pushViewController:vc animated:YES];
  
}


-(void)facebookLogin{
  
  [[OIHSocialManager sharedInstance] loginFacebook:self];
  
}


@end
