//
//  OIHDatePickerTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHDatePickerTableViewCell.h"

@implementation OIHDatePickerTableViewCell
@synthesize datePicker;

- (void)awakeFromNib {
  // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {

  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {

    datePicker = [UIDatePicker new];
    datePicker.tag = 99;
    datePicker.minimumDate = [NSDate date];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:datePicker];

    [self.contentView
        addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"V:|[datePicker]|"
                                    options:0
                                    metrics:0
                                      views:NSDictionaryOfVariableBindings(
                                                datePicker)]];
    [self.contentView
        addConstraints:
            [NSLayoutConstraint
                constraintsWithVisualFormat:@"H:|[datePicker]|"
                                    options:0
                                    metrics:0
                                      views:NSDictionaryOfVariableBindings(
                                                datePicker)]];
  }
  return self;
}
@end
