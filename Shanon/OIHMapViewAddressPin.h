//
//  OIHMapViewAddressPin.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "OIHRestaurantModel.h"


@interface OIHMapViewAddressPin : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
// Title and subtitle for use by selection UI.
@property (nonatomic, copy) NSString *title;
@property (nonatomic) NSString *formattedAddress;
@property (nonatomic, weak) OIHRestaurantModel *restaurant;

- (id)initWithCoordinates:(CLLocationCoordinate2D)location placeName:(NSString *)placeName formattedAddress: (NSString *)formattedAddress;


@end
