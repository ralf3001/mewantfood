//
//  OIHDatePickerTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHDatePickerTableViewCell : UITableViewCell
@property(nonatomic) UIDatePicker *datePicker;

@end
