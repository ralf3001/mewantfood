//
//  OIHUserProfileHeaderCollectionReusableView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"


typedef NS_ENUM(NSInteger, UtilityButtonFunction) {
    
    UtilityButtonBuyFoodForUser = 0,
    UtilityButtonUnfriendUser = 1,
    UtilityButtonUserSetProfilePicture = 2
};

@protocol OIHUserProfileHeaderButtonDelegate <NSObject>

-(void)didPressUtilityButton: (UtilityButtonFunction)type;

@end



@interface OIHUserProfileHeaderCollectionReusableView : UICollectionReusableView

@property (nonatomic) UtilityButtonFunction buttonType;
@property (nonatomic) UILabel *nameLabel;
@property (nonatomic) UIButton *personUtilityButton;
@property (nonatomic) UIImageView *imageView;

@property (nonatomic, weak) id<OIHUserProfileHeaderButtonDelegate> delegate;

-(void)setFunctionForUtilityButton: (UtilityButtonFunction) type user: (OIHUserModel *)user;
-(void)setNameLabelText: (NSString *)text;

@end
