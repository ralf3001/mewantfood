//
//  OIHRestaurantMealListViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCalendarInfiniteScrollView.h"
#import "OIHRestaurantMealListViewController.h"
#import "OIHMealModel.h"
#import "OIHCalendarMealView.h"
#import "OIHCalendarDataSource.h"
#import "OIHCalendarMealPopupView.h"

@interface OIHRestaurantMealListViewController () <CalendarDataSourceDelegate>
@property(nonatomic) OIHCalendarInfiniteScrollView *calendar;
@property (nonatomic) OIHCalendarDataSource *calendarDataSource;
@end

@implementation OIHRestaurantMealListViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  self.edgesForExtendedLayout = UIRectEdgeNone;

  self.view.backgroundColor = [UIColor whiteColor];
  
  _calendarDataSource = [[OIHCalendarDataSource alloc] initWithRestaurant:_restaurantInfo];
  _calendarDataSource.delegate = self;
  
  _calendar = [OIHCalendarInfiniteScrollView new];
  _calendar.translatesAutoresizingMaskIntoConstraints = NO;
  _calendar.dataSource = _calendarDataSource;
  
  [self.view addSubview:_calendar];

  [self.view addConstraints:
                 [NSLayoutConstraint
                     constraintsWithVisualFormat:@"H:|[_calendar]|"
                                         options:0
                                         metrics:0
                                           views:NSDictionaryOfVariableBindings(
                                                     _calendar)]];
  [self.view addConstraints:
                 [NSLayoutConstraint
                     constraintsWithVisualFormat:@"V:|-[_calendar]-|"
                                         options:0
                                         metrics:0
                                           views:NSDictionaryOfVariableBindings(
                                                     _calendar)]];
}

-(void)calendarDidTapContentView: (OIHMealModel *)meal{

  OIHCalendarMealPopupView *popup = [[OIHCalendarMealPopupView alloc] initWithMeal:meal];
  [self presentViewController:popup animated:YES completion:nil];
  
}


@end
