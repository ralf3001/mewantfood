//
//  OIHMapAnnotation.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMapAnnotation.h"
#import "OIHMapAnnotationView.h"

@interface OIHMapAnnotation ()

@end

@implementation OIHMapAnnotation

-(id)initWithTitle: (NSString *)title location: (CLLocationCoordinate2D) coordinate{

  self = [super init];
  if (self) {
    _title = title;
    _coordinate = coordinate;
    
  }
  return self;
}

-(MKAnnotationView *)annotationView{
  
  OIHMapAnnotationView *annotationView = [[OIHMapAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"annotation"];
  return annotationView;
  
}

@end
