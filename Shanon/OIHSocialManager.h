//
//  OIHSocialManager.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 13/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OIHSocialManagerDelegate <NSObject>

@optional
- (void)socialManagerProfilePicDidFinishUploading:(id)data;
- (void)socialManagerUploadProgress:(float)progress;

- (void)socialManagerDidLogInFacebook;

@end

typedef void (^OIHRequestHandler)(id result, NSError *error);

@interface OIHSocialManager : NSObject

@property(nonatomic, weak) id<OIHSocialManagerDelegate> delegate;
+ (OIHSocialManager *)sharedInstance;

- (void)fetchFacebookFriends:(OIHRequestHandler)handler;
- (void)loginFacebook:(UIViewController *)vc;
- (void)facebookLogout;

- (void)uploadProfilePhoto:(UIImage *)image
         completionHandler:(OIHRequestHandler)handler;
- (void)uploadActivityImage:(UIImage *)image
          completionHandler:(OIHRequestHandler)handler;
- (void)uploadProductImage:(UIImage *)image
         completionHandler:(OIHRequestHandler)handler;
- (void)uploadStoreImage:(UIImage *)image
        completionHandler:(OIHRequestHandler)handler;

- (void)uploadOrderAudioFile:(NSData *)audioFile
           completionHandler:(OIHRequestHandler)handler;

@end
