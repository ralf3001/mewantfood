//
//  OIHLoginViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHSignUpViewController.h"

@class OIHLoginViewController;

@protocol LoginViewProtocol <NSObject>

-(void)userDidLogin:(UserProfile *)profile withLoginController:(OIHLoginViewController *)loginViewController;

@end

@interface OIHLoginViewController : UIViewController
@property (nonatomic, weak) id<LoginViewProtocol> delegate;
@property (nonatomic) OIHSignUpViewController *signupViewController;
@end
