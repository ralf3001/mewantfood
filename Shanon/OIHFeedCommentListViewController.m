//
//  OIHFeedCommentListViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHFeedCommentListViewController.h"
#import "OIHEmptyView.h"
#import "OIHMessageInputView.h"
#import "OIHFeedCommentTableViewCell.h"

@interface OIHFeedCommentListViewController () <UITableViewDelegate, UITableViewDataSource, OIHMessageInputDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) OIHEmptyView *emptyView;
@property (nonatomic) OIHMessageInputView *messageInput;
@property (nonatomic) NSMutableArray *comments;
@property (nonatomic) OIHActivityFeed *post;

@end

@implementation OIHFeedCommentListViewController
@synthesize emptyView;


-(id)initWithPost: (OIHActivityFeed *)feed{
    
    self = [super init];
    if (self) {
        self.post = feed;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _comments = [NSMutableArray new];
    
    _messageInput = [OIHMessageInputView new];
    _messageInput.translatesAutoresizingMaskIntoConstraints = NO;
    _messageInput.delegate = self;
    [self.view addSubview:_messageInput];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_messageInput]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_messageInput)]];
    
    [self fetchComments];
    [self createTableView];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
}

-(void)fetchComments{
    
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"postcomment" httpMethod:HTTP_GET];
    setting.requestData = @{@"activityId": self.post._id, @"type": @"comment"};
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        if (!error) {
            [self populateData:data isCacheData:isCachedResult];
        }
        
    }];
    
}


-(void)messageInputViewDidPressSendButton:(NSString *)text{
    
    OIHActivityFeed *newComment = [OIHActivityFeed new];
    newComment.content = text;
    newComment.type = UserComment;
    [_comments addObject:newComment];
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"activity" httpMethod:HTTP_POST];
    setting.requestData = @{@"content": newComment.content, @"activityType": [newComment activityType], @"activityId": _post._id};
    
    [[JSONRequest sharedInstance] fetchBackground:setting];
    
    [_tableView reloadData];
    
}




- (void)populateData:(NSArray *)data isCacheData:(BOOL)isCache {
    
    NSArray *feedArray = [OIHActivityFeed activitiesFromArray:data];
    
    if (isCache) {
        
        // initial fetch
        _comments = [NSMutableArray arrayWithArray:feedArray];
        _comments.isDataDirty = [NSNumber numberWithBool:isCache];
        
    } else { // new data, flush cache data in the array
        
        if (_comments.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            // new data from network
            if (data.count > 0) {
                _comments = [NSMutableArray arrayWithArray:feedArray];
                _comments.isDataDirty = [NSNumber numberWithBool:NO];
            }else{
                //[self setupEmptyRestaurantView];
            }
            
        } else {
            
            // new data from network
            if (data.count > 0) {
                [_comments addObjectsFromArray:feedArray];
                
            }else{
                // hasMoreStories = NO;
            }
            
        }
    }
    [self createTableView];
}


-(void)createTableView{
    
    if (!_tableView) {
        
        if (emptyView) {
            [emptyView removeFromSuperview];
        }
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        [_tableView registerClass:[OIHFeedCommentTableViewCell class] forCellReuseIdentifier:@"cell"];
        
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.contentInset = UIEdgeInsetsZero;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        _tableView.estimatedRowHeight = 200.0f;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
        tap.cancelsTouchesInView = NO;
        [_tableView addGestureRecognizer:tap];
        
        [self.view addSubview:_tableView];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView][_messageInput(==50)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView, _messageInput)]];
        
    }
    
    [_tableView reloadData];
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _comments.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHFeedCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.comment = _comments[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


- (void)dismiss {
    
    [_messageInput.inputView resignFirstResponder];
    
}



- (void)keyboardWillHide:(__unused NSNotification *)notification {
    
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    if (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES height:kbSize.height];
    }
    else if (self.view.frame.origin.y < 0){
        
        [self setViewMovedUp:NO height:kbSize.height];
    }
    
    
}

- (void)keyboardWillShow:(__unused NSNotification *)notification {
    
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES height:kbSize.height];
    }
    else if (self.view.frame.origin.y < 0){
        [self setViewMovedUp:NO height:kbSize.height];
    }
    
}



-(void)setViewMovedUp:(BOOL)movedUp height: (CGFloat) keyBoardHeight{
    
    keyBoardHeight -= 40.0f;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp){
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size ofkeyBoardHeightthe view so that the area behind the keyboard is covered up.
        rect.origin.y -= keyBoardHeight;
    }
    else{
        // revert back to the normal state.
        rect.origin.y += keyBoardHeight;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


@end
