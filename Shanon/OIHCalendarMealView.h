//
//  OIHCalendarMealView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMealModel.h"
#import <UIKit/UIKit.h>
#import "OIHCalendarDay.h"

@class OIHCalendarMealView;

@protocol CalendarMealViewDelegate <NSObject>

-(void)calendarViewDidTap: (OIHCalendarMealView *)mealView;

@end

@interface OIHCalendarMealView : UIView
@property(nonatomic, weak) OIHMealModel *meal;
@property(nonatomic) UILabel *mealNameLabel;
@property(nonatomic) UILabel *dateLabel;
@property(nonatomic) UILabel *priceLabel;

@property(nonatomic) UIColor *breakfastBackgroundColor;
@property(nonatomic) UIColor *lunchBackgroundColor;
@property(nonatomic) UIColor *dinnerBackgroundColor;
@property(nonatomic) UIColor *snackBackgroundColor;

@property(nonatomic) UIColor *mealNameLabelTextColor;
@property(nonatomic) UIColor *dateLabelTextColor;

@property (nonatomic, weak) id<CalendarMealViewDelegate> delegate;

@property (nonatomic, weak) NSDate *date;

@end
