//
//  OIHPriceWithAnimationLabel.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 19/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, AnimationType) {
    LinearAnimation = 0,
    EaseInEaseOutAnimation = 1
};


typedef NSString* (^AnimateLoop)(float value);

@interface OIHPriceWithAnimationLabel : UILabel

@property (nonatomic) float duration;
@property (nonatomic) float beginValue;
@property (nonatomic) float endValue;
@property (nonatomic, copy) AnimateLoop animateLoop;
@property (nonatomic) AnimationType animationType;


-(void)startCounter: (float) start end: (float)end;

@end
