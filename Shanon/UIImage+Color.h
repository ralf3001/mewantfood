//
//  UIImage+Color.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

+(UIImage *)imageFromColor:(UIColor *)color;

-(UIImage *)resizeImage;

+(UIImage *)defaultImage;

@end
