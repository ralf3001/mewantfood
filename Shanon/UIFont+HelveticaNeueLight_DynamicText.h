//
//  UIFont+HelveticaNeueLight_DynamicText.h
//  Shanon
//
//  Created by Ralf Cheung on 8/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (HelveticaNeueLight_DynamicText)

+ (UIFont *)preferredHelveticaNeueLightForTextStyle:(NSString *)textStyle;

@end
