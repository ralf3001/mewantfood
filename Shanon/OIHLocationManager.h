//
//  OIHLocationManager.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol OIHLocationManagerDelegate <NSObject>

-(void)locationManagerDidFailWithError: (CLError)error;
-(void)locationManagerDidUpdateLocation: (CLLocation *)location;

@end

@interface OIHLocationManager : NSObject

typedef void (^placemarkBlock)(CLPlacemark *placemark);

@property (nonatomic, weak) id <OIHLocationManagerDelegate> delegate;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *lastLocation;

-(void)requestLocation;
+ (OIHLocationManager *)sharedInstance;
- (void)getLastPlacemark:(placemarkBlock)completionBlock;

@end
