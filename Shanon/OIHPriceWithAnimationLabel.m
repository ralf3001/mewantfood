//
//  OIHPriceWithAnimationLabel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 19/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHPriceWithAnimationLabel.h"

@interface OIHPriceWithAnimationLabel ()
@property (nonatomic) NSMutableAttributedString *attrString;
@end


@implementation OIHPriceWithAnimationLabel
@synthesize duration;
@synthesize beginValue;
@synthesize endValue;


-(id)init{
    
    self = [super init];
    
    if (self) {
    
        _attrString = [[NSMutableAttributedString alloc] init];
        
    }
    
    return self;
}


-(void)startCounter: (float) start end: (float)end{
    
    beginValue = start;
    endValue = end;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        int steps = 100;
        
        for (int i =1; i <= steps; i++) {
            
            float t = (float)i / 100;
            
            usleep(duration/100 * 1000000); // sleep in microseconds
            
            float e = [self easeInEaseOut:t b:beginValue c:(endValue - beginValue) d:1];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.text = self.animateLoop(e);
            });
            
            
        }
    });
    

    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(float)linear: (float) t b: (float)b c:(float) c d:(float)d{
    return c*t/d + b;
}


-(float)easeInEaseOut: (float) t b: (float)b c:(float) c d:(float)d{
    
    if ((t/=d/2) < 1) return c/2*t*t + b;
    return -c/2 * ((--t)*(t-2) - 1) + b;
}




@end
