//
//  OIHMerchantOrderDetailViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMerchantOrderDetailViewController.h"
#import "StretchyHeaderCollectionViewLayout.h"

@interface OIHMerchantOrderDetailViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic, readonly) UICollectionReusableView *header;
@property (nonatomic) MKMapView *mapView;

@end

@implementation OIHMerchantOrderDetailViewController
@synthesize header;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupCollectionView];
    
}




-(void)setupCollectionView{
    
    StretchyHeaderCollectionViewLayout *stretchyLayout;
    stretchyLayout = [[StretchyHeaderCollectionViewLayout alloc] init];
    [stretchyLayout setSectionInset:UIEdgeInsetsMake(0, 10.0, 10.0, 10.0)];
    [stretchyLayout setHeaderReferenceSize:CGSizeMake(320.0, self.view.frame.size.height - 150)];
    [stretchyLayout setFooterReferenceSize:CGSizeMake(320.0, 80.0)];
    
    stretchyLayout.minimumInteritemSpacing = 0;
    stretchyLayout.minimumLineSpacing = 0;
    
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:stretchyLayout];
    _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    [_collectionView setAlwaysBounceVertical:YES];
    [_collectionView setShowsVerticalScrollIndicator:NO];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [self.view addSubview:_collectionView];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [_collectionView registerClass:[UICollectionReusableView class]
        forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
               withReuseIdentifier:@"header"];
    
    
//    [_collectionView registerClass:[UICollectionReusableView class] forCellWithReuseIdentifier:@"footer"];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
    
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section {
    
    return 4;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if (!header) {
        
        header = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                    withReuseIdentifier:@"header"
                                                           forIndexPath:indexPath];        
        
//        [header addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fullScreenMapView)]];
        
        _mapView = [MKMapView new];
        _mapView.translatesAutoresizingMaskIntoConstraints = NO;
        _mapView.showsUserLocation = YES;
        _mapView.showsBuildings = YES;
        _mapView.showsPointsOfInterest = YES;
        _mapView.showsCompass = YES;
        _mapView.showsScale = YES;
        
        CLGeocoder *geocoder = [CLGeocoder new];
        [geocoder geocodeAddressString:_orderInfo.recipientAddress.userEnteredAddress completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            if (placemarks && placemarks.count > 0) {
                
                CLPlacemark *clPlacemark = placemarks[0];
                MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:clPlacemark];
                [_mapView addAnnotation:placemark];
                
                MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(clPlacemark.location.coordinate, 400, 400);
                [_mapView setRegion:region animated:YES];

            }
        }];
        
        
        [header addSubview:_mapView];
        
        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_mapView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_mapView)]];
        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_mapView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_mapView)]];
        
    }
    
    return header;
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return CGSizeMake(collectionView.frame.size.width, 100);
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
