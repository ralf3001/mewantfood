//
//  OIHFeedCommentTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHFeedCommentTableViewCell.h"
#define kProfileImageHeight 50

@implementation OIHFeedCommentTableViewCell
@synthesize profileImageView;
@synthesize userLabel, contentLabel, timeLabel;


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
  if (highlighted) {
    self.contentView.backgroundColor = [UIColor highlightColor];
  }else{
    self.contentView.backgroundColor = [UIColor whiteColor];
  }
}
-(void)setSelected:(BOOL)selected animated:(BOOL)animated{};


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
  
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    
    profileImageView = [UIImageView new];
    profileImageView.translatesAutoresizingMaskIntoConstraints = NO;
    profileImageView.layer.masksToBounds = YES;
    profileImageView.layer.cornerRadius = kProfileImageHeight / 2;
    [self.contentView addSubview:profileImageView];
    
    userLabel = [UILabel new];
    userLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    userLabel.translatesAutoresizingMaskIntoConstraints = NO;
    userLabel.numberOfLines = 0;
    [self.contentView addSubview:userLabel];
    
    contentLabel = [UILabel new];
    contentLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
    contentLabel.translatesAutoresizingMaskIntoConstraints = NO;
    contentLabel.numberOfLines = 0;
    [self.contentView addSubview:contentLabel];
    
    timeLabel = [UILabel new];
    timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    timeLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    timeLabel.numberOfLines = 0;
    [self.contentView addSubview:timeLabel];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:profileImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:profileImageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[timeLabel]" options:0 metrics:0 views:NSDictionaryOfVariableBindings(timeLabel)]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[profileImageView(==imageHeight)]" options:0 metrics:@{@"imageHeight": @(kProfileImageHeight)} views:NSDictionaryOfVariableBindings(profileImageView)]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[profileImageView]-15-[userLabel]-[timeLabel(<=60)]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(profileImageView, userLabel, timeLabel)]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[userLabel]-[contentLabel]-|" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:0 views:NSDictionaryOfVariableBindings(userLabel, contentLabel)]];
    
  }
  return self;
  
}


-(void)setComment:(OIHActivityFeed *)comment{
  
  _comment = comment;
  
  self.userLabel.text = _comment.fromUser.userName;
  self.contentLabel.text = _comment.content;
  
  [profileImageView sd_setImageWithURL:[NSURL URLWithString:_comment.fromUser.profilePicURL] placeholderImage:nil];
  
  if (!_comment.fromUser) {
    self.userLabel.text = [UserProfile currentUser].name;
    [profileImageView sd_setImageWithURL:[NSURL URLWithString:[UserProfile currentUser].profilePics[0]] placeholderImage:nil];
    
  }

  timeLabel.text = [NSDate daysBetweenDate:comment.createdAt andDate:[NSDate date]];

}


@end
