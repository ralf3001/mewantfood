//
//  OIHLoginOperation.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHLoginOperation : NSOperation{
    
    BOOL executing;
    BOOL finished;
}

-(id)initWithRequest: (NSMutableURLRequest *)request session: (NSURLSession *)session completionHandler:(void (^)(id data, NSError *error))completionHandler;

@end
