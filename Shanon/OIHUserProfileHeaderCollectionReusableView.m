//
//  OIHUserProfileHeaderCollectionReusableView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserProfileHeaderCollectionReusableView.h"

@implementation OIHUserProfileHeaderCollectionReusableView
@synthesize nameLabel;
@synthesize personUtilityButton;
@synthesize imageView;

-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        CGRect bounds;
        bounds = [self bounds];
        
        imageView = [[UIImageView alloc] initWithFrame:bounds];
        [imageView setImage:[UIImage defaultImage]];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
        [imageView setClipsToBounds:YES];
        [self addSubview:imageView];

        
        nameLabel = [UILabel new];
        nameLabel.numberOfLines = 0;
        nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:nameLabel];

        
        personUtilityButton = [UIButton buttonWithType:UIButtonTypeCustom];
        personUtilityButton.translatesAutoresizingMaskIntoConstraints = NO;
        [personUtilityButton setTitle:@" " forState:UIControlStateNormal];
        personUtilityButton.contentEdgeInsets = UIEdgeInsetsMake(5, 10, 5, 10);
        [personUtilityButton setTintColor:[UIColor whiteColor]];
        [personUtilityButton setTitleColor:personUtilityButton.tintColor forState:UIControlStateNormal];
        personUtilityButton.layer.borderColor = personUtilityButton.tintColor.CGColor;
        personUtilityButton.layer.borderWidth = 0.5f;
        personUtilityButton.layer.cornerRadius = 5;
        personUtilityButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [personUtilityButton addTarget:self action:@selector(utilityButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:personUtilityButton];
        
        UIView *overlay = [UIView new];
        overlay.translatesAutoresizingMaskIntoConstraints = NO;
        overlay.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        [self insertSubview:overlay belowSubview:nameLabel];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[overlay]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(overlay)]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[overlay]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(overlay)]];
        
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[nameLabel]-[personUtilityButton]" options:NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(nameLabel, personUtilityButton)]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:nameLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-8.0f]];

        
        
    }
    return self;
    
}

-(void)utilityButtonPressed{
    
    if ([self.delegate respondsToSelector:@selector(didPressUtilityButton:)]) {
        [self.delegate didPressUtilityButton:self.buttonType];
    }
    
}

-(void)setFunctionForUtilityButton: (UtilityButtonFunction) type user: (OIHUserModel *)user{
    
    self.buttonType = type;
    
    switch (type) {
        case UtilityButtonBuyFoodForUser:
            [personUtilityButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"USER_BUY_%@_FOOD", @"Buy [person] food"), user.userName] forState:UIControlStateNormal];
            break;
            
        case UtilityButtonUnfriendUser:
            [personUtilityButton setTitle:NSLocalizedString(@"USER_UNFRIEND", @"Unfriend User") forState:UIControlStateNormal];
            break;
            
        case UtilityButtonUserSetProfilePicture:
            [personUtilityButton setTitle:@"" forState:UIControlStateNormal];

            [personUtilityButton setImage:[IonIcons imageWithIcon:ion_ios_camera_outline size:25.0f color:personUtilityButton.tintColor] forState:UIControlStateNormal];
            personUtilityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
            personUtilityButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
            personUtilityButton.imageView.contentMode = UIViewContentModeScaleAspectFit;

            break;
            
        default:
            break;
    }
    
}


-(void)setNameLabelText: (NSString *)text{
    
    NSAssert(text != nil, @"");

    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                (id)[UIFont fontWithName:@"HelveticaNeue-Thin" size:40.0f], NSFontAttributeName,
                                (id)[UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
    nameLabel.attributedText = attrText;

    
}


@end
