//
//  OIHMapAnnotationView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface OIHMapAnnotationView : MKAnnotationView

@end
