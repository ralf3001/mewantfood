//
//  OIHItemRatingView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OIHRatingContentViewDelegate <NSObject>

-(void)ratingViewDidChooseEmoji: (NSInteger) emoji;

@optional
-(void)ratingViewWillDismiss: (NSInteger) emoji;

@end

@interface OIHItemRatingView : UIView
@property (nonatomic) NSInteger chosenEmoji;

-(id)initWithChosenItem: (NSInteger)item;
@property (nonatomic, weak) id<OIHRatingContentViewDelegate> delegate;

@end
