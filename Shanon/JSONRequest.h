//
//  JSONRequest.h
//  Ferrle
//
//  Created by Ralf Cheung on 2/18/15.
//  Copyright (c) 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONRequestSettings.h"


@interface JSONRequest : NSObject <NSURLSessionDelegate>


+(JSONRequest *)sharedInstance;

-(NSMutableURLRequest *)createMutableURLRequest: (NSString*)requestMethod method: (NSString *)method dictionary: (NSDictionary *)dictionary;
-(NSMutableURLRequest *)createMutableURLRequest: (NSString*)requestMethod method: (NSString *)method array: (NSArray *)array;


-(void)fetch: (JSONRequestSettings *)setting completionHandler:(void (^)(id data, BOOL isCachedResult, NSError *error))completionHandler;

-(void)fetchBackground: (JSONRequestSettings *)setting;


-(void)fetch: (NSString*)requestMethod method: (NSString *)method data: (id)data completionHandler:(void (^)(id data, NSError *error))completionHandler;


-(void)fetch: (NSString*)requestMethod method: (NSString *)method dictionary: (NSDictionary *)dictionary;
-(void)loginWithToken:(void (^)(id data, NSError *error))completionHandler;
-(void)setupNotifications;
-(void)dataTaskReply: (NSData *)data response:(NSURLResponse *)response error:(NSError *)error;

-(void)addNotificationToken: (NSString *)token key: (NSString *)key;
-(void)removeNotificationToken: (NSString  *)token key: (NSString *)key;

//For Reachability
-(NSString *)connectionURL;

@end
