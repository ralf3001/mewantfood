//
//  OIHHomeScreenDataSource.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 3/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHHomeScreenDataSource.h"
#import "OIHActivityFeed.h"
#import "OIHHomeScreenReviewTableViewCell.h"
#import "OIHHomeScreenOrderTableViewCell.h"
#import "OIHUserActivityTableViewCell.h"
#import "OIHUserOrderReviewViewController.h"


@interface OIHHomeScreenDataSource () <OIHActivityCellButtonDelegate>

@property (nonatomic) NSMutableArray *activityReducedLikesData;
@property (assign) BOOL hasMoreStories;
@property (assign) BOOL isFetching;

@end

@implementation OIHHomeScreenDataSource
@synthesize hasMoreStories;
@synthesize isFetching;



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return _activityReducedLikesData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == _activityReducedLikesData.count) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoMoreDataCell" forIndexPath:indexPath];
        
        cell.textLabel.text = NSLocalizedString(@"ACTIVITY_NO_MORE_STORIES", @"no more stories");
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1.0f];
        
        return cell;
        
    }else{
        
        OIHActivityFeed *activity = _activityReducedLikesData[indexPath.row];
        
        if (activity.type == UserReview) {
            
            OIHHomeScreenReviewTableViewCell *cell = (OIHHomeScreenReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"reviewCell" forIndexPath:indexPath];
            
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:activity.postPicture] placeholderImage:[UIImage imageFromColor:[UIColor colorWithWhite:0.8 alpha:1.0f]]];
            cell.feed = activity;
            
            return cell;
            
        }else if (activity.type == UserOrder && [activity.fromUser._id isEqualToString:activity.toUser._id]){
            
            OIHHomeScreenOrderTableViewCell *cell = (OIHHomeScreenOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"orderCell" forIndexPath:indexPath];
            cell.feed = activity;
            return cell;
        }
        else{
            
            OIHUserActivityTableViewCell *cell = (OIHUserActivityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            
            cell.activityInfo = activity;
            cell.delegate = self;
            
            return cell;
            
        }
        
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OIHActivityFeed *activity = _activityReducedLikesData[indexPath.row];
    if (activity.type == UserReview) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
//            OIHUserOrderReviewViewController *vc = [[OIHUserOrderReviewViewController alloc] initWithReview:activity];
            
//            [self.view.window.rootViewController presentViewController:vc animated:YES completion:nil];
            
        });
        
        
    }

}


-(void)userDidTapThanksButton:(OIHActivityFeed *)activity isPostThanked:(BOOL)isPostThanked{
    
    if (isPostThanked) {
        
        [[JSONRequest sharedInstance] fetch:@"activity" method:@"POST" data:@{@"activityId": activity._id, @"activityType": @"thank"} completionHandler:^(id data, NSError *error) {
            if (error) {
                
            }
        }];
        
    }else{
        
        [[JSONRequest sharedInstance] fetch:@"activity" method:@"DELETE" data:@{@"activityId": activity._id, @"activityType": @"thank"} completionHandler:^(id data, NSError *error) {
            if (error) {
                
            }
        }];
        
    }

    
}


-(void)populateData: (NSArray *)data isCacheData: (BOOL) isCache{
    
    
    NSArray *feedArray = [OIHActivityFeed activitiesFromArray:data];
    
    if(isCache){
        
        //initial fetch
        _feedData = [NSMutableArray arrayWithArray:feedArray];
        _feedData.isDataDirty = [NSNumber numberWithBool:isCache];
        _activityReducedLikesData = [NSMutableArray arrayWithArray:[self filterData:feedArray]];
        
    }else{  //new data, flush cache data in the array
        
        if (_feedData.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            //new data from network
            if (data.count > 0) {
                _feedData = [NSMutableArray arrayWithArray:feedArray];
                
                _activityReducedLikesData = [NSMutableArray arrayWithArray:[self filterData:feedArray]];
                _feedData.isDataDirty = [NSNumber numberWithBool:NO];
            }else{
//                [self emptyView];
            }
            
        }else{
            
            //new data from network
            if (data.count > 0) {
                [_feedData addObjectsFromArray:feedArray];
                if (!_activityReducedLikesData) {
                    _activityReducedLikesData = [NSMutableArray arrayWithArray:[self filterData:feedArray]];
                }else{
                    [_activityReducedLikesData addObjectsFromArray:[self filterData:feedArray]];
                }
                
            }else{
                hasMoreStories = NO;
                
            }
        }
        
    }
    
}


-(NSArray *)filterData: (NSArray *)array{
    
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"type != 2"];
    return [_feedData filteredArrayUsingPredicate:bPredicate];
    
}




-(void)fetchActivities: (NSString *)date{
    
    NSMutableDictionary *getData = [NSMutableDictionary new];
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"userActivity" httpMethod:HTTP_GET];
    
    if (date) {
        getData[@"feedDate"] = date;
        setting.cachePolicy = IgnoreCache;
    }else{
        setting.cachePolicy = CacheFirstThenLoadData;
    }
    
    setting.requestData = getData;
    
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
//        [refreshControl endRefreshing];
        isFetching = NO;
        if (!error) {
            [self populateData:data isCacheData:isCachedResult];
        }
    }];
    
}


-(void)fetchActivities{
    [self fetchActivities:nil];
}

-(void)fetchMoreActivities{
    
    OIHActivityFeed *lastActivity = [_activityReducedLikesData lastObject];
    [self fetchActivities:[lastActivity.createdAt JSONDateFromDate]];
    
}




@end
