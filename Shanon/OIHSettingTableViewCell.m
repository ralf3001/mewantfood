//
//  OIHSettingTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHSettingTableViewCell.h"

@implementation OIHSettingTableViewCell
@synthesize numberLabel;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImageView *imageView = self.imageView;
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:imageView];
        
        UILabel *textLabel = self.textLabel;
        textLabel.translatesAutoresizingMaskIntoConstraints = NO;
        textLabel.numberOfLines = 0;
        [textLabel sizeToFit];
//        textLabel.font = [UIFont fontWithName:@"AvenirNext-Bold" size:30];
        

        [self.contentView addSubview:textLabel];
        
        numberLabel = [UILabel new];
        numberLabel.translatesAutoresizingMaskIntoConstraints = NO;
//        numberLabel.layer.cornerRadius = 17;
        numberLabel.layer.masksToBounds = YES;
        numberLabel.textAlignment = NSTextAlignmentCenter;
        numberLabel.textColor = [UIColor whiteColor];
        
        [self.contentView addSubview:numberLabel];
        

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:30]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:numberLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:30]];

        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];

        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:numberLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:numberLabel attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[imageView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[imageView]-15-[textLabel]-[numberLabel]-|" options:NSLayoutFormatAlignAllCenterY metrics:0 views:NSDictionaryOfVariableBindings(imageView, textLabel, numberLabel)]];
        
        
//        self.contentView.backgroundColor = [UIColor darkBlue];
//        textLabel.textColor = [UIColor whiteColor];
        
    }
    
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];

    numberLabel.layer.cornerRadius = CGRectGetHeight(numberLabel.frame) / 2;

}



-(void)setNotificationNumber: (NSInteger) notifications{
    
    if (notifications > 0) {
        numberLabel.backgroundColor = [UIColor redColor];
    }else{
        numberLabel.backgroundColor = [UIColor clearColor];

    }

    numberLabel.text = [NSString stringWithFormat:@"%ld", (long)notifications];
    
}


@end
