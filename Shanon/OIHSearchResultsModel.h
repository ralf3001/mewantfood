//
//  OIHSearchResultsModel.h
//  Shanon
//
//  Created by Ralf Cheung on 8/5/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHRestaurantModel.h"
#import "OIHMealModel.h"
#import "OIHBaseModel.h"

@interface OIHSearchResultsModel : OIHBaseModel

@property (nonatomic) NSArray <OIHMealModel *> *meals;
@property (nonatomic) NSArray <OIHRestaurantModel *> *restaurants;
@property (nonatomic) NSNumber *surchargeRate;
@property (nonatomic) OIHMealModel *purchaseItem;

-(id)initWithInfo: (NSDictionary *)info;

@end
