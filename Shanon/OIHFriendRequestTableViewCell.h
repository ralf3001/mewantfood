//
//  OIHFriendRequestTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 13/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>


@class OIHFriendRequestTableViewCell;

@protocol OIHTableViewCellActionButtonDelegate <NSObject>

-(void)userDidPressActionButton: (UIButton *)button cell:(OIHFriendRequestTableViewCell *)cell;

@end


@interface OIHFriendRequestTableViewCell : UITableViewCell
@property (nonatomic) UILabel *nameLabel;
@property (nonatomic) UIButton *actionButton;
@property (nonatomic, weak) id<OIHTableViewCellActionButtonDelegate> delegate;
@end
