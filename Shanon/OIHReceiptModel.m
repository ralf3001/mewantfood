//
//  OIHReceiptModel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 1/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHReceiptModel.h"

@implementation OIHReceiptModel
-(id)initWithInfo: (NSDictionary *)info{

    self = [super init];
    if (self) {
        self._id = info[@"_id"];
        _totalAmount = info[@"totalAmount"];
        _braintreeReceiptId = info[@"receiptId"];
        
    }
    return self;
}
@end
