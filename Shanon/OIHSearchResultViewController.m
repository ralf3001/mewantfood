//
//  OIHSearchResultViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 21/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSearchResultViewController.h"
#import "OIHUserModel.h"
#import "OIHFriendsListTableViewCell.h"


@interface OIHSearchResultViewController () <UITableViewDelegate, UITableViewDataSource, FriendTableViewCellDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSMutableArray *dataList;
@end

@implementation OIHSearchResultViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;

    _dataList = [NSMutableArray new];
    [self createTableView];
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
}


-(void)createTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [_tableView registerClass:[OIHFriendsListTableViewCell class] forCellReuseIdentifier:@"cell"];
    _tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0);
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 60.0f;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:_tableView];

    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];

}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self searchFriend: searchBar.text];
}


-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
    [self searchFriend: searchBar.text];
}


-(void)searchFriend: (NSString *)name{
    
    if (![name isEqualToString:@""]) {
        
        [[JSONRequest sharedInstance] fetch:@"user" method:@"GET" data:@{@"username": name} completionHandler:^(id data, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!error) {
                    [_dataList removeAllObjects];
                    [_dataList addObjectsFromArray:[OIHUserModel profilesFromArray:data]];
                    [_tableView reloadData];
                }
            });
            
        }];
        
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _dataList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHFriendsListTableViewCell *cell = (OIHFriendsListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    OIHUserModel *user = _dataList[indexPath.row];

    cell.user = user;
    
    cell.delegate = self;

    return cell;
}

#pragma mark - <FriendTableViewCellDelegate>

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if ([self.delegate respondsToSelector:@selector(searchResultViewControllerDidSelectUser:)]) {
        [self.delegate searchResultViewControllerDidSelectUser:_dataList[indexPath.row]];
    }

}




-(void)tableViewCellDidPressActionButton: (OIHFriendsListTableViewCell *)cell{
    
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    OIHUserModel *user = _dataList[indexPath.row];
    
    if ([user isUserType:UserFriend]) {
        
    }else if ([user isUserType:UserFriendRequest]){
        
        
    }else if([user isUserType:UserPendingRequest]){
        
    }else{
//        [cell setType:PersonNormal];
    }


    
}



-(void)confirmFriend: (OIHFriendsListTableViewCell *)cell{
    
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    OIHUserModel *user = _dataList[indexPath.row];

    [user resetUserType];
    [user setUserType:UserFriend];
    
    [[JSONRequest sharedInstance] fetch:@"confirmFriend" method:@"GET" data:@{@"userId": user._id} completionHandler:^(id data, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                NSLog(@"Done");
            }else{
                NSLog(@"Error");
                [user resetUserType];
                [user setUserType:UserFriendRequest];
            }
            
        });
        
    }];
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
