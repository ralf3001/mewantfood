//
//  OIHAudioRecordingActionSheet.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 31/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol OIHAudioRecordingActionSheetDelegate <NSObject>

@optional
-(void)audioActionSheetWillDismiss: (NSURL *)fileURL;
-(void)audioActionSheetDidDismiss: (NSURL *)fileURL;


@end

@interface OIHAudioRecordingActionSheet : UIViewController <UIViewControllerTransitioningDelegate>
@property (nonatomic, weak) id<OIHAudioRecordingActionSheetDelegate> delegate;

@end
