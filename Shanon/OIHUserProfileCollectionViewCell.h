//
//  OIHUserProfileCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>



typedef NS_ENUM(NSInteger, AddressType){
    UserDefaultAddress = 0,
    UserHomeAddress = 1,
    UserOtherTypes = 2
};



@class OIHUserProfileCollectionViewCell;

@protocol OIHUserCollectionViewCellDelegate <NSObject>

-(void)userDidPressActionButton: (UIButton *)button cell: (OIHUserProfileCollectionViewCell *)cell;

@end


@interface OIHUserProfileCollectionViewCell : UICollectionViewCell
@property (nonatomic) UILabel *descriptionField;
@property (nonatomic) UILabel *descriptionContent;
@property (nonatomic) UIButton *moreButton;
@property (nonatomic) NSString *attribute;
@property (nonatomic) AddressType type;

@property (nonatomic, weak) id<OIHUserCollectionViewCellDelegate> delegate;


-(void)setDescriptionContentText: (NSString *)content;
-(void)setMoreButton;

@end
