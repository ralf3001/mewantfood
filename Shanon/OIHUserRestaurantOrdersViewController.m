//
//  OIHUserRestaurantOrdersViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserRestaurantOrdersViewController.h"
#import "OIHOrderModel.h"
#import "OIHMerchantRestaurantOrderListTableViewCell.h"
#import "OIHUserFBFriendsTableViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "OIHMerchantOrderDetailViewController.h"
#import "OIHSocialManager.h"

@interface OIHUserRestaurantOrdersViewController () <UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchBarDelegate, OIHEmptyViewDelegate, OIHSocialManagerDelegate>


@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSMutableArray <OIHOrderModel *> *ordersList;
@property (nonatomic, assign) BOOL isFetching;
@property (nonatomic) UISearchController *searchController;
@property (nonatomic) UIRefreshControl *refreshControl;

@property (nonatomic) OIHEmptyView *emptyView;

@end

@implementation OIHUserRestaurantOrdersViewController
@synthesize tableView = _tableView;
@synthesize ordersList;
@synthesize searchController;
@synthesize isFetching;
@synthesize refreshControl;
@synthesize restaurantInfo;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    isFetching = NO;
    
    ordersList = [NSMutableArray new];
//    [SVProgressHUD showWithStatus:NSLocalizedString(@"ORDERS_FETCHING", @"")];

    [self fetchOrders: nil];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fetchOrders)
                                                 name:OIHUpdateOrderNotification
                                               object:nil];
    
    
}


-(void) updateSearchResultsForSearchController:(UISearchController *)searchController{}



-(void)createTableView{
    
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        [_tableView registerClass:[OIHMerchantRestaurantOrderListTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.contentInset = UIEdgeInsetsZero;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.tableHeaderView = nil;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        refreshControl = [UIRefreshControl new];
        [refreshControl addTarget:self action:@selector(fetchOrders) forControlEvents:UIControlEventValueChanged];
        [_tableView addSubview:refreshControl];
        
        [self.view addSubview:_tableView];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    }else{
        
        [_emptyView removeFromSuperview];
        [_tableView reloadData];
    }
    
    
}

-(void)setupEmptyView{
    
    if (!_emptyView) {
        
        _emptyView = [[OIHEmptyView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_filing_outline size:90 color:[UIColor lightGrayColor]] descriptionText:@"Tell your friends about this app?"];
        _emptyView.translatesAutoresizingMaskIntoConstraints = NO;
        _emptyView.delegate = self;
        [self.view addSubview:_emptyView];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_emptyView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_emptyView)]];
        
        [_tableView removeFromSuperview];
    }
    
}

-(void)fetchMoreOrders{
    
    OIHOrderModel *order = [ordersList lastObject];
    [self fetchOrders:order.infoDictionary[kOIHOrderDate]];
    
}

-(void)fetchOrders{
    [self fetchOrders:nil];
}

-(void)fetchOrders: (NSString *)orderDate{
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"restaurantOrders" httpMethod:HTTP_GET];
    
    if (orderDate) {
        dict[@"orderDate"] = orderDate;
        setting.cachePolicy = IgnoreCache;
    }
    
    if (restaurantInfo) {
        dict[@"restaurant"] = restaurantInfo._id;
    }
    
    setting.requestData = dict;
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        isFetching = NO;
        [SVProgressHUD dismiss];
        [refreshControl endRefreshing];
        
        if (!error) {
            [self populateData:data isCacheData:isCachedResult];
        }
        
    }];
    
    
}

-(void)populateData: (NSArray *)data isCacheData: (BOOL) isCache{
    
    NSArray *feedArray = [OIHOrderModel initFromArray:data];
    
    if(isCache){
        
        //initial fetch
        ordersList = [NSMutableArray arrayWithArray:feedArray];
        ordersList.isDataDirty = [NSNumber numberWithBool:isCache];
        
    }else{  //new data, flush cache data in the array
        
        if (ordersList.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            //new data from network
            if (feedArray.count == 0) {
                
                [self setupEmptyView];
                
            }else{
                ordersList = [NSMutableArray arrayWithArray:feedArray];
                ordersList.isDataDirty = [NSNumber numberWithBool:NO];
            }
            
        }else{
            
            //new data from network, more data when table view at the bottom
            [ordersList addObjectsFromArray:feedArray];
            
        }
        
    }
    
    [self createTableView];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return ordersList.count;
}


- (OIHMerchantRestaurantOrderListTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHMerchantRestaurantOrderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    OIHOrderModel *order = ordersList[indexPath.row];
    cell.orderModel = order;
    
    if (order.recipientAddress.userEnteredAddress) {
        cell.orderRecipientAddressLabel.text = [NSString stringWithFormat: NSLocalizedString(@"MANAGEMENT_ORDER_CELL_RECIPIENT_ADDRESS_%@", @"address"), order.recipientAddress.userEnteredAddress];
    }else{
        cell.orderRecipientAddressLabel.text = @"";
    }
    
    cell.orderRecipientLabel.text = [NSString stringWithFormat:NSLocalizedString(@"MANAGEMENT_ORDER_CELL_RECIPIENT_NAME_%@", @"Recipient: [name]"), order.orderRecipient.userName];
    
    
    
    return cell;
}



//-(void)setOrderStatus: (NSString *)_id status: (OrderStatus)status indexPath: (NSIndexPath *)indexPath{}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OIHMerchantOrderDetailViewController *vc = [OIHMerchantOrderDetailViewController new];
    vc.orderInfo = ordersList[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat actualPosition = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height - (1200); //very arbitrary number
    
    if (scrollView == _tableView && isFetching == NO && actualPosition >= contentHeight) {
        isFetching = YES;
        [self fetchMoreOrders];
    }
    
}


#pragma mark - OIHEmptyViewDelegate

-(void)emptyViewDidPressReloadButton:(OIHEmptyView *)emptyView{
    
    
    if ([FBSDKAccessToken currentAccessToken]) {
        OIHUserFBFriendsTableViewController *vc = [OIHUserFBFriendsTableViewController new];
        [self presentViewController:vc animated:YES completion:nil];
    }else{
        [OIHSocialManager sharedInstance].delegate = self;
        [[OIHSocialManager sharedInstance] loginFacebook:self];
        
    }
    
}


-(void)socialManagerDidLogInFacebook{
    
    OIHUserFBFriendsTableViewController *vc = [OIHUserFBFriendsTableViewController new];
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [OIHSocialManager sharedInstance].delegate = nil;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
