//
//  OIHRestaurantInfoOwnerCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"

@interface OIHRestaurantInfoOwnerCollectionViewCell : UICollectionViewCell
@property (nonatomic) UILabel *nameLabel;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) OIHUserModel *user;

@end
