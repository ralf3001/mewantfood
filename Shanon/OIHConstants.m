//
//  PAPConstants.m
//
//  Created by Mattieu Gamache-Asselin on 5/25/12.
//  Copyright (c) 2013 Parse. All rights reserved.
//

#import "OIHConstants.h"

NSString *const OIHFacebookAppId = @"452377668296945";

NSString *const kOIHS3BucketName = @"oih-images";
NSString *const kOIHS3BucketActivityImage = @"oih-activity-images";
NSString *const kOIHS3BucketProductImage = @"oih-food-images";
NSString *const kOIHS3BucketStorefrontImage = @"oih-storefront-images";

NSString *const kOIHS3BucketOrderAudio = @"oih-order-audio-files";


NSString *const kOIHRSAPublicKey = @"-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArKbZm+5UeEbJxQ+IYHBa\nW66HenjI4zlarari/KxCV1sTTbaCMME0/C/4DNsvJhdW9sJJfSl6S4DjjNuEGFCe\n7ZbRZJRqscGdV1voqxIb3f/V/6IJ/WWPxHpWG6ZHlge/O41PE3eBUv0K6hj2N81e\nOGF7MNL869LmHSQOnaoFCSufiZLvyDy9QuOB5ar7ugxSSCIzPM1nXjEK4/+8Tz1t\nL6/NXrl8+4HntG/mr1uKtJiIxI2DhHGHaPorUuOQX1CiIdFpOk7BRPjBmEmMGzzG\nWFeiftKf34k9bPf8ELJ3nTjSP4DjU5qBVXuSoFB/vr+hLBk8fspOXK8QjRWcxsSc\nOQIDAQAB\n-----END PUBLIC KEY-----";

NSString *const kOIHPublicKey = @"PVoC5z9fsZdwWK4Xvc2pjXtiNq5BfGBK";


NSString *const kEinerApplicationID = @"8XtY7NkekQ0n5rpc6g52OUZ0YcTWOWDjWixUYnS3";
NSString *const kEinerClientKey = @"nPjlbmhpPxpsI0qE3yP7bnsYTeZNSFqudZDAnDnH";
NSString *const kEinerTwitterConsumerKey = @"kFklwQTMenuiiI34xCUFrX3f9";
NSString *const kEinerTwitterConsumerSecret = @"gWl21HKyqGUZbf1AE6NGoiTBH0RSU3Gfnnr2zsbWf87guxE4ub";

NSString *const kOIHTwitterConsumerKey = @"QrQcygJW2loRzGW1iqHwqSq7B";
NSString *const kOIHTwitterConsumerSecret = @"prYHFe2r2pEWMZIeviZvcw64NCccjT1Z2Yldc2vEt9I6V84RU5";


NSString *const kPAPUserDefaultsActivityFeedViewControllerLastRefreshKey    = @"com.parse.Anypic.userDefaults.activityFeedViewController.lastRefresh";
NSString *const kPAPUserDefaultsCacheFacebookFriendsKey = @"com.parse.Anypic.userDefaults.cache.facebookFriends";


#pragma mark - Launch URLs

NSString *const kPAPLaunchURLHostTakePicture = @"camera";


#pragma mark - NSNotification
NSString *const EinerUserDidUpdateProfilePictureNotification = @"com.ralfcheung.Einer.CanvasCollection.DidUpdateProfilePicture";
NSString *const OIHUpdateOrderNotification = @"com.ralfcheung.OIH.UpdateOrderNotification";
NSString *const OIHUserDidReceiveANewGiftOrder = @"com.ralfcheung.OIH.UserDidReceiveANewOrder";

NSString *const OIHTabBarWillCreateNotificationIcon = @"com.ralfcheung.OIH.tabBarWillCreateNotificationIcon";
NSString *const OIHTabBarWillDismissNotificationIcon = @"com.ralfcheung.OIH.tabBarWillDismissNotificationIcon";
NSString *const OIHTabBarWillSelectTabAtIndexNotification = @"com.ralfcheung.OIH.tabBarWillSelectTabAtIndexNotification";

NSString *const OIHViewControllerPresentModalViewControllerNotification = @"com.ralfcheung.OIH.viewControllerPresentModalViewControllerNotification";


NSString *const OIHUserDidReceiveFriendRequestsNotification = @"com.ralfcheung.OIH.friendRequestsNotification";


NSString *const OIHUserDidUpdateProfileNotification = @"com.ralfcheung.OIH.UserUpdateProfileNotification";

NSString *const OIHSocialManagerUploadDidCompleteNotification = @"com.ralfcheung.OIH.SocialManagerUploadDidCompleteNotification";

NSString *const OIHMessageUserIsTypingNotification = @"com.ralfcheung.OIH.MessageUserIsTypingNotification";
NSString *const OIHMessageUserWillFetchMessage = @"com.ralfcheung.OIH.MessageUserWillFetchMessage";

NSString *const OIHAudioPlayerAVPlayerStatusDidFail = @"com.ralfcheung.OIHAVPlayerStatusDidFailNotification";


#pragma mark - PayPal API Keys
NSString *const kOIHPayPalClientIDSandbox = @"AXSVxneP8mUHKxNqoUcvA7j6VupexuBQVHQDuiYf56qEZNMqZDuyb9BO9llJUO6cLvx-VXnWeeQelC25";
NSString *const kOIHPayPalClientIDProduction = @"";


NSString *const kOIHRestaurantName = @"restaurantName";
NSString *const kOIHRestaurantAddress = @"addressString";
NSString *const kOIHRestaurantId = @"_id";
NSString *const kOIHRestaurantPhoneNumber = @"phoneNumber";
NSString *const kOIHRestaurantCurrency = @"currency";
NSString *const kOIHRestaurantOperatingHours = @"operatingHours";
#pragma mark - Meal Class

NSString *const kOIHMealName = @"name";
NSString *const kOIHMealAvailableDate = @"availableDate";
NSString *const kOIHMealPriceInLocalCurrency = @"priceInLocalCurrency";
NSString *const kOIHMealType = @"mealType";
NSString *const kOIHMealRestaurant = @"restaurant";
NSString *const kOIHMealIngredients = @"productIngredients";
NSString *const kOIHMealNetWeight = @"productNetWeight";
NSString *const kOIHMealQuantity = @"productQuantity";
NSString *const kOIHMealNote = @"productNote";


#pragma mark - User Class

NSString *const kOIHUserEmail = @"email";
NSString *const kOIHUserLocalProfile = @"local";
NSString *const kOIHUserPhoneNumber = @"phoneNumber";
NSString *const kOIHUserName = @"name";
NSString *const kOIHUserLastName = @"lastName";
NSString *const kOIHUserPastOrders = @"orders";
NSString *const kOIHUserFriends = @"friends";
NSString *const kOIHUserAddress = @"address";
NSString *const kOIHUserId = @"_id";
NSString *const kOIHUserAllowToBeAddedAsFriend = @"allowToBeAddedAsFriend";
NSString *const kOIHUserIsFriend = @"isFriend";
NSString *const kOIHUserIsFollowing = @"isFollowing";
NSString *const kOIHUserPendingRequest = @"isPendingRequest";
NSString *const kOIHUserIsWaitingForConfirmation = @"isWaitingForConfirmation";
NSString *const kOIHUserProfilePic = @"profilePic";



#pragma mark - Address Class
NSString *const kOIHAddressUserEnteredAddress = @"userEnteredAddress";
NSString *const kOIHAddressFormattedAddress = @"formattedAddress";
NSString *const kOIHAddressLatitude = @"lat";
NSString *const kOIHAddressLongitude = @"lng";
NSString *const kOIHAddressTag = @"tag";



#pragma mark - Order Class

NSString *const kOIHOrderId = @"_id";
NSString *const kOIHOrderRestaurant = @"sellerRestaurant";
NSString *const kOIHOrderUser = @"orderUser";
NSString *const kOIHOrderRecipient = @"recipientUser";
NSString *const kOIHOrderStatus = @"orderStatus";
NSString *const kOIHOrderReceipt = @"orderReceipt";
NSString *const kOIHOrderDate = @"orderDate";
NSString *const kOIHOrderMeal = @"orderMeal";
NSString *const kOIHOrderIsDelivered = @"isDelivered";
NSString *const kOIHOrderPaymentMethod = @"paymentMethod";
NSString *const kOIHOrderQuantity = @"quantity";


NSString *const kOIHOrderStatusOrdered = @"ordered";
NSString *const kOIHOrderStatusAccepted = @"accepted";
NSString *const kOIHOrderStatusDelivered = @"delivered";
NSString *const kOIHOrderStatusCanceled = @"canceled";


#pragma mark - Activity
NSString *const kOIHActivityId = @"_id";
NSString *const kOIHActivityFromUser = @"fromUser";
NSString *const kOIHActivityToUser = @"toUser";
NSString *const kOIHActivityType = @"activityType";
NSString *const kOIHActivityOrder = @"order";
NSString *const kOIHActivityCreatedAt = @"createdAt";

NSString *const kOIHActivityTypeLike = @"like";
NSString *const kOIHActivityTypeUserOrder = @"orderFood";
NSString *const kOIHActivityTypeThank = @"thank";





NSString *const kOIHImageFBIcon = @"FB-f-Logo__blue.png";
NSString *const kOIHImageFeedIcon = @"feed_icon";
NSString *const kOIHImageProfileIcon = @"profile-icon";
