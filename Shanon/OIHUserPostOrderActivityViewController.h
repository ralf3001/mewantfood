//
//  OIHUserPostOrderActivityViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHOrderModel.h"


@class OIHUserPostOrderActivityViewController;

@protocol OIHPostOrderDelegate <NSObject>

@optional

-(void)userDidPostComment;
-(void)activityOrderViewControllerWillDismiss: (OIHUserPostOrderActivityViewController *)vc;
-(void)activityOrderViewControllerDidDismiss: (OIHUserPostOrderActivityViewController *)vc;

@end

@interface OIHUserPostOrderActivityViewController : UIViewController <UIViewControllerTransitioningDelegate>

@property (nonatomic, weak) id<OIHPostOrderDelegate> delegate;
@property (nonatomic) OIHOrderModel *order;

@end
