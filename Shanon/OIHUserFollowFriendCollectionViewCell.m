//
//  OIHUserFollowFriendCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserFollowFriendCollectionViewCell.h"


@interface OIHUserFollowFriendCollectionViewCell ()
@end

@implementation OIHUserFollowFriendCollectionViewCell
@synthesize followButton, addAsFriendButton;
@synthesize buyButton;


- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
 
        followButton = [UIButton buttonWithType:UIButtonTypeSystem];
        followButton.translatesAutoresizingMaskIntoConstraints = NO;
        [followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [followButton addTarget:self action:@selector(followPerson:) forControlEvents:UIControlEventTouchUpInside];
        [followButton setTitle:NSLocalizedString(@"USER_FOLLOW", @"") forState:UIControlStateNormal];
        [self.contentView addSubview:followButton];
        
        addAsFriendButton = [UIButton buttonWithType:UIButtonTypeSystem];
        addAsFriendButton.translatesAutoresizingMaskIntoConstraints = NO;
        [addAsFriendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addAsFriendButton setTitle:NSLocalizedString(@"USER_ADD_FRIEND", @"") forState:UIControlStateNormal];
        [self.contentView addSubview:addAsFriendButton];
        
        buyButton = [UIButton buttonWithType:UIButtonTypeSystem];
        buyButton.translatesAutoresizingMaskIntoConstraints = NO;
        [buyButton addTarget:self action:@selector(buyFood) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:buyButton];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[followButton][addAsFriendButton(==followButton)]|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllCenterY | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(followButton, addAsFriendButton)]];

    }
    
    return self;
}

-(void)setFriendProfile:(OIHUserModel *)friendProfile{
  
  _friendProfile = friendProfile;
  
  if ([friendProfile isUserType:UserFriend]) {
    
    [followButton removeFromSuperview];
    followButton = nil;
    
    [addAsFriendButton removeFromSuperview];
    addAsFriendButton = nil;
    
    [buyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buyButton.backgroundColor = [UIColor colorWithRed:46/256.0f green:65/256.0f blue:114/256.0f alpha:1.0f];
    [buyButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"USER_BUY_%@_FOOD", @"buy [person] food"), friendProfile.userName] forState:UIControlStateNormal];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[buyButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(buyButton)]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[buyButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(buyButton)]];
    
  }
  else if ([friendProfile isUserType:UserPendingRequest]){
    
    followButton.backgroundColor = [UIColor colorWithRed:224/256.0f green:80/256.0f blue:76/256.0f alpha:1.0f];
    addAsFriendButton.backgroundColor = [UIColor colorWithRed:46/256.0f green:65/256.0f blue:114/256.0f alpha:1.0f];
    
    [self.addAsFriendButton setTitle:NSLocalizedString(@"USER_REMOVE_FRIEND_REQUEST", @"") forState:UIControlStateNormal];
    [addAsFriendButton addTarget:self action:@selector(removeFriendRequestButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [buyButton removeFromSuperview];
    buyButton = nil;
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[addAsFriendButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(addAsFriendButton)]];
    
    
  }else if (!friendProfile.isUserAllowToBeAddedAsFriend && [friendProfile isUserType:UserFriend]){
  
    [addAsFriendButton removeFromSuperview];
    addAsFriendButton = nil;
    NSAssert(followButton, @"follow button is null");
    followButton.backgroundColor = [UIColor colorWithRed:224/256.0f green:80/256.0f blue:76/256.0f alpha:1.0f];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[followButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(followButton)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[followButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(followButton)]];

  
  }else if([friendProfile isUserType:UserIsFollowing]){
    
    [addAsFriendButton removeFromSuperview];
    addAsFriendButton = nil;
    [self.followButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"USER_UNFOLLOW_%@", @""), _friendProfile.userName] forState:UIControlStateNormal];
    [self.followButton removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    [self.followButton addTarget:self action:@selector(unfollowUserButton:) forControlEvents:UIControlEventTouchUpInside];

    self.followButton.backgroundColor = [UIColor colorWithRed:224/256.0f green:80/256.0f blue:76/256.0f alpha:1.0f];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[followButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(followButton)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[followButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(followButton)]];

  }
  else{
    
    followButton.backgroundColor = [UIColor colorWithRed:224/256.0f green:80/256.0f blue:76/256.0f alpha:1.0f];
    addAsFriendButton.backgroundColor = [UIColor colorWithRed:46/256.0f green:65/256.0f blue:114/256.0f alpha:1.0f];
    
    [self.addAsFriendButton setTitle:NSLocalizedString(@"USER_ADD_FRIEND", @"") forState:UIControlStateNormal];
    [addAsFriendButton addTarget:self action:@selector(addAsFriend:) forControlEvents:UIControlEventTouchUpInside];
    
    [buyButton removeFromSuperview];
    buyButton = nil;
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[addAsFriendButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(addAsFriendButton)]];
    
  }

  
}


-(void)removeFriendRequestButton: (UIButton *)button{
    
    if ([self.delegate respondsToSelector:@selector(didPressRemoveFriendRequestButton:)]) {
        [self.delegate didPressRemoveFriendRequestButton:self];
    }

    [self.addAsFriendButton setTitle:NSLocalizedString(@"USER_ADD_FRIEND", @"") forState:UIControlStateNormal];
    
    [addAsFriendButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [addAsFriendButton addTarget:self action:@selector(addAsFriend:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)followPerson: (UIButton *)button{
    
  if ([self.delegate respondsToSelector:@selector(didPressFollowFriendButton:)]) {
    [self.delegate didPressFollowFriendButton:self];
    [self.followButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"USER_UNFOLLOW_%@", @""), _friendProfile.userName] forState:UIControlStateNormal];
    [self.followButton removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    [self.followButton addTarget:self action:@selector(unfollowUserButton:) forControlEvents:UIControlEventTouchUpInside];
  }
 
  
}


-(void)addAsFriend: (UIButton *)button{
    
  if ([self.delegate respondsToSelector:@selector(didPressAddFriendButton:)]) {
    
    [self.delegate didPressAddFriendButton:self];
    
    [self.addAsFriendButton setTitle:NSLocalizedString(@"USER_REMOVE_FRIEND_REQUEST", @"") forState:UIControlStateNormal];
    [addAsFriendButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [addAsFriendButton addTarget:self action:@selector(removeFriendRequestButton:) forControlEvents:UIControlEventTouchUpInside];

  }


}

-(void)unfollowUserButton: (UIButton *)button{
  
  //-(void)didPressUnfollowFriendButton: (OIHUserFollowFriendCollectionViewCell *)cell;
  if ([self.delegate respondsToSelector:@selector(didPressUnfollowFriendButton:)]) {
    [self.delegate didPressUnfollowFriendButton:self];
    [self.followButton setTitle:NSLocalizedString(@"USER_FOLLOW", @"") forState:UIControlStateNormal];
    [self.followButton removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    [self.followButton addTarget:self action:@selector(followPerson:) forControlEvents:UIControlEventTouchUpInside];
  }
  
}


-(void)buyFood{
    
    if ([self.delegate respondsToSelector:@selector(didPressBuyFoodButton:)]) {
        [self.delegate didPressBuyFoodButton:self];
    }
    
}



@end
