//
//  OIHRestaurantInfoUserRatingsCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantInfoUserRatingsCollectionViewCell.h"

#define kProgressViewBarWidth 200

@interface OIHRestaurantInfoUserRatingsCollectionViewCell ()
@property (nonatomic) UIProgressView *angryProgressBar;
@property (nonatomic) UIProgressView *notSatisfiedProgressBar;
@property (nonatomic) UIProgressView *sickProgressBar;
@property (nonatomic) UIProgressView *tastyProgressBar;
@property (nonatomic) UIProgressView *loveItProgressBar;

@property (nonatomic) UILabel *totalRatingLabel;

@end

@implementation OIHRestaurantInfoUserRatingsCollectionViewCell
@synthesize angryLabel, notSatisfiedLabel, sickLabel, tastyLabel, loveItLabel;
@synthesize totalRatingLabel;

-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {

        totalRatingLabel = [self newLabel:[NSString stringWithFormat:NSLocalizedString(@"RESTAURANT_INFO_TOTAL_REVIEWS_%ld", @"total ratings: [number]"), 0]];
        totalRatingLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-Light" size:25.0f];
        [self.contentView addSubview:totalRatingLabel];

        angryLabel = [self newLabel: [NSString stringWithUTF8String:"\xF0\x9F\x98\xA1"]];
        [self.contentView addSubview:angryLabel];
        
        notSatisfiedLabel = [self newLabel: [NSString stringWithUTF8String:"\xF0\x9F\x98\x9E"]];
        [self.contentView addSubview:notSatisfiedLabel];
        
        sickLabel = [self newLabel: [NSString stringWithUTF8String:"\xF0\x9F\x98\xB7"]];
        [self.contentView addSubview:sickLabel];
        
        tastyLabel = [self newLabel: [NSString stringWithUTF8String:"\xF0\x9F\x98\x8B"]];
        [self.contentView addSubview:tastyLabel];
        
        loveItLabel = [self newLabel: [NSString stringWithUTF8String:"\xF0\x9F\x98\x8D"]];
        [self.contentView addSubview:loveItLabel];
        
        
        UIView *ratingLabelSeparator = [UIView new];
        ratingLabelSeparator.translatesAutoresizingMaskIntoConstraints = NO;
        ratingLabelSeparator.backgroundColor = [UIColor highlightColor];
        [self.contentView addSubview:ratingLabelSeparator];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:ratingLabelSeparator attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:30]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[totalRatingLabel]-15-[ratingLabelSeparator(==1)]-20-[angryLabel]-[notSatisfiedLabel]-[sickLabel]-[tastyLabel]-[loveItLabel]-20-|" options:NSLayoutFormatAlignAllLeft metrics:0 views:NSDictionaryOfVariableBindings(totalRatingLabel, angryLabel, notSatisfiedLabel, sickLabel, tastyLabel, loveItLabel, ratingLabelSeparator)]];
        
        
        _angryProgressBar = [self newProgressView];
        [self.contentView addSubview:_angryProgressBar];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_angryProgressBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:5]];

        _notSatisfiedProgressBar = [self newProgressView];
        [self.contentView addSubview:_notSatisfiedProgressBar];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_notSatisfiedProgressBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:5]];

        _sickProgressBar = [self newProgressView];
        [self.contentView addSubview:_sickProgressBar];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_sickProgressBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:5]];

        _tastyProgressBar = [self newProgressView];
        [self.contentView addSubview:_tastyProgressBar];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_tastyProgressBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:5]];

        _loveItProgressBar = [self newProgressView];
        [self.contentView addSubview:_loveItProgressBar];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_loveItProgressBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:5]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[angryLabel]-[_angryProgressBar(==width)]-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"width": @(kProgressViewBarWidth)} views:NSDictionaryOfVariableBindings(angryLabel, _angryProgressBar)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[notSatisfiedLabel]-[_notSatisfiedProgressBar(==width)]-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"width": @(kProgressViewBarWidth)} views:NSDictionaryOfVariableBindings(notSatisfiedLabel, _notSatisfiedProgressBar)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[sickLabel]-[_sickProgressBar(==width)]-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"width": @(kProgressViewBarWidth)} views:NSDictionaryOfVariableBindings(sickLabel, _sickProgressBar)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[tastyLabel]-[_tastyProgressBar(==width)]-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"width": @(kProgressViewBarWidth)} views:NSDictionaryOfVariableBindings(tastyLabel, _tastyProgressBar)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[loveItLabel]-[_loveItProgressBar(==width)]-|" options:NSLayoutFormatAlignAllCenterY metrics:@{@"width": @(kProgressViewBarWidth)} views:NSDictionaryOfVariableBindings(loveItLabel, _loveItProgressBar)]];
        
    }
    
    return self;
    
}

-(void)setRestaurantInfo:(OIHRestaurantModel *)restaurantInfo{
    
    _restaurantInfo = restaurantInfo;
    NSMutableArray *ratings = [_restaurantInfo.rating mutableCopy];
    
    for (int i = 0; i < ratings.count; i++) {
        ratings[i] = [NSNumber numberWithInteger:arc4random_uniform(74)];
    }
    
    long totalRatings = 0;
    for (int i = 0; i < ratings.count; i++) {
        totalRatings += [ratings[i] integerValue];
    }
    
    totalRatingLabel.text = [NSString stringWithFormat:NSLocalizedString(@"RESTAURANT_INFO_TOTAL_REVIEWS_%ld", @"total reviews: [number]"), totalRatings];
    
    if (totalRatings > 0) {
       
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            [_angryProgressBar setProgress:(float)[ratings[0] integerValue] / totalRatings animated:YES];
            [_notSatisfiedProgressBar setProgress:(float)[ratings[1] integerValue] / totalRatings animated:YES];
            [_sickProgressBar setProgress:(float)[ratings[2] integerValue] / totalRatings animated:YES];
            [_tastyProgressBar setProgress:(float)[ratings[3] integerValue] / totalRatings animated:YES];
            [_loveItProgressBar setProgress:(float)[ratings[4] integerValue] / totalRatings animated:YES];

        });
        
    }
    
    angryLabel.text = [NSString stringWithFormat:@"%@ : %ld", [NSString stringWithUTF8String:"\xF0\x9F\x98\xA1"], (long)[ratings[0] integerValue]];
    notSatisfiedLabel.text = [NSString stringWithFormat:@"%@ : %ld", [NSString stringWithUTF8String:"\xF0\x9F\x98\x9E"], (long)[ratings[1] integerValue]];
    sickLabel.text = [NSString stringWithFormat:@"%@ : %ld", [NSString stringWithUTF8String:"\xF0\x9F\x98\xB7"], (long)[ratings[2] integerValue]];
    tastyLabel.text = [NSString stringWithFormat:@"%@ : %ld", [NSString stringWithUTF8String:"\xF0\x9F\x98\x8B"], (long)[ratings[3] integerValue]];
    loveItLabel.text = [NSString stringWithFormat:@"%@ : %ld", [NSString stringWithUTF8String:"\xF0\x9F\x98\x8D"], (long)[ratings[4] integerValue]];
    
}

-(UILabel *)newLabel: (NSString *)text{
    
    UILabel *label = [UILabel new];
    label.text = text;
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    return label;
    
}


-(UIProgressView *)newProgressView{
    
    UIProgressView *p = [UIProgressView new];
    p.translatesAutoresizingMaskIntoConstraints = NO;
    p.progressTintColor = [UIColor colorWithRed:212/255.0f green:106/255.0f blue:106/255.0f alpha:1.0f]; //progress bar color
    p.trackTintColor = [UIColor highlightColor]; //background
    p.progress = 0.0f;
    
    return p;
}

@end
