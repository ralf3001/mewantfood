//
//  OIHCalendarDay.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCalendarDay.h"

@implementation OIHCalendarDay

- (id)init {

  self = [super init];
  if (self) {
    [self commonInit];
  }
  return self;
}

- (id)initWithFrame:(CGRect)frame {

  self = [super initWithFrame:frame];
  if (self) {
    [self commonInit];
  }
  return self;
}

- (void)commonInit {

  _mealViewList = [[NSMutableArray alloc] init];
  _dateLabel = [[UILabel alloc] init];
  _dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
  [self addSubview:_dateLabel];
  
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_dateLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_dateLabel)]];
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_dateLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_dateLabel)]];
}

- (void)setDate:(NSDate *)date {

  _date = date;
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  dateFormatter.dateStyle = NSDateFormatterShortStyle;
  [_dateLabel setText:[dateFormatter stringFromDate:_date]];

  if ([_date onSameDay:[NSDate date]]) {
    self.backgroundColor = [UIColor highlightColor];
  }
  
}

@end
