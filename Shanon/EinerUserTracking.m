//
//  EinerUserTracking.m
//  Einer
//
//  Created by Ralf Cheung on 5/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "EinerUserTracking.h"
#import "AppDelegate.h"



@interface EinerUserTracking ()

@property (nonatomic) NSDate *startTimeForView;
@property (nonatomic) NSDate *endTimeForView;
@end




@implementation EinerUserTracking
@synthesize startTimeForView, endTimeForView;
@synthesize VCsInThisSection;
@synthesize userId;


-(id)init{
    
    self = [self commonInit];
    if (self) {
        startTimeForView = [NSDate date];
        VCsInThisSection = [NSMutableArray new];
    }
    return self;
}


-(id)commonInit{
    
    return [super init];
    
}



+(EinerUserTracking *)sharedInstance{
    
    static EinerUserTracking *shared = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[EinerUserTracking alloc] init];
    });
    
    return shared;
}


-(void)applicationDidEnterBackground{
    
    
    if (VCsInThisSection.count == 1) {
        NSTimeInterval intervalInSeconds = [startTimeForView timeIntervalSinceNow]; //time elapsed in seconds for the last view controller
        
        ViewControllerTrackingInfo *view = [self findAndRemoveLastObject];
        if (view) {
            view.timeElapsed = -intervalInSeconds;
            [VCsInThisSection addObject:view];
        }
    }
    
    [VCsInThisSection addObject:[[ViewControllerTrackingInfo alloc] initWithViewControllerString:@"APP PAUSED" type:ApplicationDidGoBackground]];
    
}

-(void)applicationDidBecomeActive{
    
    
    [VCsInThisSection addObject:[[ViewControllerTrackingInfo alloc] initWithViewControllerString:@"APP RESUMED" type:ApplicationDidGoBackground]];
    
    
}



-(void)addTabBarControllerToMonitor: (UITabBarController *)tabBar{
    
    tabBar.delegate = self;
    UINavigationController *nav = (UINavigationController *)[tabBar.viewControllers objectAtIndex:0];
    nav.delegate = self;
    
}


-(void)addNavigationControllerToMonitor: (UINavigationController *)nav{
    
    nav.delegate = self;
    
}



-(ViewControllerTrackingInfo *)findAndRemoveLastObject{
    
    
    NSArray *filtered = [VCsInThisSection filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pageType == %d", ViewControllerEnum]];
    
    ViewControllerTrackingInfo *view = [filtered lastObject];
    if (VCsInThisSection.count > 0) {
        [VCsInThisSection removeLastObject];
    }
    return view;
}



-(void)ionoWhatToNameThisMethod: (UIViewController *)viewController{
    
    
    NSTimeInterval intervalInSeconds = [startTimeForView timeIntervalSinceNow]; //time elapsed in seconds for the last view controller

    ViewControllerTrackingInfo *view = [self findAndRemoveLastObject];
    if (view) { //update the last view controller's time
        view.timeElapsed = -intervalInSeconds;
        [VCsInThisSection addObject:view];
    }
    
    
    //add the current view controller to the array
    
    ViewControllerTrackingInfo *newView = [[ViewControllerTrackingInfo alloc] initWithViewControllerString:NSStringFromClass([viewController class]) type:ViewControllerEnum];
    [VCsInThisSection addObject:newView];
    
    startTimeForView = [NSDate date];

    
    
}


-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    if ([viewController isKindOfClass:[UINavigationController class]]) {

        UINavigationController *nav = (UINavigationController *)viewController;
        nav.delegate = self;
        
    }else{
        [self ionoWhatToNameThisMethod:viewController];
        
    }
    
}


-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    [self ionoWhatToNameThisMethod:viewController];
    
}

#pragma Incomplete Implementation
-(void)addEvent: (NSString *)eventName{
    
    [VCsInThisSection addObject:[[ViewControllerTrackingInfo alloc] initWithViewControllerString:eventName type:UIControlEvent]];
    
}

-(void)openedPushNotification: (NSDictionary *)notification{
    
    
}



void exceptionHandler(NSException *exception) {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:[exception reason] forKey:@"reason"];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:[exception callStackSymbols]] forKey:@"stack"];
    [defaults setObject:[exception name] forKey:@"name"];
    [defaults setObject:[exception userInfo] forKey:@"userInfo"];
    [defaults setObject:[NSDate date] forKey:@"timeStamp"];
    [defaults synchronize];
    
}


-(void)uploadCrashReport{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSArray *callStackSymbolsArray = [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"stack"]] ;
    NSString *crashReason = [defaults objectForKey:@"reason"];
    NSDate *crashTimeStamp = [defaults objectForKey:@"timeStamp"];
    NSString *name = [defaults objectForKey:@"name"];
    
    NSDictionary *mainBundle = [[NSBundle mainBundle] infoDictionary];
    
    NSDictionary *crashDictionary = [NSDictionary dictionaryWithObjectsAndKeys:callStackSymbolsArray, @"callStackSymbols",
                                    crashReason, @"crashReason",
                                    [crashTimeStamp JSONDateFromDate], @"timeStamp",
                                    name, @"crashName",
                                    mainBundle[@"CFBundleShortVersionString"], @"version",
                                    mainBundle[@"CFBundleVersion"], @"build",
                                    [[UIDevice currentDevice] systemVersion], @"systemVersion",
                                    [[UIDevice currentDevice] model], @"model" ,nil];
    
    //for instance
    [[JSONRequest sharedInstance] fetch:@"crashReport" method:@"POST" data:crashDictionary completionHandler:^(id data, NSError *error) {
        if (!error) {

            [defaults removeObjectForKey:@"reason"];
            [defaults removeObjectForKey:@"timeStamp"];
            [defaults removeObjectForKey:@"name"];
            [defaults removeObjectForKey:@"userInfo"];
            [defaults removeObjectForKey:@"stack"];

        }
    }];

}

-(void)handleException{
    NSSetUncaughtExceptionHandler(&exceptionHandler);
}


-(void)generateCrashAnalyticReport{
    
    [self handleException];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if ([defaults objectForKey:@"reason"]) {
        #ifndef DEBUG
            [self uploadCrashReport];
        #endif
        }
        
    });
    
    
    
}


@end
