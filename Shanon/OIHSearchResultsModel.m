//
//  OIHSearchResultsModel.m
//  Shanon
//
//  Created by Ralf Cheung on 8/5/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSearchResultsModel.h"

@implementation OIHSearchResultsModel

-(id)initWithInfo: (NSDictionary *)info{
    
    if (self = [super initWithInfo:info]) {
        
        self.surchargeRate = info[@"surchargeRate"];

        if ([info[@"searchedStores"] isKindOfClass:[NSArray class]]) {
            self.restaurants = [OIHRestaurantModel restaurantsFromArray:info[@"searchedStores"]];
        }

        if ([info[@"searchedMeals"] isKindOfClass:[NSArray class]]) {
            
            self.meals = [OIHMealModel mealsFromArray:info[@"searchedMeals"]];
            
            for (int i = 0; i < self.restaurants.count; i++) {
                
                OIHMealModel *meal = self.meals[i];
                meal.chargeRate = self.surchargeRate;
                meal.restaurant = self.restaurants[i];
                
            }
            
        }
        
        
    }
    return self;
}



@end
