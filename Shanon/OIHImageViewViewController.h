//
//  OIHImageViewViewController.h
//  Shanon
//
//  Created by Ralf Cheung on 23/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ModalViewControllerDismissDelegate <NSObject>

-(void)modalViewControllerWillDismissWithCancelOption;
-(void)modalViewControllerWillDismissWithDoneOption;

@end

@interface OIHImageViewViewController : UIViewController <UIViewControllerTransitioningDelegate>

@property (nonatomic, weak) id <ModalViewControllerDismissDelegate> delegate;
-(id)initWithImage: (UIImage *)image;

@end
