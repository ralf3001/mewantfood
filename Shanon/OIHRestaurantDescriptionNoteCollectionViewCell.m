//
//  OIHRestaurantDescriptionNoteCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 15/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantDescriptionNoteCollectionViewCell.h"

@implementation OIHRestaurantDescriptionNoteCollectionViewCell

-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *divider = [UIView new];
        divider.translatesAutoresizingMaskIntoConstraints = NO;
        divider.backgroundColor = [UIColor highlightColor];
        [self.contentView addSubview:divider];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[divider]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(divider)]];

        _descriptionLabel = [UILabel new];
        _descriptionLabel.numberOfLines = 0;
        _descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _descriptionLabel.textColor = [UIColor grayColor];
        _descriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        
        [self.contentView addSubview:_descriptionLabel];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_descriptionLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_descriptionLabel)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_descriptionLabel]-[divider(==1)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_descriptionLabel, divider)]];
    }
    return self;
}

@end
