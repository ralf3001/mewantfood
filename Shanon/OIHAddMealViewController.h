//
//  OIHAddMealViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHMealModel.h"
#import "OIHRestaurantModel.h"
#import <UIKit/UIKit.h>

@interface OIHAddMealViewController : UIViewController
@property(nonatomic) OIHRestaurantModel *restaurantInfo;
@end
