//
//  OIHUserRestaurantListTableViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHAddRestaurantViewController.h"
#import "OIHMapViewAddressPin.h"
#import "OIHMapViewFullScreenViewController.h"
#import "OIHMerchantRestaurantCollectionViewCell.h"
#import "OIHMerchantRestaurantListTableViewCell.h"
#import "OIHRestaurantModel.h"
#import "OIHUserProfileCollectionViewCell.h"
#import "OIHUserRestaurantInfoViewController.h"
#import "OIHUserRestaurantListTableViewController.h"
#import "StretchyHeaderCollectionViewLayout.h"
#import <MapKit/MapKit.h>

#define kTableViewHeaderHeight 200

@interface OIHUserRestaurantListTableViewController () <
UICollectionViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout, UISearchBarDelegate,
UISearchResultsUpdating, UISearchControllerDelegate>

@property(nonatomic) NSMutableArray *restaurantList;
@property(nonatomic) UIImageView *sadFaceImageView;
@property(nonatomic) UILabel *sadfaceLabel;
@property(nonatomic) UIView *overlayView;
@property(nonatomic) UIImageView *arrowImageView;
@property(nonatomic) UICollectionView *collectionView;
@property(nonatomic, readonly) UICollectionReusableView *header;
@property(nonatomic, strong, retain) MKMapView *mapView;
@property(nonatomic) UIView *headerView;
@property(nonatomic) BOOL isFetching;

@property(nonatomic) UIRefreshControl *refreshControl;
@property(nonatomic) NSInteger currentPage;

@property(nonatomic) UISearchController *searchController;

@property(nonatomic) NSArray *searchResults;
@end

@implementation OIHUserRestaurantListTableViewController
@synthesize restaurantList;
@synthesize sadFaceImageView;
@synthesize sadfaceLabel;
@synthesize overlayView;
@synthesize arrowImageView;
@synthesize collectionView = _collectionView;
@synthesize header;
@synthesize mapView = _mapView;
@synthesize headerView;
@synthesize isFetching;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIBarButtonItem *addRestaurantButton = [[UIBarButtonItem alloc]
                                            initWithImage:[IonIcons imageWithIcon:ion_ios_plus_empty
                                                                             size:30.0f
                                                                            color:self.navigationController
                                                           .navigationBar.tintColor]
                                            style:UIBarButtonItemStylePlain
                                            target:self
                                            action:@selector(addRestaurant)];
    
    self.navigationItem.rightBarButtonItem = addRestaurantButton;
    
    _searchController =
    [[UISearchController alloc] initWithSearchResultsController:nil];
    _searchController.searchResultsUpdater = self;
    _searchController.delegate = self;
    _searchController.searchBar.delegate = self;
    _searchController.dimsBackgroundDuringPresentation = false;
    _searchController.hidesNavigationBarDuringPresentation = false;
    self.navigationItem.titleView = _searchController.searchBar;
    
    restaurantList = [NSMutableArray new];
    isFetching = NO;
    
    [self fetchRestaurants];
}

- (NSArray *)dataSourceForSearch {
    return restaurantList;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    [self searchFriend:searchController.searchBar.text];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchFriend:searchBar.text];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [_collectionView reloadData];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    [self searchFriend:searchBar.text];
}

- (void)searchFriend:(NSString *)name {
    
    if (![name isEqualToString:@""]) {
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:@"restaurantName CONTAINS[cd] %@", name];
        _searchResults = [restaurantList filteredArrayUsingPredicate:predicate];
        [_collectionView reloadData];
    }
}

- (void)createCollectionView {
    
    if (!_collectionView) {
        
        [sadFaceImageView removeFromSuperview];
        sadFaceImageView = nil;
        
        [sadfaceLabel removeFromSuperview];
        sadfaceLabel = nil;
        [overlayView removeFromSuperview];
        
        StretchyHeaderCollectionViewLayout *stretchyLayout;
        stretchyLayout = [[StretchyHeaderCollectionViewLayout alloc] init];
        [stretchyLayout setSectionInset:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)];
        [stretchyLayout
         setHeaderReferenceSize:CGSizeMake(self.view.frame.size.width, 200.0)];
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:stretchyLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView setAlwaysBounceVertical:YES];
        [_collectionView setShowsVerticalScrollIndicator:NO];
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        _collectionView.layer.shadowColor = [UIColor blackColor].CGColor;
        _collectionView.layer.shadowOffset = CGSizeMake(-1, -1);
        
        _refreshControl = [[UIRefreshControl alloc] init];
        [_refreshControl addTarget:self
                            action:@selector(fetchRestaurants)
                  forControlEvents:UIControlEventValueChanged];
        [_collectionView addSubview:_refreshControl];
        
        [self.view addSubview:_collectionView];
        
        [_collectionView registerClass:
         [OIHMerchantRestaurantCollectionViewCell class]
            forCellWithReuseIdentifier:@"cell"];
        [_collectionView registerClass:[UICollectionReusableView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:@"header"];
        
        [self.view
         addConstraints:
         [NSLayoutConstraint
          constraintsWithVisualFormat:@"H:|[_collectionView]|"
          options:0
          metrics:0
          views:NSDictionaryOfVariableBindings(
                                               _collectionView)]];
        [self.view
         addConstraints:
         [NSLayoutConstraint
          constraintsWithVisualFormat:@"V:|[_collectionView]|"
          options:0
          metrics:0
          views:NSDictionaryOfVariableBindings(
                                               _collectionView)]];
        
    } else {
        
        [sadFaceImageView removeFromSuperview];
        sadFaceImageView = nil;
        
        [sadfaceLabel removeFromSuperview];
        sadfaceLabel = nil;
        [overlayView removeFromSuperview];
        
        [_collectionView reloadData];
    }
}

- (void)setupEmptyRestaurantView {
    
    if (!sadFaceImageView) {
        
        sadFaceImageView = [[UIImageView alloc]
                            initWithImage:[UIImage imageNamed:@"sadface_icon"]];
        sadFaceImageView.translatesAutoresizingMaskIntoConstraints = NO;
        sadFaceImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:sadFaceImageView];
        
        sadfaceLabel = [UILabel new];
        sadfaceLabel.numberOfLines = 0;
        sadfaceLabel.textAlignment = NSTextAlignmentCenter;
        sadfaceLabel.textColor = [UIColor lightGrayColor];
        sadfaceLabel.text =
        NSLocalizedString(@"NO_RESTAURANT_YET",
                          @"No restaurant message, ask to add a restaurant");
        sadfaceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:sadfaceLabel];
        
        [self.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem:sadFaceImageView
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:0
                                  multiplier:1.0f
                                  constant:200]];
        
        [self.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem:sadFaceImageView
                                  attribute:NSLayoutAttributeWidth
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:sadFaceImageView
                                  attribute:NSLayoutAttributeHeight
                                  multiplier:1.0f
                                  constant:0.0f]];
        
        [self.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem:sadFaceImageView
                                  attribute:NSLayoutAttributeCenterY
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:self.view
                                  attribute:NSLayoutAttributeCenterY
                                  multiplier:1.0f
                                  constant:0.0f]];
        
        [self.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem:sadfaceLabel
                                  attribute:NSLayoutAttributeTop
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:sadFaceImageView
                                  attribute:NSLayoutAttributeBottom
                                  multiplier:1.0f
                                  constant:20.0f]];
        
        [self.view
         addConstraint:[NSLayoutConstraint
                        constraintWithItem:sadfaceLabel
                        attribute:NSLayoutAttributeWidth
                        relatedBy:NSLayoutRelationEqual
                        toItem:nil
                        attribute:0
                        multiplier:1.0
                        constant:self.view.frame.size.width - 90]];
        
        [self.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem:sadfaceLabel
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:self.view
                                  attribute:NSLayoutAttributeCenterX
                                  multiplier:1.0f
                                  constant:0.0f]];
        
        [self.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem:sadFaceImageView
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:self.view
                                  attribute:NSLayoutAttributeCenterX
                                  multiplier:1.0f
                                  constant:0.0f]];
        
        overlayView = [UIView new];
        overlayView.translatesAutoresizingMaskIntoConstraints = NO;
        overlayView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        
        arrowImageView = [[UIImageView alloc]
                          initWithImage:[IonIcons imageWithIcon:ion_ios_arrow_up
                                                           size:50
                                                          color:[UIColor whiteColor]]];
        arrowImageView.center = CGPointMake(self.view.frame.size.width - 40, 40);
        [overlayView addSubview:arrowImageView];
        
        [self.view insertSubview:overlayView aboveSubview:sadFaceImageView];
        
        [self.view
         addConstraints:
         [NSLayoutConstraint
          constraintsWithVisualFormat:@"H:|[overlayView]|"
          options:0
          metrics:0
          views:NSDictionaryOfVariableBindings(
                                               overlayView)]];
        [self.view
         addConstraints:
         [NSLayoutConstraint
          constraintsWithVisualFormat:@"V:|[overlayView]|"
          options:0
          metrics:0
          views:NSDictionaryOfVariableBindings(
                                               overlayView)]];
        
        CABasicAnimation *animation =
        [CABasicAnimation animationWithKeyPath:@"position"];
        [animation setDuration:0.3];
        [animation setRepeatCount:100];
        [animation setAutoreverses:YES];
        
        [animation
         setFromValue:[NSValue
                       valueWithCGPoint:CGPointMake(arrowImageView.center.x,
                                                    arrowImageView.center.y -
                                                    10)]];
        [animation
         setToValue:[NSValue
                     valueWithCGPoint:CGPointMake(arrowImageView.center.x,
                                                  arrowImageView.center.y +
                                                  10)]];
        
        [arrowImageView.layer addAnimation:animation forKey:@"position"];
    }
}


- (void)addRestaurant {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [overlayView removeFromSuperview];
        [self.navigationController
         pushViewController:[OIHAddRestaurantViewController new]
         animated:YES];
    });
}

- (void)fetchRestaurants:(NSInteger)page {
    
    isFetching = YES;
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"GETTING_RESTAURANTS", @"")];
    
    JSONRequestSettings *setting =
    [[JSONRequestSettings alloc] initWithRoute:@"restaurant"
                                    httpMethod:HTTP_GET];
    setting.cachePolicy = UseCacheIfTimeout;
    
    [[JSONRequest sharedInstance]
     fetch:setting
     completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
         
         [_refreshControl endRefreshing];
         
         if (!error) {
             
             [SVProgressHUD dismiss];
             isFetching = NO;
             
             [self populateData:data isCacheData:isCachedResult];
             
         } else {
             
             [SVProgressHUD
              showErrorWithStatus:NSLocalizedString(@"SERVER_CONNECTION_ERROR",
                                                    @"")
              maskType:SVProgressHUDMaskTypeBlack];
             
             dispatch_after(
                            dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
                            dispatch_get_main_queue(), ^{
                                [SVProgressHUD dismiss];
                            });
             
             //[self setupEmptyRestaurantView];
         }
         
     }];
}

- (void)fetchRestaurants {
    
    [self fetchRestaurants:0];
}

- (void)populateData:(NSArray *)data isCacheData:(BOOL)isCache {
    
    NSArray *feedArray = [OIHRestaurantModel restaurantsFromArray:data];
    
    if (isCache) {
        
        // initial fetch
        restaurantList = [NSMutableArray arrayWithArray:feedArray];
        restaurantList.isDataDirty = [NSNumber numberWithBool:isCache];
        
    } else { // new data, flush cache data in the array
        
        if (restaurantList.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            // new data from network
            if (data.count > 0) {
                [self addMapPinsFromLocations:feedArray];
                restaurantList = [NSMutableArray arrayWithArray:feedArray];
                restaurantList.isDataDirty = [NSNumber numberWithBool:NO];
            }else{
                [self setupEmptyRestaurantView];
            }
            
        } else {
            
            // new data from network
            if (data.count > 0) {
                [restaurantList addObjectsFromArray:feedArray];
                
            }else{
                //hasMoreStories = NO;
            }
            
        }
    }
    [self createCollectionView];
}

#pragma mark - <UICollectionViewDelegate>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)cv
     numberOfItemsInSection:(NSInteger)section {
    
    if (!self.searchController.active) {
        return restaurantList.count;
    } else {
        return _searchResults.count;
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if (!header) {
        
        header = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                    withReuseIdentifier:@"header"
                                                           forIndexPath:indexPath];
        
        [header
         addGestureRecognizer:[[UITapGestureRecognizer alloc]
                               initWithTarget:self
                               action:@selector(fullScreenMapView)]];
        [self createMapView];
        
        [header addSubview:_mapView];
        [header addConstraints:
         [NSLayoutConstraint
          constraintsWithVisualFormat:@"H:|[_mapView]|"
          options:0
          metrics:0
          views:NSDictionaryOfVariableBindings(
                                               _mapView)]];
        [header addConstraints:
         [NSLayoutConstraint
          constraintsWithVisualFormat:@"V:|[_mapView]|"
          options:0
          metrics:0
          views:NSDictionaryOfVariableBindings(
                                               _mapView)]];
    }
    
    return header;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHRestaurantModel *info = restaurantList[indexPath.row];
    
    NSDictionary *attributes = [NSDictionary
                                dictionaryWithObjectsAndKeys:(id)[UIFont
                                                                  fontWithName:@"HelveticaNeue-Thin"
                                                                  size:30.0f],
                                NSFontAttributeName,
                                (id)[UIColor lightGrayColor],
                                NSForegroundColorAttributeName, nil];
    
    NSString *correctString = info.restaurantName;
    if (!correctString) {
        correctString = @"";
    }
    
    NSAttributedString *attrText;
    attrText = [[NSAttributedString alloc] initWithString:correctString
                                               attributes:attributes];
    // use user's address in production
    CGRect rect = [attrText
                   boundingRectWithSize:CGSizeMake(collectionView.frame.size.width - 36,
                                                   10000)
                   options:NSStringDrawingUsesLineFragmentOrigin |
                   NSStringDrawingUsesFontLeading
                   context:nil];
    
    CGRect addressLabelRect = [info.addressInfo
                               boundingRectWithSize:CGSizeMake(collectionView.frame.size.width - 36,
                                                               10000)
                               options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:nil
                               context:nil];
    
    return CGSizeMake(collectionView.frame.size.width,
                      rect.size.height + 18 + 35 + addressLabelRect.size.height +
                      18);
}

- (OIHMerchantRestaurantCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHMerchantRestaurantCollectionViewCell *cell =
    (OIHMerchantRestaurantCollectionViewCell *)[collectionView
                                                dequeueReusableCellWithReuseIdentifier:@"cell"
                                                forIndexPath:indexPath];
    
    if (!self.searchController.active) {
        
        OIHRestaurantModel *restaurant = restaurantList[indexPath.row];
        [cell setAttributes:restaurant];
        
    } else {
        OIHRestaurantModel *restaurant = _searchResults[indexPath.row];
        [cell setAttributes:restaurant];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    OIHUserRestaurantInfoViewController *vc =
    [OIHUserRestaurantInfoViewController new];
    
    if (!self.searchController.active) {
        vc.restaurantInfo = restaurantList[indexPath.row];
    } else {
        vc.restaurantInfo = _searchResults[indexPath.row];
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)addMapPinsFromLocations:(NSArray *)data {
    
    NSMutableArray *pins = [NSMutableArray new];
    
    double maxLng = -10, minLng = 180, maxLat = -90,
    minLat = 90; // coz those are the minimum and max. values for lng and
    // lat respectively. google it.
    
    for (OIHRestaurantModel *restaurant in data) {
        
        double lng =
        [restaurant.restaurantInfo[@"geometry"][@"coordinates"][0] doubleValue];
        double lat =
        [restaurant.restaurantInfo[@"geometry"][@"coordinates"][1] doubleValue];
        
        if (lat > maxLat) {
            maxLat = lat;
        }
        if (lat < minLat) {
            minLat = lat;
        }
        if (lng > maxLng) {
            maxLng = lng;
        }
        
        if (lng < minLng) {
            minLng = lng;
        }
        
        OIHMapViewAddressPin *pin = [[OIHMapViewAddressPin alloc]
                                     initWithCoordinates:CLLocationCoordinate2DMake(lat, lng)
                                     placeName:restaurant.restaurantName
                                     formattedAddress:restaurant.addressInfo];
        [pins addObject:pin];
    }
    
    [_mapView addAnnotations:pins];
    
    //  MKCoordinateRegion region = MKCoordinateRegionMake(
    //      CLLocationCoordinate2DMake((maxLat + minLat) / 2, (maxLng + minLng) / 2),
    //      MKCoordinateSpanMake(
    //          maxLat - minLat + (maxLat - minLat) / data.count,
    //          maxLng - minLng + (maxLng - minLng) / data.count));
    //
    //      [_mapView setRegion:region animated:YES];
}

- (void)createMapView {
    
    if (!_mapView) {
        _mapView = [MKMapView new];
        _mapView.translatesAutoresizingMaskIntoConstraints = NO;
        _mapView.showsBuildings = YES;
        _mapView.showsPointsOfInterest = YES;
        _mapView.showsCompass = YES;
        _mapView.showsScale = YES;
    } else {
        [_mapView reloadInputViews];
    }
}

- (void)fullScreenMapView {
    
    OIHMapViewFullScreenViewController *vc =
    [[OIHMapViewFullScreenViewController alloc]
     initWithAnnotations:_mapView.annotations
     region:_mapView.region];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGFloat actualPosition = scrollView.contentOffset.y;
    CGFloat contentHeight =
    scrollView.contentSize.height - (_collectionView.frame.size.height);
    
    if (scrollView == _collectionView && isFetching == NO && actualPosition >= contentHeight && contentHeight > scrollView.frame.size.height) {
        [self fetchRestaurants];
    }
}

- (void)deleteRestaurant:(NSIndexPath *)indexPath {
    
    NSDictionary *restaurant = restaurantList[indexPath.row];
    
    [[JSONRequest sharedInstance]
     fetch:[NSString stringWithFormat:@"restaurant/%@",
            restaurant[kOIHRestaurantId]]
     method:@"DELETE"
     data:nil
     completionHandler:^(id data, NSError *error) {
         if (!error) {
             [restaurantList removeObjectAtIndex:indexPath.row];
         }
     }];
}

@end
