//
//  OIHAnimatedTextTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 19/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHAnimatedTextTableViewCell.h"

@implementation OIHAnimatedTextTableViewCell
@synthesize descriptionTitleLabel;
@synthesize descriptionContentLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        descriptionTitleLabel = [UILabel new];
        descriptionTitleLabel.textColor = [UIColor grayColor];
        descriptionTitleLabel.numberOfLines = 0;
        descriptionTitleLabel.font = [UIFont tableViewCellDescriptionFont];
        descriptionTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:descriptionTitleLabel];
        
        
        descriptionContentLabel = [OIHPriceWithAnimationLabel new];
        descriptionContentLabel.font = [UIFont tableViewCellContentFont];
        descriptionContentLabel.numberOfLines = 0;
        descriptionContentLabel.duration = 1.0f;
        descriptionContentLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:descriptionContentLabel];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descriptionTitleLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionTitleLabel)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descriptionContentLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionContentLabel)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[descriptionTitleLabel]-[descriptionContentLabel]-|" options:NSLayoutFormatAlignAllLeading metrics:0 views:NSDictionaryOfVariableBindings(descriptionTitleLabel, descriptionContentLabel)]];
        
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:0 multiplier:1.0f constant:60.0f]];
        
        
    }
    
    return self;
}



@end
