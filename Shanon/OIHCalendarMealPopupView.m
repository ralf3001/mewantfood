//
//  OIHCalendarMealPopupView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 6/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCalendarMealPopupView.h"

@interface OIHCalendarMealPopupView ()
@property (nonatomic) UILabel *nameLabel;
@property (nonatomic) UILabel *priceLabel;
@property (nonatomic) UILabel *availableDateLabel;
@property (nonatomic) UILabel *availableEndDateLabel;

@property (nonatomic) UIView *contentView;

@end

@implementation OIHCalendarMealPopupView

-(id)initWithMeal: (OIHMealModel *)meal{
  
  self = [super init];
  if (self) {
    self.transitioningDelegate = self;
    self.modalPresentationStyle = UIModalPresentationOverFullScreen;
    _mealInfo = meal;
  }
  return self;
}

-(void)viewDidLoad{
  
  [super viewDidLoad];
  
  [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)] ];
  
  _contentView = [UIView new];
  _contentView.translatesAutoresizingMaskIntoConstraints = NO;
  _contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
  _contentView.layer.cornerRadius = 15.0f;
  [self.view addSubview:_contentView];
  
  [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:self.view.frame.size.width - 80]];
  
  [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];

  [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
  
  
  _nameLabel = [self createLabel];
  _nameLabel.text = _mealInfo.name;
  [_contentView addSubview:_nameLabel];
  
  _priceLabel = [self createLabel];

  NSString *currencyCode = _mealInfo.restaurant.restaurantCurrency;
  NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: currencyCode}];
  
  NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
  [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
  [currency setLocale:[NSLocale localeWithLocaleIdentifier:localeIde]];
  
  NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:_mealInfo.priceInString locale:[NSLocale localeWithLocaleIdentifier:localeIde]];
  
  _priceLabel.text = [currency stringFromNumber: price];

  [_contentView addSubview:_priceLabel];
  
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  formatter.dateStyle = NSDateFormatterShortStyle;
  
  _availableDateLabel = [self createLabel];
  _availableDateLabel.text = [NSString stringWithFormat:@"From: %@", [formatter stringFromDate:_mealInfo.availableDate]];
  [_contentView addSubview:_availableDateLabel];
  
  _availableEndDateLabel = [self createLabel];
  _availableEndDateLabel.text = [NSString stringWithFormat:@"To: %@", [formatter stringFromDate:_mealInfo.availableEndDate]];
  [_contentView addSubview:_availableEndDateLabel];
  
  
  [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_nameLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_nameLabel)]];

  
  [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_nameLabel]-[_priceLabel]-[_availableDateLabel]-[_availableEndDateLabel]-|" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:0 views:NSDictionaryOfVariableBindings(_nameLabel, _priceLabel, _availableEndDateLabel, _availableDateLabel)]];

  
}

-(void)dismissView{
  
  [self dismissViewControllerAnimated:YES completion:nil];
  
}

-(UILabel *)createLabel{
  
  UILabel *label = [UILabel new];
  label.translatesAutoresizingMaskIntoConstraints = NO;
  label.numberOfLines = 0;
  
  return label;
}


#pragma mark - <UIViewControllerTransitioningDelegate>

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
  
  CalendarMealPopupAnimatedTransitioning *transitioning = [CalendarMealPopupAnimatedTransitioning new];
  transitioning.presenting = YES;
  return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
  
  CalendarMealPopupAnimatedTransitioning * transitioning = [CalendarMealPopupAnimatedTransitioning new];
  transitioning.presenting = NO;
  return transitioning;
  
}


@end


@implementation CalendarMealPopupAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext{
  return 0.45;
}


- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
  
  UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
  UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
  
  UIView *fromView = fromViewController.view;
  UIView *toView = toViewController.view;
  UIView *containerView = [transitionContext containerView];
  
  
  if (self.presenting) {
    
    _backgroundView = [[UIView alloc] init];
    _backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    _backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:_backgroundView];
    
    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_backgroundView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_backgroundView)]];
    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_backgroundView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_backgroundView)]];
    
    [containerView insertSubview:toViewController.view aboveSubview:fromViewController.view];
    
    toView.alpha = 0.0f;
    toView.transform = CGAffineTransformMakeScale(0.6, 0.6);
    [containerView addSubview:toView];
    
    _indexOfBackgroundView = [[containerView subviews] indexOfObject:_backgroundView];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 usingSpringWithDamping:0.9 initialSpringVelocity:0.3 options:0 animations:^{
      toView.alpha = 1.0f;
      toView.transform = CGAffineTransformIdentity;
      
    } completion:^(BOOL finished) {
      [transitionContext completeTransition:YES];
    }];
    
  }else{
    
    _backgroundView = [[containerView subviews] objectAtIndex:_indexOfBackgroundView];
    
    if (self.dismissUp) {
      fromView.transform = CGAffineTransformMakeTranslation(0, -1000);
    }else{
      fromView.transform = CGAffineTransformMakeTranslation(0, 1000);
    }
    
    containerView.backgroundColor = [UIColor clearColor];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.05 usingSpringWithDamping:0.9 initialSpringVelocity:0.3 options:0 animations:^{
       
      toView.transform = CGAffineTransformIdentity; //home screen
      _backgroundView.layer.opacity = 0.0f;
      [_backgroundView removeFromSuperview];
      
    } completion:^(BOOL finished) {
      [transitionContext completeTransition:YES];
    }];
    
  }
  
  
}



@end