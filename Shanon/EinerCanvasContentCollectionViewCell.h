//
//  EinerCanvasContentCollectionViewCell.h
//  Einer
//
//  Created by Ralf Cheung on 24/8/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSInteger, ImageType) {
    
    URL = 1 << 0,
    ImageName = 1 << 1,
    Image = 1 << 2
};



@interface EinerCanvasContentCollectionViewCell : UICollectionViewCell
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *nameLabel;

-(void)setImage: (NSString *)url;
-(void)setImage: (id)url withType: (ImageType) type;
@end
