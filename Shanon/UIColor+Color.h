//
//  UIColor+Color.h
//  Oh It's Here
//
//  Created by Chan Carol on 7/3/16.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Color)
+(UIColor *)highlightColor;
+(UIColor *)systemDefaultColor;

+(UIColor *)orderOrderedColor;
+(UIColor *)orderAccptedColor;
+(UIColor *)orderDeliveredColor;
+(UIColor *)orderCanceledColor;

+(UIColor *)darkBlue;
+ (UIColor *)colorFromHexString:(NSString *)hexString;

-(UIColor *)darkerColorForColor;

@end
