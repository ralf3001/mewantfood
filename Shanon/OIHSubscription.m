//
//  OIHSubscription.m
//  Shanon
//
//  Created by Ralf Cheung on 17/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSubscription.h"

@implementation OIHSubscription

-(id)initWithInfo: (NSDictionary *)info{
    
    if (self = [super init]) {
        __id = info[@"_id"];
        _subscriptionName = info[@"subscriptionName"];
        _subscriptionDescription = info[@"subscriptionDescription"];
        _createdAt = [NSDate dateFromJSONDate:info[@"createdAt"]];
        _priceInLocalCurrencyString = info[@"priceInLocalCurrencyString"];
        _priceInLocalCurrency = info[@"priceInLocalCurrency"];
        
    }
    return self;
}

-(void)subscribeToThis :(void (^)(id data, NSError *error))completionHandler{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"subscription/%@", self._id] httpMethod:HTTP_POST];
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        if (completionHandler) {
            completionHandler(data, error);
        }
        
    }];
    
}

-(void)unsubscribe :(void (^)(id data, NSError *error))completionHandler{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"subscription/%@", self._id] httpMethod:HTTP_DELETE];
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        if (completionHandler) {
            completionHandler(data, error);
        }
        
    }];

}



@end

