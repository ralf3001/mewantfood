//
//  OIHTabBar.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 1/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHTabBar.h"
#import <objc/runtime.h>
#import "OIHTabBarItemView.h"
#import "OIHTabBarViewController.h"
#define kIndicatorWidth 20

@interface OIHTabBar ()

@property (nonatomic, weak) OIHTabBarViewController *tabBarController;

@property (nonatomic) NSArray *viewControllers;
@property (nonatomic) UIView *indicatorView;
@property (nonatomic) NSMutableArray *tabBarItems;
@end

@implementation OIHTabBar
@synthesize indicatorView;
@synthesize tabBarItems;


-(OIHTabBarViewController *)tabBarController{
    return _tabBarController;
}


-(id)initWithTabBarItems: (NSArray *)viewControllers{
    
    self = [super init];
    if (self) {
        
        self.viewControllers = viewControllers;
        [self commonInit];
        [self initBar];
        
        _selectedIndex = 0;
    }
    return self;
}

-(void)initBar{
    
    tabBarItems = [NSMutableArray new];
    
    NSInteger tag = 0;
    
    for (UIViewController *vc in self.viewControllers) {
        
        UIButton *button = [self addTabBarItem:((UINavigationController *)vc).tabBarItem.title tabBarImage:((UINavigationController *)vc).tabBarItem.image];
        button.tag = tag++;
        
        OIHTabBarItemView *itemView = [[OIHTabBarItemView alloc] initWithButton:button];
        
        [tabBarItems addObject:itemView];
        [self addSubview:itemView];
        
    }
    
    CGFloat widthPerItem = self.frame.size.width / self.viewControllers.count;
    CGFloat indicatorWidth = widthPerItem / 8;
    
    indicatorView = [UIView new];
    indicatorView.backgroundColor = [UIColor redColor];
    
    indicatorView.frame = CGRectMake(_selectedIndex * widthPerItem + (widthPerItem - indicatorWidth) / 2, self.frame.size.height - 5, indicatorWidth, 5);
    
    [self addSubview:indicatorView];
    
    
}





-(void)layoutSubviews{
    
    CGFloat widthPerItem = self.frame.size.width / self.viewControllers.count;
    CGFloat xPos = 0;
    
    for (OIHTabBarItemView *button in self.tabBarItems) {
        
        button.frame = CGRectMake(xPos, 0, widthPerItem, self.frame.size.height-5);
        xPos += widthPerItem;
        
    }
    
    CGFloat indicatorWidth = widthPerItem / 8;
    
    indicatorView.frame = CGRectMake(_selectedIndex * widthPerItem + (widthPerItem - indicatorWidth) / 2, self.frame.size.height - 5, indicatorWidth, 5);

}




-(UIButton *)addTabBarItem: (NSString *)title tabBarImage: (UIImage *)image{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(itemTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
    
}


-(void)itemTouched: (UIButton *)button{
    
    if ([self.delegate respondsToSelector:@selector(barItemSelected:)]) {
        [self.delegate barItemSelected:button.tag];
    }
    
    self.selectedIndex = button.tag;
    
}


-(void)setSelectedIndex:(NSInteger)selectedIndex{

    if (_selectedIndex != selectedIndex) {
        _selectedIndex = selectedIndex;
        
        CGRect frame = [self indicatorPosition];
        
        [UIView animateWithDuration:0.2 animations:^{
            indicatorView.frame = frame;
        }];
        
    }

}


-(CGRect)indicatorPosition{
    
    CGFloat widthPerItem = self.frame.size.width / self.viewControllers.count;
    CGFloat indicatorWidth = widthPerItem / 8;
    
    CGRect frame = indicatorView.frame;
    frame.origin.x = _selectedIndex * widthPerItem + (widthPerItem - indicatorWidth) / 2;
    
    return frame;
}

-(void)commonInit{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addNotificationSpot:) name:OIHTabBarWillCreateNotificationIcon object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeNotification:) name:OIHTabBarWillDismissNotificationIcon object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moveIndicatorPosition:) name:OIHTabBarWillSelectTabAtIndexNotification object:nil];
    
    self.backgroundColor = [UIColor blackColor];
    
}


-(void)removeNotification: (NSNotification *)notification{
    
    NSDictionary *obj = notification.object;
    NSInteger tab = [obj[@"tab"] integerValue];
    if (tab < self.viewControllers.count && tab > 0) {
        [self removeNotificationFroBarItem:tab];
    }
    
}

-(void)addNotificationSpot: (NSNotification *)notification{
    
    NSDictionary *obj = notification.object;
    NSInteger tab = [obj[@"tab"] integerValue];
    if (tab < self.viewControllers.count && tab > 0) {
        [self setNotificationForBarItem:tab];
    }
}

-(void)setNotificationForBarItem: (NSInteger)barItem{
    
    OIHTabBarItemView *item = tabBarItems[barItem];
    [item setNotificationCountForView:1];
}

-(void)removeNotificationFroBarItem: (NSInteger)barItem{
    
    OIHTabBarItemView *item = tabBarItems[barItem];
    [item setNotificationCountForView:0];
}

-(void)moveIndicatorPosition: (NSNotification *)notification{
    
    NSDictionary *obj = notification.object;
    NSInteger tab = [obj[@"tab"] integerValue];

    if (tab < self.viewControllers.count && tab > 0) {
        self.selectedIndex = tab;
    }
    
}

@end
