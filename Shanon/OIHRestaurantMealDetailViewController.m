//
//  OIHRestaurantMealDetailViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantMealDetailViewController.h"
#import "OIHUserRatingView.h"
#import "NSNumberFormatter+OIHCurrencyNumberFormatter.h"



@interface OIHRestaurantMealDetailViewController ()
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *priceLabel;
@property (nonatomic) UILabel *mealNameLabel;
@property (nonatomic) UILabel *restaurantNameLabel;
@property (nonatomic) UILabel *productNetWeightLabel;
@property (nonatomic) UILabel *productQuantity;
@property (nonatomic) UILabel *productNote;

@property (nonatomic) UILabel *ratingView;

@property (nonatomic) UIButton *confirmButton;
@property (nonatomic) UIButton *cancelButton;

@property (nonatomic) UIButton *favouriteButton;
//@property (nonatomic) OIHUserRatingView *ratingView;

@end

@implementation OIHRestaurantMealDetailViewController

-(id)init{
    
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        //self.transitioningDelegate = self;
    }
    return self;
}

-(void)setMealInfo:(OIHMealModel *)mealInfo{
    _mealInfo = mealInfo;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self createGradientLayer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _imageView = [UIImageView new];
    _imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [_imageView sd_setImageWithURL:[NSURL URLWithString: _mealInfo.productImage]];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.view addSubview:_imageView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_imageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_imageView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_imageView)]];
    
    
    _priceLabel = [UILabel new];
    _priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    NSNumberFormatter *currency = [NSNumberFormatter formatterWithCurrency:_mealInfo.restaurant.restaurantCurrency];

    
    
    NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: _mealInfo.restaurant.restaurantCurrency}];
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:_mealInfo.priceInString locale:[NSLocale localeWithLocaleIdentifier:localeIde]];
    
    if (_mealInfo.chargeRate) {
        
        NSDecimalNumber *surcharge = [NSDecimalNumber decimalNumberWithDecimal:[_mealInfo.chargeRate decimalValue]];
        NSDecimalNumber *total = [price decimalNumberByMultiplyingBy:surcharge];
        _priceLabel.text = [currency stringFromNumber: total];
        
    }else{
        _priceLabel.text = [currency stringFromNumber: price];
        
    }
    
    _restaurantNameLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    _priceLabel.textColor = [UIColor whiteColor];
    [self.view insertSubview:_priceLabel aboveSubview:_imageView];
    
    _mealNameLabel = [UILabel new];
    _mealNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:40];
    _mealNameLabel.numberOfLines = 0;
    _mealNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _mealNameLabel.text = _mealInfo.name;
    _mealNameLabel.textColor = [UIColor whiteColor];
    
    [self.view insertSubview:_mealNameLabel aboveSubview:_imageView];
    
    _restaurantNameLabel = [UILabel new];
    _restaurantNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _restaurantNameLabel.numberOfLines = 0;
    _restaurantNameLabel.text = _mealInfo.restaurant.restaurantName;
    _restaurantNameLabel.textColor = [UIColor whiteColor];
    _restaurantNameLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleTitle3];
    [self.view insertSubview:_restaurantNameLabel aboveSubview:_imageView];
    
    _productNote = [UILabel new];
    _productNote.translatesAutoresizingMaskIntoConstraints = NO;
    _productNote.textColor = [UIColor whiteColor];
    _productNote.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    [self.view insertSubview:_productNote aboveSubview:_imageView];
    
    _productQuantity = [UILabel new];
    _productQuantity.translatesAutoresizingMaskIntoConstraints = NO;
    _productQuantity.textColor = [UIColor whiteColor];
    
    [self.view insertSubview:_productQuantity aboveSubview:_imageView];
    
    _productNetWeightLabel = [UILabel new];
    _productNetWeightLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _productNetWeightLabel.textColor = [UIColor whiteColor];
    [self.view insertSubview:_productNetWeightLabel aboveSubview:_imageView];
    
    _ratingView = [UILabel new];
    _ratingView.translatesAutoresizingMaskIntoConstraints = NO;
    _ratingView.textColor = [UIColor whiteColor];
    
    NSMutableString *string = [NSMutableString new];
    
    for (NSInteger i = 0; i < _mealInfo.restaurant.rating.count; i++) {
        [string appendString:[NSString stringWithFormat:@"%@ %@ ", [NSString abbreviateNumber:[_mealInfo.restaurant.rating[i] integerValue]],[self emojiAtIndex:i]]];
    }
    
    _ratingView.text = string;
    _ratingView.adjustsFontSizeToFitWidth = YES;
    
    [self.view insertSubview:_ratingView aboveSubview:_imageView];
    
    _confirmButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _confirmButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_confirmButton setTitle:NSLocalizedString(@"BUY", @"Buy") forState:UIControlStateNormal];
    [_confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _confirmButton.backgroundColor = [UIColor colorWithRed:84/256.0f green:148/256.0f blue:49/256.0f alpha:1.0f];
    [_confirmButton addTarget:self action:@selector(userConfirmOrder) forControlEvents:UIControlEventTouchUpInside];
    [self.view insertSubview:_confirmButton aboveSubview:_imageView];
    
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_cancelButton setTitle:NSLocalizedString(@"CANCEL", @"Cancel") forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _cancelButton.backgroundColor = [UIColor colorWithRed:175/256.0f green:60/256.0f blue:60/256.0f alpha:1.0f];
    [_cancelButton addTarget:self action:@selector(userCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.view insertSubview:_cancelButton aboveSubview:_imageView];
    
    
    _favouriteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _favouriteButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_favouriteButton addTarget:self action:@selector(favouriteItem) forControlEvents:UIControlEventTouchUpInside];
    if (self.mealInfo.isFavourited) {
        [_favouriteButton setBackgroundImage:[IonIcons imageWithIcon:ion_ios_heart size:30.0f color:[UIColor redColor]] forState:UIControlStateNormal];
        
    }else{
        [_favouriteButton setBackgroundImage:[IonIcons imageWithIcon:ion_ios_heart_outline size:30.0f color:[UIColor whiteColor]] forState:UIControlStateNormal];
    }
    
    [self.mealInfo addObserver:self forKeyPath:@"isFavourited" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.view addSubview:_favouriteButton];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_cancelButton][_confirmButton(==_cancelButton)]|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(_cancelButton, _confirmButton)]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_confirmButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:60.0f]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_mealNameLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_mealNameLabel)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_priceLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_priceLabel)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_restaurantNameLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_restaurantNameLabel)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_ratingView]-[_favouriteButton(==30)]-|" options:NSLayoutFormatAlignAllBottom  metrics:0 views:NSDictionaryOfVariableBindings(_ratingView, _favouriteButton)]];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_ratingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_confirmButton attribute:NSLayoutAttributeTop multiplier:1.0f constant:-10.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_priceLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_ratingView attribute:NSLayoutAttributeTop multiplier:1.0f constant:-10.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_restaurantNameLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_priceLabel attribute:NSLayoutAttributeTop multiplier:1.0f constant:-10.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_mealNameLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_restaurantNameLabel attribute:NSLayoutAttributeTop multiplier:1.0f constant:-10.0f]];
    
    
}


-(void)createGradientLayer{
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.view.frame;
    
    gradientLayer.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor colorWithWhite:0.0 alpha:1.0f] CGColor], nil];
    gradientLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.4],[NSNumber numberWithInt:1], nil];
    
    [self.view.layer insertSublayer:gradientLayer above:_imageView.layer];
    
}

-(void)userConfirmOrder{
    if ([self.delegate respondsToSelector:@selector(userDidPressConfirmOrder:viewController:)]) {
        [self.delegate userDidPressConfirmOrder:_mealInfo viewController:self];
    }
}

-(void)userCancel{
    if ([self.delegate respondsToSelector:@selector(userDidCancelOrder:)]) {
        [self.delegate userDidCancelOrder: self];
    }
}

-(void)favouriteItem{
    self.mealInfo.isFavourited ^= YES;
    
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"isFavourited"]) {
        
        BOOL newVal = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
        if (newVal) {
            [_favouriteButton setBackgroundImage:[IonIcons imageWithIcon:ion_ios_heart size:30.0f color:[UIColor redColor]] forState:UIControlStateNormal];
            [_favouriteButton.layer addAnimation:[self favoriteButtonAnimation] forKey:@"scale"];
        }else{
            
            [_favouriteButton setBackgroundImage:[IonIcons imageWithIcon:ion_ios_heart_outline size:30.0f color:[UIColor whiteColor]] forState:UIControlStateNormal];
            
        }
        
    }
    
}

-(CABasicAnimation *)favoriteButtonAnimation{
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.fromValue = [NSNumber numberWithInteger:1];
    animation.toValue = [NSNumber numberWithDouble:1.5f];
    animation.duration = 0.1;
    animation.autoreverses = YES;
    animation.removedOnCompletion = YES;
    
    return animation;
}


-(NSString *)emojiAtIndex: (NSInteger)index{
    
    switch (index) {
        case 0:
            //angry face
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\xA1"];
        case 1:
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x9E"];
        case 2:
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\xB7"];
        case 3:
            //delicious face
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x8B"];
        case 4:
            //throw a kiss face, love it
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x8D"];
        default:
            break;
    }
    
    return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    MealDetailAnimatedTransitioning *transitioning = [MealDetailAnimatedTransitioning new];
    transitioning.isPresenting = YES;
    transitioning.fromFrame = self.originalCellFrame;
    
    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    MealDetailAnimatedTransitioning * transitioning = [MealDetailAnimatedTransitioning new];
    transitioning.isPresenting = NO;
    return transitioning;
}

-(void)dealloc{
    [_mealInfo removeObserver:self forKeyPath:@"isFavourited"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end



@implementation MealDetailAnimatedTransitioning

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return 0.2;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    self.transitionContext = transitionContext;
    
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *containerView = [transitionContext containerView];
    [containerView addSubview:toViewController.view];
    
    
    UIBezierPath *circleMaskPathInitial = [UIBezierPath bezierPathWithOvalInRect:_fromFrame];
    
    CGPoint extremePoint = CGPointMake(_fromFrame.size.width / 2 + _fromFrame.origin.x, -(1000));
    
    CGFloat radius = sqrt((extremePoint.x*extremePoint.x) + (extremePoint.y * extremePoint.y));
    
    UIBezierPath *circleMaskPathFinal = [UIBezierPath bezierPathWithOvalInRect:CGRectInset(_fromFrame, -radius, -radius)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = circleMaskPathFinal.CGPath;
    
    toViewController.view.layer.mask = maskLayer;
    
    CABasicAnimation *maskLayerAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
    maskLayerAnimation.fromValue = (__bridge id _Nullable)(circleMaskPathInitial.CGPath);
    maskLayerAnimation.toValue = (__bridge id _Nullable)(circleMaskPathFinal.CGPath);
    maskLayerAnimation.duration = [self transitionDuration:transitionContext];
    maskLayerAnimation.delegate = self;
    [maskLayer addAnimation:maskLayerAnimation forKey:@"path"];
    
}


-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    
    [self.transitionContext completeTransition:flag];
    [self.transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey].view.layer.mask = nil;
    
}


@end
