//
//  OIHUserSettingObject.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserSettingObject.h"

@implementation OIHUserSettingObject


-(id)initWithTitle: (NSString *)title method: (SEL)method icon: (UIImage *)image{
    
    self = [super init];
    if (self) {
        self.settingMethod = method;
        self.settingTitle = title;
        self.settingIconImage = image;
    }
    
    return self;
}



@end
