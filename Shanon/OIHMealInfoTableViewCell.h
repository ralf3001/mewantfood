//
//  OIHMealInfoTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 27/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>


@class OIHMealInfoTableViewCell;

@protocol OIHMealInfoTableViewCellDelegate <NSObject>

@optional
-(void)textViewFinishedEditing: (UITextView *)textView cell: (OIHMealInfoTableViewCell *)cell;

@end


@interface OIHMealInfoTableViewCell : UITableViewCell

@property (nonatomic) UILabel *descriptionTitleLabel;
@property (nonatomic) UITextView *descriptionContentLabel;
@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic) NSString *cellAttribute;
@property (nonatomic) NSString *placeHolderText;
@property (nonatomic) UIButton *navButton;

@property (nonatomic, weak) id<OIHMealInfoTableViewCellDelegate> delegate;

-(void)setWarningForContentLabel: (NSString *) warningMessage;

-(void)createNavButton;
-(void)activateNavButton: (BOOL)isActive;

@end
