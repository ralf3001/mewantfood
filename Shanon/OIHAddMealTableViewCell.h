//
//  OIHAddMealTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OIHAddMealTableViewCell;

@protocol OIHTableViewCellTextViewProtocol <NSObject>

- (void)textViewFinishedEditing:(UITextView *)textView
                           cell:(OIHAddMealTableViewCell *)cell;
- (void)textViewDidBeginEditing:(OIHAddMealTableViewCell *)cell;

@end

@interface OIHAddMealTableViewCell : UITableViewCell <UITextViewDelegate>
@property(nonatomic) UILabel *descriptionLabel;
@property(nonatomic) UITextView *descriptionContentTextField;
@property(nonatomic) NSString *attribute;
@property(nonatomic, weak) UITableView *tableView;
@property(nonatomic, weak) id<OIHTableViewCellTextViewProtocol> delegate;

@end
