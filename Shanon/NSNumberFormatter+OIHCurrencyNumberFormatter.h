//
//  NSNumberFormatter+OIHCurrencyNumberFormatter.h
//  Shanon
//
//  Created by Ralf Cheung on 8/5/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (OIHCurrencyNumberFormatter)

+(id)formatterWithCurrency: (NSString *)currency;

@end
