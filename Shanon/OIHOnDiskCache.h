//
//  OIHOnDiskCache.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHCacheObject.h"

@interface OIHOnDiskCache : NSObject


typedef enum{
    CachePolicyDay,
    CachePolicyMonth
} CachePolicy;

+(id)sharedCache;

-(id)loadObjectFromCache: (NSString *)key;
-(void)cacheObject: (id)object withKey: (NSString *)key toDisk: (BOOL) writeToDisk;

-(NSURL *)isKeyCached: (NSString *)key;

-(void)deleteCache: (NSString *)key;
-(void)clearAllCache;


@end
