//
//  OIHUserOrderInformationTableViewCell.h
//  Shanon
//
//  Created by Ralf Cheung on 12/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHOrderModel.h"
#import "MCSwipeTableViewCell.h"

@interface OIHUserOrderInformationTableViewCell : MCSwipeTableViewCell

@property (nonatomic) UIImageView *orderImageView;

@property (nonatomic) UILabel *orderItemNameLabel;
@property (nonatomic) UILabel *orderRestaurantLabel;
@property (nonatomic) UILabel *orderRecipientNameLabel;
@property (nonatomic) UILabel *orderDateLabel;
@property (nonatomic) UILabel *orderPriceLabel;
@property (nonatomic) UILabel *orderStateLabel;
@property (nonatomic) UILabel *orderRecipientAddressLabel;
@property (nonatomic) UILabel *orderQuantityLabel;

@property (nonatomic, weak) OIHOrderModel *order;


@end
