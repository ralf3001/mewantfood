//
//  OIHFBProfile.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHFBProfile.h"

#define OIHCACHEITEM_TOKEN_KEY @"token"
#define OIHCACHEITEM_PROFILE_KEY @"profile"

@implementation OIHFBProfile

+ (BOOL)supportsSecureCoding{
    
    return YES;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    OIHFBProfile *item = [[OIHFBProfile alloc] init];
    item.profile = [aDecoder decodeObjectOfClass:[FBSDKProfile class] forKey:OIHCACHEITEM_PROFILE_KEY];
    item.token = [aDecoder decodeObjectOfClass:[FBSDKAccessToken class] forKey:OIHCACHEITEM_TOKEN_KEY];
    return item;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.profile forKey:OIHCACHEITEM_PROFILE_KEY];
    [aCoder encodeObject:self.token forKey:OIHCACHEITEM_TOKEN_KEY];
}


@end
