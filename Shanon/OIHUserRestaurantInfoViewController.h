//
//  OIHUserRestaurantInfoViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantModel.h"
#import <UIKit/UIKit.h>

@interface OIHUserRestaurantInfoViewController : UIViewController
@property(nonatomic) OIHRestaurantModel *restaurantInfo;
@end
