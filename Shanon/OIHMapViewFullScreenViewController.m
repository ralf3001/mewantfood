//
//  OIHMapViewFullScreenViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHMapViewFullScreenViewController.h"
#import "OIHMapLocationDetailViewController.h"
#import "OIHMapViewAddressPin.h"




@interface OIHMapViewFullScreenViewController () <MKMapViewDelegate, OIHMapLocationDetailProtocol>
@property (nonatomic) UIButton *dismissButton;
@end

@implementation OIHMapViewFullScreenViewController
@synthesize mapView = _mapView;
@synthesize dismissButton;
@synthesize annotationsArray;

-(id)initWithAnnotations: (NSArray *)annotations region:(MKCoordinateRegion) region{

    self = [super init];

    if (self) {
        [self commonInit];
        annotationsArray = annotations;
        [_mapView addAnnotations:annotations];
        [_mapView setRegion:region animated:YES];
    }
    return self;
}

-(void)commonInit{
    
    _mapView = [MKMapView new];
    _mapView.delegate = self;
    _mapView.showsBuildings = YES;
    _mapView.showsPointsOfInterest = YES;
    _mapView.showsCompass = YES;
    _mapView.showsUserLocation = YES;
    _mapView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_mapView];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_mapView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_mapView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_mapView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_mapView)]];

    dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    [dismissButton setBackgroundImage:[IonIcons imageWithIcon:ion_ios_close_outline size:50 color:[UIColor grayColor]] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:dismissButton];
    [self.view bringSubviewToFront:dismissButton];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dismissButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:40]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dismissButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0f constant:20]];

    
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    
    if ([view.annotation isKindOfClass:[OIHMapViewAddressPin class]]) {
        OIHMapViewAddressPin *annot = view.annotation;
        NSInteger index = [annotationsArray indexOfObject:annot];

        if (index != NSNotFound) {
            OIHMapLocationDetailViewController *detail = [[OIHMapLocationDetailViewController alloc] initWithLocationDetail:annot];
            detail.delegate = self;
            
            //why not use presentViewController: http://stackoverflow.com/questions/19890761/warning-presenting-view-controllers-on-detached-view-controllers-is-discourage
            [self presentViewController:detail animated:YES completion:nil];
//            [self.view.window.rootViewController presentViewController:detail animated:YES completion:nil];
        }
        
    }

}

-(void)mapLocationDetailControllerDidDismiss:(OIHMapLocationDetailViewController *)mapDetail{

    [_mapView deselectAnnotation:mapDetail.locationDetail animated:YES];
    
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}


-(void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
