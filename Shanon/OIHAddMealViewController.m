//
//  OIHAddMealViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHActionSheet.h"
#import "OIHAddMealViewController.h"
#import "OIHDatePickerTableViewCell.h"
#import "OIHMealInfoTableViewCell.h"
#import "OIHSocialManager.h"

#define kDatePickerViewTag 99

#define kDishNameCellTag 0
#define kDishPriceCellTag 1

@interface OIHAddMealViewController () <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate,
OIHMealInfoTableViewCellDelegate, OIHActionSheetProtocol,
UIImagePickerControllerDelegate, UINavigationControllerDelegate,
OIHSocialManagerDelegate, UIScrollViewDelegate>

@property(nonatomic) UISegmentedControl *mealType;
@property(nonatomic) UITableView *tableView;
@property(nonatomic) NSIndexPath *datePickerIndexPath;
@property(nonatomic) CGFloat pickerCellRowHeight;
@property(nonatomic) NSArray *menuItems;
@property(nonatomic) NSMutableDictionary *mealInformation;
@property(nonatomic) NSMutableArray *meals;
@property(nonatomic) NSLayoutConstraint *dynamicTVHeight;

@property(nonatomic) OIHMealModel *mealToAdd;

@property(nonatomic) UIImage *chosenImage;

@end

@implementation OIHAddMealViewController
@synthesize mealInformation;
@synthesize tableView = _tableView;
@synthesize mealType;
@synthesize pickerCellRowHeight;
@synthesize menuItems;
@synthesize mealToAdd;

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [OIHSocialManager sharedInstance].delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mealInformation = [NSMutableDictionary new];
    
    mealToAdd = [OIHMealModel new];
    [mealToAdd setAttribute:@"restaurantId" data:_restaurantInfo._id];
    
    self.meals = [NSMutableArray new];
    mealType = [[UISegmentedControl alloc]
                initWithItems:@[ @"Breakfast", @"Lunch", @"Dinner", @"Snack"]];
    mealType.translatesAutoresizingMaskIntoConstraints = NO;
    mealType.selectedSegmentIndex = 0;
    [self.view addSubview:mealType];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                              target:self
                                              action:@selector(launchCamera)];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    _tableView.estimatedRowHeight = 80.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [_tableView registerClass:[OIHMealInfoTableViewCell class]
       forCellReuseIdentifier:@"cell"];
    [_tableView registerClass:[OIHDatePickerTableViewCell class]
       forCellReuseIdentifier:@"datePicker"];
    [_tableView registerClass:[OIHMealInfoTableViewCell class]
       forCellReuseIdentifier:@"availabilitiy"];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    [doneButton setTitle:NSLocalizedString(@"DONE", @"Done")
                forState:UIControlStateNormal];
    doneButton.backgroundColor = [UIColor colorWithRed:46 / 256.0f
                                                 green:65 / 256.0f
                                                  blue:114 / 256.0f
                                                 alpha:1.0f];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton addTarget:self
                   action:@selector(addMealToAccount)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIView *footerView = [[UIView alloc]
                          initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 80)];
    
    [footerView addSubview:doneButton];
    
    [footerView
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"H:|[doneButton]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           doneButton)]];
    [footerView
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"V:|[doneButton]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           doneButton)]];
    
    _tableView.tableFooterView = footerView;
    
    [self.view addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"H:|[_tableView]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           _tableView)]];
    
    [self.view addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"V:|[_tableView]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           _tableView)]];
    
    pickerCellRowHeight = 150.0f;
}

- (BOOL)datePickerAppeared {
    
    return self.datePickerIndexPath ? YES : NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (BOOL)indexPathHasPicker:(NSIndexPath *)indexPath {
    
    return [self datePickerAppeared] && (self.datePickerIndexPath == indexPath);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self datePickerAppeared] && indexPath == self.datePickerIndexPath) {
        return 150.0f;
    } else {
        return self.tableView.rowHeight;
    }
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self datePickerIndexPath] ? 9 : 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self datePickerAppeared]) {
        switch (indexPath.row) {
            case 0: // item name
            case 1: // price
            case 2: // ingredients
            case 3: // quantity
            case 4: // net weight
            case 5:
                return [self setMealInfoTableViewCell:tableView indexPath:indexPath];
            case 6:
                return [self setAvailabilityTableViewCell:tableView indexPath:indexPath];
            case 7:
                return [self setDatePickerTableViewCell:tableView indexPath:indexPath];
            case 8:
                return [self setMealInfoTableViewCell:tableView indexPath:indexPath];
            default:
                break;
        }
        
    } else {
        switch (indexPath.row) {
            case 0: // item name
            case 1: // price
            case 2: // ingredients
            case 3: // quantity
            case 4: // net weight
            case 5: // product note
                return [self setMealInfoTableViewCell:tableView indexPath:indexPath];
            case 6:
                return [self setAvailabilityTableViewCell:tableView indexPath:indexPath];
            case 7:
                return [self setMealInfoTableViewCell:tableView indexPath:indexPath];
            default:
                break;
        }
    }
    
    return nil;
}


- (UITableViewCell *)setMealInfoTableViewCell:(UITableView *)tableView
                                    indexPath:(NSIndexPath *)indexPath {
    
    OIHMealInfoTableViewCell *cell = (OIHMealInfoTableViewCell *)[tableView
                                                                  dequeueReusableCellWithIdentifier:@"cell"
                                                                  forIndexPath:indexPath];
    
    cell.tableView = _tableView;
    
    // case 0: //item name
    // case 1: //price
    // case 2: //ingredients
    // case 3: //quantity
    // case 4: //net weight
    
    switch (indexPath.row) {
        case 0:
            
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"MANAGEMENT_ADD_ITEM_NAME", "Item name:");
            cell.cellAttribute = kOIHMealName;
            
            if (![mealToAdd.name isEqualToString:@""]) {
                cell.descriptionContentLabel.text = mealToAdd.name;
            } else {
                cell.placeHolderText = @"Item Name...";
            }
            
            cell.descriptionContentLabel.editable = YES;
            cell.descriptionContentLabel.returnKeyType = UIReturnKeyDone;
            
            break;
        case 1:
            
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"MANAGEMENT_ADD_ITEM_PRICE", @"Price:");
            cell.descriptionContentLabel.keyboardType = UIKeyboardTypeDecimalPad;
            cell.cellAttribute = kOIHMealPriceInLocalCurrency;
            
            if (mealToAdd.priceInLocalCurrency) {
                NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
                [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
                [currency setLocale:[NSLocale currentLocale]];
                cell.descriptionContentLabel.text =
                [currency stringFromNumber:mealToAdd.priceInLocalCurrency];
            } else {
                cell.placeHolderText = @"Price of the item...";
            }
            
            cell.descriptionContentLabel.editable = YES;
            
            break;
        case 2:
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"MANAGEMENT_ADD_ITEM_INGREDIENTS", @"Ingredients:");
            cell.cellAttribute = kOIHMealIngredients;
            cell.descriptionContentLabel.editable = YES;
            
            if (mealToAdd.productIngredients) {
                cell.descriptionContentLabel.text = mealToAdd.productIngredients;
            } else {
                cell.placeHolderText =
                NSLocalizedString(@"MANAGEMENT_ADD_ITEM_PLACEHOLDER_TEXT_INGREDIENTS",
                                  @"list of ingredients");
            }
            break;
        case 3:
            cell.descriptionTitleLabel.text = NSLocalizedString(
                                                                @"MANAGEMENT_ADD_ITEM_PRODUCT_QUANTITY", @"Quantity:");
            cell.cellAttribute = kOIHMealQuantity;
            cell.descriptionContentLabel.keyboardType = UIKeyboardTypeDecimalPad;
            cell.descriptionContentLabel.returnKeyType = UIReturnKeyDone;
            cell.descriptionContentLabel.editable = YES;
            if (mealToAdd.productQuantity) {
                cell.descriptionContentLabel.text =
                [mealToAdd.productQuantity stringValue];
            } else {
                cell.placeHolderText =
                NSLocalizedString(@"MANAGEMENT_ADD_ITEM_PLACEHOLDER_TEXT_QUANTITY",
                                  @"list of ingredients");
            }
            break;
        case 4:
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"MANAGEMENT_ADD_ITEM_NET_WEIGHT", @"Net weight:");
            cell.cellAttribute = kOIHMealNetWeight;
            cell.descriptionContentLabel.returnKeyType = UIReturnKeyDone;
            cell.descriptionContentLabel.editable = YES;
            if (mealToAdd.productNetWeight) {
                cell.descriptionContentLabel.text = mealToAdd.productNetWeight;
            }
            break;
        case 5:
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"MANAGEMENT_ADD_ITEM_NOTE", @"Product note:");
            cell.cellAttribute = kOIHMealNote;
            cell.descriptionContentLabel.returnKeyType = UIReturnKeyDone;
            cell.descriptionContentLabel.editable = YES;
            
            if (mealToAdd.productNote) {
                cell.descriptionContentLabel.text = mealToAdd.productNote;
            } else {
                cell.placeHolderText = NSLocalizedString(
                                                         @"MANAGEMENT_ADD_ITEM_PLACEHOLDER_TEXT_NOTE", @"product note");
            }
            
            break;
        case 7:
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"MANAGEMENT_ADD_ITEM_TYPE", @"Type:");
            cell.cellAttribute = kOIHMealType;
            cell.descriptionContentLabel.text = [mealToAdd mealTypeString];
            cell.descriptionContentLabel.userInteractionEnabled = NO;
            cell.descriptionContentLabel.textColor = [UIColor blackColor];
            cell.descriptionContentLabel.editable = YES;
            
            break;
            
        case 8:
            
            cell.descriptionTitleLabel.text =
            NSLocalizedString(@"MANAGEMENT_ADD_ITEM_TYPE", @"Type:");
            cell.cellAttribute = kOIHMealType;
            cell.descriptionContentLabel.text = [mealToAdd mealTypeString];
            cell.descriptionContentLabel.userInteractionEnabled = NO;
            cell.descriptionContentLabel.textColor = [UIColor blackColor];
            
            break;
        default:
            break;
    }
    
    cell.tableView = tableView;
    cell.delegate = self;
    
    return cell;
}

- (UITableViewCell *)setAvailabilityTableViewCell:(UITableView *)tableView
                                        indexPath:(NSIndexPath *)indexPath {
    
    OIHMealInfoTableViewCell *cell = (OIHMealInfoTableViewCell *)[tableView
                                                                  dequeueReusableCellWithIdentifier:@"availabilitiy"
                                                                  forIndexPath:indexPath];
    
    cell.cellAttribute = kOIHMealAvailableDate;
    
    cell.descriptionTitleLabel.text =
    NSLocalizedString(@"MANAGEMENT_ADD_ITEM_AVAILABILITY", @"Available:");
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    
    cell.descriptionContentLabel.text =
    [formatter stringFromDate:mealToAdd.availableDate];
    
    return cell;
}

- (UITableViewCell *)setDatePickerTableViewCell:(UITableView *)tableView
                                      indexPath:(NSIndexPath *)indexPath {
    
    OIHDatePickerTableViewCell *cell = (OIHDatePickerTableViewCell *)[tableView
                                                                      dequeueReusableCellWithIdentifier:@"datePicker"
                                                                      forIndexPath:indexPath];
    UIDatePicker *dPicker = (UIDatePicker *)[cell viewWithTag:kDatePickerViewTag];
    
    if (mealInformation[kOIHMealAvailableDate]) {
        [dPicker setDate:mealToAdd.availableDate];
    }
    
    [dPicker addTarget:self
                action:@selector(datePicked:)
      forControlEvents:UIControlEventValueChanged];
    
    return cell;
}

- (void)displayInlineDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // display the date picker inline with the table content
    [self.tableView beginUpdates];
    
    BOOL before = NO; // indicates if the date picker is below "indexPath", help
    // us determine which row to reveal
    if ([self datePickerAppeared]) {
        before = self.datePickerIndexPath.row < indexPath.row;
    }
    
    // remove any date picker cell if it exists
    if ([self datePickerAppeared]) {
        [self.tableView deleteRowsAtIndexPaths:@[
                                                 [NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]
                                                 ]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.datePickerIndexPath = nil;
    } else {
        NSIndexPath *indexPathToReveal =
        [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0];
        
        [self.tableView insertRowsAtIndexPaths:@[ indexPathToReveal ]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
        
        self.datePickerIndexPath =
        [NSIndexPath indexPathForRow:indexPathToReveal.row inSection:0];
    }
    
    // always deselect the row containing the start or end date
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.tableView endUpdates];
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 6) {
        [self displayInlineDatePickerForRowAtIndexPath:indexPath];
        [self.tableView reloadData];
    } else {
        if ([self datePickerAppeared]) {
            // index == 4
            if (indexPath.row == 8) {
                [self openMealTypePicker];
            }
        } else {
            
            if (indexPath.row == 7) {
                [self openMealTypePicker];
            }
        }
    }
}

- (void)openMealTypePicker {
    
    OIHActionSheet *actionSheet = [OIHActionSheet new];
    actionSheet.delegate = self;
    actionSheet.dataModel = @[
                              NSLocalizedString(@"ITEM_TYPE_BREAKFAST", @"Breakfast"),
                              NSLocalizedString(@"ITEM_TYPE_LUNCH", @"Lunch"),
                              NSLocalizedString(@"ITEM_TYPE_DINNER", @"Dinner"),
                              NSLocalizedString(@"ITEM_TYPE_SNACK", @"Snack")
                              ];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)actionSheetWillDismiss:(NSString *)result atRow:(NSInteger)row {
    
    NSArray *type = @[ @"breakfast", @"lunch", @"dinner", @"snack" ];
    
    [mealToAdd setAttribute:kOIHMealType data:type[row]];
    [_tableView reloadData];
}

- (void)datePicked:(UIDatePicker *)datePicker {
    
    [mealToAdd setAttribute:kOIHMealAvailableDate data:datePicker.date];
    
    [self.tableView reloadData];
}

- (void)addMealToAccount {
    
    [SVProgressHUD
     showWithStatus:NSLocalizedString(@"LABEL_SENDING", @"Sending...")];
    
    if (self.meals.count > 0) {
        [[JSONRequest sharedInstance] fetch:@"meal"
                                     method:@"POST"
                                       data:self.meals
                          completionHandler:^(id data, NSError *error) {
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [self finishBlock:data error:error];
                              });
                              
                          }];
        
    } else {
        
        if (_chosenImage) {
            
            [SVProgressHUD showWithStatus:@"Uploading..."];
            
            [OIHSocialManager sharedInstance].delegate = self;
            
            [[OIHSocialManager sharedInstance]
             uploadProductImage:[_chosenImage resizeImage]
             completionHandler:^(id result, NSError *error) {
                 
                 if (!error) {
                     
                     mealToAdd.productImage = result[@"url"];
                     
                     [[JSONRequest sharedInstance]
                      fetch:@"meal"
                      method:@"POST"
                      data:@{
                             @"meal" : [mealToAdd toDictionary]
                             }
                      completionHandler:^(id data, NSError *error) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [SVProgressHUD dismiss];
                              [self finishBlock:data error:error];
                          });
                      }];
                     
                 } else {
                     NSLog(@"%@", [error localizedDescription]);
                 }
                 
             }];
            
        } else {
            
            [[JSONRequest sharedInstance] fetch:@"meal"
                                         method:@"POST"
                                           data:@{
                                                  @"meal" : [mealToAdd toDictionary]
                                                  }
                              completionHandler:^(id data, NSError *error) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [SVProgressHUD dismiss];
                                      [self finishBlock:data error:error];
                                  });
                              }];
        }
    }
}

- (void)finishBlock:(id)data error:(NSError *)error {
    
    if (error) {
        
        [SVProgressHUD showErrorWithStatus:@"Oops, somthing's gone wrong"];
        
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [SVProgressHUD dismiss];
                       });
        
    } else {
        
        [SVProgressHUD showSuccessWithStatus:@"Done"];
        
        dispatch_after(
                       dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
                           [SVProgressHUD dismiss];
                           [self.navigationController popViewControllerAnimated:YES];
                       });
    }
}

- (void)launchCamera {
    
    UIImagePickerController *imagePickr = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController availableMediaTypesForSourceType:
         UIImagePickerControllerSourceTypeCamera]
        .count > 0) {
        imagePickr.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        imagePickr.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    imagePickr.delegate = self;
    [self presentViewController:imagePickr animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    _chosenImage = info[UIImagePickerControllerOriginalImage];
}

- (void)socialManagerUploadProgress:(float)progress {
    
    // handled in main thread
    [SVProgressHUD showProgress:progress status:@"Uploading..."];
}

- (void)addMealToArray {
    
    if (!self.mealInformation[kOIHMealName] ||
        !self.mealInformation[kOIHMealPriceInLocalCurrency]) {
        return;
    }
    
    self.mealInformation[kOIHMealRestaurant] = self.restaurantInfo._id;
    self.mealInformation[kOIHMealType] =
    [NSNumber numberWithInteger:self.mealType.selectedSegmentIndex];
    [self.meals addObject:self.mealInformation];
}

- (void)textViewFinishedEditing:(UITextView *)textView
                           cell:(OIHMealInfoTableViewCell *)cell {
    
    if ([cell.cellAttribute isEqualToString:kOIHMealPriceInLocalCurrency] ||
        [cell.cellAttribute isEqualToString:kOIHMealQuantity]) {
        [mealToAdd
         setAttribute:cell.cellAttribute
         data:[NSNumber numberWithDouble:[textView.text doubleValue]]];
    } else {
        [mealToAdd setAttribute:cell.cellAttribute data:textView.text];
    }
    
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little
 preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
