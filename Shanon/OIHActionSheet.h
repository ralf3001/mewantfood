//
//  OIHActionSheet.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 3/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol OIHActionSheetProtocol <NSObject>

@optional
-(void)actionSheetWillDismiss: (NSString *)result atRow: (NSInteger) row;

@end


@interface OIHActionSheet : UIViewController <UIViewControllerTransitioningDelegate>

@property (nonatomic) NSArray *dataModel;
@property (nonatomic) NSDictionary *dataDictionary;

@property (nonatomic, weak) id<OIHActionSheetProtocol> delegate;
@end

@interface CustomAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (assign) BOOL presenting;
@end
