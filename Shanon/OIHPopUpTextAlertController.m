//
//  OIHPopUpTextAlertController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHPopUpTextAlertController.h"
#import "OIHMapLocationDetailViewController.h"


@interface OIHPopUpTextAlertController () <UITextFieldDelegate>

@property (nonatomic) UIView *contentView;


@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *descriptionLabel;

@property (nonatomic) UITextField *textField;

@property (nonatomic) UIButton *doneButton;
@property (nonatomic) UIButton *cancelButton;


@property (nonatomic) NSLayoutConstraint *contentViewCenterXConstraint;
@property (nonatomic) NSLayoutConstraint *doneViewCenterXConstraint;
@property (nonatomic) NSLayoutConstraint *cancelViewCenterYConstraint;


@end

@implementation OIHPopUpTextAlertController
@synthesize doneAction;
@synthesize cancelAction;

-(id)init{
    
    self = [super init];
    if (self) {
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];

    _contentView = [UIView new];
    _contentView.translatesAutoresizingMaskIntoConstraints = NO;
    _contentView.backgroundColor = [UIColor whiteColor];
    _contentView.layer.cornerRadius = 10.0f;
    [_contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTapped)]];
    [self.view addSubview:_contentView];

    _contentViewCenterXConstraint = [NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f];
    
    [self.view addConstraint:_contentViewCenterXConstraint];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:-30.0f]];
        
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:self.view.frame.size.width * 0.8]];
    
    
    [self setupContent];
    [self setupDoneView];
    [self setupCancelView];
    
    
}



-(void)setupContent{
    
    _titleLabel = [UILabel new];
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _titleLabel.text = self.labelText;
    _titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [_contentView addSubview:_titleLabel];
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_titleLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel)]];
    
    _descriptionLabel = [UILabel new];
    _descriptionLabel.numberOfLines = 0;
    _descriptionLabel.text = self.descriptionText;
    _descriptionLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
    _descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _descriptionLabel.textAlignment = NSTextAlignmentCenter;
    [_contentView addSubview:_descriptionLabel];
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_descriptionLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_descriptionLabel)]];
    
    UIView *buttonTopDivider = [UIView new];
    buttonTopDivider.translatesAutoresizingMaskIntoConstraints = NO;
    buttonTopDivider.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
    [_contentView addSubview:buttonTopDivider];
    
    UIView *sideDivider = [UIView new];
    sideDivider.translatesAutoresizingMaskIntoConstraints = NO;
    sideDivider.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
    [_contentView addSubview:sideDivider];
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[buttonTopDivider]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(buttonTopDivider)]];
    
    _textField = [UITextField new];
    _textField.translatesAutoresizingMaskIntoConstraints = NO;
    _textField.layer.borderColor = [UIColor colorWithWhite:0.8 alpha:1.0f].CGColor;
    _textField.layer.borderWidth = 0.5f;
    _textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _textField.enablesReturnKeyAutomatically = YES;
    _textField.keyboardType = UIKeyboardTypeEmailAddress;
    _textField.delegate = self;
    
    [_contentView addSubview:_textField];
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_textField]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_textField)]];
    
    
    _doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_doneButton addTarget:self action:@selector(contentDoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [_doneButton setTitle:NSLocalizedString(@"DONE", @"Cancel") forState:UIControlStateNormal];
    _doneButton.layer.masksToBounds = YES;
    [_contentView addSubview:_doneButton];
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_cancelButton setTitle:NSLocalizedString(@"CANCEL", @"Cancel") forState:UIControlStateNormal];
    _cancelButton.layer.masksToBounds = YES;
    [_cancelButton addTarget:self action:@selector(contentCancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [_contentView addSubview:_cancelButton];

    
    UIView *imageViewBackground = [UIView new];
    imageViewBackground.translatesAutoresizingMaskIntoConstraints = NO;
    imageViewBackground.layer.borderWidth = 3.0f;
    imageViewBackground.layer.cornerRadius = 35.0f;
    imageViewBackground.layer.borderColor = [UIColor whiteColor].CGColor;
    imageViewBackground.backgroundColor = _iconBackgroundColor;
    imageViewBackground.layer.masksToBounds = YES;
    [_contentView addSubview:imageViewBackground];
    
    
    UIImageView *imageView = [UIImageView new];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    imageView.image = _alertViewIcon;
    [imageViewBackground addSubview:imageView];
    
    [imageViewBackground addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[imageView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
    [imageViewBackground addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[imageView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
    
    
    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageViewBackground attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageViewBackground attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f]];
    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageViewBackground attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:70.0f]];
    [_contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageViewBackground attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:imageViewBackground attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_cancelButton][sideDivider(==1)][_doneButton(==_cancelButton)]|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(_cancelButton, _doneButton, sideDivider)]];
    
    [_contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-45-[_titleLabel]-15-[_descriptionLabel]-10-[_textField(==30)]-10-[buttonTopDivider(==1)][_doneButton(==40)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel, _textField, _doneButton, buttonTopDivider, _descriptionLabel)]];
    
    
}

-(void)setupDoneView{
    
    _doneContentView = [[OIHPopupContentView alloc] initWithTitleText:@"" descriptionText:@"" withImage:[IonIcons imageWithIcon:ion_ios_paperplane_outline size:60 color:[UIColor whiteColor]]];
    
    _doneContentView.imageViewBackground.backgroundColor = [UIColor colorWithRed:45/256.0f green:136/256.0f blue:45/256.0f alpha:1.0f];
    
    [_doneContentView.doneButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    
    [_doneContentView.cancelButton addTarget:self action:@selector(showContentView) forControlEvents:UIControlEventTouchUpInside];
    
    
    _doneContentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_doneContentView];
    
    _doneViewCenterXConstraint = [NSLayoutConstraint constraintWithItem:_doneContentView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:self.view.frame.size.width];
    
    [self.view addConstraint:_doneViewCenterXConstraint];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_doneContentView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:-30.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_doneContentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:self.view.frame.size.width * 0.8]];

    
    
}

-(void)setupCancelView{
    
    _cancelContentView = [[OIHPopupContentView alloc] initWithTitleText:NSLocalizedString(@"CANCEL", @"Cancel") descriptionText:@"" withImage:[IonIcons imageWithIcon:ion_ios_close_empty size:60 color:[UIColor whiteColor]]];
    _cancelContentView.imageViewBackground.backgroundColor = [UIColor colorWithRed:216/256.0f green:33/256.0f blue:33/256.0f alpha:1.0f];
    
    
    [_cancelContentView.cancelButton addTarget:self action:@selector(showContentView) forControlEvents:UIControlEventTouchUpInside];

    [_cancelContentView.doneButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    
    
    _cancelContentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_cancelContentView];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_cancelContentView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0]];
    
    _cancelViewCenterYConstraint = [NSLayoutConstraint constraintWithItem:_cancelContentView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:self.view.frame.size.height + 100];
    
    
    [self.view addConstraint:_cancelViewCenterYConstraint];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_cancelContentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:self.view.frame.size.width * 0.8]];

    
    
}


-(void)addCancelAction:(OIHCancelHandler)action{
    
    cancelAction = action;
    
}

-(void)addDoneAction:(OIHActionHandler)action{
    doneAction = action;
}

-(void)contentDoneButtonPressed{

    if (![self.delegate popupCheckValidity:_textField.text]) {
        
        _descriptionLabel.text = NSLocalizedString(@"NOT_AN_EMAIL", @"this is not an email");
        _descriptionLabel.textColor = [UIColor redColor];
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            _descriptionLabel.text = weakSelf.descriptionText;
            _descriptionLabel.textColor = [UIColor blackColor];
        });
        
    }else{
        __weak typeof(self) weakSelf = self;
        doneAction(self.textField.text, weakSelf);
    }

}


-(void)switchToCompleteView{
    
    _contentViewCenterXConstraint.constant = self.view.frame.size.width;
    _doneViewCenterXConstraint.constant = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
}

-(void)switchToFailView{
    
    _contentViewCenterXConstraint.constant = self.view.frame.size.width;
    _cancelViewCenterYConstraint.constant = -30;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
}


-(void)contentCancelButtonPressed{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    if (cancelAction) {
        cancelAction();
    }

}


-(void)showContentView{
    
    _contentViewCenterXConstraint.constant = 0;
    _cancelViewCenterYConstraint.constant = self.view.frame.size.height + 100;
    _doneViewCenterXConstraint.constant = self.view.frame.size.width;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    
}




-(void)dismissView{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{

    PopUpTextAnimatedTransitioning *transitioning = [PopUpTextAnimatedTransitioning new];
    transitioning.isPresenting = YES;
    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{

    PopUpTextAnimatedTransitioning * transitioning = [PopUpTextAnimatedTransitioning new];
    transitioning.isPresenting = NO;
    return transitioning;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    if (![self.delegate popupCheckValidity:textField.text]) {

        _descriptionLabel.text = NSLocalizedString(@"NOT_AN_EMAIL", @"this is not an email");
        _descriptionLabel.textColor = [UIColor redColor];
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            _descriptionLabel.text = weakSelf.descriptionText;
            _descriptionLabel.textColor = [UIColor blackColor];
        });
   
    }
//    [self checkEmail:textField.text];
    
    return NO;
}



-(void)contentViewTapped{
    
    if ([_textField isFirstResponder]) {
        [_textField resignFirstResponder];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end



@implementation PopUpTextAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext{
    return 0.2;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
    
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *containerView = [transitionContext containerView];
    
    UIViewController *animatingVC = self.isPresenting? toViewController : fromViewController;
    UIView *animatingView = [animatingVC view];
    
    
    CGRect appearedFrame = [transitionContext finalFrameForViewController:animatingVC];
    CGRect dismissedFrame = appearedFrame;
    dismissedFrame.origin.y -= 80;
    
    CGRect initialFrame = self.isPresenting ? dismissedFrame : appearedFrame;
    CGRect finalFrame = self.isPresenting ? appearedFrame : dismissedFrame;
    
    [animatingView setFrame:initialFrame];
    
    
    if (self.isPresenting) {

        UIVisualEffectView *backgroundView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];        
        backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        [containerView addSubview:backgroundView];

        [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(backgroundView)]];
        [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(backgroundView)]];
        
        [containerView insertSubview:toViewController.view aboveSubview:fromViewController.view];
        
        _indexOfBackgroundView = [[containerView subviews] indexOfObject:backgroundView];

        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0 usingSpringWithDamping:0.35 initialSpringVelocity:12 options:UIViewAnimationOptionCurveLinear animations:^{
            [animatingView setFrame:finalFrame];
            toViewController.view.alpha = 1.0f;

        } completion:^(BOOL finished) {
            
            if (finished) {
                [transitionContext completeTransition:YES];
            }
        }];
        
    }
    else {
        
        UIVisualEffectView *backgroundView = [[containerView subviews] objectAtIndex:_indexOfBackgroundView];

        dismissedFrame.origin.y = 0;
        CGRect finalFrame = dismissedFrame;

        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            
            animatingView.alpha = 0.0f;
            [animatingView setFrame:finalFrame];
            backgroundView.layer.opacity = 0.0f;
            
        } completion:^(BOOL finished) {
            if (finished) {
                [transitionContext completeTransition:YES];
                
            }
        }];
        
    }
}

@end
