//
//  OIHFBCache.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHFBCache.h"

#import <Security/Security.h>

#define USER_DEFAULTS_INSTALLED_KEY @"com.ralfcheung.oih.fbusers"

@implementation OIHFBCache

+ (void)initialize{
    
    if (self == [OIHFBCache class]) {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:USER_DEFAULTS_INSTALLED_KEY]) {
            [OIHFBCache clearCache];
        }
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:USER_DEFAULTS_INSTALLED_KEY];
    }
}


+ (void)saveItem:(OIHFBProfile *)item{
    
    // Delete any old values
    [self deleteProfile];
    
    NSString *key = @"fbprofile";
    NSString *error;
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:item];
    if (error) {
        NSLog(@"Failed to serialize item for insertion into keychain: %@", error);
        return;
    }
    NSDictionary *keychainQuery = @{
                                    (__bridge id)kSecAttrAccount : key,
                                    (__bridge id)kSecValueData : data,
                                    (__bridge id)kSecClass : (__bridge id)kSecClassGenericPassword,
                                    (__bridge id)kSecAttrAccessible : (__bridge id)kSecAttrAccessibleWhenUnlockedThisDeviceOnly,
                                    };
    OSStatus result = SecItemAdd((__bridge CFDictionaryRef)keychainQuery, nil);
    if(result != noErr){
        NSLog(@"Failed to add item to keychain");
        return;
    }
}

+ (void)deleteProfile{
    
    NSString *key = @"fbprofile";
    
    NSDictionary *keychainQuery = @{
                                    (__bridge id)kSecAttrAccount : key,
                                    (__bridge id)kSecClass : (__bridge id)kSecClassGenericPassword,
                                    (__bridge id)kSecAttrAccessible : (__bridge id)kSecAttrAccessibleWhenUnlockedThisDeviceOnly,
                                    };
    OSStatus result = SecItemDelete((__bridge CFDictionaryRef) keychainQuery);
    if(result != noErr){
        return;
    }
}

+ (OIHFBProfile *)profile{
    
    NSString *key = @"fbprofile";
    
    NSDictionary *keychainQuery = @{
                                    (__bridge id)kSecAttrAccount : key,
                                    (__bridge id)kSecReturnData : (__bridge id)kCFBooleanTrue,
                                    (__bridge id)kSecClass : (__bridge id)kSecClassGenericPassword
                                    };
    
    CFDataRef serializedDictionaryRef;
    OSStatus result = SecItemCopyMatching((__bridge CFDictionaryRef)keychainQuery, (CFTypeRef *)&serializedDictionaryRef);
    if(result == noErr) {
        NSData *data = (__bridge_transfer NSData*)serializedDictionaryRef;
        if (data) {
            return [NSKeyedUnarchiver unarchiveObjectWithData:data];
        }
    }
    return nil;
}



+ (void)clearCache{
    
    NSArray *secItemClasses = @[(__bridge id)kSecClassGenericPassword,
                                (__bridge id)kSecClassInternetPassword,
                                (__bridge id)kSecClassCertificate,
                                (__bridge id)kSecClassKey,
                                (__bridge id)kSecClassIdentity];
    for (id secItemClass in secItemClasses) {
        NSDictionary *spec = @{(__bridge id)kSecClass: secItemClass};
        SecItemDelete((__bridge CFDictionaryRef)spec);
    }
}



@end
