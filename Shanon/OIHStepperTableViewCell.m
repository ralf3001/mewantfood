//
//  OIHStepperTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 18/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHStepperTableViewCell.h"

@interface OIHStepperTableViewCell ()
@end


@implementation OIHStepperTableViewCell
@synthesize quantityStepper;
@synthesize cellContentLabel, cellDescriptionLabel, cellWarningLabel;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        quantityStepper = [UIStepper new];
        quantityStepper.maximumValue = 20;
        quantityStepper.minimumValue = 1;
        quantityStepper.value = 1;
        [quantityStepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventTouchUpInside];
        quantityStepper.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:quantityStepper];
        
        
        cellContentLabel = [UILabel new];
        cellContentLabel.font = [UIFont tableViewCellContentFont];
        cellContentLabel.text = [NSString stringWithFormat:@"%ld", (long)quantityStepper.value];
        cellContentLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:cellContentLabel];
        
        cellDescriptionLabel = [UILabel new];
        cellDescriptionLabel.textColor = [UIColor grayColor];
        cellDescriptionLabel.font = [UIFont tableViewCellDescriptionFont];
        cellDescriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:cellDescriptionLabel];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[cellDescriptionLabel]-[cellContentLabel]-|" options:NSLayoutFormatAlignAllLeading metrics:0 views:NSDictionaryOfVariableBindings(cellDescriptionLabel, cellContentLabel)]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:quantityStepper attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[cellDescriptionLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(cellDescriptionLabel)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[cellContentLabel]-[quantityStepper]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(cellContentLabel, quantityStepper)]];

        
    }
    
    return self;
}


-(void)stepperValueChanged: (UIStepper *)stepper{
    
    cellContentLabel.text = [NSString stringWithFormat:@"%ld", (long)stepper.value];
    if ([self.delegate respondsToSelector:@selector(stepperValueDidChange:)]) {
        [self.delegate stepperValueDidChange:stepper];
    }
}



@end
