//
//  OIHRestaurantSalesGraphCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <CorePlot/ios/CorePlot.h>

@interface OIHRestaurantSalesGraphCollectionViewCell : UICollectionViewCell
//@property (nonatomic, strong) CPTGraphHostingView *hostView;
@property (nonatomic, weak) NSArray *periodDataSource;

@end
