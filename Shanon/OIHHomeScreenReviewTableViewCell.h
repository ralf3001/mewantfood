//
//  OIHHomeScreenReviewTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 19/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHActivityFeed.h"
#import "OIHActivityCellButtonDelegate.h"
#import "OIHFloatingView.h"

@interface OIHHomeScreenReviewTableViewCell : UITableViewCell

@property (nonatomic) UITextView *textView;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *descriptionLabel;
@property (nonatomic) UILabel *timeLabel;

@property (nonatomic) UIButton *thanksButton;
@property (nonatomic) UIButton *commentButton;

@property (nonatomic, weak) UITableView *tableView;

@property (weak, nonatomic) OIHActivityFeed *feed;
@property (nonatomic) OIHFloatingView *floatingView;
@property (nonatomic, weak) id<OIHActivityCellButtonDelegate> delegate;


@end
