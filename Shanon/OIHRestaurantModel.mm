//
//  OIHRestaurantModel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantModel.h"
#import "OIHMealModel.h"

@implementation OIHRestaurantModel
-(id)initWithDictionary: (NSDictionary *)info{
    
    self = [super init];
    if (self) {
        
        const char *string = [info[kOIHRestaurantName] cStringUsingEncoding:NSISOLatin1StringEncoding];
        if (string) {
            self.restaurantName = [NSString stringWithCString:string encoding:NSUTF8StringEncoding];
        }else{
            self.restaurantName = info[kOIHRestaurantName];
        }
        
        if (info[@"rating"]) {
            self.rating = info[@"rating"];
        }
        if ([info[@"meal"] isKindOfClass:[NSDictionary class]]) {
            self.meal = [[OIHMealModel alloc] initWithInfoDictionary: info[@"meal"]];
        }
        
        self._id = info[kOIHRestaurantId];
        self.operatingHours = info[kOIHRestaurantOperatingHours];
        self.addressInfo = info[kOIHRestaurantAddress];
        self.phoneNumber = info[kOIHRestaurantPhoneNumber];
        self.restaurantCurrency = info[kOIHRestaurantCurrency];
        self.sellersNote = info[@"sellersNote"];
        self.restaurantPictures = [NSArray arrayWithArray:info[@"restaurantPictures"]];
        
        if (info[@"geometry"][@"coordinates"]) {
          self.coordinate = CLLocationCoordinate2DMake([info[@"geometry"][@"coordinates"][1] doubleValue], [info[@"geometry"][@"coordinates"][0] doubleValue]);
        }
        self.restaurantInfo = info;
    
    }
    return self;
}


-(void)setAttribute: (NSString *) attribute data: (id)data{
    
    [self setValue:data forKeyPath:attribute];
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    //override it just to prevent an exception
}


+(NSArray *) restaurantsFromArray: (id)data{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary *info in data) {
        OIHRestaurantModel *rest = [[OIHRestaurantModel alloc] initWithDictionary:info];
        [array addObject:rest];
    }
    
    return [array copy];
}

- (void)encodeWithCoder:(NSCoder *)encoder {

    [encoder encodeObject:self.restaurantName forKey:kOIHRestaurantName];
    [encoder encodeObject:self._id forKey:kOIHRestaurantId];
    [encoder encodeObject:self.operatingHours forKey:kOIHRestaurantOperatingHours];
    [encoder encodeObject:self.phoneNumber forKey:kOIHRestaurantPhoneNumber];
    [encoder encodeObject:self.restaurantInfo forKey:@"info"];
    
}


- (id)initWithCoder:(NSCoder *)decoder {
    
    self = [super init];
    if (self != nil) {
        self.restaurantName = [decoder decodeObjectForKey:kOIHRestaurantName];
        self._id = [decoder decodeObjectForKey:kOIHRestaurantId];
        self.operatingHours = [decoder decodeObjectForKey:kOIHRestaurantOperatingHours];
        self.phoneNumber = [decoder decodeObjectForKey:kOIHRestaurantPhoneNumber];
        self.restaurantInfo = [decoder decodeObjectForKey:@"info"];
    }
    return self;

}


-(void)fetchMore: (NSString *)fields completionHandler :(void (^)(id data, NSError *error))completionHandler{

  
  
  
}


-(NSDictionary *)toDictionary{
    
    if (self.restaurantInfo) {
        return self.restaurantInfo;
    }else{
        
        return @{
                 kOIHRestaurantName: self.restaurantName,
                 kOIHRestaurantAddress: self.addressInfo,
                 kOIHRestaurantPhoneNumber: self.phoneNumber,
                 @"sellersNote": self.sellersNote
                 };
    }

}

@end
