//
//  OIHSocialAccountsViewController.m
//  Shanon
//
//  Created by Ralf Cheung on 20/5/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSocialAccountsViewController.h"
#import "EinerCanvasContentCollectionViewCell.h"
#import "OIHUserSettingObject.h"
#import <TwitterKit/TwitterKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "OIHFBCache.h"
#import "OIHFBProfile.h"
#import "OIHSocialManager.h"
#import "OIHUserFBFriendsTableViewController.h"


@interface OIHRowItem : NSObject
@property (nonatomic) NSString *title;
@property (nonatomic) ImageType imageType;
@property (nonatomic) NSString *imageURL;

@end


@interface OIHSocialAccountsViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, OIHSocialManagerDelegate>


@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *socialNetworks;
@end

@implementation OIHSocialAccountsViewController

static NSString * const reuseIdentifier = @"cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _socialNetworks = [NSMutableArray new];
    [_socialNetworks addObject:NSLocalizedString(@"SOCIAL_TWITTER", @"Twitter")];
    [_socialNetworks addObject:NSLocalizedString(@"SOCIAL_FACEBOOK", @"Facebook")];
    
    [self createCollectionView];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_collectionView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createCollectionView{
    
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    [flowLayout setFooterReferenceSize:CGSizeMake(CGRectGetWidth(self.view.frame), 20.0)];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.collectionView registerClass:[EinerCanvasContentCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.view addSubview:_collectionView];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];

    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
            [self loginTwitter];
            break;
        case 1:
            [self loginFacebook];
            break;
        default:
            break;
    }
    
}


-(void)loginTwitter{
    
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
            [self.collectionView reloadData];
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
}

-(void)loginFacebook{
    
    if ([FBSDKAccessToken currentAccessToken]) {
        OIHUserFBFriendsTableViewController *vc = [OIHUserFBFriendsTableViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [[OIHSocialManager sharedInstance] loginFacebook:self];
    }

}


-(void)sendSocialTokenToServer: (FBSDKLoginManagerLoginResult *)result{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"socialtokens" httpMethod:HTTP_POST];
    setting.requestData = @{@"type": @"facebook", @"userId": result.token.userID, @"access_token": result.token.tokenString, @"expiry_date": [result.token.expirationDate JSONDateFromDate]};
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (error) {
            
        }
    }];
    
}



-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    EinerCanvasContentCollectionViewCell *cell = (EinerCanvasContentCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:{
            
            cell.backgroundColor = [UIColor colorFromHexString:@"55acee"];
            [cell setImage:@"TwitterLogo_white" withType:ImageName];

            TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
            
            TWTRSession *lastSession = store.session;
            if (lastSession) {
                
                cell.nameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"SOCIAL_LOGOUT_TWITTER_%@", "Logout [account]"), lastSession.userName];
                
            }else{

                NSString *row = _socialNetworks[indexPath.row];
                cell.nameLabel.text = row;

            }
            
            break;
        }
        case 1:{
            
            cell.backgroundColor = [UIColor bc_colorForBrand:@"Facebook"];
            [cell setImage: [IonIcons imageWithIcon:ion_social_facebook size:80 color:[UIColor whiteColor]] withType:Image];
            
            if ([FBSDKAccessToken currentAccessToken]) {

                OIHFBProfile *cachedProfile = [OIHFBCache profile];
                cell.nameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"SOCIAL_LOGOUT_TWITTER_%@", "Logout [account]"), cachedProfile.profile.name];
                
            }else{
                cell.nameLabel.text = _socialNetworks[indexPath.row];

            }
            
            break;

        }
        default:
            break;
    }
    
    return cell;
}


-(void)twitterUserProfile{

    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    
    TWTRSession *lastSession = store.session;
    if (lastSession) {
        TWTRAPIClient *client = [[TWTRAPIClient alloc] initWithUserID:lastSession.userID];

        [client loadUserWithID:lastSession.userID completion:^(TWTRUser * _Nullable user, NSError * _Nullable error) {
            if (!error) {
                
            }
        }];
    
    }
    
    
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _socialNetworks.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(collectionView.bounds.size.width / 2 , collectionView.bounds.size.width / 2);
    
}

- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


@end
