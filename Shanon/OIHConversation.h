//
//  OIHConversation.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHUserModel.h"


@class OIHMessageModel;

@interface OIHConversation : NSObject
@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *conversationName;
@property (nonatomic) NSArray <OIHUserModel *> *users;
@property (nonatomic) OIHMessageModel *lastMessage;

+(NSArray *)conversationsFromArray: (id)list;
-(id)initWithDictionary: (NSDictionary *)info;

-(void)startConversationWithMessage: (OIHMessageModel *)message user: (OIHUserModel *)user completionHandler:(void (^)(id data, NSError *error))completionHandler;

@end
