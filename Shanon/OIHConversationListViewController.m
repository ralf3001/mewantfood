//
//  OIHConversationListViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHConversationListViewController.h"
#import "OIHConversation.h"
#import "OIHChatWindowViewController.h"
#import "OIHConversationListTableViewCell.h"
#import "OIHMessageModel.h"

@interface OIHConversationListViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSMutableArray <OIHConversation *> *conversationList;
@property (nonatomic) NSMutableArray <OIHMessageModel *> *lastMessageList;

@end

@implementation OIHConversationListViewController
@synthesize conversationList;
@synthesize lastMessageList;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    conversationList = [NSMutableArray new];
    lastMessageList = [NSMutableArray new];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self fetchConversations];
    
}

-(void)createTableView{
    
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tableView registerClass:[OIHConversationListTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.contentInset = UIEdgeInsetsZero;
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        _tableView.rowHeight = 80;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 35)];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        [self.view addSubview:_tableView];
        
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        
    }else{
        [_tableView reloadData];
    }
    
}

-(void)fetchConversations{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"conversation" httpMethod:HTTP_GET];
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            [self populateData:data toArray: conversationList isCacheData:isCachedResult];
        }
    }];
    
}


- (void)populateData:(NSArray *)data toArray: (NSMutableArray *)array isCacheData:(BOOL)isCache {
    
    NSArray *feedArray = [OIHConversation conversationsFromArray:data];
    
    if (isCache) {
        
        // initial fetch
        array = [NSMutableArray arrayWithArray:feedArray];
        array.isDataDirty = [NSNumber numberWithBool:isCache];
        
    } else { // new data, flush cache data in the array
        
        if (conversationList.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            // new data from network
            if (data.count > 0) {
                array = [NSMutableArray arrayWithArray:feedArray];
                array.isDataDirty = [NSNumber numberWithBool:NO];
            }else{
                //[self setupEmptyRestaurantView];
            }
            
        } else {
            
            // new data from network
            if (data.count > 0) {
                [array addObjectsFromArray:feedArray];
                
            }else{
                //hasMoreStories = NO;
            }
            
        }
    }
    [self createTableView];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return conversationList.count;
}


- (OIHConversationListTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHConversationListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.conversation = conversationList[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OIHChatWindowViewController *vc = [[OIHChatWindowViewController alloc] initWithConversation:conversationList[indexPath.row] withUser:nil];
    [self.navigationController pushViewController:vc animated:YES];
    
}





@end
