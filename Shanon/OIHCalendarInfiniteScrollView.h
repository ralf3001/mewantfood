//
//  OIHCalendarInfiniteScrollView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMealModel.h"
#import <UIKit/UIKit.h>
#import "OIHCalendarMealView.h"
#import "OIHCalendarDay.h"

@class OIHCalendarInfiniteScrollView;

@protocol OIHMealSummaryViewDelegate <NSObject>

@required
- (void)mealSummaryFetchFutureEvents:(OIHCalendarInfiniteScrollView *)calendar
                                date:(NSDate *)date;
- (void)mealSummaryFetchPastEvents:(OIHCalendarInfiniteScrollView *)calendar
                              date:(NSDate *)date;

-(NSArray *)mealSummaryCellForCalendar: (OIHCalendarInfiniteScrollView *)calendar onDate: (NSDate *)date;

@end

@interface OIHCalendarInfiniteScrollView : UIScrollView <UIScrollViewDelegate>
@property(nonatomic, weak) id<OIHMealSummaryViewDelegate> dataSource;
@property(nonatomic, assign) BOOL isFetchingPast;
@property(nonatomic, assign) BOOL isFetchingFuture;

- (void)reloadView;
- (void)initialFetch; // to be removed, max hacky
@end
