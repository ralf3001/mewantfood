//
//  OIHUserRatingView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 21/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserRatingView.h"

#define kMaxRating 5


@implementation OIHUserRatingView

-(id)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


-(void)setRatingScore:(double)ratingScore{
    
    _ratingScore = MAX(MIN(ratingScore, 5), 0);
    
    int ratingInt = floor(_ratingScore);
    
    BOOL generateHalf = (_ratingScore - ratingInt >= 0.5);
    
    int i = 0;
    
    for (i = 0; i < ratingInt; i++) {
        
        UIImageView *ratingImageView = [[UIImageView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_star size:20.0f color:[UIColor colorWithRed:254/256.0f green:242/256.0f blue:20/256.0f alpha:1.0f]]];
        
        ratingImageView.frame = CGRectMake((i * 20) + 5, 0, 20, 20);
        [self addSubview:ratingImageView];
        
    }

    if (generateHalf) {
        
        UIImageView *halfStarIV = [[UIImageView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_star_half size:20.0f color:[UIColor colorWithRed:254/256.0f green:242/256.0f blue:20/256.0f alpha:1.0f]]];
        
        halfStarIV.frame = CGRectMake((i * 20) + 5, 0, 20, 20);
        [self addSubview:halfStarIV];

        for (int j = ++i; j < kMaxRating; j++) {
            
            UIImageView *ratingImageView = [[UIImageView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_star_outline size:20.0f color:[UIColor colorWithWhite:0.8 alpha:1.0f]]];
            ratingImageView.frame = CGRectMake((j * 20) + 5, 0, 20, 20);
            [self addSubview:ratingImageView];
            
        }
        
    }else{
        
        for (int j = i; j < kMaxRating; j++) {
            
            UIImageView *ratingImageView = [[UIImageView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_star_outline size:20.0f color:[UIColor colorWithWhite:0.8 alpha:1.0f]]];
            ratingImageView.frame = CGRectMake((j * 20) + 5, 0, 20, 20);
            [self addSubview:ratingImageView];
            
        }

    }
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
