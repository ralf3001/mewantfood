//
//  OIHActivityFeedDataSource.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHRestaurantModel.h"
#import "OIHActivityFeed.h"

@protocol ActivityFeedDataSourceDelegate <NSObject>

-(void) tableViewDidSelectRowForReview: (OIHActivityFeed *)feed;

@end

@interface OIHActivityFeedDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) OIHRestaurantModel *restaurantInfo;
@property (nonatomic, weak) id <ActivityFeedDataSourceDelegate> delegate;
@property (nonatomic, weak) UITableView *tableView;

-(id)initWithRestaurant: (OIHRestaurantModel *)restaurantInfo;

-(void)fetchActivities: (NSString *)date;

@end
