//
//  OIHRestaurantListHeaderCollectionReusableView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol OIHRestaurantMenuListHeaderDelegate <NSObject>

-(void)headerScrollDownButtonDidPress;
-(void)headerDidPressPopNavigationButton;

@end


@interface OIHRestaurantListHeaderCollectionReusableView : UICollectionReusableView

@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UITextView *descriptionTextView;
@property (nonatomic) UIButton *downArrowImageView;
@property (nonatomic) UIButton *popNavigationButton;

@property (nonatomic, weak) id<OIHRestaurantMenuListHeaderDelegate> delegate;


@end
