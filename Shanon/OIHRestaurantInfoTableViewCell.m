//
//  OIHRestaurantInfoTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantInfoTableViewCell.h"
#import "UIImage+Color.h"


@interface OIHRestaurantInfoTableViewCell ()
@property (nonatomic) UIView *floatingView;

@end

@implementation OIHRestaurantInfoTableViewCell
@synthesize backgroundImageView;
@synthesize dishLabel;
@synthesize restaurantLabel;
@synthesize priceLabel;

@synthesize floatingView;



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        self.backgroundColor = [UIColor whiteColor];

        backgroundImageView = [UIImageView new];
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
        backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        backgroundImageView.layer.masksToBounds = YES;
        [self.contentView addSubview:backgroundImageView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundImageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(backgroundImageView)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundImageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(backgroundImageView)]];

        
        UIView *overlay = [UIView new];
        overlay.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        overlay.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:overlay];
        
        dishLabel = [UILabel new];
        dishLabel.numberOfLines = 0;
        dishLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleTitle1];
        dishLabel.textColor = [UIColor whiteColor];
        dishLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [overlay addSubview:dishLabel];
        
        
        restaurantLabel = [UILabel new];
        restaurantLabel.numberOfLines = 0;
        restaurantLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1.0f];
        restaurantLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        restaurantLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [overlay addSubview:restaurantLabel];

        
        priceLabel = [UILabel new];
        priceLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1.0f];
        priceLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [overlay addSubview:priceLabel];
        
        
        [overlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[restaurantLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(restaurantLabel)]];
        [overlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[dishLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(dishLabel)]];
        [overlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[priceLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(priceLabel)]];

        
        [overlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[dishLabel]-[restaurantLabel]-[priceLabel]-10-|" options:NSLayoutFormatAlignAllLeft metrics:0 views:NSDictionaryOfVariableBindings(restaurantLabel, dishLabel, priceLabel)]];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[overlay]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(overlay)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[overlay]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(overlay)]];

    }
    return self;
    
}


-(void)setMealModel:(OIHMealModel *)mealModel{
    
    _mealModel = mealModel;
    
    self.restaurantLabel.text = mealModel.restaurant.restaurantName;
    self.dishLabel.text = mealModel.name;
    
    NSString *currencyCode = mealModel.restaurant.restaurantCurrency;
    NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: currencyCode}];
    
    NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
    [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currency setLocale:[NSLocale localeWithLocaleIdentifier:localeIde]];
    self.priceLabel.text = [currency stringFromNumber: mealModel.priceInLocalCurrency];

}



@end
