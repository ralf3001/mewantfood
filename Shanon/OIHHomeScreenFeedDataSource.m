//
//  OIHHomeScreenFeedDataSource.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 27/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHHomeScreenFeedDataSource.h"

@interface OIHHomeScreenFeedDataSource ()
@property(nonatomic) NSMutableArray *feedList;
@end

@implementation OIHHomeScreenFeedDataSource
@synthesize feedAttributedActivities;
@synthesize feedList;

- (id)initWithInitialFeed:(NSMutableArray *)feed {

  self = [super init];
  if (self) {

    self.feedList = feed;
  }
  return self;
}

- (void)addNewFeedStories:(NSMutableArray *)feed {

  NSArray *feedCopy = [feed copy];

  if (feedList) {
    [feedList addObjectsFromArray:feedCopy];
  } else {
    feedList = [NSMutableArray arrayWithArray:feedCopy];
  }
}

- (void)fetchAttributes {
}

@end
