//
//  OIHChatWindowViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"
#import "OIHConversation.h"


@interface OIHChatWindowViewController : UIViewController
@property (nonatomic) OIHUserModel *friendProfile;

-(id)initWithConversation: (OIHConversation *)conversation withUser: (OIHUserModel *)user;

@end
