//
//  OIHSalesAmountLabel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSalesAmountLabel.h"

@interface OIHSalesAmountLabel ()
@property (nonatomic) NSMutableAttributedString *attrString;

@end

@implementation OIHSalesAmountLabel

-(id)init{
    
    self = [super init];
    if (self) {
        _attrString = [NSMutableAttributedString new];
    }
    return self;
}



-(void)startCounter: (float) start end: (float)end{
    
    self.beginValue = start;
    self.endValue = end;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        int steps = 100;
        
        for (int i =1; i <= steps; i++) {
            
            float t = (float)i / 100;
            
            usleep(self.duration/100 * 1000000); // sleep in microseconds
            
            float e = [self easeInEaseOut:t b:self.beginValue c:(self.endValue - self.beginValue) d:1];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _attrString = [[NSMutableAttributedString alloc] initWithString:self.animateLoop(e)];
                
                [_attrString setAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20]} range:NSMakeRange(0, _currencySymbol.length)];
                [_attrString setAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20]} range:NSMakeRange(_attrString.length - 2, 2)];
                
                self.attributedText = _attrString;
                
            });
            
            
        }
    });
    
    
    
}



-(float)easeInEaseOut: (float) t b: (float)b c:(float) c d:(float)d{
    
    if ((t/=d/2) < 1) return c/2 * t * t + b;
    return -c/2 * ((--t) * (t - 2) - 1) + b;
}




@end
