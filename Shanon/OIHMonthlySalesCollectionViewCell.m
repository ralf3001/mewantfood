//
//  OIHMonthlySalesCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMonthlySalesCollectionViewCell.h"

@implementation OIHMonthlySalesCollectionViewCell
@synthesize monthLabel, salesAmountLabel;


-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        monthLabel = [UILabel new];
        monthLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [monthLabel sizeToFit];
        [self.contentView addSubview:monthLabel];
        
        
        salesAmountLabel = [OIHSalesAmountLabel new];
        salesAmountLabel.translatesAutoresizingMaskIntoConstraints = NO;
        salesAmountLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:40];
        salesAmountLabel.duration = 1.0f;
        salesAmountLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:salesAmountLabel];

        [salesAmountLabel sizeToFit];
        
        UIView *bottomDivider = [UIView new];
        bottomDivider.translatesAutoresizingMaskIntoConstraints = NO;
        bottomDivider.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
        [self.contentView addSubview:bottomDivider];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[bottomDivider]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(bottomDivider)]];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[salesAmountLabel(==80)]-[monthLabel]-[bottomDivider(==1)]|" options:NSLayoutFormatAlignAllCenterX metrics:0 views:NSDictionaryOfVariableBindings(salesAmountLabel, monthLabel, bottomDivider)]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:salesAmountLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0f]];
        
        
    }
    return self;
}

@end
