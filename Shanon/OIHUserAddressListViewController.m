//
//  OIHUserAddressListViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 15/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserAddressListViewController.h"
#import "OIHUserAddAddressViewController.h"
#import "StretchyHeaderCollectionViewLayout.h"
#import "OIHUserProfileCollectionViewCell.h"
#import <MapKit/MapKit.h>
#import "OIHMapViewAddressPin.h"
#import "OIHMapViewFullScreenViewController.h"
#import "OIHAddressModel.h"


#define kCellIdent @"Cell"
#define HEADER_HEIGHT (150.0)
#define SCALED_IMAGE_HEIGHT (300.0)

// zoom image up to this offset
#define MAX_ZOOM (150.0)


@interface OIHUserAddressListViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic) NSMutableArray <OIHAddressModel *>*addressList;
@property (nonatomic) UITableView *tableView;
@property (nonatomic) UIImageView *sadFaceImageView;
@property (nonatomic) UILabel *sadfaceLabel;
@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic, readonly) UICollectionReusableView *header;
@property (nonatomic) MKMapView *mapView;
@end



@implementation OIHUserAddressListViewController
@synthesize addressList;
@synthesize tableView = _tableView;
@synthesize sadFaceImageView;
@synthesize sadfaceLabel;
@synthesize collectionView = _collectionView;
@synthesize header;
@synthesize mapView = _mapView;



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_plus_empty size:44.0f color:self.view.tintColor] style:UIBarButtonItemStylePlain target:self action:@selector(addAddress)];
    
    
    [self fetchAddresses];
}

-(void)addAddress{
    
    OIHUserAddAddressViewController *vc = [OIHUserAddAddressViewController new];
    vc.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)fetchAddresses{

    
    [[JSONRequest sharedInstance] fetch:@"address" method:@"GET" data:nil completionHandler:^(id data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                
                addressList = [NSMutableArray arrayWithArray:[OIHAddressModel addressFromArray:data]];
                
                if (addressList.count > 0) {
                    [self createCollectionView];
                }else{
                    [self createEmptyView];
                }
                
            }else {
                NSLog(@"%@", [error localizedDescription]);
            }
            
        });

    }];
    
}


-(void)createCollectionView{
    
    
    StretchyHeaderCollectionViewLayout *stretchyLayout;
    stretchyLayout = [[StretchyHeaderCollectionViewLayout alloc] init];
    [stretchyLayout setSectionInset:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)];
    [stretchyLayout setHeaderReferenceSize:CGSizeMake(320.0, 250.0)];

    
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:stretchyLayout];
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView setAlwaysBounceVertical:YES];
        [_collectionView setShowsVerticalScrollIndicator:NO];
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        _collectionView.layer.shadowColor = [UIColor blackColor].CGColor;
        _collectionView.layer.shadowOffset = CGSizeMake(-1, -1);

        [self.view addSubview:_collectionView];
        
        
        [_collectionView registerClass:[OIHUserProfileCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        [_collectionView registerClass:[UICollectionReusableView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:@"header"];

        
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
        
    }

}



-(void)createEmptyView{
    sadFaceImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sadface_icon"]];
    sadFaceImageView.translatesAutoresizingMaskIntoConstraints = NO;
    sadFaceImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:sadFaceImageView];
    
    
    sadfaceLabel = [UILabel new];
    sadfaceLabel.numberOfLines = 0;
    sadfaceLabel.textAlignment = NSTextAlignmentCenter;
    sadfaceLabel.textColor = [UIColor lightGrayColor];
    sadfaceLabel.text = NSLocalizedString(@"USER_NO_ADDRESS", @"No address message, ask to add a restaurant");
    sadfaceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:sadfaceLabel];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:sadFaceImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual
                                                             toItem:nil attribute:0 multiplier:1.0f constant:200]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:sadFaceImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual
                                                             toItem:sadFaceImageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:sadFaceImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:sadfaceLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual
                                                             toItem:sadFaceImageView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:20.0f]];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:sadfaceLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0 constant:self.view.frame.size.width - 90]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:sadfaceLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:sadFaceImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];

}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section {
    
    return addressList.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if (!header) {
        
        header = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                    withReuseIdentifier:@"header"
                                                           forIndexPath:indexPath];
        CGRect bounds;
        bounds = [header bounds];
    
        
        [header addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fullScreenMapView)]];
        
        _mapView = [MKMapView new];
        _mapView.translatesAutoresizingMaskIntoConstraints = NO;
        _mapView.showsUserLocation = YES;
        _mapView.showsBuildings = YES;
        _mapView.showsPointsOfInterest = YES;
        _mapView.showsCompass = YES;
        _mapView.showsScale = YES;

        NSMutableArray *pins = [NSMutableArray new];

        double maxLng = -10, minLng = 200, maxLat = -99, minLat = 99;   //coz those are the minimum and max. values for lng and lat resp., google it.
        
        for (OIHAddressModel *address in addressList) {
//            [lng, lat]
            double lat = address.coordinates.latitude;
            double lng = address.coordinates.longitude;
            
            if (lat > maxLat) {
                maxLat = lat;
            }
            if (lat < minLat) {
                minLat = lat;
            }
            if (lng > maxLng) {
                maxLng = lng;
            }
            
            if (lng < minLng) {
                minLng = lng;
            }
            
            OIHMapViewAddressPin *pin = [[OIHMapViewAddressPin alloc] initWithCoordinates:address.coordinates placeName:address.addressTag formattedAddress:address.userEnteredAddress];
            [pins addObject:pin];
        }
    
        [_mapView addAnnotations:pins];
        
        [header addSubview:_mapView];

        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_mapView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_mapView)]];
        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_mapView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_mapView)]];

        MKCoordinateRegion region = MKCoordinateRegionMake(CLLocationCoordinate2DMake((maxLat + minLat) / 2, (maxLng + minLng) / 2), MKCoordinateSpanMake(maxLat - minLat + (maxLat - minLat)/addressList.count, maxLng - minLng + (maxLng - minLng)/addressList.count));
        
        [_mapView setRegion:region animated:YES];

    }
    
    return header;
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OIHAddressModel *addressDictionary = addressList[indexPath.row];

    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                (id)[UIFont fontWithName:@"HelveticaNeue-Thin" size:30.0f], NSFontAttributeName,
                                (id)[UIColor lightGrayColor], NSForegroundColorAttributeName, nil];
    
    NSAttributedString *attrText;
    attrText = [[NSAttributedString alloc] initWithString:addressDictionary.userEnteredAddress
                                               attributes:attributes];
    //use user's address in production
    CGRect rect = [attrText boundingRectWithSize:CGSizeMake(collectionView.frame.size.width - 36, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    return CGSizeMake(collectionView.frame.size.width, rect.size.height + 40);

}


- (OIHUserProfileCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHUserProfileCollectionViewCell *cell = (OIHUserProfileCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    OIHAddressModel *addressDictionary = addressList[indexPath.row];
    
    if (indexPath.row == 0) {
        cell.descriptionField.text = NSLocalizedString(@"DEFAULT_ADDRESS", @"default address");
    }else{
        cell.descriptionField.text = addressDictionary.addressTag;
    }
    
    [cell setDescriptionContentText:addressDictionary.userEnteredAddress];

    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    if (self.delegate) {
        [self.delegate userDidSelectAddress:addressList[indexPath.row]];

        if ([self.navigationController.viewControllers containsObject:self]) {
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }

    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ADDRESS_DELETE_ADDRESS", @"Delete Address") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            OIHAddressModel *address = addressList[indexPath.row];
            JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:@"address" httpMethod:HTTP_DELETE];
            settings.requestData = @{@"id": address._id};

            [addressList removeObjectAtIndex:indexPath.row];
            [collectionView deleteItemsAtIndexPaths:@[indexPath]];
            
            [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
                if (error) {
                    [addressList insertObject:address atIndex:indexPath.row];
                    [collectionView insertItemsAtIndexPaths:@[indexPath]];
                }
                
            }];
            
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"CANCEL", @"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        
        [self presentViewController:alert animated:YES completion:nil];

    }
    
}


-(void)fullScreenMapView{
    
    OIHMapViewFullScreenViewController *vc = [[OIHMapViewFullScreenViewController alloc] initWithAnnotations:_mapView.annotations region: _mapView.region];
    [self presentViewController:vc animated:YES completion:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
