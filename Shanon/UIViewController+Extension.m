//
//  UIViewController+Extension.m
//  Einer
//
//  Created by Ralf Cheung on 7/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "UIViewController+Extension.h"
#import <objc/runtime.h>

@implementation UIViewController (Extension)


+ (void)load {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        SEL originalSelector = @selector(viewWillAppear:);
        SEL replacementSelector = @selector(newViewWillAppear:);
        
        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method replacementMethod = class_getInstanceMethod(class, replacementSelector);
        //    method_exchangeImplementations(originalMethod, replacementMethod);
        
        BOOL didAddMethod =
        class_addMethod(class,
                        originalSelector,
                        method_getImplementation(replacementMethod),
                        method_getTypeEncoding(replacementMethod));
        
        if (didAddMethod) {
            class_replaceMethod(class,
                                replacementSelector,
                                method_getImplementation(originalMethod),
                                method_getTypeEncoding(originalMethod));
        } else {
            method_exchangeImplementations(originalMethod, replacementMethod);
        }

    });
    

}


-(void)newViewWillAppear: (BOOL) animated{
    
    
    [self newViewWillAppear:animated];
}



@end
