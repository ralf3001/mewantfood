//
//  OIHImagePickerViewController.h
//  Shanon
//
//  Created by Ralf Cheung on 5/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHImagePickerViewController : UIViewController
@property (nonatomic) UIImagePickerController *imagePicker;

@end
