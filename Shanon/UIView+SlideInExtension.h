//
//  UIView+SlideInExtension.h
//  slidingtest
//
//  Created by Ralf Cheung on 24/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SlideInExtension)

typedef NS_ENUM(NSInteger, SlideInDirection) {
    
    UIViewSlideInFromTop = 1 << 0,
    UIViewSlideInFromRight = 1 << 1,
    UIViewSlideInFromBottom = 1 << 2,
    UIViewSlideInFromLeft = 1 << 3,
};


-(void)slideInFromDirection: (SlideInDirection) direction;

@end
