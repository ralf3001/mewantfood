//
//  OIHUserActivityTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 3/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHActivityFeed.h"
#import "OIHActivityCellButtonDelegate.h"

//@protocol OIHUserActivityTableViewCellDelegate <NSObject>
//
//-(void)userDidTapThanksButton: (OIHActivityFeed *) activity isPostThanked: (BOOL) isPostThanked;
//
//@optional
//-(void)userDidTapLikeButton: (OIHActivityFeed *) activity isPostLiked: (BOOL) isPostLiked;
//
//
//@end


@interface OIHUserActivityTableViewCell : UITableViewCell

@property (nonatomic) OIHActivityFeed *activityInfo;
@property (nonatomic) UIImageView *fromUserImageView;
@property (nonatomic) UIImageView *toUserImageView;
@property (nonatomic) UILabel *descriptionLabel;
@property (nonatomic) UIButton *likeButton;
@property (nonatomic, weak) id<OIHActivityCellButtonDelegate> delegate;
@property (assign, nonatomic) BOOL isPostLiked;
@property (nonatomic) UIButton *thanksButton;
@property (nonatomic) UIButton *commentButton;

//@property (assign, nonatomic) BOOL isPostThanked;

-(void) setDescriptionLabelText: (NSString *) text;


@end
