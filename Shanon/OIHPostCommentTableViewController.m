//
//  OIHPostCommentTableViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 3/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHPostCommentTableViewController.h"
#import "OIHMessageInputView.h"


@interface OIHPostCommentTableViewController () <UITableViewDelegate, UITableViewDataSource, OIHMessageInputDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) OIHMessageInputView *messageInput;
@property CGFloat KEYBOARD_HEIGHT;
@property (nonatomic) NSLayoutConstraint *textInputHeigtConstraint;
@property (nonatomic) NSMutableArray *messageList;

@end

@implementation OIHPostCommentTableViewController
@synthesize KEYBOARD_HEIGHT;

-(void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    _messageList = [NSMutableArray new];
    [self createTableView];

}


-(void)createTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    _tableView.estimatedRowHeight = 300;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.scrollEnabled = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    tap.cancelsTouchesInView = NO;
    [_tableView addGestureRecognizer:tap];
    
    [self.view addSubview:_tableView];
    
    _messageInput = [[OIHMessageInputView alloc] init];
    _messageInput.delegate = self;
    _messageInput.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_messageInput];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_messageInput]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_messageInput)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView][_messageInput(0)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView, _messageInput)]];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return _messageList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

#pragma mark - OIHMessageInputDelegate

-(void)messageInputViewDidPressSendButton:(NSString *)text{
    
    if (text.length > 0) {
        
//        OIHMessageModel *message = [[OIHMessageModel alloc] init];
//        message.sentDate = [NSDate date];
//        message.content = text;
//        
//        [_messageList addObject:message];
        
        [_tableView reloadData];
        
    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)keyboardWillHide:(__unused NSNotification *)notification {
    
    if (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0){
        
        [self setViewMovedUp:NO];
    }
    
    
}

- (void)keyboardWillShow:(__unused NSNotification *)notification {
    
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    KEYBOARD_HEIGHT = kbSize.height;
    
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0){
        [self setViewMovedUp:NO];
    }
    
    
}



-(void)setViewMovedUp:(BOOL)movedUp{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp){
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= KEYBOARD_HEIGHT;
        //rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else{
        // revert back to the normal state.
        rect.origin.y += KEYBOARD_HEIGHT;
        //rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}



@end
