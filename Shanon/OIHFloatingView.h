//
//  OIHFloatingView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHFloatingView : UIView
@property (nonatomic) CGFloat cornerRadius;
@property (nonatomic) CGFloat shadowRadius;
@property (nonatomic) CGFloat shadowOpacity;

@property (nonatomic) UIColor *borderColor;

-(void)setHighlight: (BOOL)highlighted;

@end
