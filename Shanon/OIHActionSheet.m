//
//  OIHActionSheet.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 3/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHActionSheet.h"


@interface OIHActionSheet () <UIPickerViewDataSource, UIPickerViewDelegate, UIViewControllerTransitioningDelegate>
@property (nonatomic) UIToolbar *toolBar;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic) UIPickerView *pickerView;
@property (nonatomic) UIView *pickerBackgroundView;
@end

@implementation OIHActionSheet
@synthesize dataModel;
@synthesize toolBar;
@synthesize pickerView;
@synthesize pickerBackgroundView;
@synthesize dataDictionary;


-(id)init{
    
    self = [super init];
    if (self) {
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}




-(void)viewDidLoad{
    [super viewDidLoad];
    [self setupViews];
}

-(void)setupViews{
    
    pickerBackgroundView = [UIView new];
    pickerBackgroundView.backgroundColor = [UIColor whiteColor];
    pickerBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:pickerBackgroundView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pickerBackgroundView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(pickerBackgroundView)]];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:pickerBackgroundView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:30.0f]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:pickerBackgroundView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0]];

    
    toolBar = [[UIToolbar alloc] init];
    toolBar.translatesAutoresizingMaskIntoConstraints = NO;
    toolBar.barStyle = UIBarStyleDefault;
    [pickerBackgroundView addSubview:toolBar];

    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"SELECT", @"") style:UIBarButtonItemStylePlain target:self action:@selector(dismissView)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

    [toolBar setItems:@[flex, done]];
 
    [pickerBackgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[toolBar]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(toolBar)]];


    pickerView = [[UIPickerView alloc] init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.translatesAutoresizingMaskIntoConstraints = NO;
    pickerView.showsSelectionIndicator = YES;
    [pickerBackgroundView addSubview:pickerView];

    
    [pickerBackgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pickerView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(pickerView)]];
    [pickerBackgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[toolBar(==40)][pickerView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(pickerView, toolBar)]];
    
    
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return dataModel.count;
}



- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return dataModel[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.selectedIndex = row;
}


-(void)dismissView{
    
    
    if ([self.delegate respondsToSelector:@selector(actionSheetWillDismiss:atRow:)]) {
        [self.delegate actionSheetWillDismiss:dataModel[self.selectedIndex] atRow:_selectedIndex];
    }
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    CustomAnimatedTransitioning *transitioning = [CustomAnimatedTransitioning new];
    transitioning.presenting = YES;
    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    CustomAnimatedTransitioning * transitioning = [CustomAnimatedTransitioning new];
    transitioning.presenting = NO;
    return transitioning;
}

@end



@implementation CustomAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext{
    return 0.25;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
    
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIViewController *animatingVC = self.presenting? toViewController : fromViewController;
    UIView *animatingView = [animatingVC view];

    UIView *containerView = [transitionContext containerView];
    
    CGRect appearedFrame = [transitionContext finalFrameForViewController:animatingVC];
    CGRect dismissedFrame = appearedFrame;
    dismissedFrame.origin.y += dismissedFrame.size.height;
    
    CGRect initialFrame = self.presenting ? dismissedFrame : appearedFrame;
    CGRect finalFrame = self.presenting ? appearedFrame : dismissedFrame;
    
    [animatingView setFrame:initialFrame];

    
    if (self.presenting) {
        
        [containerView insertSubview:toViewController.view aboveSubview:fromViewController.view];
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
            fromViewController.view.transform = CGAffineTransformMakeScale(0.98, 0.98);
            [animatingView setFrame:finalFrame];
            toViewController.view.alpha = 1.0f;

        } completion:^(BOOL finished) {
            if (finished) {
                [transitionContext completeTransition:YES];
            }
        }];
    
    
    }
    else {
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            
            toViewController.view.transform = CGAffineTransformIdentity;
            [animatingView setFrame:finalFrame];

        } completion:^(BOOL finished) {
            if (finished) {
                [transitionContext completeTransition:YES];

            }
        }];

    }
}

@end

