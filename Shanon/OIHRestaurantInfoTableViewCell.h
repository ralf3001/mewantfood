//
//  OIHRestaurantInfoTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OIHMealModel.h"

@interface OIHRestaurantInfoTableViewCell : UITableViewCell
@property (nonatomic) UIImageView *backgroundImageView;
@property (nonatomic) UILabel *dishLabel;
@property (nonatomic) UILabel *restaurantLabel;
@property (nonatomic) UILabel *priceLabel;

@property (nonatomic, weak) OIHMealModel *mealModel;

@end
