//
//  OIHUserProfileViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "OIHUserProfileViewController.h"
#import "StretchyHeaderCollectionViewLayout.h"
#import "OIHUserProfileCollectionViewCell.h"
#import "UILabel+DynamicHeight.h"
#import "OIHUserAddressListViewController.h"
#import "OIHUserReviewOrdersViewController.h"
#import "OIHUserFollowFriendCollectionViewCell.h"
#import "OIHRestaurantListViewController.h"
#import "OIHUserProfileHeaderCollectionReusableView.h"
#import "OIHChatWindowViewController.h"


#import "OIHSocialManager.h"
#import "OIHPopUpTextAlertController.h"
#import "OIHOrderModel.h"

@interface OIHUserProfileViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, OIHUserCollectionViewCellDelegate, OIHUserFollowFriendCellDelegate, OIHUserProfileHeaderButtonDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, OIHSocialManagerDelegate, PopUpTextDelegate>

@property (nonatomic) UITableView *tableView;
@property (nonatomic) CAGradientLayer *headerViewGradient;
@property (nonatomic) UILabel *nameLabel;
@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) OIHOrderModel *mostRecentOrder;

@property (nonatomic) UIProgressView *progressViewBar;
@property (nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) NSArray *storesList;

@end

NSString *const kCellIdent = @"Cell";
NSString *const kFollowFriendCellIdent = @"follow";
NSString *const kHeaderIdent = @"Header";

@implementation OIHUserProfileViewController
@synthesize tableView = _tableView;
@synthesize headerViewGradient;
@synthesize nameLabel;
@synthesize friendProfile;
@synthesize mostRecentOrder;




-(id)initWithFriendProfile: (OIHUserModel *)profile{
    
    self = [super self];
    if (self) {
        self.friendProfile = profile;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupCollectionView];
    
    [self fetchMostRecentOrder];
    [self fetchRestaurants];
    
    if (friendProfile) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Message" style:UIBarButtonItemStylePlain target:self action:@selector(message)];
    }
    
}


-(void)message{
    
    OIHChatWindowViewController *vc = [[OIHChatWindowViewController alloc] init];
    vc.friendProfile = friendProfile;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)setupCollectionView{
    
    StretchyHeaderCollectionViewLayout *stretchyLayout;
    stretchyLayout = [[StretchyHeaderCollectionViewLayout alloc] init];
    [stretchyLayout setSectionInset:UIEdgeInsetsMake(0, 10.0, 10.0, 10.0)];
    [stretchyLayout setHeaderReferenceSize:CGSizeMake(320.0, 160.0)];
    [stretchyLayout setFooterReferenceSize:CGSizeMake(320.0, 80.0)];
    
    stretchyLayout.minimumInteritemSpacing = 0;
    stretchyLayout.minimumLineSpacing = 0;
    
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:stretchyLayout];
    _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    [_collectionView setAlwaysBounceVertical:YES];
    [_collectionView setShowsVerticalScrollIndicator:NO];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(updateProfile) forControlEvents:UIControlEventValueChanged];
    _collectionView.alwaysBounceVertical = YES;
//    [_collectionView addSubview:_refreshControl];
    [self.view addSubview:_collectionView];
    
    
    [_collectionView registerClass:[OIHUserProfileCollectionViewCell class] forCellWithReuseIdentifier:kCellIdent];
    
    [_collectionView registerClass:[OIHUserFollowFriendCollectionViewCell class] forCellWithReuseIdentifier:kFollowFriendCellIdent];
    
    
    [_collectionView registerClass:[OIHUserProfileHeaderCollectionReusableView class]
        forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
               withReuseIdentifier:kHeaderIdent];
    
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv {
    
    return 1;
}

- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section {
    
    return friendProfile ? 3 : 5;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                (id)[UIFont fontWithName:@"HelveticaNeue-Thin" size:24.0f], NSFontAttributeName,
                                (id)[UIColor lightGrayColor], NSForegroundColorAttributeName, nil];
    
    NSAttributedString *attrText;
    
    if (!friendProfile) {
        
        if (indexPath.row == 1) {
            
            NSDictionary *user = [[UserProfile currentUser] userDictionary];
            NSArray *addresses = user[kOIHUserAddress];
            
            if (addresses.count > 0) {
                NSDictionary *defaultAddress = addresses[0];
                
                attrText = [[NSAttributedString alloc] initWithString:defaultAddress[kOIHAddressUserEnteredAddress] attributes:attributes];
                
            }else{
                attrText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"USER_HAS_NO_ADDRESS", @"") attributes:attributes];
                
            }
        }else if (indexPath.row == 3){
            
            NSString *orderString;
            
            if (mostRecentOrder) {
                orderString = [NSString stringWithFormat:@"%ld x %@ from %@", (long)mostRecentOrder.quantity, mostRecentOrder.orderMeal.name, mostRecentOrder.restaurant.restaurantName];
            }else{
                orderString = NSLocalizedString(@"USER_NO_RECENT_ORDER", @"");
            }
            
            attrText = [[NSAttributedString alloc] initWithString:orderString attributes:attributes];
            
        }
        
    }
    else{
        
        switch (indexPath.row) {
            case 0:{
                return CGSizeMake(collectionView.frame.size.width, 80);
            }
            case 1:{
                NSString *orderString;
                
                if (mostRecentOrder) {
                    orderString = [NSString stringWithFormat:@"%ld x %@ from %@", (long)mostRecentOrder.quantity, mostRecentOrder.orderMeal.name, mostRecentOrder.restaurant.restaurantName];
                }else{
                    orderString = [NSString stringWithFormat:NSLocalizedString(@"%@_HAS_NO_RECENT_ORDER", @"[person] has no recent order"), friendProfile.userName];
                }
                
                attrText = [[NSAttributedString alloc] initWithString:orderString attributes:attributes];
                break;
                
            }
            case 2:{
                
                break;
            }
            default:
                break;
        }
        
    }
    
    if (attrText) {
        CGRect rect = [attrText boundingRectWithSize:CGSizeMake(collectionView.frame.size.width - 40, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        
        return CGSizeMake(collectionView.frame.size.width, rect.size.height + 70);
        
    }else{
        return CGSizeMake(collectionView.frame.size.width, 90);
        
    }
    
}



- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *view;
    
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        OIHUserProfileHeaderCollectionReusableView *header = (OIHUserProfileHeaderCollectionReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:kHeaderIdent forIndexPath:indexPath];
        
        if (friendProfile) {
            
            OIHUserModel *user = friendProfile;
            
            if ([user isUserType:UserFriend]) {
                [header setFunctionForUtilityButton:UtilityButtonUnfriendUser user:friendProfile];
            } else{
                [header.personUtilityButton removeFromSuperview];
            }
            
            [header setNameLabelText:user.userName];
            [header.imageView sd_setImageWithURL: [NSURL URLWithString:user.profilePicURL] placeholderImage:[UIImage imageFromColor:[UIColor lightGrayColor]]];
            
        }else{
            
            NSString *url = [[UserProfile currentUser].profilePics firstObject];
            if (url) {
                [header.imageView sd_setImageWithURL: [NSURL URLWithString:url] placeholderImage:[UIImage imageFromColor:[UIColor lightGrayColor]]];
            }
            
            [header setNameLabelText:[[UserProfile currentUser] name]];
            [header setFunctionForUtilityButton:UtilityButtonUserSetProfilePicture user:nil];
            
        }
        
        header.delegate = self;
        view = header;
        
    }else{
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"footer" forIndexPath:indexPath];
        view = footer;
    }
    
    return view;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (friendProfile) {
        
        switch (indexPath.row) {
            case 0:{
                OIHUserFollowFriendCollectionViewCell *cell = (OIHUserFollowFriendCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kFollowFriendCellIdent forIndexPath:indexPath];
                cell.friendProfile = friendProfile;
                
                cell.delegate = self;
                return cell;
            }
            case 1:{
                OIHUserProfileCollectionViewCell *cell = (OIHUserProfileCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCellIdent forIndexPath:indexPath];
                
                cell.descriptionField.text = NSLocalizedString(@"USER_RECENT_ORDERS", @"");
                if (mostRecentOrder) {
                    [cell setDescriptionContentText:[NSString stringWithFormat:@"%ld x %@ from %@", (long)mostRecentOrder.quantity, mostRecentOrder.orderMeal.name, mostRecentOrder.restaurant.restaurantName]];
                }else{
                    [cell setDescriptionContentText:[NSString stringWithFormat:NSLocalizedString(@"%@_HAS_NO_RECENT_ORDER", @"[person] has no recent order"), friendProfile.userName]];
                }
                
                [cell setMoreButton];
                cell.delegate = self;
                
                return cell;
            }
            case 2:{
                OIHUserProfileCollectionViewCell *cell = (OIHUserProfileCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCellIdent forIndexPath:indexPath];
                
                cell.descriptionField.text = @"Stores";
                [cell setDescriptionContentText:[NSString stringWithFormat:@"%ld store(s)", (unsigned long)_storesList.count]];
                return cell;
            }
            default:
                break;
        }
        
    }
    else{
        
        OIHUserProfileCollectionViewCell *cell = (OIHUserProfileCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCellIdent forIndexPath:indexPath];
        
        NSDictionary *user = [[UserProfile currentUser] userDictionary];
        
        switch (indexPath.row) {
            case 0:
                cell.descriptionField.text = NSLocalizedString(@"USER_EMAIL", @"Email");
                [cell setDescriptionContentText:[UserProfile currentUser].email];
                break;
            case 1:{
                NSArray *addresses = user[kOIHUserAddress];
                if (addresses.count > 0) {
                    NSDictionary *defaultAddress = addresses[0];
                    [cell setDescriptionContentText:defaultAddress[kOIHAddressUserEnteredAddress]];
                }else{
                    [cell setDescriptionContentText:NSLocalizedString(@"USER_HAS_NO_ADDRESS", @"")];
                }
                
                cell.descriptionField.text = NSLocalizedString(@"USER_ADDRESS", @"");
                [cell setMoreButton];
                cell.delegate = self;
                break;
            }
            case 2:
                cell.descriptionField.text = NSLocalizedString(@"USER_PHONE_NUMBER", @"");
                
                if ([UserProfile currentUser].phoneNumber.count > 0) {
                    [cell setDescriptionContentText:[UserProfile currentUser].phoneNumber[0]];
                }else{
                    [cell setDescriptionContentText:NSLocalizedString(@"USER_NO_PHONE_NUMBER", @"")];
                }
                [cell setMoreButton];
                
                break;
            case 3:
                cell.descriptionField.text = NSLocalizedString(@"USER_RECENT_ORDERS", @"");
                
                if (mostRecentOrder) {
                    
                    [cell setDescriptionContentText:[NSString stringWithFormat:@"%ld x %@ from %@", mostRecentOrder.quantity, mostRecentOrder.orderMeal.name, mostRecentOrder.restaurant.restaurantName]];
                }else{
                    [cell setDescriptionContentText:NSLocalizedString(@"USER_NO_RECENT_ORDER", @"")];
                }
                
                [cell setMoreButton];
                cell.delegate = self;
                break;
            case 4:{
                
                cell.descriptionField.text = @"Stores";
                //fetch user restaurants;
                [cell setDescriptionContentText:[NSString stringWithFormat:@"%ld store(s)", _storesList.count]];
                
            }
            default:
                break;
        }
        
        [cell.descriptionContent sizeToFit];
        
        return cell;
        
    }
    
    return nil;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    if (friendProfile) {
        switch (indexPath.row) {
            case 1:{
                OIHUserReviewOrdersViewController *vc = [[OIHUserReviewOrdersViewController alloc] initWithPersonProfile:friendProfile];
                vc.view.backgroundColor = [UIColor whiteColor];
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case 1:{
                
                OIHUserAddressListViewController *vc = [OIHUserAddressListViewController new];
                vc.view.backgroundColor = [UIColor whiteColor];
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 2:{
                
                OIHPopUpTextAlertController *alert = [[OIHPopUpTextAlertController alloc] init];
                alert.labelText = NSLocalizedString(@"USER_ENTER_YOUR_PHONE_NUMBER", @"Enter your phone number");
                alert.alertViewIcon = [IonIcons imageWithIcon:ion_ios_telephone size:60 color:[UIColor whiteColor]];
                alert.iconBackgroundColor = [UIColor colorWithRed:50/256.0f green:103/256.0f blue:135/256.0f alpha:1.0f];
                
                alert.delegate = self;
                
                [alert addDoneAction:^(NSString *text, OIHPopUpTextAlertController *alert) {
                    
                    
                    JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute: @"profile" httpMethod: HTTP_PUT];
                    settings.requestData = @{@"phoneNumber": text};
                    
                    [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
                        if (!error) {
                            [alert switchToCompleteView];
                        }else{
                            alert.cancelContentView.title = NSLocalizedString(@"ERROR_BOOM", @"Boom");
                            alert.cancelContentView.descriptionText = NSLocalizedString([error domain], @"");
                            [alert switchToFailView];
                        }
                    }];
                    
                    
                }];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
                break;
            }
            case 3:{
                
                OIHUserReviewOrdersViewController *vc = [OIHUserReviewOrdersViewController new];
                vc.view.backgroundColor = [UIColor whiteColor];
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
                
            default:
                break;
        }
        
    }
    
    
}

#pragma mark - CollectionViewCell delegates

-(void)userDidPressActionButton:(UIButton *)button cell:(OIHUserProfileCollectionViewCell *)cell{
    
    NSIndexPath *indexPath = [_collectionView indexPathForCell:cell];
    
    if (!friendProfile) {
        switch (indexPath.row) {
            case 1:{
                
                OIHUserAddressListViewController *vc = [OIHUserAddressListViewController new];
                vc.view.backgroundColor = [UIColor whiteColor];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
            }
            case 2:{
                
                break;
            }
            case 3:{
                OIHUserReviewOrdersViewController *vc = [OIHUserReviewOrdersViewController new];
                vc.view.backgroundColor = [UIColor whiteColor];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
            }
                
            default:
                break;
        }
        
    }
    
}



#pragma mark - PopUpTextDelegate


-(BOOL)popupCheckValidity:(NSString *)text{
    return text.length > 0;
}

-(void)didPressRemoveFriendRequestButton:(OIHUserFollowFriendCollectionViewCell *)cell{
    
    [friendProfile cancelRequest:nil];
    
}

-(void)didPressFollowFriendButton: (OIHUserFollowFriendCollectionViewCell *)cell{
    [friendProfile followPerson];
}

-(void)didPressUnfollowFriendButton: (OIHUserFollowFriendCollectionViewCell *)cell{
    [friendProfile unfollowPerson];
}

-(void)didPressAddFriendButton: (OIHUserFollowFriendCollectionViewCell *)cell{
    
    if ([friendProfile isUserType:UserFriend] == YES) {
        //unfriend
        [friendProfile removeUserType:UserFriend];
        [self friendOperation:NO];
        
    }else{
        [friendProfile addUserType:UserFriend];
        //add as friend
        [self friendOperation:YES];
        
    }
    
}

-(void)didPressBuyFoodButton: (OIHUserFollowFriendCollectionViewCell *)cell{
    
    [self restaurantListViewController];
    
}


-(void)didPressUtilityButton: (UtilityButtonFunction)type{
    
    switch (type) {
        case UtilityButtonBuyFoodForUser:
            [self restaurantListViewController];
            break;
        case UtilityButtonUnfriendUser:
            [self friendOperation:NO];
            break;
        case UtilityButtonUserSetProfilePicture:
            [self uploadProfilePic];
            break;
        default:
            break;
    }
    
}


-(void)friendOperation: (BOOL)addFriend{
    
    if (addFriend) {
        [friendProfile sendFriendRequest:nil];
    }else{
        [friendProfile removeFromFriendsList:nil];
    }
    
}


-(void)fetchMostRecentOrder{
    
    if (friendProfile) {
        
        [friendProfile userLastOrder:^(id data, NSError *error) {
            
            if (!error && data) {
                mostRecentOrder = [[OIHOrderModel alloc] initWithOrderInfo:data];
                [self refreshCollectionViewWithoutAnimation];
            }
            
        }];
        
    }else{
        
        JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"mostRecentOrder" httpMethod:HTTP_GET];
        setting.requestData = @{@"userId": [UserProfile currentUser]._id};
        
        [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            if (!error && data) {
                mostRecentOrder = [[OIHOrderModel alloc] initWithOrderInfo:data];
                [self refreshCollectionViewWithoutAnimation];
            }
            
        }];
        
    }
    
}

-(void)fetchRestaurants{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"restaurant" httpMethod:HTTP_GET];
    
    if (friendProfile) {
        setting.requestData = @{@"userId": friendProfile._id};
    }
    
    setting.cachePolicy = IgnoreCache;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            _storesList = [OIHRestaurantModel restaurantsFromArray:data];
            [self refreshCollectionViewWithoutAnimation];
        }
    }];
    
}


-(void)refreshCollectionViewWithoutAnimation{
    
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [_collectionView reloadData];
    [UIView setAnimationsEnabled:animationsEnabled];
    
}


-(void)restaurantListViewController{
    
    OIHRestaurantListViewController *vc = [OIHRestaurantListViewController new];
    vc.friendProfile = friendProfile;
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)uploadProfilePic{
    
    UIImagePickerController *imagePickr = [[UIImagePickerController alloc] init];
    imagePickr.delegate = self;
    imagePickr.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePickr animated:YES completion:NULL];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
        [self createProgressViewBar];
        
        [OIHSocialManager sharedInstance].delegate = self;
        
        __weak __typeof__(self) weakSelf = self;
        
        [[OIHSocialManager sharedInstance] uploadProfilePhoto:[chosenImage resizeImage] completionHandler:^(id result, NSError *error) {
            if (!error) {
                
                typeof(weakSelf) strongSelf = weakSelf;
                
                if (strongSelf) {
                    
                    OIHUserProfileHeaderCollectionReusableView *header = (OIHUserProfileHeaderCollectionReusableView *)[strongSelf.collectionView supplementaryViewForElementKind:UICollectionElementKindSectionHeader atIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    header.imageView.image = chosenImage;
                    //                    [header.imageView sd_setImageWithURL: [NSURL URLWithString:result[@"url"]] placeholderImage:[UIImage imageFromColor:[UIColor lightGrayColor]]];
                    [strongSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
                    
                }
                
                JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"profilepic" httpMethod:HTTP_POST];
                setting.requestData = result;
                
                
                [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
                    if (!error) {
                        //done
                        //http://stackoverflow.com/questions/21113963/is-the-weakself-strongself-dance-really-necessary-when-referencing-self-inside-a
                        /*
                         You only need the weakSelf/strongSelf combination where it's critical to have a strong reference (e.g., you're dereferencing ivars) or if you need to worry about race conditions. That does not appear to be the case here.
                         */
                        [[NSNotificationCenter defaultCenter] postNotificationName:OIHUserDidUpdateProfileNotification object:nil];
                        
                        [weakSelf.progressViewBar removeFromSuperview];
                        
                    }
                }];
            }else{
                
                //BOOM
                
                
            }
        }];
        
    });
    
}

-(void)updateProfile{
    
    if (friendProfile) {
        
        JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"user/%@", friendProfile._id] httpMethod:HTTP_GET];
        
        [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            
            [_refreshControl endRefreshing];
            
            if (!error) {
                friendProfile = [[OIHUserModel alloc] initWithUserInfo:data];
                [_collectionView reloadData];
            }
            
        }];
        
    }else{
        
        [[UserProfile currentUser] updateProfile:^(NSError *error) {
            
            [_refreshControl endRefreshing];
            
            if (!error) {
                [_collectionView reloadData];
            }
            
        }];
        
    }
    
    
}


#pragma mark - <OIHSocialManagerDelegate>

-(void)socialManagerUploadProgress:(float)progress{
    //handled in main thread
    [_progressViewBar setProgress:progress animated:YES];
}


-(void)socialManagerProfilePicDidFinishUploading:(id)data{
    
    
    
}

-(void)createProgressViewBar{
    
    _progressViewBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    _progressViewBar.progressTintColor = [UIColor colorWithRed:216/256.0f green:106/256.0f blue:106/256.0f alpha:1.0f];
    _progressViewBar.trackTintColor = [UIColor whiteColor];
    _progressViewBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.navigationController.view addSubview:_progressViewBar];
    
    UIView *nav = self.navigationController.navigationBar;
    
    [self.navigationController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[nav][_progressViewBar(==5)]" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(nav, _progressViewBar)]];
    [self.navigationController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_progressViewBar]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_progressViewBar)]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
