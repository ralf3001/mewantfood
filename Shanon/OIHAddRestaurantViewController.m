//
//  OIHAddRestaurantViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHAddRestaurantViewController.h"
#import "OIHAddMealTableViewCell.h"
#import "NBPhoneNumberUtil.h"
#import "OIHMealInfoTableViewCell.h"
#import "OIHRestaurantModel.h"
#import <CoreLocation/CoreLocation.h>


@interface OIHAddRestaurantViewController () <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, OIHMealInfoTableViewCellDelegate, OIHActionSheetProtocol, CLLocationManagerDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSLayoutConstraint *dynamicTVHeight;
@property (nonatomic) OIHRestaurantModel *restaurantModel;
@property (assign) BOOL isUsingCoreLocationForAddress;

@end

@implementation OIHAddRestaurantViewController
@synthesize tableView = _tableView;
@synthesize dynamicTVHeight;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;

    _restaurantModel = [[OIHRestaurantModel alloc] init];
    self.isUsingCoreLocationForAddress = NO;
    
    [self createTableView];

}


-(void)createTableView{
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [_tableView registerClass:[OIHMealInfoTableViewCell class] forCellReuseIdentifier:@"cell"];
    
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    
    _tableView.estimatedRowHeight = 130;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    [doneButton setTitle:NSLocalizedString(@"DONE", @"Done") forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.backgroundColor = [UIColor colorWithRed:46/256.0f green:65/256.0f blue:114/256.0f alpha:1.0f];
    
    UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeSystem];
    clearButton.translatesAutoresizingMaskIntoConstraints = NO;
    [clearButton setTitle:NSLocalizedString(@"CLEAR", @"Clear") forState:UIControlStateNormal];
    [clearButton addTarget:self action:@selector(clearForm) forControlEvents:UIControlEventTouchUpInside];
    [clearButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    clearButton.backgroundColor = [UIColor colorWithRed:224/256.0f green:80/256.0f blue:76/256.0f alpha:1.0f];

    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 80)];
    
    [footerView addSubview:doneButton];
    [footerView addSubview:clearButton];
    
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[clearButton]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(clearButton)]];
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[clearButton][doneButton(==clearButton)]|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(doneButton, clearButton)]];
    
    _tableView.tableFooterView = footerView;
    
    [self.view addSubview:_tableView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    
    
    
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    OIHMealInfoTableViewCell *cell = (OIHMealInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            cell.descriptionTitleLabel.text = NSLocalizedString(@"RESTAURANT_NAME", @"");
            cell.cellAttribute = kOIHRestaurantName;
            if (_restaurantModel.restaurantName) {
                cell.descriptionContentLabel.text = _restaurantModel.restaurantName;
            }else{
                cell.placeHolderText = NSLocalizedString(@"MANAGEMENT_ADD_RESTAURANT_RESTAURANT_NAME_PLACEHOLDER", @"");
            }
            break;
        case 1:{
            cell.descriptionTitleLabel.text = NSLocalizedString(@"RESTAURANT_ADDRESS", @"");
            cell.cellAttribute = @"addressInfo";
            if (_restaurantModel.addressInfo) {
                cell.descriptionContentLabel.textColor = [UIColor blackColor];
                cell.descriptionContentLabel.text = _restaurantModel.addressInfo;
            }else{
                cell.placeHolderText = NSLocalizedString(@"MANAGEMENT_ADD_RESTAURANT_RESTAURANT_ADDRESS_PLACEHOLDER", @"");
            }
            
            [cell createNavButton];

            [cell.navButton addTarget:self action:@selector(activateLocation) forControlEvents:UIControlEventTouchUpInside];

            [cell activateNavButton:_isUsingCoreLocationForAddress];
            
            break;
        }
            
        case 2:
            cell.descriptionTitleLabel.text = NSLocalizedString(@"MANAGEMENT_ADD_RESTAURANT_RESTAURANT_SELLERS_NOTE", @"Notes");
            cell.cellAttribute = @"sellersNote";
            if (_restaurantModel.sellersNote) {
                cell.descriptionContentLabel.text = _restaurantModel.sellersNote;
            }else{
                cell.placeHolderText = NSLocalizedString(@"MANAGEMENT_ADD_RESTAURANT_RESTAURANT_SELLERS_NOTE_PLACEHOLDER", @"");
            }
            break;
        case 3:
            cell.descriptionTitleLabel.text = NSLocalizedString(@"RESTAURANT_PHONE", @"");
            cell.cellAttribute = kOIHRestaurantPhoneNumber;
            cell.descriptionContentLabel.keyboardType = UIKeyboardTypePhonePad;
            if (_restaurantModel.phoneNumber) {
                cell.descriptionContentLabel.text = _restaurantModel.phoneNumber;
            }else{
                cell.placeHolderText = NSLocalizedString(@"MANAGEMENT_ADD_RESTAURANT_RESTAURANT_PHONE_NUMBER_PLACEHOLDER", @"");
            }
            break;
        default:
            break;
    }
    
    cell.tableView = _tableView;
    cell.delegate = self;
    cell.descriptionContentLabel.editable = YES;
    
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)clearForm{
    _restaurantModel = nil;
    _restaurantModel = [[OIHRestaurantModel alloc] init];
    [_tableView reloadData];
}

-(void)done{
    
    if (!_restaurantModel.restaurantName || !_restaurantModel.phoneNumber || !_restaurantModel.addressInfo || !_restaurantModel.sellersNote) {
        return;
    }
    
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"LABEL_SENDING", @"Sending...")];
    
    [[JSONRequest sharedInstance] fetch:@"restaurant" method:@"POST" data:[_restaurantModel toDictionary] completionHandler:^(id data, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{

            [SVProgressHUD dismiss];
            
            if (error) {
                //ion-ios-close-outline
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"ERROR_SOMETHING_GONE_WRONG", @"error: something's gone wrong")];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });

            }else{

                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"DONE", @"Done")];

                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [self.navigationController popViewControllerAnimated:YES];
                });
                
            }

        });

        
    }];
    
}


-(void)textViewFinishedEditing: (UITextView *)textView cell: (OIHMealInfoTableViewCell *)cell{

    if (!_restaurantModel) {
        _restaurantModel = [OIHRestaurantModel new];
    }
    
    if ([cell.cellAttribute isEqualToString:kOIHRestaurantPhoneNumber]) {

        [_restaurantModel setAttribute:cell.cellAttribute data:textView.text];
        [self geocodeLocation:_restaurantModel.addressInfo phoneNumber: textView.text];
        [_restaurantModel setAttribute:cell.cellAttribute data:textView.text];

    }else{
        [_restaurantModel setAttribute:cell.cellAttribute data:textView.text];
    }
    
}


-(NSString *)verifyPhoneNumber: (NSString *)phoneNumber region: (NSString *)region{

    
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *anError = nil;

    
    if (IS_IPHONE) {
        
        NBPhoneNumber *myNumber = [phoneUtil parseWithPhoneCarrierRegion:phoneNumber error:&anError];
        
        
        if (!anError) {
            
            if (![phoneUtil isValidNumber:myNumber]) {
                
                if (region) {
                    NBPhoneNumber *myNumber = [phoneUtil parse:phoneNumber defaultRegion:region error:&anError];
                    if (!anError) {
                        return [phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatINTERNATIONAL error:&anError];
                    }
                }else{
                    return nil;

                }
                
            }else{

                return [phoneUtil format:myNumber
                            numberFormat:NBEPhoneNumberFormatINTERNATIONAL
                                   error:&anError];
            }
            
        }

    }else{
    
        if (region) {
            NBPhoneNumber *myNumber = [phoneUtil parse:phoneNumber defaultRegion:region error:&anError];
            if (!anError) {
                return [phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatINTERNATIONAL error:&anError];
            }
        }
    
    
    }
    return nil;
}


-(void)phoneRegionActionSheet{
    
    NSLocale *locale = [NSLocale currentLocale];
    
    NSArray *countryArray = [NSLocale ISOCountryCodes];
    NSMutableArray *sortedCountryArray = [[NSMutableArray alloc] init];
    NSMutableDictionary *countryDictionary = [NSMutableDictionary new];
    
    for (NSString *countryCode in countryArray) {
        
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        [sortedCountryArray addObject:displayNameString];
        countryDictionary[countryCode] = displayNameString;
        
    }
    
    
    OIHActionSheet *actionSheet = [OIHActionSheet new];
    actionSheet.delegate = self;
    actionSheet.dataModel = [sortedCountryArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];;
    [self presentViewController:actionSheet animated:YES completion:nil];

    
    
}


-(void)activateLocation{
    
    if (!_isUsingCoreLocationForAddress) {

        _isUsingCoreLocationForAddress = YES;

        [[OIHLocationManager sharedInstance] getLastPlacemark:^(CLPlacemark *placemark) {
            
            _restaurantModel.addressInfo = placemark.addressDictionary[@"FormattedAddressLines"][0];
            NSString *number = [self verifyPhoneNumber:_restaurantModel.phoneNumber region:placemark.ISOcountryCode];
            if (number) {
                [_restaurantModel setAttribute:kOIHRestaurantPhoneNumber data:number];
            }
            [_tableView reloadData];
            
        }];
        
    }else{
        
        _isUsingCoreLocationForAddress = NO;
        _restaurantModel.addressInfo = @"";
        [_tableView reloadData];

    }
    
    
    
}



-(void)geocodeLocation: (NSString *)address phoneNumber: (NSString *)phoneNumber{
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];

    [geocoder geocodeAddressString:address
                 completionHandler:^(NSArray* placemarks, NSError* error){

                     if (placemarks.count > 0) {
                         
                         CLPlacemark* aPlacemark = placemarks[0];
                         NSString *number = [self verifyPhoneNumber:_restaurantModel.phoneNumber region:aPlacemark.ISOcountryCode];
                         
                         if (number) {
                             [_restaurantModel setAttribute:kOIHRestaurantPhoneNumber data:number];
                             [_tableView reloadData];
                         }else{
                             //highlight the text color to red for 3 sec.
                             [self setTableViewCellWarningAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
                     
                         }
                     }
                     
                     if (error) {
                         [[OIHLocationManager sharedInstance] getLastPlacemark:^(CLPlacemark *placemark) {
                             
                             NSString *number = [self verifyPhoneNumber:_restaurantModel.phoneNumber region:placemark.ISOcountryCode];
                             if (number) {
                                 [_restaurantModel setAttribute:kOIHRestaurantPhoneNumber data:number];
                                 [_tableView reloadData];
                             }else{
                                 [self setTableViewCellWarningAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
                             }
                             
                         }];

                     }
                 }];
    
    
}


-(void)setTableViewCellWarningAtIndexPath: (NSIndexPath *)indexPath{
    
    OIHMealInfoTableViewCell *cell = (OIHMealInfoTableViewCell *)[_tableView cellForRowAtIndexPath:indexPath];
    [cell setWarningForContentLabel:NSLocalizedString(@"MANAGEMENT_ADD_RESTAURANT_PHONE_NUMBER_ERROR_FORMAT", @"wrong phone number format")];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
