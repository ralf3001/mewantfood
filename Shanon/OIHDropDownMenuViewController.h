//
//  OIHDropDownMenuViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 1/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OIHDropDownMenuViewController;

@protocol OIHDropDownMenuDelegate <NSObject>

@optional
-(void)dropDownMenu: (OIHDropDownMenuViewController *)dropDownMenu didSelectIndx: (NSInteger )selectedIndex;
-(void)dropDownMenuWillAppear: (OIHDropDownMenuViewController *)dropDownMenu;

@end

@interface OIHDropDownMenuViewController : UIViewController <UIViewControllerTransitioningDelegate>

@property (nonatomic) NSArray *menuItems;
@property (nonatomic, weak) id<OIHDropDownMenuDelegate> delegate;
@property (nonatomic) NSDictionary *menuMiscInfo;

-(id)initWithMenuItems: (NSArray *)items;
-(void)updateTableViewNotifications;

@end

@interface DropDownMenuAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (assign) BOOL presenting;
@property (nonatomic) UIView *snapShot;
@end
