//
//  OIHHomeScreenTopScrollView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 6/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHHomeScreenTopScrollView : UIScrollView <UIScrollViewDelegate>

@end
