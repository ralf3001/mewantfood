//
//  NSString+extraBits.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 7/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (extraBits)

-(BOOL)isValidEmail;
-(BOOL)hasDomain;

-(NSString *)md5;
+(NSString *)abbreviateNumber:(NSInteger)num;

@end
