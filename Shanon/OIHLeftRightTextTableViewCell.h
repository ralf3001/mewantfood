//
//  OIHLeftRightTextTableViewCell.h
//  Shanon
//
//  Created by Ralf Cheung on 8/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHLeftRightTextTableViewCell : UITableViewCell

@property (nonatomic) UILabel *leftLabel;
@property (nonatomic) UILabel *rightLabel;

@end
