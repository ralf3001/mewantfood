//
//  OIHReceiptDetailViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 18/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHReceiptDetailViewController.h"
#import "OIHUserPostOrderActivityViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+Color.h"
#import "UIView+SlideInExtension.h"
#import "OIHItemRatingView.h"
#import "OIHActivityFeed.h"

#import "OIHLeftRightTextTableViewCell.h"


@interface OIHReceiptDetailViewController () <OIHPostOrderDelegate, OIHRatingContentViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *orderUserLabel;
@property (nonatomic) UILabel *recipientUserLabel;
@property (nonatomic) UILabel *mealNameLabel;
@property (nonatomic) UILabel *orderDateLabel;
@property (nonatomic) UILabel *orderIdLabel;
@property (nonatomic) UILabel *restaurantNameLabel;
@property (nonatomic) UILabel *priceLabel;
@property (nonatomic) UILabel *orderQuantityLabel;
@property (nonatomic) UILabel *addressTagLabel;
@property (nonatomic) UILabel *formattedAddressLabel;
@property (nonatomic) UILabel *orderStatusLabel;
@property (nonatomic) OIHItemRatingView *ratingView;
@property (nonatomic) OIHActivityFeed *review;

@property (nonatomic) UIScrollView *scrollView;

@property (nonatomic) UITableView *tableView;

@end

@implementation OIHReceiptDetailViewController

@synthesize orderInfo;
@synthesize orderDateLabel, orderIdLabel, orderUserLabel;
@synthesize mealNameLabel, recipientUserLabel;
@synthesize imageView;
@synthesize restaurantNameLabel;
@synthesize priceLabel;
@synthesize orderQuantityLabel;
@synthesize addressTagLabel, formattedAddressLabel;
@synthesize scrollView;
@synthesize orderStatusLabel;




-(id)initWithOrder: (OIHOrderModel *)order{
    self = [super init];
    if (self) {
        self.orderInfo = order;
        if ([self.orderInfo.orderRecipient._id isEqualToString:[UserProfile currentUser]._id] && (self.orderInfo.status == OrderDelivered || self.orderInfo.status == OrderCanceled)) {
            [self fetchComment];
            [self fetchTransaction];
        }
    }
    return self;
}


-(id)init{
    self = [super init];
    if (self) {
//        [self commonInit];
    }
    return self;
}

-(void)viewWithTableView{

    [self createTableView];
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
//    [self sizeHeaderToFit];
}


-(void)createTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [_tableView registerClass:[OIHLeftRightTextTableViewCell class] forCellReuseIdentifier:@"cell"];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    _tableView.estimatedRowHeight = 60;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [self.view addSubview:_tableView];


    _tableView.tableHeaderView = [self tableViewHeaderView];
    
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 20)];
    _tableView.tableFooterView = footer;
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    
}

-(UIView *)tableViewHeaderView{
    
    UIView *header;
    
    if ([self.orderInfo.orderRecipient._id isEqualToString:[UserProfile currentUser]._id] && (self.orderInfo.status == OrderDelivered)) {
       header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 236)];

    }else{
       header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 200)];
    }
    
    imageView = [self createImageView];
    [header addSubview:imageView];
    
    [header addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    
    [header addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];

    
    
    if ([self.orderInfo.orderRecipient._id isEqualToString:[UserProfile currentUser]._id] && (self.orderInfo.status == OrderDelivered)) {
        
        UIView *ratingViewBackground = [UIView new];
        ratingViewBackground.translatesAutoresizingMaskIntoConstraints = NO;
        ratingViewBackground.layer.cornerRadius = 30.0f;
        ratingViewBackground.layer.borderColor = [UIColor colorWithWhite:0.95 alpha:1.0f].CGColor;
        ratingViewBackground.layer.borderWidth = 0.5f;
        ratingViewBackground.backgroundColor = [UIColor whiteColor];
        [header addSubview:ratingViewBackground];
        
        _ratingView = [[OIHItemRatingView alloc] init];
        _ratingView.translatesAutoresizingMaskIntoConstraints = NO;
        _ratingView.delegate = self;
        [ratingViewBackground addSubview:_ratingView];
        
        [ratingViewBackground addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_ratingView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_ratingView)]];
        
        [ratingViewBackground addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_ratingView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_ratingView)]];
        
        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[ratingViewBackground]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(ratingViewBackground)]];

        [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[imageView(==150)]-[ratingViewBackground(==60)]-(>=10)-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView, ratingViewBackground)]];

    }else{
        
        [header addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:150.0f]];
        [header addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
        
    }
    
    
    return header;
}

-(void)sizeHeaderToFit{

    UIView *header = self.tableView.tableHeaderView;
    
    [header setNeedsLayout];
    [header layoutIfNeeded];
    
    CGFloat height = [header systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = header.frame;
    
    frame.size.height = height;
    header.frame = frame;
    
    self.tableView.tableHeaderView = header;

}

-(void)fetchTransaction{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"transaction" httpMethod:HTTP_GET];
    setting.requestData = @{@"orderId": self.orderInfo._id};
    setting.cachePolicy = IgnoreCache;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            NSLog(@"%@", data);
        }
    }];

}



-(void)fetchComment{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"activity" httpMethod:HTTP_GET];
    setting.requestData = @{@"order": self.orderInfo._id, @"activityType": @"review"};
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            
            if (data) {
                _review = [[OIHActivityFeed alloc] initWithActivity:data];
                [imageView sd_setImageWithURL:[NSURL URLWithString:_review.postPicture] placeholderImage:[UIImage imageFromColor:[UIColor colorWithWhite:0.8 alpha:1.0f]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{

                        [UIView transitionWithView:imageView
                                          duration:0.2f
                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                        animations:^{
                                            imageView.image = image;
                                        } completion:NULL];

                    });


                }];
                
                if (_ratingView) {
                    [_ratingView setChosenEmoji:_review.rating];
                }
                //create a comment view of some sort
            }else{
                _review = [OIHActivityFeed new];
                _review.order = orderInfo;
                
                imageView.userInteractionEnabled = YES;
                [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(postComment)]];

            }
        }
    }];

}


-(void)commonInit{
    
    
    scrollView = [UIScrollView new];
    scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    scrollView.backgroundColor = [UIColor colorWithWhite:0.94 alpha:1.0f];
    [self.view addSubview:scrollView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[scrollView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(scrollView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(scrollView)]];
    
    imageView = [UIImageView new];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.cornerRadius = 75.0f;
    imageView.layer.borderWidth = 3.0f;
    imageView.layer.masksToBounds = YES;
    imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [scrollView addSubview:imageView];
    
    orderUserLabel = [self createLabel];
    [scrollView addSubview:orderUserLabel];
    
    recipientUserLabel = [self createLabel];
    [scrollView addSubview:recipientUserLabel];
    
    mealNameLabel = [self createLabel];
    mealNameLabel.text = orderInfo.orderMeal.name;
    [scrollView addSubview:mealNameLabel];
    
    orderDateLabel = [self createLabel];
    [scrollView addSubview:orderDateLabel];
    
    orderIdLabel = [self createLabel];
    [scrollView addSubview:orderIdLabel];

    orderQuantityLabel = [self createLabel];
    [scrollView addSubview:orderQuantityLabel];
    
    orderStatusLabel = [self createLabel];
    orderStatusLabel.textColor = [orderInfo statusColor];
    [scrollView addSubview:orderStatusLabel];

    restaurantNameLabel = [self createLabel];
    restaurantNameLabel.numberOfLines = 0;
    restaurantNameLabel.textColor = [UIColor grayColor];
    [scrollView addSubview:restaurantNameLabel];

    priceLabel = [self createLabel];
    [scrollView addSubview:priceLabel];
    
    formattedAddressLabel = [self createLabel];
    formattedAddressLabel.numberOfLines = 0;
    [scrollView addSubview:formattedAddressLabel];
    
    addressTagLabel = [self createLabel];
    [scrollView addSubview:addressTagLabel];
    
    if ([self.orderInfo.orderRecipient._id isEqualToString:[UserProfile currentUser]._id] && (self.orderInfo.status == OrderDelivered)) {
    
        UIView *ratingViewBackground = [UIView new];
        ratingViewBackground.translatesAutoresizingMaskIntoConstraints = NO;
        ratingViewBackground.layer.cornerRadius = 30.0f;
        ratingViewBackground.layer.borderColor = [UIColor colorWithWhite:0.95 alpha:1.0f].CGColor;
        ratingViewBackground.layer.borderWidth = 0.5f;
        ratingViewBackground.backgroundColor = [UIColor whiteColor];
        [scrollView addSubview:ratingViewBackground];
        
        _ratingView = [[OIHItemRatingView alloc] init];
        _ratingView.translatesAutoresizingMaskIntoConstraints = NO;
        _ratingView.delegate = self;
        [ratingViewBackground addSubview:_ratingView];
        
        [ratingViewBackground addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_ratingView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_ratingView)]];
        
        [ratingViewBackground addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_ratingView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_ratingView)]];
        
        
        [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[ratingViewBackground]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(ratingViewBackground)]];

    
        [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[imageView(==imageViewHeight)]-20-[ratingViewBackground(==60)]-20-[mealNameLabel]-[orderQuantityLabel]-[priceLabel]-[restaurantNameLabel]-20-[orderStatusLabel]-(>=20)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"imageViewHeight": @(150.0f)} views:NSDictionaryOfVariableBindings(imageView, mealNameLabel, restaurantNameLabel, orderQuantityLabel, formattedAddressLabel, addressTagLabel, orderStatusLabel, priceLabel, orderIdLabel, orderDateLabel, orderUserLabel, ratingViewBackground)]];

    }else{
        [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[imageView(==imageViewHeight)]-40-[mealNameLabel]-[orderQuantityLabel]-[priceLabel]-[restaurantNameLabel]-20-[orderStatusLabel]-(>=20)-|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"imageViewHeight": @(150.0f)} views:NSDictionaryOfVariableBindings(imageView, mealNameLabel, restaurantNameLabel, orderQuantityLabel, formattedAddressLabel, addressTagLabel, orderStatusLabel, priceLabel, orderIdLabel, orderDateLabel, orderUserLabel)]];

    }

    
    [scrollView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0f]];
    [scrollView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:scrollView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];

    
}

-(void)setLabelText{
    
    restaurantNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"SOLD_BY_%@", @"Sold by [restaurant]"), orderInfo.restaurant.restaurantName];
    
    NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
    [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currency setLocale:[NSLocale currentLocale]];
    [currency setCurrencyCode:orderInfo.restaurant.restaurantCurrency];
    
    priceLabel.text = [NSString stringWithFormat:NSLocalizedString(@"PRICE_%@", @"Total Price: [price]"), [currency stringFromNumber: orderInfo.orderReceipt.totalAmount]];
    
    orderQuantityLabel.text = [NSString stringWithFormat:NSLocalizedString(@"QUANTITY_%@", @"Quantity: [quantity]"), [@(orderInfo.quantity) stringValue]];
 
    orderStatusLabel.text = [orderInfo orderStatus];
    
}

#pragma mark - OIHRatingContentViewDelegate

-(void)ratingViewDidChooseEmoji: (NSInteger) emoji{
    
    if (_review._id) {

        _review.rating = emoji;
        [_review save:nil];

    }else{
        
        _ratingView.userInteractionEnabled = NO;

        NSDictionary *data = @{@"rating":@(emoji), @"orderId": orderInfo._id};
    
        JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"ordercomment" httpMethod:HTTP_POST];
        setting.requestData = data;
        setting.cachePolicy = IgnoreCache;
        
        [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            if (!error) {
                _review = [[OIHActivityFeed alloc] initWithActivity:data];
                _review.rating = emoji;
                _ratingView.userInteractionEnabled = YES;
            }
        }];

    }
    
}


-(void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;

    if ([self.orderInfo.orderRecipient._id isEqualToString:[UserProfile currentUser]._id] && (self.orderInfo.status == OrderDelivered || self.orderInfo.status == OrderCanceled)) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"REVIEW_POST_A_REVIEW", @"") style:UIBarButtonItemStylePlain target:self action:@selector(postComment)];
    }

    
    [self viewWithTableView];
//    [self sizeHeaderToFit];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textSizeChanged) name:UIContentSizeCategoryDidChangeNotification object:nil];
    [imageView sd_setImageWithURL:[NSURL URLWithString:orderInfo.orderMeal.productImage] placeholderImage:[UIImage imageFromColor:[UIColor lightGrayColor]]];


}

-(void)textSizeChanged{
    [_tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OIHLeftRightTextTableViewCell *cell = (OIHLeftRightTextTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    
    switch (indexPath.row) {
        case 0:
            cell.leftLabel.text = NSLocalizedString(@"ITEM_ITEM_NAME", @"Item name");
            cell.rightLabel.text = orderInfo.orderMeal.name;
            break;
        case 1:
            cell.rightLabel.text = orderInfo.restaurant.restaurantName;
            cell.leftLabel.text = NSLocalizedString(@"ITEM_RESTAURANT", @"Seller");
            break;
        case 2:{

            NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
            [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
            [currency setLocale:[NSLocale currentLocale]];
            [currency setCurrencyCode:orderInfo.restaurant.restaurantCurrency];

            cell.leftLabel.text = NSLocalizedString(@"ITEM_PRICE", @"Price");
            cell.rightLabel.text = [currency stringFromNumber: orderInfo.orderReceipt.totalAmount];
            break;
        }
        case 3:
            cell.leftLabel.text = NSLocalizedString(@"ITEM_QUANTITY", @"Quantity");
            cell.rightLabel.text = [@(orderInfo.quantity) stringValue];
            break;
            
        case 4:
            cell.leftLabel.text = NSLocalizedString(@"ORDER_STATE", @"State");
            cell.rightLabel.attributedText = [[NSAttributedString alloc] initWithString:[orderInfo orderStatus] attributes:@{NSForegroundColorAttributeName: (id)[orderInfo statusColor]}];
            break;
        case 5:
            
            cell.leftLabel.text = NSLocalizedString(@"ITEM_DELIVERY_ADDRESS", @"Address");
            
            if (![orderInfo.orderRecipient._id isEqualToString:[UserProfile currentUser]._id]) {
                cell.rightLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_DEFAULT_ADDRESS", @"PERSON's default address"), orderInfo.orderRecipient.userName];
            }else{
                cell.rightLabel.text = orderInfo.recipientAddress.userEnteredAddress;
            }
            break;
        default:
            break;
    }
    
    return cell;
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(void)postComment{
    
    OIHUserPostOrderActivityViewController *vc = [[OIHUserPostOrderActivityViewController alloc] init];
    vc.order = self.orderInfo;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(UIImageView *)createImageView{
    
    UIImageView *iV = [UIImageView new];
    iV.contentMode = UIViewContentModeScaleAspectFill;
    iV.layer.cornerRadius = 75.0f;
    iV.layer.borderWidth = 3.0f;
    iV.layer.masksToBounds = YES;
    iV.layer.borderColor = [UIColor whiteColor].CGColor;
    iV.translatesAutoresizingMaskIntoConstraints = NO;

    return iV;
}

-(UILabel *)createLabel{
    
    UILabel *label = [UILabel new];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    
    return label;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
