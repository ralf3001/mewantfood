//
//  OIHMealInfoViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHMealInfoViewController.h"
#import "OIHMealInfoTableViewCell.h"
#import "OIHUserAddressListViewController.h"
#import "OIHStepperTableViewCell.h"
#import "OIHAnimatedTextTableViewCell.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "OIHUserOrderConfirmedViewController.h"
#import "AppDelegate.h"
#import "OIHRestaurantInfoViewController.h"

#import "BraintreeUI.h"
#import "BraintreeApplePay.h"
#import "OIHSettingTableViewCell.h"

@import PassKit;

#import "OIHOrderModel.h"
#import <AddressBook/ABPerson.h>

@interface OIHMealInfoViewController () <UITableViewDataSource, UITableViewDelegate, BTDropInViewControllerDelegate, OIHUserAddressViewControllerDelegate, OIHSteppTableViewCellDelegate, PKPaymentAuthorizationViewControllerDelegate>

@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UIButton *confirmButton;
@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSArray <OIHAddressModel *> *addressList;
@property (nonatomic) OIHAddressModel *selectedAddress;
@property (nonatomic, strong) BTAPIClient *braintreeClient;
@property (nonatomic) NSInteger orderQuantity;

@end

@implementation OIHMealInfoViewController
@synthesize tableView = _tableView;
@synthesize confirmButton;
@synthesize addressList;
@synthesize friendProfile;
@synthesize scrollView;
@synthesize selectedAddress;
@synthesize orderQuantity;

//-(id)initWithMeal: (OIHMealModel *)meal{
//    
//    self = [super init];
//    if (self) {
//        self.meal = restaurant;
//    }
//    return self;
//    
//}


-(id)initWithResults: (OIHSearchResultsModel *)searchResults{

    if (self = [super init]) {
        
        _searchResults = searchResults;
    
    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    scrollView.backgroundColor = _tableView.backgroundColor;
    
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 80;
    [_tableView registerClass:[OIHMealInfoTableViewCell class] forCellReuseIdentifier:@"cell"];
    [_tableView registerClass:[OIHStepperTableViewCell class] forCellReuseIdentifier:@"stepper"];
    [_tableView registerClass:[OIHAnimatedTextTableViewCell class] forCellReuseIdentifier:@"counting"];
    [_tableView registerClass:[OIHSettingTableViewCell class] forCellReuseIdentifier:@"card"];
    
    UIView *footerView;
    UIButton *doneButton;
    
    UIButton *addToCart = [UIButton buttonWithType:UIButtonTypeSystem];
    addToCart.translatesAutoresizingMaskIntoConstraints = NO;
    [addToCart setTitle:NSLocalizedString(@"ADD_TO_CART", @"Add to cart") forState:UIControlStateNormal];
    [addToCart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addToCart addTarget:self action:@selector(addToCartButton) forControlEvents:UIControlEventTouchUpInside];
    addToCart.backgroundColor = [UIColor colorWithRed:225/256.0f green:90/256.0f blue:90/256.0f alpha:1.0f];
    addToCart.layer.cornerRadius = 3.0f;
    
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 50)];
    [footerView addSubview:addToCart];
    
    if ([PKPaymentAuthorizationViewController canMakePaymentsUsingNetworks:@[PKPaymentNetworkVisa, PKPaymentNetworkMasterCard, PKPaymentNetworkAmex, PKPaymentNetworkChinaUnionPay]]) {
        
        doneButton = [self createApplePayButton];
        
    }else{
        
        doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
        doneButton.layer.cornerRadius = 3.0f;
        [doneButton setTitle:NSLocalizedString(@"PAY", @"") forState:UIControlStateNormal];
        doneButton.backgroundColor = [UIColor colorWithRed:46/256.0f green:65/256.0f blue:114/256.0f alpha:1.0f];
        [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [doneButton addTarget:self action:@selector(payButton) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    [footerView addSubview:doneButton];
    
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[addToCart]-[doneButton(==addToCart)]-|" options:NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(addToCart, doneButton)]];
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[doneButton]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(doneButton)]];
    
    
    _tableView.tableFooterView = footerView;
    
    [self.view addSubview:_tableView];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    
    orderQuantity = 1;
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self fetchClientToken];
    [self fetchAddresses];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    switch (indexPath.row) {
        case 0:{
            
            OIHMealInfoTableViewCell *cell = (OIHMealInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            cell.descriptionContentLabel.text = _searchResults.purchaseItem.restaurant.restaurantName;
            cell.descriptionTitleLabel.text = NSLocalizedString(@"ITEM_RESTAURANT", @"Restaurant Name");
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            return cell;
        }
        case 1:{
            
            OIHMealInfoTableViewCell *cell = (OIHMealInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            cell.descriptionContentLabel.text = _searchResults.purchaseItem.name;
            cell.descriptionTitleLabel.text = NSLocalizedString(@"ITEM_ITEM_NAME", @"Item Name");
            return cell;
            
        }
        case 2:{
            
            OIHAnimatedTextTableViewCell *cell = (OIHAnimatedTextTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"counting" forIndexPath:indexPath];
            
            NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: _searchResults.purchaseItem.restaurant.restaurantCurrency}];
            
            
            NSNumberFormatter *currency = [[NSNumberFormatter alloc] init];
            [currency setNumberStyle:NSNumberFormatterCurrencyStyle];
            [currency setLocale:[NSLocale localeWithLocaleIdentifier:localeIde]];
            
            cell.descriptionContentLabel.animateLoop = ^NSString*(float value){
                
                return [currency stringFromNumber: [NSNumber numberWithDouble: value]];
            };
            
            
            NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:_searchResults.purchaseItem.priceInString locale:[NSLocale localeWithLocaleIdentifier:localeIde]];
            NSDecimalNumber *surcharge = [NSDecimalNumber decimalNumberWithDecimal:[_searchResults.purchaseItem.chargeRate decimalValue]];
            
            NSDecimalNumber *total = [[price decimalNumberByMultiplyingBy:surcharge] decimalNumberByMultiplyingBy:(NSDecimalNumber *)[NSDecimalNumber numberWithInteger:orderQuantity]];
            
            [cell.descriptionContentLabel startCounter:0 end:total.floatValue];
            
            cell.descriptionTitleLabel.text = NSLocalizedString(@"ITEM_PRICE", @"Item Price");
            return cell;
        }
        case 3:{
            
            OIHMealInfoTableViewCell *cell = (OIHMealInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            
            cell.descriptionTitleLabel.text = NSLocalizedString(@"ITEM_DELIVERY_ADDRESS", @"Item delivery address");
            
            if (!friendProfile || [friendProfile._id isEqualToString:[UserProfile currentUser]._id]) {
                
                if (selectedAddress) {
                    cell.descriptionContentLabel.text = selectedAddress.userEnteredAddress;
                }else{
                    cell.descriptionContentLabel.text = NSLocalizedString(@"USER_HAS_NO_ADDRESS", @"");
                }
                
            }else{
                cell.descriptionContentLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_DEFAULT_ADDRESS", @"PERSON's default address"), friendProfile.userName];
                
            }
            return cell;
            
        }
        case 4:{
            OIHStepperTableViewCell *cell = (OIHStepperTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"stepper" forIndexPath:indexPath];
            cell.cellDescriptionLabel.text = NSLocalizedString(@"ITEM_QUANTITY", @"Quantity");
            cell.delegate = self;
            
            return cell;
            
        }
        case 5:{
            
            OIHSettingTableViewCell *cell = (OIHSettingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"card" forIndexPath:indexPath];
            cell.imageView.image = nil;
            cell.textLabel.text = @"";
            
            return cell;
            
        }
        default:
            break;
    }
    
    return nil;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:{
            
            OIHRestaurantInfoViewController *vc = [[OIHRestaurantInfoViewController alloc] initWithRestaurantId: _searchResults.purchaseItem.restaurant._id];
            [self.navigationController pushViewController:vc animated:YES];
            
            break;
        }
        case 3:{
            if (!friendProfile) {
                
                OIHUserAddressListViewController *vc = [OIHUserAddressListViewController new];
                vc.delegate = self;
                [self.navigationController pushViewController:vc animated:YES];
            }
            break;
        }
        default:
            break;
    }
    
    
    
}

-(PKPaymentButton *)createApplePayButton{
    PKPaymentButton *button = [PKPaymentButton buttonWithType:PKPaymentButtonTypeBuy style:PKPaymentButtonStyleWhite];
    [button addTarget:self action:@selector(tappedApplePay) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

-(void)addToCartButton{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"cart" httpMethod:HTTP_POST];
    setting.requestData = @{@"meal": _searchResults.purchaseItem._id};
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            NSLog(@"Added to cart");
        }else{
            NSLog(@"[Error]: failed to add to cart");
        }
    }];
    
    
}

-(void)tappedApplePay{
    
    PKPaymentRequest *paymentRequest = [self paymentRequest];
    PKPaymentAuthorizationViewController *vc = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:paymentRequest];
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (PKPaymentRequest *)paymentRequest {
    
    PKPaymentRequest *paymentRequest = [[PKPaymentRequest alloc] init];
    paymentRequest.merchantIdentifier = @"merchant.com.ralfcheung.shanon-sandbox";
    paymentRequest.supportedNetworks = @[PKPaymentNetworkAmex, PKPaymentNetworkVisa, PKPaymentNetworkMasterCard];
    paymentRequest.merchantCapabilities = PKMerchantCapability3DS;
    paymentRequest.countryCode = @"HK"; // e.g. US
    
    paymentRequest.currencyCode = _searchResults.purchaseItem.restaurant.restaurantCurrency; // e.g. USD
    
    NSString *currencyCode = _searchResults.purchaseItem.restaurant.restaurantCurrency;
    
    NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: currencyCode}];
    
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:_searchResults.purchaseItem.priceInString locale:[NSLocale localeWithLocaleIdentifier:localeIde]];
    
    NSDecimalNumber *surcharge = [NSDecimalNumber decimalNumberWithDecimal:[_searchResults.purchaseItem.chargeRate decimalValue]];
    NSDecimalNumber *total = [[price decimalNumberByMultiplyingBy:surcharge] decimalNumberByMultiplyingBy:(NSDecimalNumber *)[NSDecimalNumber numberWithInteger:orderQuantity]];
    
    NSMutableArray *paymentSummaryItems = [NSMutableArray new];
    [paymentSummaryItems addObject:[PKPaymentSummaryItem summaryItemWithLabel:_searchResults.purchaseItem.name amount:total type:PKPaymentSummaryItemTypeFinal]];
    paymentRequest.paymentSummaryItems = paymentSummaryItems;
    
    paymentRequest.shippingContact = [self createShippingContact:friendProfile ? friendProfile._id : [UserProfile currentUser]._id withPaymentRequest:paymentRequest];
    
    return paymentRequest;
}

-(PKContact *)createShippingContact: (NSString *)userId withPaymentRequest: (PKPaymentRequest *)paymentRequest{
    
    PKContact *contact = [[PKContact alloc] init];
    
    if ([userId isEqualToString:[UserProfile currentUser]._id]) {
        
        paymentRequest.requiredShippingAddressFields = PKAddressFieldPostalAddress;
        
        NSPersonNameComponents *name = [[NSPersonNameComponents alloc] init];
        name.givenName = [UserProfile currentUser].name;
        name.familyName = [UserProfile currentUser].lastName;
        contact.name = name;
        contact.emailAddress = [UserProfile currentUser].email;
        
        if (selectedAddress) {
            
            CNMutablePostalAddress *address = [[CNMutablePostalAddress alloc] init];
            
            address.street = selectedAddress.userEnteredAddress;
            address.city = selectedAddress.city;
            address.postalCode = selectedAddress.postalCode;
            address.country = selectedAddress.country;
            address.ISOCountryCode = selectedAddress.ISOCountryCode;
            
            contact.postalAddress = address;
        }
        
        
    }else{
        
        NSPersonNameComponents *name = [[NSPersonNameComponents alloc] init];
        name.givenName = friendProfile.userName;
        name.familyName = friendProfile.userLastName;
        contact.emailAddress = friendProfile.email;
        
    }
    
    return contact;
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus))completion {
    
    // Example: Tokenize the Apple Pay payment
    BTApplePayClient *applePayClient = [[BTApplePayClient alloc]
                                        initWithAPIClient:self.braintreeClient];
    
    [applePayClient tokenizeApplePayPayment:payment
                                 completion:^(BTApplePayCardNonce *tokenizedApplePayPayment,
                                              NSError *error) {
                                     if (tokenizedApplePayPayment) {
                                         // On success, send nonce to your server for processing.
                                         // If applicable, address information is accessible in `payment`.
                                         NSLog(@"nonce = %@", tokenizedApplePayPayment.nonce);
                                         [controller dismissViewControllerAnimated:YES completion:nil];
                                         [self sendPaymentToSeverWithPaymentNounce:tokenizedApplePayPayment];
                                         
                                         // Then indicate success or failure via the completion callback, e.g.
                                         completion(PKPaymentAuthorizationStatusSuccess);
                                     } else {
                                         // Tokenization failed. Check `error` for the cause of the failure.
                                         [controller dismissViewControllerAnimated:YES completion:nil];
                                         
                                         [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
                                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                             [SVProgressHUD dismiss];
                                         });
                                         
                                         // Indicate failure via the completion callback:
                                         completion(PKPaymentAuthorizationStatusFailure);
                                     }
                                 }];
}

// Be sure to implement -paymentAuthorizationViewControllerDidFinish:
- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)payButton{
    
    
    if ([self hasAddress]) {
        BTDropInViewController *dropInViewController = [[BTDropInViewController alloc]
                                                        initWithAPIClient:self.braintreeClient];
        
        dropInViewController.delegate = self;
        [dropInViewController fetchPaymentMethodsOnCompletion:^{
            
        }];
        
        UIBarButtonItem *item = [[UIBarButtonItem alloc]
                                 initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                 target:self
                                 action:@selector(userDidCancelPayment)];
        dropInViewController.navigationItem.leftBarButtonItem = item;
        UINavigationController *navigationController = [[UINavigationController alloc]
                                                        initWithRootViewController:dropInViewController];
        [self presentViewController:navigationController animated:YES completion:nil];
        
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ERROR_BOOM", @"Ouch")  message:NSLocalizedString(@"USER_NO_ADDRESS", @"") preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}


- (void)dropInViewControllerDidCancel:(BTDropInViewController *)viewController{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}



- (void)dropInViewController:(BTDropInViewController *)viewController didSucceedWithTokenization:(BTPaymentMethodNonce *)paymentMethodNonce{
    [viewController dismissViewControllerAnimated:YES completion:nil];
    [self sendPaymentToSeverWithPaymentNounce:paymentMethodNonce];
}


-(void)sendPaymentToSeverWithPaymentNounce: (BTPaymentMethodNonce *)paymentMethodNonce{
    
    [SVProgressHUD showWithStatus:@"Placing your order..."];
    
    NSMutableDictionary *postData = [NSMutableDictionary new];
    
    postData[@"paymentNounce"] = paymentMethodNonce.nonce;
    postData[@"restaurant"] = _searchResults.purchaseItem.restaurant._id;
    postData[@"mealId" ] = _searchResults.purchaseItem._id;
    postData[@"currency"] = _searchResults.purchaseItem.restaurant.restaurantCurrency;
    postData[@"amount"] = _searchResults.purchaseItem.priceInLocalCurrency;
    postData[@"quantity"] = [NSNumber numberWithInteger:orderQuantity];
    postData[@"addressId"] = selectedAddress._id;
    postData[@"searchId"] = _searchResults._id;
    
    
    postData[@"totalChargeInString"] = [self convertPriceToString:_searchResults.purchaseItem.priceInString withSurcharge:_searchResults.purchaseItem.chargeRate quantity:orderQuantity currency:_searchResults.purchaseItem.restaurant.restaurantCurrency];
    
    if (friendProfile) {
        postData[@"friend"] = friendProfile._id;
    }
    
    __weak typeof(self) weakSelf = self;
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"order" httpMethod:HTTP_POST];
    setting.requestData = postData;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (error) {
            
            [SVProgressHUD showErrorWithStatus:@"Ouch, something's gone wrong, try again later"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
        }else{
            
            [weakSelf.navigationController popToRootViewControllerAnimated:NO];
                        
            OIHUserOrderConfirmedViewController *confirmVC = [[OIHUserOrderConfirmedViewController alloc] initWithOrder:[[OIHOrderModel alloc] initWithOrderInfo:data]];
            confirmVC.friendProfile = friendProfile;
            confirmVC.mealInfo = _searchResults.purchaseItem;
            confirmVC.restaurantInfo = _searchResults.purchaseItem.restaurant;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:OIHTabBarWillSelectTabAtIndexNotification object:@{@"tab": @(0)}];
            [[NSNotificationCenter defaultCenter] postNotificationName:OIHViewControllerPresentModalViewControllerNotification object:confirmVC];
        }
        
    }];
    
}


-(void)userDidCancelPayment{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)fetchAddresses{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"address" httpMethod:HTTP_GET];
    
    
    if (friendProfile) {
        setting.requestData = @{@"userId": friendProfile._id};
    }else{
        setting.requestData = @{@"userId": [UserProfile currentUser]._id};
    }
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            
            addressList = [OIHAddressModel addressFromArray:data];
            if (addressList.count > 0) {
                selectedAddress = addressList[0];
                [_tableView reloadData];
            }else{
                confirmButton.enabled = NO;
            }
        }else {
            NSLog(@"%@", [error localizedDescription]);
        }
        
    }];
    
    
}



-(BOOL)hasAddress{
    
    return addressList.count > 0;
    
}


-(void)fetchClientToken{
    
    [[JSONRequest sharedInstance] fetch:@"client_token" method:@"GET" data:nil completionHandler:^(id data, NSError *error) {
        if (!error) {
            self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:data[@"clientToken"]];
        }
    }];
    
}


-(void)stepperValueDidChange:(UIStepper *)stepper{
    
    orderQuantity = (long)stepper.value;
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}


-(double) customRounding: (double) value {
    
    const float roundingValue = 0.1;
    int mulitpler = floor(value / roundingValue);
    
    return mulitpler * roundingValue;
    
}


#pragma mark - OIHUserAddressViewControllerDelegate

-(void)userDidSelectAddress:(OIHAddressModel *)address{
    
    selectedAddress = address;
    [_tableView reloadData];
    
}


#pragma mark - Local Authentication

-(void)confirm{
    
    
    if ([[LAContext new] canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil]) {
        [self authenticateWithTouchID];
        
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_PAYMENT_CONFIRMATION", @"") message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = NSLocalizedString(@"PASSWORD", @"");
            textField.secureTextEntry = YES;
            
        }];
        
    }
    
}


- (void)authenticateWithTouchID{
    
    NSDictionary *query = @{
                            (__bridge id)kSecClass: (__bridge id)kSecClassGenericPassword,
                            (__bridge id)kSecAttrService: NSLocalizedString(@"USER_PAYMENT_CONFIRMATION", @"User Confirmation"),
                            (__bridge id)kSecReturnData: @YES,
                            (__bridge id)kSecUseOperationPrompt: NSLocalizedString(@"TOUCH_ID_CONFIRM_IDENTITY", nil)
                            };
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        CFTypeRef dataTypeRef = NULL;
        
        OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)(query), &dataTypeRef);
        
        NSData *resultData = (__bridge NSData *)dataTypeRef;
        NSString* result = [[NSString alloc] initWithData:resultData encoding:NSUTF8StringEncoding];
        NSString *msg = [NSString stringWithFormat:NSLocalizedString(@"SEC_ITEM_COPY_MATCHING_STATUS", nil), [self keychainErrorToString:status]];
        if (resultData)
            msg = [msg stringByAppendingString:[NSString stringWithFormat:NSLocalizedString(@"RESULT", nil), result]];
        
        if (status == noErr) {
            //            [self.delegate passwordRetrieved:result];
        }else{
            //            [self.delegate passwordRetrieved:nil];
        }
        
    });
}

- (NSString *)keychainErrorToString: (NSInteger)error{
    
    
    NSString *msg = [NSString stringWithFormat:@"%ld",(long)error];
    
    switch (error) {
        case errSecSuccess:
            msg = NSLocalizedString(@"SUCCESS", nil);
            break;
        case errSecDuplicateItem:
            msg = NSLocalizedString(@"ERROR_ITEM_ALREADY_EXISTS", nil);
            break;
        case errSecItemNotFound :
            msg = NSLocalizedString(@"ERROR_ITEM_NOT_FOUND", nil);
            break;
        case -26276: // this error will be replaced by errSecAuthFailed
            msg = NSLocalizedString(@"ERROR_ITEM_AUTHENTICATION_FAILED", nil);
            
        default:
            break;
    }
    
    return msg;
}


-(NSString *)convertPriceToString: (NSString *)price withSurcharge: (NSNumber *)surcharge quantity: (NSInteger)quantity currency: (NSString *)currency{
    
    NSString * localeIde = [NSLocale localeIdentifierFromComponents:@{NSLocaleCurrencyCode: currency}];
    NSDecimalNumber *priceInNumber = [NSDecimalNumber decimalNumberWithString:price locale:[NSLocale localeWithLocaleIdentifier:localeIde]];
    
    double charge = 1.0f;
    if ([surcharge doubleValue] > 1.0f) {
        charge = [surcharge doubleValue];
    }
    
    NSDecimalNumber *surchargeNumber = (NSDecimalNumber *)[NSDecimalNumber numberWithDouble:charge];
    
    NSDecimalNumber *total = [[priceInNumber decimalNumberByMultiplyingBy:surchargeNumber] decimalNumberByMultiplyingBy:
                              (NSDecimalNumber *)[NSDecimalNumber numberWithInteger:orderQuantity]];
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setMinimumFractionDigits:2];
    [nf setMaximumFractionDigits:2];
    
    return [nf stringFromNumber:total];
    
}


/*
 -(void)payButton{
 
 [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
 
 
 NSLocale* locale = [NSLocale currentLocale];
 NSString* identifier = locale.localeIdentifier;
 NSDictionary* components = [NSLocale componentsFromLocaleIdentifier:identifier];
 
 
 payPalConfiguration = [[PayPalConfiguration alloc] init];
 payPalConfiguration.languageOrLocale = [NSString stringWithFormat:@"%@-%@", [components valueForKey:NSLocaleLanguageCode], [components valueForKey:NSLocaleScriptCode]];
 payPalConfiguration.acceptCreditCards = YES;
 payPalConfiguration.rememberUser = YES;
 payPalConfiguration.alwaysDisplayCurrencyCodes = YES;
 
 
 
 // Or if you wish to have the user choose a Shipping Address from those already
 // associated with the user's PayPal account, then add:
 //payPalConfiguration.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
 
 
 
 PayPalPayment *payment = [[PayPalPayment alloc] init];
 
 // Amount, currency, and description
 payment.amount = [NSDecimalNumber decimalNumberWithDecimal:[mealInfo[kOIHMealPriceInLocalCurrency] decimalValue]];
 payment.currencyCode = mealInfo[kOIHMealRestaurant][kOIHRestaurantCurrency];
 NSString *name = mealInfo[kOIHMealName];
 const char *translated = [name cStringUsingEncoding:NSUTF8StringEncoding];
 payment.shortDescription = [NSString stringWithCString:translated encoding:NSUTF8StringEncoding];
 
 payment.intent = PayPalPaymentIntentSale;
 
 // If your app collects Shipping Address information from the customer,
 // or already stores that information on your server, you may provide it here.
 
 // Several other optional fields that you can set here are documented in PayPalPayment.h,
 // including paymentDetails, items, invoiceNumber, custom, softDescriptor, etc.
 
 // Check whether payment is processable.
 if (!payment.processable) {
 // If, for example, the amount was negative or the shortDescription was empty, then
 // this payment would not be processable. You would want to handle that here.
 }
 
 payPalConfiguration.merchantName = @"Ultramagnetic Omega Supreme";
 payPalConfiguration.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.omega.supreme.example/privacy"];
 payPalConfiguration.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.omega.supreme.example/user_agreement"];
 
 
 //    PayPalFuturePaymentViewController *fpViewController;
 //    fpViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfiguration
 //                                                                               delegate:self];
 //
 //    // Present the PayPalFuturePaymentViewController
 //    [self presentViewController:fpViewController animated:YES completion:nil];
 
 
 
 PayPalPaymentViewController *paymentViewController;
 paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
 configuration:self.payPalConfiguration
 delegate:self];
 
 // Present the PayPalPaymentViewController.
 [self presentViewController:paymentViewController animated:YES completion:nil];
 
 
 
 }
 
 */




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
