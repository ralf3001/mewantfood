//
//  AppDelegate.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "AFMInfoBanner.h"
#import "AppDelegate.h"
#import "BraintreeCore.h"
#import "OIHAddMealViewController.h"
#import "OIHAddRestaurantViewController.h"
#import "OIHLoginViewController.h"
#import "OIHRestaurantListViewController.h"
#import "OIHRestaurantManagementStatisticsViewController.h"
#import "OIHSignUpViewController.h"
#import "OIHUserFriendsListTableViewController.h"
#import "OIHUserHomeScreenViewController.h"
#import "OIHUserRestaurantListTableViewController.h"
#import "OIHUserRestaurantOrdersViewController.h"
#import "OIHUserSettingsViewController.h"
#import "Reachability.h"
#import <AWSCognito/AWSCognito.h>
#import <AWSCore/AWSCore.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <TwitterKit/TwitterKit.h>
#import <Fabric/Fabric.h>
#import "OIHMealModel.h"

#import "OIHRestaurantMealListViewController.h"
#import "OIHSalesGraphViewController.h"
#import "OIHSignUpIntroductionViewController.h"
#import "OIHUserAddressListViewController.h"
#import "OIHUserOrderConfirmedViewController.h"
#import "OIHSignUpFacebookLoginViewController.h"
#import "OIHUserFavouritesViewController.h"

#import "TestViewController.h"
#import "OIHPopUpNoticeViewController.h"


@interface OIHAppModule : JSObjectionModule{
    
}

@end

@implementation OIHAppModule

-(void)configure{
    [self bind:[UIApplication sharedApplication] toClass:[UIApplication class]];
    [self bind:[UIApplication sharedApplication].delegate toProtocol:@protocol(UIApplicationDelegate)];
    
}

@end


@interface AppDelegate () <LoginViewProtocol, SignupViewProtocol,
OIHIntroDelegate>
@property(nonatomic) OIHSignUpIntroductionViewController *intro;
@property(nonatomic) OIHLoginViewController *loginVC;

@end

@implementation AppDelegate
@synthesize tabBar = _tabBarController;
@synthesize intro;



- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //    OIHRestaurantInfoViewController *vc = [[OIHRestaurantInfoViewController alloc] initWithRestaurantId:@"567133db47571f8f664cf58b"];
    
    //    OIHPopUpNoticeViewController *vc = [[OIHPopUpNoticeViewController alloc] initWithTitle:[LoremIpsum wordsWithNumber:1] description:[LoremIpsum wordsWithNumber:10]];
    //    vc.buttonTitle = NSLocalizedString(@"OK", @"");
    //
    //    [vc setButtonAction:^(OIHPopUpNoticeViewController *vc){
    //        NSLog(@"boo");
    //    }];
    
    //    self.window.rootViewController = vc;
    
    JSObjectionInjector *injector = [JSObjection createInjector:[[OIHAppModule alloc] init]];
    [JSObjection setDefaultInjector:injector];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    
    [[EinerUserTracking sharedInstance] generateCrashAnalyticReport];
    
    [self monitorReachability];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    NSString *bundleId = [[NSBundle mainBundle] bundleIdentifier];
    
    [FBSDKSettings setAppID:OIHFacebookAppId];
    
    [BTAppSwitch setReturnURLScheme:[NSString stringWithFormat:@"%@.payments", bundleId]];
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    
    UIViewController *dummy = [UIViewController new];
    dummy.view.backgroundColor = [UIColor whiteColor];
    [self setKeyWindowViewController:dummy];
    
    
    if ([[UserProfile currentUser] getProfile]) {
        
        [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
        [Fabric with:@[[Twitter class]]];
        [self setupConsumerTabBar:notification];
        
        [self.window.rootViewController presentViewController:_tabBarController
                                                     animated:NO
                                                   completion:nil];
        //[[EinerUserTracking sharedInstance] addTabBarControllerToMonitor:_tabBarController];
        
    } else {
        
        _loginVC = [OIHLoginViewController new];
        _loginVC.delegate = self;
        _loginVC.signupViewController.delegate = self;
        [self.window.rootViewController presentViewController:_loginVC
                                                     animated:NO
                                                   completion:nil];
    }
    
    
    
    return YES;
}


- (OIHTabBarViewController *)setupConsumerTabBar:(NSDictionary *)notification {
    
    [OIHLocationManager sharedInstance];
    
    _tabBarController = [[OIHTabBarViewController alloc] init];
    
    OIHUserHomeScreenViewController *feed = [[OIHUserHomeScreenViewController alloc] init];
    feed.title = NSLocalizedString(@"TAB_BAR_FEED", @"");
    feed.tabBarItem.image = [IonIcons imageWithIcon:ion_ios_list_outline
                                               size:49.0f
                                              color:[UIColor whiteColor]];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:feed];
    nav.navigationBar.barTintColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
    
    OIHUserFriendsListTableViewController *friends = [[OIHUserFriendsListTableViewController alloc] init];
    friends.tabBarItem.image = [IonIcons imageWithIcon:ion_ios_people_outline
                                                  size:49.0f
                                                 color:[UIColor whiteColor]];
    friends.title = NSLocalizedString(@"TAB_BAR_FRIENDS", @"");
    UINavigationController *friendsNav = [[UINavigationController alloc] initWithRootViewController:friends];
    friendsNav.navigationBar.barTintColor =
    [UIColor colorWithWhite:0.9 alpha:1.0f];
    
    OIHUserSettingsViewController *userProfile = [[OIHUserSettingsViewController alloc] init];
    userProfile.tabBarItem.image = [IonIcons imageWithIcon:ion_ios_gear_outline
                                                      size:49.0f
                                                     color:[UIColor whiteColor]];
    userProfile.title = NSLocalizedString(@"TAB_BAR_SETTINGS", @"");
    UINavigationController *profile = [[UINavigationController alloc] initWithRootViewController:userProfile];
    profile.navigationBar.barTintColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
    
    OIHRestaurantListViewController *restaurants = [OIHRestaurantListViewController new];
    restaurants.tabBarItem.image = [IonIcons imageWithIcon:ion_ios_navigate_outline
                                                      size:49.0f
                                                     color:[UIColor whiteColor]];
    UINavigationController *restaurantsNav = [[UINavigationController alloc] initWithRootViewController:restaurants];
    restaurantsNav.navigationBar.barTintColor =
    [UIColor colorWithWhite:0.9 alpha:1.0f];
    
    
    OIHUserFavouritesViewController *userFavorites = [OIHUserFavouritesViewController new];
    userFavorites.tabBarItem.image = [IonIcons imageWithIcon:ion_ios_heart_outline size:49.0f color:[UIColor whiteColor]];
    UINavigationController *favoritesNav = [[UINavigationController alloc] initWithRootViewController:userFavorites];
    favoritesNav.navigationBar.barTintColor = [UIColor highlightColor];
    
    
    _tabBarController.viewControllers = [NSArray arrayWithObjects:nav, friendsNav, restaurantsNav, favoritesNav, profile, nil];
    _tabBarController.edgesForExtendedLayout = UIRectEdgeNone;
    
    if (notification) {
        [self notificationAction:notification];
    }
    
    return _tabBarController;
}

- (OIHTabBarViewController *)setupMerchantTabBar:(NSDictionary *)notification {
    
    _tabBarController = [[OIHTabBarViewController alloc] init];
    
    OIHUserRestaurantListTableViewController *vc = [OIHUserRestaurantListTableViewController new];
    vc.title = NSLocalizedString(@"TAB_BAR_MY_RESTAURANTS", @"My restaurants");
    vc.tabBarItem.image = [IonIcons imageWithIcon:ion_ios_home_outline
                                             size:49.0f
                                            color:[UIColor whiteColor]];
    UINavigationController *nav =
    [[UINavigationController alloc] initWithRootViewController:vc];
    
    OIHUserRestaurantOrdersViewController *ordersList = [OIHUserRestaurantOrdersViewController new];
    ordersList.title = NSLocalizedString(@"TAB_BAR_RECENT_ORDERS", @"Recent Orders");
    ordersList.tabBarItem.image = [IonIcons imageWithIcon:ion_ios_filing_outline
                                                     size:49.0f
                                                    color:[UIColor whiteColor]];
    UINavigationController *orderNav = [[UINavigationController alloc] initWithRootViewController:ordersList];
    
    OIHUserSettingsViewController *userProfile = [[OIHUserSettingsViewController alloc] init];
    userProfile.tabBarItem.image = [IonIcons imageWithIcon:ion_ios_gear_outline
                                                      size:49.0f
                                                     color:[UIColor whiteColor]];
    userProfile.title = NSLocalizedString(@"TAB_BAR_SETTINGS", @"");
    UINavigationController *profile = [[UINavigationController alloc] initWithRootViewController:userProfile];
    
    OIHRestaurantManagementStatisticsViewController *stats =
    [OIHRestaurantManagementStatisticsViewController new];
    stats.tabBarItem.image = [IonIcons
                              imageWithIcon:ion_ios_pie_outline
                              size:49.0f
                              color:[UIColor whiteColor]]; // it looks a bit big at 30.0f
    UINavigationController *statsNav =
    [[UINavigationController alloc] initWithRootViewController:stats];
    
    _tabBarController.viewControllers = [NSArray arrayWithObjects:nav, orderNav, statsNav, profile, nil];
    _tabBarController.edgesForExtendedLayout = UIRectEdgeNone;
    
    if (notification) {
        [self notificationAction:notification];
    }
    
    return _tabBarController;
}

- (void)userDidLogin:(UserProfile *)profile
 withLoginController:(OIHLoginViewController *)loginViewController {
    
    if ([[UserProfile currentUser] isMerchant])
        [self setupMerchantTabBar:nil];
    else
        [self setupConsumerTabBar:nil];
    
    [loginViewController
     dismissViewControllerAnimated:YES
     completion:^{
         [self.window.rootViewController
          presentViewController:_tabBarController
          animated:YES
          completion:nil];
     }];
}

- (void)userDidSignUp:(UserProfile *)profile
 withSignUpController:(OIHSignUpViewController *)signupViewController {
    
    intro = [OIHSignUpIntroductionViewController new];
    intro.delegate = self;
    
    [signupViewController dismissViewControllerAnimated:YES completion:^{
        [_loginVC presentViewController:intro animated:YES completion:nil];
    }];
    
}
- (void)introWillDismiss:
(OIHSignUpIntroductionViewController *)introViewController {
    
    [self setupConsumerTabBar:nil];
    
    [introViewController
     dismissViewControllerAnimated:YES
     completion:^{
         [_loginVC dismissViewControllerAnimated:NO completion:^{
             
             UIViewController *dummy = [UIViewController new];
             dummy.view.backgroundColor = [UIColor whiteColor];
             [self setKeyWindowViewController:dummy];
             
             [self.window.rootViewController
              presentViewController:_tabBarController
              animated:YES
              completion:nil];
             
         }];
         
     }];
}

- (void)setKeyWindowViewController:(UIViewController *)viewController {
    
    self.window.backgroundColor = [UIColor blackColor];
    self.window.rootViewController = viewController;
    [self.window makeKeyAndVisible];
}

- (void)switchToMerchant:(BOOL)isMerchant {
    
    [_tabBarController
     dismissViewControllerAnimated:YES
     completion:^{
         
         if (isMerchant) {
             [self setupMerchantTabBar:nil];
         } else {
             [self setupConsumerTabBar:nil];
         }
         
         [self.window.rootViewController
          presentViewController:_tabBarController
          animated:YES
          completion:nil];
         
     }];
}

- (void)logout {
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"PushNotificationDeviceToken"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PushNotificationDeviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[UserProfile currentUser] logout];
    
    if (token &&
        [UserProfile currentUser]._id) { // prevent simulator from crashing
        [[JSONRequest sharedInstance]
         removeNotificationToken:token
         key:[UserProfile currentUser]._id];
    }
    
    [_tabBarController dismissViewControllerAnimated:YES
                                          completion:^{
                                              OIHLoginViewController *vc =
                                              [OIHLoginViewController new];
                                              vc.delegate = self;
                                              vc.signupViewController.delegate =
                                              self;
                                              [self.window.rootViewController
                                               presentViewController:vc
                                               animated:YES
                                               completion:nil];
                                          }];
}

- (BOOL)application:(__unused UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    
    NSString *bundleId = [[NSBundle mainBundle] bundleIdentifier];
    
    if ([url.scheme localizedCaseInsensitiveCompare: [NSString stringWithFormat:@"%@.payments", bundleId]] == NSOrderedSame) {
        return [BTAppSwitch handleOpenURL:url options:options];
        
    } else {
        
        return [[FBSDKApplicationDelegate sharedInstance]
                application:app
                openURL:url
                sourceApplication:options
                [UIApplicationOpenURLOptionsSourceApplicationKey]
                annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    return YES;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    if (application.applicationState == UIApplicationStateActive) {
        //NSString *message = userInfo[@"message"];
        if ([userInfo[@"message"] isEqualToString:OIHMessageUserWillFetchMessage]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@-%@", OIHMessageUserWillFetchMessage, userInfo[@"id"]] object:nil];
        }
    } else {
        [self notificationAction:userInfo];
    }
}

- (void)notificationAction:(NSDictionary *)notification {
    
    NSString *message = notification[@"message"];
    
    if ([message isEqualToString:OIHUpdateOrderNotification]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:OIHUpdateOrderNotification object:nil];
        
    } else if ([message isEqualToString:OIHUserDidReceiveANewGiftOrder]) {
        
        OIHUserAddressListViewController *vc = [OIHUserAddressListViewController new];
        [self.window.rootViewController presentViewController:vc
                                                     animated:YES
                                                   completion:nil];
        
    } else {
        
    }
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // register itself to the server
    NSString *tokenString = [deviceToken description];
    tokenString = [tokenString stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    tokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (tokenString != nil) {
        [[NSUserDefaults standardUserDefaults]
         setObject:tokenString
         forKey:@"PushNotificationDeviceToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code != 3010) { // 3010 is for the iPhone Simulator
        NSLog(@"Application failed to register for push notifications: %@", error);
    }
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    NSString *bundleId = [[NSBundle mainBundle] bundleIdentifier];
    
    if ([url.scheme localizedCaseInsensitiveCompare:
         [NSString stringWithFormat:@"%@.payments", bundleId]] ==
        NSOrderedSame) {
        return [BTAppSwitch handleOpenURL:url sourceApplication:sourceApplication];
    } else {
        NSLog(@"%@", url.scheme);
    }
    
    return NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state.
    // This can occur for certain types of temporary interruptions (such as an
    // incoming phone call or SMS message) or when the user quits the application
    // and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down
    // OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate
    // timers, and store enough application state information to restore your
    // application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called
    // instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state;
    // here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the
    // application was inactive. If the application was previously in the
    // background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if
    // appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the
    // application terminates.
    [self saveContext];
}



#pragma mark - Reachability
- (BOOL)isNetworkReachable {
    return self.networkStatus != NotReachable;
}

- (void)monitorReachability {
    Reachability *hostReach =
    [Reachability reachabilityWithHostName:[[JSONRequest sharedInstance] connectionURL]];
    
    hostReach.reachableBlock = ^(Reachability *reach) {
        _networkStatus = [reach currentReachabilityStatus];
        
        //        if ([self isParseReachable] && [PFUser currentUser]) {
        // Refresh home timeline on network restoration. Takes care of a freshly
        // installed app that failed to load the main timeline under bad network
        // conditions.
        // In this case, they'd see the empty timeline placeholder and have no way
        // of refreshing the timeline unless they followed someone.
        dispatch_async(dispatch_get_main_queue(), ^{
            //            [AFMInfoBanner showWithText:@"Online"
            //                                  style:AFMInfoBannerStyleInfo
            //                           andHideAfter:1.0];
        });
        
        //        }
    };
    
    hostReach.unreachableBlock = ^(Reachability *reach) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //            [AFMInfoBanner showWithText:@"Currently Offline"
            //                                  style:AFMInfoBannerStyleError
            //                           andHideAfter:1.0];
        });
        
        //        [[NSNotificationCenter defaultCenter]
        //        postNotificationName:EinerNetworkDidGoOfflineNotification
        //        object:nil];
        _networkStatus = [reach currentReachabilityStatus];
        
    };
    
    [hostReach startNotifier];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This
    // code uses a directory named "com.ralfcheung.Oh_It_s_Here" in the
    // application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask]
            lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the
    // application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Oh_It_s_Here"
                                              withExtension:@"momd"];
    _managedObjectModel =
    [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation
    // creates and returns a coordinator, having added the store for the
    // application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory]
                       URLByAppendingPathComponent:@"Oh_It_s_Here.sqlite"];
    NSError *error = nil;
    NSString *failureReason =
    @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:nil
                                                           error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] =
        @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error =
        [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You
        // should not use this function in a shipping application, although it may
        // be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already
    // bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc]
                             initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] &&
            ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error
            // appropriately.
            // abort() causes the application to generate a crash log and terminate.
            // You should not use this function in a shipping application, although it
            // may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
