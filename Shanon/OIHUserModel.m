//
//  OIHUserModel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 10/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserModel.h"
#define OptionsHasValue(options, value) (((options) & (value)) == (value))

@implementation OIHUserModel

+(NSMutableArray *) profilesFromArray: (NSArray *)data{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary *info in data) {
        if ([info isKindOfClass:[NSDictionary class]]) {
            [array addObject:[[OIHUserModel alloc] initWithUserInfo: info]];
        }
    }
    
    return array;
}


-(id)initWithUserInfo: (NSDictionary *)info{
    
    self = [super init];
    if (self) {
        
        self.userName = info[kOIHUserName];
        self.userLastName = info[kOIHUserLastName];
        self._id = info[kOIHUserId];
        self.isUserAllowToBeAddedAsFriend = [info[kOIHUserAllowToBeAddedAsFriend] boolValue];
        self.email = info[@"email"];
        
        UserType type = 0;
        
        if ([info[@"isFriend"] boolValue]) {
            type |= UserFriend;
        }
        if ([info[@"isPendingRequest"] boolValue]) {
            type |= UserPendingRequest;
        }
        if ([info[@"isFollowingUser"] boolValue]) {
            type |= UserIsFollowing;
        }
        
        if ([info[@"isWaitingForConfirmation"] boolValue]) {
            type |= UserFriendRequest;
        }
        if ([info[@"isMerchant"] boolValue]){
            type |= UserIsMerchant;
        }
        
        self.userType = type;
        self.userInfo = info;
        self.profilePicURL = [info[@"profilePic"] firstObject];
        
    }
    return self;
}





-(void)confirmFriend :(void (^)(id data, NSError *error))completionHandler{
    
    [[JSONRequest sharedInstance] fetch:@"confirmFriend" method:@"GET" data:@{@"userId": self._id} completionHandler:^(id data, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(data, error);
        });
        
    }];
    
    
}


-(void)cancelRequest :(void (^)(id data, NSError *error))completionHandler{
    
    [[JSONRequest sharedInstance] fetch:@"pendingRequests" method:@"PUT" data:@{@"userId": self._id} completionHandler:^(id data, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionHandler) {
                completionHandler(data, error);
            }
        });
        
    }];
    
}

-(void)removeFromFriendsList :(void (^)(id data, NSError *error))completionHandler{
    
    [[JSONRequest sharedInstance] fetch:@"friend" method:@"DELETE" data:@{@"userId": self._id} completionHandler:^(id data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionHandler) {
                completionHandler(data, error);
            }
        });
    }];
    
}

-(void)sendFriendRequest :(void (^)(id data, NSError *error))completionHandler{
    
    [[JSONRequest sharedInstance] fetch:@"friend" method:@"POST" data:@{@"userId": self._id} completionHandler:^(id data, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionHandler) {
                completionHandler(data, error);
            }
        });
    }];
    
}


-(void)userLastOrder :(void (^)(id data, NSError *error))completionHandler {
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"mostRecentOrder" httpMethod:HTTP_GET];
    setting.requestData = @{@"userId": self._id};
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (completionHandler) {
            completionHandler(data, error);
        }
    }];
    
}

-(void)followPerson{
    [self followUnfollowPerson:YES];
}

-(void)unfollowPerson{
    [self followUnfollowPerson:NO];
}

-(void)followUnfollowPerson: (BOOL) followUser{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"follow" httpMethod:HTTP_POST];
    
    setting.requestData = @{@"userId": self._id};
    
    if (!followUser) {
        setting.HTTPMethod = HTTP_DELETE;
    }
    
    [[JSONRequest sharedInstance] fetchBackground:setting];
    
}


-(NSDictionary *)toDictionary{
    return self.userInfo;
}

//why == is needed after the AND operation;
//http://sam.dods.co/blog/2014/02/01/checking-for-a-value-in-a-bit-mask/

-(void)resetUserType{
    self.userType = 0;
}

-(void)removeUserType: (UserType) type{
    self.userType &= ~type;
}
-(void)addUserType: (UserType) type{
    self.userType |= type;
}

-(BOOL)isUserType: (UserType) type{
    return OptionsHasValue(self.userType, type);
}

-(BOOL) isUserFriend{
    return OptionsHasValue(UserFriend, self.userType);
}

-(BOOL) isUserPendingRequest{
    return OptionsHasValue(UserPendingRequest, self.userType);
}

-(BOOL) isUserFriendRequest{
    return OptionsHasValue(UserFriendRequest, self.userType);
}

-(BOOL) isUserMerchant{
    return OptionsHasValue(UserIsMerchant, self.userType);
}


@end
