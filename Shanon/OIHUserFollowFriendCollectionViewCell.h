//
//  OIHUserFollowFriendCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"

@class OIHUserFollowFriendCollectionViewCell;

@protocol OIHUserFollowFriendCellDelegate <NSObject>

@required

-(void)didPressFollowFriendButton: (OIHUserFollowFriendCollectionViewCell *)cell;
-(void)didPressUnfollowFriendButton: (OIHUserFollowFriendCollectionViewCell *)cell;

-(void)didPressAddFriendButton: (OIHUserFollowFriendCollectionViewCell *)cell;
-(void)didPressRemoveFriendRequestButton: (OIHUserFollowFriendCollectionViewCell *)cell;
@optional
-(void)didPressBuyFoodButton: (OIHUserFollowFriendCollectionViewCell *)cell;


@end


@interface OIHUserFollowFriendCollectionViewCell : UICollectionViewCell

@property (nonatomic) UIButton *buyButton;
@property (nonatomic) UIButton *followButton;
@property (nonatomic) UIButton *addAsFriendButton;
@property (nonatomic, weak) OIHUserModel *friendProfile;

@property (nonatomic, weak) id<OIHUserFollowFriendCellDelegate>delegate;

//-(void)personIsFriendMode: (OIHUserModel *)user isFriend:(BOOL)isFriend;
//-(void)personFollowMode;
@end
