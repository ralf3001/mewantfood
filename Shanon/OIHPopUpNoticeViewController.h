//
//  OIHPopUpNoticeViewController.h
//  Shanon
//
//  Created by Ralf Cheung on 13/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OIHPopUpNoticeViewController;

typedef void (^OIHButtonHandler)(OIHPopUpNoticeViewController *viewController);

@interface OIHPopUpNoticeViewController : UIViewController

@property (nonatomic) NSString *titleText;
@property (nonatomic) NSString *descriptionText;

@property (nonatomic) NSString *buttonTitle;
@property (nonatomic, weak) OIHButtonHandler buttonAction;

@property (nonatomic) UIColor *backgroundColor;

-(id)initWithTitle: (NSString *)title description: (NSString *)description;

@end
