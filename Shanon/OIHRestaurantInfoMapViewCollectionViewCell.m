//
//  OIHRestaurantInfoMapViewCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantInfoMapViewCollectionViewCell.h"
#import "OIHMapAnnotationView.h"

@interface OIHRestaurantInfoMapViewCollectionViewCell () <MKMapViewDelegate>

@end

@implementation OIHRestaurantInfoMapViewCollectionViewCell

-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        
        _mapView = [MKMapView new];
        _mapView.translatesAutoresizingMaskIntoConstraints = NO;
        _mapView.showsUserLocation = YES;
        _mapView.delegate = self;
        _mapView.userInteractionEnabled = NO;
        [self.contentView addSubview:_mapView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_mapView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_mapView)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_mapView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_mapView)]];
        
    }
    return self;
}

-(void)setAnnotation:(OIHMapAnnotation *)annotation{
    
    if (self.mapView.annotations.count == 0) {
        [self.mapView addAnnotation:annotation];
    }
    
    MKCoordinateRegion mapRegion;
    mapRegion.center = annotation.coordinate;
    mapRegion.span.latitudeDelta = 0.1;
    mapRegion.span.longitudeDelta = 0.1;
    
    [_mapView setRegion:mapRegion animated: YES];

}

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    
    if ([annotation isKindOfClass:[OIHMapAnnotation class]]) {
        
        OIHMapAnnotation *a = (OIHMapAnnotation *)annotation;
        OIHMapAnnotationView *annotationView = (OIHMapAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"annotation"];
        
        if (annotationView) {
            annotationView.annotation = a;
        }else{
            annotationView = [[OIHMapAnnotationView alloc] initWithAnnotation:a reuseIdentifier:@"annotation"];
        }
        
        return annotationView;
        
    }else{
        return nil;
    }
    
}


@end
