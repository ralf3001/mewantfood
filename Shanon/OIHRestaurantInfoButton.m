//
//  OIHRestaurantInfoButton.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantInfoButton.h"



@implementation OIHRestaurantInfoButton

-(id)initWithTitle: (NSString *)title color: (UIColor *)color buttonImage: (UIImage *)image{
    
    self = [super init];
    if (self) {
        
        _titleLabel = [UILabel new];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.numberOfLines = 0;
        _titleLabel.text = title;
        _titleLabel.textColor = [UIColor darkGrayColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption2];
        [self addSubview:_titleLabel];
        
        _button = [UIButton buttonWithType:UIButtonTypeSystem];
        _button.translatesAutoresizingMaskIntoConstraints = NO;
        [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _button.backgroundColor = color;
        [_button setImage:image forState:UIControlStateNormal];
        _button.layer.cornerRadius = 25.0f;
        
        [self addSubview:_button];
        
        self.layer.shadowRadius = 5.0f;
        self.layer.shadowOpacity = 0.3f;
        self.layer.shadowOffset = CGSizeMake(0, 5);

        
        [self autoLayout];
    }
    return self;
}



-(void)autoLayout{
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_button attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_button attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_button(==height)]-[_titleLabel]|" options:NSLayoutFormatAlignAllCenterX metrics:@{@"height": @kButtonHeight} views:NSDictionaryOfVariableBindings(_button, _titleLabel)]];

    [self addConstraint:[NSLayoutConstraint constraintWithItem:_button attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_titleLabel]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_titleLabel)]];

}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
