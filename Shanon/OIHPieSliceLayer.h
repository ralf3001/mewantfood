//
//  OIHPieSliceLayer.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 18/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface OIHPieSliceLayer : CALayer
@property (nonatomic) CGFloat startAngle, endAngle;
@property (nonatomic) CGFloat lineWidth;

@property (nonatomic) UIColor *fillColor;
@property (nonatomic) UIColor *strokeColor;

@end
