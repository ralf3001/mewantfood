//
//  OIHDropDownMenuViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 1/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHDropDownMenuViewController.h"
#import "OIHDropDownMenuTableViewCell.h"


@interface OIHDropDownMenuViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) UIView *containerView;

@end

@implementation OIHDropDownMenuViewController
@synthesize menuItems;
@synthesize tableView = _tableView;
@synthesize menuMiscInfo;


-(id)initWithMenuItems: (NSArray *)items{
    
    self = [super init];
    if (self) {
        menuItems = items;
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationCustom;
        
    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _containerView = [UIView new];
    _containerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_containerView];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
    recognizer.cancelsTouchesInView = NO; //So the user can still interact with controls in the modal view
    [self.view addGestureRecognizer:recognizer];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_containerView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_containerView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_containerView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_containerView)]];
    
    
    [self createTableView];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([self.delegate respondsToSelector:@selector(dropDownMenuWillAppear:)]) {
        [self.delegate dropDownMenuWillAppear:self];
    }
}

-(void)updateTableViewNotifications{
    [_tableView reloadData];
}


-(void)createTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [_tableView registerClass:[OIHDropDownMenuTableViewCell class] forCellReuseIdentifier:@"cell"];
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    _tableView.rowHeight = 60;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor blackColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.scrollEnabled = NO;
    
    [_containerView addSubview:_tableView];
    
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [_containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView(==tableViewHeight)]" options:0 metrics:@{@"tableViewHeight": [NSNumber numberWithInteger:60 * menuItems.count]} views:NSDictionaryOfVariableBindings(_tableView)]];
    
}

#pragma mark - <UITableViewDelegate>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return menuItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHDropDownMenuTableViewCell *cell = (OIHDropDownMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.menuLabel.text = menuItems[indexPath.row];
    cell.menuLabel.textColor = [UIColor whiteColor];
    
    NSInteger itemNotificationCount = 0;
    BOOL showDot = NO;
    
    switch (indexPath.row) {
        case 0:
            itemNotificationCount = [menuMiscInfo[@"friends"] integerValue];
            break;
        case 1:
            itemNotificationCount = [menuMiscInfo[@"pendingRequests"] integerValue];
            showDot = YES;
            break;
        case 2:
            itemNotificationCount = [menuMiscInfo[@"friendRequests"] integerValue];
            break;
        case 3:
            itemNotificationCount = [menuMiscInfo[@"following"] integerValue];
            break;
        case 4:
            itemNotificationCount = [menuMiscInfo[@"followers"] integerValue];
            break;
            
        default:
            break;
    }
    
    [cell setNotification:itemNotificationCount notificationDot:showDot];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.delegate respondsToSelector:@selector(dropDownMenu:didSelectIndx:)]) {
        [self.delegate dropDownMenu:self didSelectIndx:indexPath.row];
    }
    
    [self dismissView];
}


-(void)dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)dismiss: (UITapGestureRecognizer *)gesture{
    
    if (gesture.state == UIGestureRecognizerStateEnded){
        CGPoint location = [gesture locationInView:nil]; //Passing nil gives us coordinates in the window
        
        //Then we convert the tap's location into the local view's coordinate system, and test to see if it's in or outside. If outside, dismiss the view.
        
        if (!CGRectContainsPoint(_tableView.frame, location)){
            // Remove the recognizer first so it's view.window is valid.
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
}



#pragma mark - <UIViewControllerTransitioningDelegate>

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    DropDownMenuAnimatedTransitioning *transitioning = [DropDownMenuAnimatedTransitioning new];
    transitioning.presenting = YES;
    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    DropDownMenuAnimatedTransitioning * transitioning = [DropDownMenuAnimatedTransitioning new];
    transitioning.presenting = NO;
    return transitioning;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end


@implementation DropDownMenuAnimatedTransitioning
@synthesize snapShot;

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext{
    return 0.35;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
    
    
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *fromView = fromViewController.view;
    UIView *toView = toViewController.view;
    UIView *containerView = [transitionContext containerView];
    
    CGAffineTransform moveDown = CGAffineTransformMakeTranslation(0, 140);
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0, -330);
    
    
    if (self.presenting) {
        toView.transform = moveUp;
        [containerView addSubview:toView];
    }
    
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 usingSpringWithDamping:0.9 initialSpringVelocity:0.3 options:0 animations:^{
        if (self.presenting) {
            fromView.transform = moveDown;
            toView.transform = CGAffineTransformIdentity;
            
        }else{
            toView.transform = CGAffineTransformIdentity;
            fromView.transform = moveUp;
        }
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
    
    
}


@end


