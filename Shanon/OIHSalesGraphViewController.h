//
//  OIHSalesGraphViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 31/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHSalesGraphViewController : UIViewController
@property (nonatomic, weak) NSArray *data;
@end
