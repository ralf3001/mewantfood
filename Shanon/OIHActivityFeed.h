//
//  OIHActivityFeed.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 23/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHModelProtocol.h"
#import "OIHUserModel.h"
#import "OIHOrderModel.h"
#import "OIHMealModel.h"


typedef NS_ENUM(NSInteger, FeedType) {
    
    UserLike = 0,
    UserOrder = 1,
    UserThank = 2,
    UserReview = 3,
    UserComment = 4,
    NewItem = 5
};

@interface OIHActivityFeed : NSObject <OIHModelProtocol>
@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *fromUserId;
@property (nonatomic) NSString *toUserId;
@property (nonatomic) FeedType type;
@property (nonatomic) NSDate *createdAt;
@property (nonatomic) NSDictionary *activityInfo;
@property (nonatomic) NSInteger rating;
@property (nonatomic) NSString *audioNote;

@property (nonatomic, assign) BOOL isPostThanked;

@property (nonatomic) OIHUserModel *fromUser;
@property (nonatomic) OIHUserModel *toUser;

@property (nonatomic) OIHMealModel *meal;
@property (nonatomic) OIHOrderModel *order;

@property (nonatomic) NSString *postPicture;
@property (nonatomic) NSString *content;

@property (nonatomic) OIHActivityFeed *parentActivity;

-(id)initWithActivity: (NSDictionary *)activity;
-(NSString *)activityType;
+(NSArray *)activitiesFromArray: (NSArray *)data;

-(void)thankActivity :(void (^)(id data, NSError *error))completionHandler;
-(void)unthankActivity :(void (^)(id data, NSError *error))completionHandler;


@end
