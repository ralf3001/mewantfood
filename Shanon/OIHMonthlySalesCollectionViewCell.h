//
//  OIHMonthlySalesCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 29/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHSalesAmountLabel.h"


@interface OIHMonthlySalesCollectionViewCell : UICollectionViewCell

@property (nonatomic) UILabel *monthLabel;
@property (nonatomic) OIHSalesAmountLabel *salesAmountLabel;

@end
