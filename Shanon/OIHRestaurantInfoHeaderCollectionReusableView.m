//
//  OIHRestaurantInfoHeaderCollectionReusableView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantInfoHeaderCollectionReusableView.h"

@implementation OIHRestaurantInfoHeaderCollectionReusableView
@synthesize imageView;

-(id)initWithFrame:(CGRect)frame{
  
  self = [super initWithFrame:frame];
  
  if (self) {
    
    CGRect bounds;
    bounds = [self bounds];
    
    imageView = [[UIImageView alloc] initWithFrame:bounds];
    [imageView setImage:[UIImage defaultImage]];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    [imageView setClipsToBounds:YES];
    [self addSubview:imageView];

  }
  return self;
}

@end
