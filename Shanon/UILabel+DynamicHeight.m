//
//  UILabel+DynamicHeight.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 15/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "UILabel+DynamicHeight.h"

@implementation UILabel (DynamicHeight)
-(CGSize)sizeOfMultiLineLabel{
    
    //Label text
    NSString *aLabelTextString = [self text];
    
    //Label font
    UIFont *aLabelFont = [self font];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.frame.size.width;
    
    
        //version >= 7.0
        
        //Return the calculated size of the Label
    return [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{
                                                    NSFontAttributeName : aLabelFont
                                                    }
                                          context:nil].size;
    
//    return [self bounds].size;
    
}

@end
