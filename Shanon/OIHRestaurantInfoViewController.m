//
//  OIHRestaurantInfoViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHRestaurantInfoViewController.h"
#import "OIHRestaurantModel.h"
#import "OIHUserModel.h"

#import "StretchyHeaderCollectionViewLayout.h"
#import "OIHRestaurantInfoHeaderCollectionReusableView.h"
#import "OIHRestaurantInfoRestaurantNameCollectionViewCell.h"
#import "OIHRestaurantInfoOwnerCollectionViewCell.h"
#import "OIHRestaurantDescriptionNoteCollectionViewCell.h"
#import "OIHRestaurantInfoUserRatingsCollectionViewCell.h"
#import "OIHRestaurantInfoMapViewCollectionViewCell.h"

#import "OIHMapAnnotation.h"
#import "OIHLocationManager.h"


@interface OIHRestaurantInfoViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic) NSString *restaurantId;
@property (nonatomic) OIHRestaurantModel *restaurantInfo;
@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) OIHUserModel *owner;

@end



@implementation OIHRestaurantInfoViewController
//NSString *const kCellIdent = @"cell";
//NSString *const kFollowFriendCellIdent = @"follow";
//NSString *const kHeaderIdent = @"header";

-(id)initWithRestaurantId: (NSString *)restaurant{
    
    self = [super init];
    if (self) {
        _restaurantId = restaurant;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[OIHLocationManager sharedInstance] requestLocation];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self fetchInfo];
    
}

-(void)fetchInfo{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"restaurant/%@", _restaurantId] httpMethod:HTTP_GET];
    setting.cachePolicy = IgnoreCache;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        if (!error) {
            _restaurantInfo = [[OIHRestaurantModel alloc] initWithDictionary:data[@"restaurant"]];
            _restaurantInfo.sellersNote = [LoremIpsum paragraphsWithNumber:3];
            _owner = [[OIHUserModel alloc] initWithUserInfo:data[@"user"]];
            
            [self setupCollectionView];
            
        }
        
    }];
    
}


-(void)setupCollectionView{
    
    StretchyHeaderCollectionViewLayout *stretchyLayout;
    stretchyLayout = [[StretchyHeaderCollectionViewLayout alloc] init];
    [stretchyLayout setSectionInset:UIEdgeInsetsMake(0, 10.0, 10.0, 10.0)];
    [stretchyLayout setHeaderReferenceSize:CGSizeMake(320.0, 160.0)];
    [stretchyLayout setFooterReferenceSize:CGSizeMake(320.0, 80.0)];
    
    stretchyLayout.minimumInteritemSpacing = 0;
    stretchyLayout.minimumLineSpacing = 0;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:stretchyLayout];
    _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    [_collectionView setAlwaysBounceVertical:YES];
    [_collectionView setShowsVerticalScrollIndicator:NO];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [self.view addSubview:_collectionView];
    ///OIHRestaurantInfoUserRatingsCollectionViewCell.h
    
    [_collectionView registerClass:[OIHRestaurantInfoRestaurantNameCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [_collectionView registerClass:[OIHRestaurantInfoOwnerCollectionViewCell class] forCellWithReuseIdentifier:@"owner"];
    [_collectionView registerClass:[OIHRestaurantDescriptionNoteCollectionViewCell class] forCellWithReuseIdentifier:@"description"];
    [_collectionView registerClass:[OIHRestaurantInfoUserRatingsCollectionViewCell class] forCellWithReuseIdentifier:@"ratings"];
    [_collectionView registerClass:[OIHRestaurantInfoMapViewCollectionViewCell class] forCellWithReuseIdentifier:@"map"];
    
    //  OIHRestaurantInfoMapViewCollectionViewCell.h
    [_collectionView registerClass:[OIHRestaurantInfoHeaderCollectionReusableView class]
        forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
               withReuseIdentifier:@"header"];
    
    [_collectionView registerClass:[UICollectionReusableView class]
        forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
               withReuseIdentifier:@"footer"];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv {
    
    return 1;
}

- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section {
    
    return 5;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSAttributedString *attrText;
    
    switch (indexPath.row) {
        case 0:{
            NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        (id)[UIFont fontWithName:@"HelveticaNeue-Light" size:35.0f], NSFontAttributeName
                                        , nil];
            
            attrText = [[NSAttributedString alloc] initWithString:_restaurantInfo.restaurantName attributes:attributes];
            
            break;
        }
        case 1:{
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Owner"];
            [string appendAttributedString:[[NSAttributedString alloc] initWithString:_owner.userName attributes:@{[UIFont fontWithName:@"AppleSDGothicNeo-Light" size:28.0f]: NSFontAttributeName}]];
            
            [string setAttributes:@{[UIFont preferredFontForTextStyle:UIFontTextStyleTitle2]: NSFontAttributeName} range:NSMakeRange(0, 5)];
            
            attrText = string;
            
            CGRect rect = [attrText boundingRectWithSize:CGSizeMake(collectionView.frame.size.width - 40, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
            
            return CGSizeMake(collectionView.frame.size.width -30, MAX(rect.size.height + 20, 110)); //add 20px for top and bottom margin
            
            break;
        }
        case 2:{
            NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        (id)[UIFont fontWithName:@"HelveticaNeue" size:18], NSFontAttributeName
                                        , nil];
            
            attrText = [[NSAttributedString alloc] initWithString:_restaurantInfo.sellersNote ? _restaurantInfo.sellersNote : @"" attributes:attributes];
            
            break;
        }
            
        case 3:{
            
            NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        (id)[UIFont fontWithName:@"HelveticaNeue" size:18], NSFontAttributeName
                                        , nil];
            //just a dummy emoji, they all have the same height
            attrText = [[NSAttributedString alloc] initWithString:[NSString stringWithUTF8String:"\xF0\x9F\x98\xA1"] attributes:attributes];
            
            CGRect rect = [attrText boundingRectWithSize:CGSizeMake(collectionView.frame.size.width - 40, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
            
            return CGSizeMake(collectionView.frame.size.width - 30, rect.size.height * 6 + 105); //add 20px for top and bottom margin
        }
        case 4:{
            return CGSizeMake(collectionView.frame.size.width, 250);
        }
        default:
            break;
    }
    
    CGRect rect = [attrText boundingRectWithSize:CGSizeMake(collectionView.frame.size.width - 40, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    return CGSizeMake(collectionView.frame.size.width -30, MAX(rect.size.height + 20, 0)); //add 20px for top and bottom margin
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        OIHRestaurantInfoHeaderCollectionReusableView *header = (OIHRestaurantInfoHeaderCollectionReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"header" forIndexPath:indexPath];
        //  [header.imageView sd_setImageWithURL: [NSURL URLWithString:_restaurantInfo] placeholderImage:[UIImage imageFromColor:[UIColor lightGrayColor]]];
        
        return header;
    }else{
        
        return [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"footer" forIndexPath:indexPath];
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 3:{
            OIHRestaurantInfoUserRatingsCollectionViewCell *a = (OIHRestaurantInfoUserRatingsCollectionViewCell *)cell;
            a.restaurantInfo = _restaurantInfo;
            break;
        }
        default:
            break;
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:{
            OIHRestaurantInfoRestaurantNameCollectionViewCell *cell = (OIHRestaurantInfoRestaurantNameCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
            cell.titleLabel.text = _restaurantInfo.restaurantName;
            
            return cell;
        }
        case 1:{
            
            OIHRestaurantInfoOwnerCollectionViewCell *cell = (OIHRestaurantInfoOwnerCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"owner" forIndexPath:indexPath];
            cell.user = _owner;
            
            return cell;
            
        }
        case 2:{
            
            OIHRestaurantDescriptionNoteCollectionViewCell *cell = (OIHRestaurantDescriptionNoteCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"description" forIndexPath:indexPath];
            cell.descriptionLabel.text = _restaurantInfo.sellersNote;
            
            return cell;
        }
        case 3:{
            OIHRestaurantInfoUserRatingsCollectionViewCell *cell = (OIHRestaurantInfoUserRatingsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ratings" forIndexPath:indexPath];
//            cell.restaurantInfo = _restaurantInfo;
            
            return cell;
        }
        case 4:{
            OIHRestaurantInfoMapViewCollectionViewCell *cell = (OIHRestaurantInfoMapViewCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"map" forIndexPath:indexPath];
            
            cell.annotation = [[OIHMapAnnotation alloc] initWithTitle:_restaurantInfo.restaurantName location:_restaurantInfo.coordinate];
            
            
            return cell;
            
        }
            
        default:
            break;
    }
    
    return nil;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    
}


-(NSString *)emojiAtIndex: (NSInteger)index{
    
    switch (index) {
        case 0:
            //angry face
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\xA1"];
        case 1:
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x9E"];
        case 2:
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\xB7"];
        case 3:
            //delicious face
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x8B"];
        case 4:
            //throw a kiss face, love it
            return [NSString stringWithUTF8String:"\xF0\x9F\x98\x8D"];
        default:
            break;
    }
    
    return nil;
}

@end
