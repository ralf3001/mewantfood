//
//  ViewControllerTrackingInfo.h
//  Einer
//
//  Created by Ralf Cheung on 5/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, ViewControllerTrackingType) {
    ViewControllerEnum = 0,
    UIControlEvent,
    PushNotificationOpened,
    ApplicationDidGoBackground,
    ApplicationDidBecomeActive
};


@interface ViewControllerTrackingInfo : NSObject

@property (nonatomic) NSString *viewControllerInString;
@property (nonatomic) double timeElapsed;
@property (nonatomic, assign) ViewControllerTrackingType pageType;


-(id)initWithViewControllerString: (NSString *)vcString type: (ViewControllerTrackingType)type;

@end
