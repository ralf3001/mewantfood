//
//  OIHFloatingTableViewCell.m
//  Shanon
//
//  Created by Ralf Cheung on 23/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHFloatingTableViewCell.h"

@interface OIHFloatingTableViewCell ()
@end

@implementation OIHFloatingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    
    [_floatingView setHighlight:highlighted];
    
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
    
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _floatingView = [OIHFloatingView new];
        _floatingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_floatingView];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_floatingView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_floatingView)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_floatingView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_floatingView)]];
    }
    return self;
}


@end
