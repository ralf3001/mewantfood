//
//  OIHRestaurantInfoMapViewCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHMapAnnotation.h"

@interface OIHRestaurantInfoMapViewCollectionViewCell : UICollectionViewCell
@property (nonatomic) MKMapView *mapView;
@property (nonatomic) OIHMapAnnotation *annotation;

@end
