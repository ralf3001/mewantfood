//
//  OIHUserFBFriendsTableViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserFBFriendsTableViewController.h"
#import "OIHEmptyView.h"
#import "OIHFriendsListTableViewCell.h"

#import "OIHSocialManager.h"

#import "OIHRestaurantListViewController.h"
#import "OIHUserProfileViewController.h"

@interface OIHUserFBFriendsTableViewController () <FriendTableViewCellDelegate>
@property (nonatomic) UITableView *tableView;
@property (nonatomic) OIHEmptyView *emptyView;

@end

@implementation OIHUserFBFriendsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;

    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"SETTING_LOGOUT", @"Logout") style:UIBarButtonItemStylePlain target:self action:@selector(logOutFacebook)];
    
    [self fetchFriends];
    
}

-(void)logOutFacebook{
    
    [[OIHSocialManager sharedInstance] facebookLogout];
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)fetchFriends{

    [[OIHSocialManager sharedInstance] fetchFacebookFriends:^(id result, NSError *error) {
        if (!error) {
            _friendsList = [OIHUserModel profilesFromArray:result];
            if (_friendsList.count > 0) {
                [self createTableView];
            }else{
                [self createEmptyView];
            }
        }
    }];
    
}


-(void)createEmptyView{
    
    if (_tableView) {
        [_tableView removeFromSuperview];
        _tableView = nil;
    }
    
    NSString *descriptionText;
    
    descriptionText = NSLocalizedString(@"NO_FRIENDS_TEXT", "No friends");

    if (_emptyView) {
        
        [_emptyView removeFromSuperview];
        _emptyView = nil;
    }
    
    _emptyView = [[OIHEmptyView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_people_outline size:90 color:[UIColor lightGrayColor]] descriptionText:descriptionText];

    _emptyView.translatesAutoresizingMaskIntoConstraints = NO;
//    _emptyView.delegate = self;
    
    [self.view addSubview:_emptyView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[_emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_emptyView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_emptyView)]];
    
    
    
}


-(void)createTableView{
    
    if (_tableView) {
        
        if (_emptyView) {
            [_emptyView removeFromSuperview];
        }
        
        [_tableView reloadData];
        
    }else{
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        [_tableView registerClass:[OIHFriendsListTableViewCell class] forCellReuseIdentifier:@"cell"];
        
        _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 60.0f;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        [self.view addSubview:_tableView];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        
        
        self.definesPresentationContext = YES;
        
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _friendsList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHFriendsListTableViewCell *cell = (OIHFriendsListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    OIHUserModel *user = _friendsList[indexPath.row];
    cell.user = user;
    
    cell.delegate = self;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    OIHUserModel *user = _friendsList[indexPath.row];

    OIHUserProfileViewController *vc = [[OIHUserProfileViewController alloc] initWithFriendProfile:user];
    
    [self.navigationController pushViewController:vc animated:YES];

}


-(void)tableViewCellDidPressActionButton: (OIHFriendsListTableViewCell *)cell{
    
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    
    OIHUserModel *user = _friendsList[indexPath.row];
    
    if ([user isUserType:UserFriend]) {
        
        OIHRestaurantListViewController *vc = [OIHRestaurantListViewController new];
        vc.friendProfile = user;
        [self.navigationController pushViewController:vc animated:YES];
        

    }else if ([user isUserType:UserPendingRequest]){
        [user confirmFriend:^(id data, NSError *error) {
            
        }];
    }else if ([user isUserType:UserFriendRequest]){
        
        
    }
    
}


@end
