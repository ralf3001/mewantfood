//
//  OIHMerchantRestaurantListTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 25/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHMerchantRestaurantListTableViewCell.h"

@implementation OIHMerchantRestaurantListTableViewCell
@synthesize restaurantAddressLabel, restaurantNameLabel;
@synthesize restaurantOperatingHours;

- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];

        restaurantAddressLabel = [UILabel new];
        restaurantAddressLabel.numberOfLines = 0;
        restaurantAddressLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:restaurantAddressLabel];
        
        restaurantNameLabel = [UILabel new];
        restaurantNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:restaurantNameLabel];
        
        restaurantOperatingHours = [UILabel new];
        restaurantOperatingHours.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:restaurantOperatingHours];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[restaurantNameLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(restaurantNameLabel)]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[restaurantAddressLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(restaurantAddressLabel)]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[restaurantNameLabel]-[restaurantAddressLabel]-|" options:NSLayoutFormatAlignAllLeading metrics:0 views:NSDictionaryOfVariableBindings(restaurantNameLabel, restaurantAddressLabel)]];
        
        
    }
    
    return self;
}
@end
