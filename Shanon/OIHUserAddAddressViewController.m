//
//  OIHUserAddAddressViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 15/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserAddAddressViewController.h"
#import "OIHActionSheet.h"
#import "OIHAddMealTableViewCell.h"
#import "UIFont+OIHSystemFonts.h"


#define kKeyboardHeight 224


@interface OIHUserAddAddressViewController () <UITableViewDataSource, UITableViewDelegate, OIHTableViewCellTextViewProtocol>

@property (nonatomic) UITextField *streetLabel;
@property (nonatomic) UITextField *cityLabel;
@property (nonatomic) UITextField *stateLabel;
@property (nonatomic) UITextField *aptLabel;
@property (nonatomic) UITextField *tagLabel;
@property (nonatomic) UILabel *countryLabel;
@property (nonatomic) UISwitch *isDefaultSwitch;
@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSLayoutConstraint *dynamicTVHeight;
@property (nonatomic) NSMutableDictionary *addressInfo;
@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) BOOL isDefaultAddress;

@property (nonatomic) CGFloat keyboardHeight;

@end

@implementation OIHUserAddAddressViewController
@synthesize streetLabel;
@synthesize cityLabel;
@synthesize stateLabel;
@synthesize aptLabel;
@synthesize countryLabel;
@synthesize tagLabel;
@synthesize isDefaultSwitch;
@synthesize tableView = _tableView;
@synthesize dynamicTVHeight;
@synthesize addressInfo;
@synthesize scrollView;

@synthesize keyboardHeight;


-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAppear:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];

    
    self.edgesForExtendedLayout = UIRectEdgeNone;

    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [_tableView registerClass:[OIHAddMealTableViewCell class] forCellReuseIdentifier:@"cell"];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    _tableView.estimatedRowHeight = 100.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    doneButton.frame = CGRectMake(0,0, self.view.frame.size.width, 80);
    [doneButton setTitle:NSLocalizedString(@"DONE", @"Done") forState:UIControlStateNormal];
    doneButton.backgroundColor = [UIColor colorWithRed:46/256.0f green:65/256.0f blue:114/256.0f alpha:1.0f];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(askIfDefault) forControlEvents:UIControlEventTouchUpInside];

    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 80)];

    [footerView addSubview:doneButton];
    _tableView.tableFooterView = footerView;
    
    
    [self.view addSubview:_tableView];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    
    
    addressInfo = [NSMutableDictionary new];
    
}


-(UITextField *)createTextField{
    
    UITextField *textField = [UITextField new];
    textField.layer.borderWidth = 0.6;
    textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    textField.translatesAutoresizingMaskIntoConstraints = NO;

    return textField;
    
}


-(void)askIfDefault{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"DEFAULT_ADDRESS", @"") message:NSLocalizedString(@"SET_ADDRESS_AS_DEFAULT", @"") preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.isDefaultAddress = YES;
        [self done];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"NO") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.isDefaultAddress = NO;
        [self done];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"CANCEL", @"YES") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

    }]];

    [self presentViewController:alert animated:YES completion:nil];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return 5;
}


- (OIHAddMealTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHAddMealTableViewCell *cell = (OIHAddMealTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            cell.descriptionLabel.text = NSLocalizedString(@"ADDRESS_STREET_PLACEHOLDER", @"Street Address");
            cell.attribute = @"street";
            cell.descriptionContentTextField.text = addressInfo[@"street"];

            break;
            
        case 1:
            cell.descriptionLabel.text = NSLocalizedString(@"ADDRESS_APT_PLACEHOLDER", @"Apt");
            cell.attribute = @"apt";
            cell.descriptionContentTextField.text = addressInfo[@"apt"];

            break;

        case 2:
            cell.descriptionLabel.text = NSLocalizedString(@"ADDRESS_CITY_PLACEHOLDER", @"Address tag");
            cell.attribute = @"city";
            cell.descriptionContentTextField.text = addressInfo[@"city"];

            break;
            
        case 3:
            cell.descriptionLabel.text = NSLocalizedString(@"ADDRESS_STATE_PLACEHOLDER", @"State");
            cell.attribute = @"state";
            cell.descriptionContentTextField.text = addressInfo[@"state"];

            break;

        case 4:
            cell.descriptionLabel.text = NSLocalizedString(@"ADDRESS_TAG_PLACEHOLDER", @"Address tag");
            cell.attribute = kOIHAddressTag;
            cell.descriptionContentTextField.text = addressInfo[kOIHAddressTag];
            break;
        default:
            break;
            
    }
    cell.descriptionLabel.textAlignment = NSTextAlignmentRight;
    cell.descriptionLabel.font = [UIFont tableViewCellDescriptionFont];
    cell.tableView = _tableView;
    cell.delegate = self;
    
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


-(void)done{
    
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"LABEL_SENDING", @"Sending")];
    
    NSLocale* locale = [NSLocale currentLocale];
    NSString* identifier = locale.localeIdentifier;
    NSDictionary* components = [NSLocale componentsFromLocaleIdentifier:identifier];
    
    addressInfo[@"language"] = [NSString stringWithFormat:@"%@-%@", [components valueForKey:NSLocaleLanguageCode], [components valueForKey:NSLocaleScriptCode]];
    addressInfo[@"isDefault"] = [NSNumber numberWithBool:self.isDefaultAddress];
    

    [[JSONRequest sharedInstance] fetch:@"address" method:@"POST" data:addressInfo
                      completionHandler:^(id data, NSError *error) {
                          
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
            if (error) {
                [SVProgressHUD showErrorWithStatus:@"Oops, somthing's gone wrong"];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });
                
            }else{
                
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"DONE", @"Done")];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [self.navigationController popViewControllerAnimated:YES];
                });
                
            }
        });
    }];
    
}


-(void)textViewFinishedEditing: (UITextView *)textView cell: (OIHAddMealTableViewCell *)cell{
    
    addressInfo[cell.attribute] = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    [UIView setAnimationsEnabled:NO];
    [_tableView reloadData];
    [UIView setAnimationsEnabled:YES];

}


-(void)textViewDidBeginEditing:(OIHAddMealTableViewCell *)cell{
    
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    CGRect frame = [_tableView rectForRowAtIndexPath:indexPath];
    
    if (frame.origin.y > (self.view.frame.size.height / 2)) {
        [_tableView setContentOffset:CGPointMake(0, kKeyboardHeight) animated:YES];
    }
    
}

-(void)cellDidHighlight:(OIHAddMealTableViewCell *)cell{
    
    if (![cell.descriptionContentTextField isFirstResponder]) {
        [self.view endEditing:YES];
    }
    
}




-(void)keyboardWillAppear: (NSNotification *)notification{
    
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    self.keyboardHeight = keyboardFrameBeginRect.size.height;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
