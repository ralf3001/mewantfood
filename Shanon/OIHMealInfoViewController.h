//
//  OIHMealInfoViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 26/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"
#import "OIHRestaurantModel.h"
#import "OIHSearchResultsModel.h"


@interface OIHMealInfoViewController : UIViewController

@property (nonatomic) OIHSearchResultsModel *searchResults;

//if buying for a friend, this will be his/her userid, if not it will be null (buying for user him/herself)
@property (nonatomic) OIHUserModel *friendProfile;

-(id)initWithResults: (OIHSearchResultsModel *)searchResults;


@end
