//
//  OIHLocationManager.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHLocationManager.h"

@interface OIHLocationManager () <CLLocationManagerDelegate>

@property(nonatomic) NSMutableArray<CLLocation *> *locationHistory;

@end

@implementation OIHLocationManager

+ (OIHLocationManager *)sharedInstance {
    
    static OIHLocationManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[OIHLocationManager alloc] init];
    });
    return _sharedInstance;
}

- (id)init {
    
    self = [super init];
    if (self) {
        
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusNotDetermined:
                //        [_locationManager requestWhenInUseAuthorization];
                break;
            case kCLAuthorizationStatusRestricted:
            case kCLAuthorizationStatusDenied:{
                [self requestWhenInUseAuthorization];
                break;
            }
                
            default:
                break;
        }
        
        // Set a movement threshold for new events.
        _locationManager.distanceFilter = 150; // meters
        [_locationManager startUpdatingLocation];
        
        [self addObserver:self.locationManager forKeyPath:@"locationServicesEnabled" options:NSKeyValueObservingOptionNew context:nil];
        
    }
    
    return self;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"locationServicesEnabled"]) {
        NSLog(@"changed");
    }
}


- (NSNotificationCenter *)notificationCenter {
    
    return [NSNotificationCenter defaultCenter];
}

-(void)requestLocation{
    
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined:
            [_locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:{
            [self requestWhenInUseAuthorization];
            break;
        }
            
        default:
            break;
    }
    
    // Set a movement threshold for new events.
    _locationManager.distanceFilter = 150; // meters
    [_locationManager requestLocation];
    
}

- (void)requestWhenInUseAuthorization {
    
    if (SYSTEM_VERSION_GREATER_THAN(8.0)) {
        
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied)
        ? @"Location services are off"
        : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'When In "
        @"Use' in the Location Services Settings";
        
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:title
                                    message:message
                                    preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction
                          actionWithTitle:@"Settings"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction *_Nonnull action) {
                              
                              NSURL *url = [NSURL
                                            URLWithString:
                                            UIApplicationOpenSettingsURLString];
                              [[UIApplication sharedApplication]
                               openURL:url];
                              
                          }]];
        
        [alert addAction:[UIAlertAction
                          actionWithTitle:@"Cancel"
                          style:UIAlertActionStyleCancel
                          handler:^(UIAlertAction *_Nonnull action){
                              
                          }]];
        //
        //      dispatch_async(dispatch_get_main_queue(), ^{
        //          [self presentViewController:alert animated:YES
        //          completion:nil];
        //      });
    }
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    if (!_locationHistory) {
        _locationHistory = [NSMutableArray new];
    }
    
    [_locationHistory addObjectsFromArray:locations];
    
    _lastLocation = [locations lastObject];
    
    if ([self.delegate respondsToSelector:@selector(locationManagerDidUpdateLocation:)]) {
        [self.delegate locationManagerDidUpdateLocation:[locations lastObject]];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    
    if ([self.delegate respondsToSelector:@selector(locationManagerDidFailWithError:)]) {
        [self.delegate locationManagerDidFailWithError:error.code];
    }
    [_locationManager stopUpdatingLocation];
}

- (void)getLastPlacemark:(placemarkBlock)completionBlock {
    
    CLGeocoder *geocoder = [CLGeocoder new];
    
    [geocoder
     reverseGeocodeLocation:[_locationHistory lastObject]
     completionHandler:^(NSArray<CLPlacemark *> *_Nullable placemarks,
                         NSError *_Nullable error) {
         
         if (!error) {
             completionBlock([placemarks lastObject]);
         }
         
     }];
}

@end
