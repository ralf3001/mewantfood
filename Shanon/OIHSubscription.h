//
//  OIHSubscription.h
//  Shanon
//
//  Created by Ralf Cheung on 17/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHUserModel.h"

@interface OIHSubscription : NSObject

@property (nonatomic) NSString *_id;
@property (nonatomic) OIHUserModel *user;
@property (nonatomic) NSString *subscriptionDescription;
@property (nonatomic) NSString *subscriptionName;
@property (nonatomic) NSDate *createdAt;
@property (nonatomic) NSString *currency;
@property (nonatomic) NSNumber *priceInLocalCurrency;
@property (nonatomic) NSString *priceInLocalCurrencyString;



-(id)initWithInfo: (NSDictionary *)info;

@end
