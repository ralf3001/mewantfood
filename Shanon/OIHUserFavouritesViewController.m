//
//  OIHUserFavouritesViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 19/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHUserFavouritesViewController.h"
#import "EinerCanvasContentCollectionViewCell.h"
#import "OIHMealModel.h"
#import "OIHEmptyView.h"
#import "OIHRestaurantMealDetailViewController.h"


@interface OIHUserFavouritesViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, OIHMealDetailDelegate>
@property (nonatomic) UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *favouritesList;
@property (nonatomic) OIHEmptyView *emptyView;
@property (nonatomic) UIRefreshControl *refreshControl;

@end

@implementation OIHUserFavouritesViewController
@synthesize favouritesList;
@synthesize emptyView;


static NSString * const reuseIdentifier = @"cell";
static NSString * const footerReuseIdentifier = @"footer";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.favouritesList = [NSMutableArray new];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self fetchFavorites];
    
}

-(void)fetchFavorites{
    
    JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:@"favourites" httpMethod:HTTP_GET];
    
    [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error) {
            [self populateData:data isCacheData:isCachedResult];
        }
        [_refreshControl endRefreshing];
        
    }];
}



-(void)populateData: (NSArray *)data isCacheData: (BOOL) isCache{
    
    NSArray *feedArray = [OIHMealModel mealsFromArray:data];
    
    if(isCache){
        
        //initial fetch
        favouritesList = [NSMutableArray arrayWithArray:feedArray];
        favouritesList.isDataDirty = [NSNumber numberWithBool:isCache];
        
    }else{  //new data, flush cache data in the array
        
        if (favouritesList.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            //new data from network
            if (data.count > 0) {
                favouritesList = [NSMutableArray arrayWithArray:feedArray];
                favouritesList.isDataDirty = [NSNumber numberWithBool:NO];
            }else{
                return [self createEmptyView];
            }
            
        }else{
            
            //new data from network, load another page
            if (data.count > 0) {
                [favouritesList addObjectsFromArray:feedArray];
                
            }else{
                //        hasMoreStories = NO;
            }
        }
        
    }
    
    [self createCollectionView];
}

-(void)createEmptyView{
    
    [self.collectionView removeFromSuperview];
    self.collectionView = nil;
    
    if (emptyView) {
        [emptyView removeFromSuperview];
        emptyView = nil;
    }
    
    
    emptyView = [[OIHEmptyView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_list_outline size:90 color:[UIColor lightGrayColor]] descriptionText:NSLocalizedString(@"HOME_SCREEN_HAS_NO_STORIES", @"")];
    
    emptyView.translatesAutoresizingMaskIntoConstraints = NO;
    //  emptyView.delegate = self;
    [self.view addSubview:emptyView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
    
    
}


-(void)createCollectionView{
    
    
    if (self.collectionView) {
        [self.collectionView reloadData];
    }else{
        
        UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc]init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 0;
        [flowLayout setFooterReferenceSize:CGSizeMake(320.0, 80.0)];
        
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        self.collectionView.backgroundColor = [UIColor whiteColor];
        self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.collectionView registerClass:[EinerCanvasContentCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
        [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerReuseIdentifier];
        
        self.collectionView.dataSource = self;
        self.collectionView.delegate = self;
        self.collectionView.alwaysBounceVertical = YES;

        self.navigationController.hidesBarsOnSwipe = YES;

        _refreshControl = [[UIRefreshControl alloc] init];
        [_refreshControl addTarget:self action:@selector(fetchFavorites) forControlEvents:UIControlEventValueChanged];
        [self.collectionView addSubview:_refreshControl];
        
        
        [self.view addSubview:_collectionView];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_collectionView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_collectionView)]];
        
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    OIHRestaurantMealDetailViewController *vc = [OIHRestaurantMealDetailViewController new];
    vc.mealInfo = self.favouritesList[indexPath.row];
    vc.delegate = self;
    
    [self presentViewController:vc animated:YES completion:nil];
    
}



-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    EinerCanvasContentCollectionViewCell *cell = (EinerCanvasContentCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    OIHMealModel *meal = self.favouritesList[indexPath.row];
    cell.nameLabel.text = meal.name;
    [cell setImage:meal.productImage];
    
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        
        UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerReuseIdentifier forIndexPath:indexPath];
        
        return view;
    }
    return nil;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.favouritesList.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(collectionView.bounds.size.width / 2 , collectionView.bounds.size.width / 2);
    
}

- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark - <OIHMealDetailDelegate>

-(void)userDidCancelOrder: (OIHRestaurantMealDetailViewController *)viewController{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}


@end
