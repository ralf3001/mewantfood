//
//  OIHMapAnnotation.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHMapAnnotation : NSObject <MKAnnotation>
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;

-(id)initWithTitle: (NSString *)title location: (CLLocationCoordinate2D) coordinate;

@end
