//
//  OIHRestaurantInfoButton.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kButtonHeight (50)


@interface OIHRestaurantInfoButton : UIView


@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UIButton *button;

-(id)initWithTitle: (NSString *)title color: (UIColor *)color buttonImage: (UIImage *)image;


@end
