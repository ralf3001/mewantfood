//
//  OIHMerchantRestaurantOrderListTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCSwipeTableViewCell.h"
#import "OIHOrderModel.h"

@interface OIHMerchantRestaurantOrderListTableViewCell : MCSwipeTableViewCell

@property (nonatomic) UILabel *orderStatusLabel;
@property (nonatomic) UILabel *orderItemNameLabel;
@property (nonatomic) UILabel *orderRestaurantLabel;
@property (nonatomic) UILabel *orderRecipientAddressLabel;
@property (nonatomic) UILabel *orderIdLabel;
@property (nonatomic) UILabel *orderDateLabel;
@property (nonatomic) UILabel *orderPriceLabel;

@property (nonatomic) UILabel *orderRecipientLabel;

@property (nonatomic) UIView *orderStatusColorLabel;

@property (nonatomic) OIHOrderModel *orderModel;

-(void)setStatusLabel: (OrderStatus)status;
-(void)setOrderStatus;

@end
