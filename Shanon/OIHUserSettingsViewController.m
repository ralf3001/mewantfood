//
//  OIHUserSettingsViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserSettingsViewController.h"
#import "OIHUserSettingObject.h"
#import "OIHSettingTableViewCell.h"
#import "UIImage+Color.h"
#import "OIHUserReviewOrdersViewController.h"
#import "OIHUserProfileViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "NSDate+JSONExtension.h"
#import "OIHSocialManager.h"
#import "OIHSignUpIntroductionViewController.h"
#import "OIHUserCartOverviewViewController.h"
#import "OIHConversationListViewController.h"

#import "OIHUserCardsListViewController.h"

#import "OIHUserFBFriendsTableViewController.h"
#import "OIHFBCache.h"
#import "OIHFBProfile.h"

#import "OIHSocialAccountsViewController.h"


@interface OIHUserSettingsViewController () <UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, OIHSocialManagerDelegate, OIHIntroDelegate>


@property (nonatomic) UITableView *_tableView;
@property (nonatomic) NSMutableArray *settingsArray;
@property (nonatomic) NSMutableArray *sectionTwoArray;

@property (nonatomic) NSMutableArray *logoutArray;

@property (nonatomic) UIProgressView *progressViewBar;
@property (nonatomic) UIView *progressView;
@property (nonatomic) UIButton *uploadCancelButton;

@property (nonatomic) NSInteger orderNotificationsCount;
@end

@implementation OIHUserSettingsViewController
@synthesize _tableView;

@synthesize settingsArray;
@synthesize sectionTwoArray;
@synthesize logoutArray;

@synthesize progressViewBar;
@synthesize progressView;

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [OIHSocialManager sharedInstance].delegate = nil;

}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textSizeChanged) name:UIContentSizeCategoryDidChangeNotification object:nil];

    [self setupTableView];

}

-(void)textSizeChanged{
    [_tableView reloadData];
}



-(void)fetchUnAckCount{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"unackordersCount" httpMethod:HTTP_GET];
    setting.requestData = @{@"userId": [UserProfile currentUser]._id};
    setting.cachePolicy = IgnoreCache;
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (!error && data) {
            
            self.orderNotificationsCount = [data[@"count"] integerValue];
            
            if (self.orderNotificationsCount > 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:OIHTabBarWillCreateNotificationIcon object:@{@"tab": @4}];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:OIHTabBarWillDismissNotificationIcon object:@{@"tab": @4}];
            }

            BOOL animationsEnabled = [UIView areAnimationsEnabled];
            [UIView setAnimationsEnabled:NO];
            [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
            [UIView setAnimationsEnabled:animationsEnabled];

        }
    }];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    if (![UserProfile currentUser].isMerchant) {
        [self fetchUnAckCount];
    }
    
    settingsArray = [NSMutableArray new];
    
    if ([UserProfile currentUser].isMerchant) {
        
        [settingsArray addObject:[[OIHUserSettingObject alloc] initWithTitle:NSLocalizedString(@"SETTING_USER_PROFILE", @"") method:@selector(userSetting) icon:[IonIcons imageWithIcon:ion_ios_person size:30 color:[UIColor lightGrayColor]]]];

        [settingsArray addObject:[[OIHUserSettingObject alloc] initWithTitle:NSLocalizedString(@"SETTING_SWITCH_TO_CUSTOMER_MODE", @"") method:@selector(switchMode) icon:[IonIcons imageWithIcon:ion_ios_person size:30 color:[UIColor lightGrayColor]]]];

    }else{
        
        [settingsArray addObject:[[OIHUserSettingObject alloc] initWithTitle:NSLocalizedString(@"SETTING_USER_PROFILE", @"") method:@selector(userSetting) icon:[IonIcons imageWithIcon:ion_ios_person size:30 color:[UIColor lightGrayColor]]]];
        
        
        [settingsArray addObject:[[OIHUserSettingObject alloc] initWithTitle:@"Social Accounts" method:@selector(socialAccounts) icon:[IonIcons imageWithIcon:ion_social_apple size:30 color:[UIColor lightGrayColor]]]];

        [settingsArray addObject:[[OIHUserSettingObject alloc] initWithTitle:@"Payment Cards" method:@selector(payment) icon:[IonIcons imageWithIcon:ion_card size:30 color:[UIColor lightGrayColor]]]];

        
        if ([UserProfile currentUser].profilePics.count == 0) {
            [settingsArray addObject:[[OIHUserSettingObject alloc] initWithTitle:NSLocalizedString(@"SETTING_ADD_PROFILE_PIC", @"") method:@selector(uploadProfilePic) icon:[IonIcons imageWithIcon:ion_ios_camera_outline size:30 color:[UIColor lightGrayColor]]]];
            
        }
        

        sectionTwoArray = [NSMutableArray new];
        
        [sectionTwoArray addObject:[[OIHUserSettingObject alloc] initWithTitle:NSLocalizedString(@"SETTING_SWITCH_TO_MERCHANT_MODE", @"") method:@selector(switchMode) icon:[IonIcons imageWithIcon:ion_ios_pricetags size:30 color:[UIColor lightGrayColor]]]];
        
        [sectionTwoArray addObject:[[OIHUserSettingObject alloc] initWithTitle:NSLocalizedString(@"SETTING_CART", @"Cart") method:@selector(shoppingCart) icon:[IonIcons imageWithIcon:ion_ios_cart_outline size:30 color:[UIColor lightGrayColor]]]];

        
        [sectionTwoArray addObject:[[OIHUserSettingObject alloc] initWithTitle:NSLocalizedString(@"SETTING_REVIEW_ORDERS", @"") method:@selector(checkOrders) icon:[IonIcons imageWithIcon:ion_bag size:30 color:[UIColor lightGrayColor]]]];
        
        [sectionTwoArray addObject:[[OIHUserSettingObject alloc] initWithTitle:NSLocalizedString(@"SETTING_MESSAGES", @"Messages") method:@selector(messages) icon:[IonIcons imageWithIcon:ion_ios_chatboxes_outline size:30 color:[UIColor lightGrayColor]]]];

      
        self.orderNotificationsCount = 0;
    }

    
    logoutArray = [NSMutableArray new];
    [logoutArray addObject:[[OIHUserSettingObject alloc] initWithTitle:NSLocalizedString(@"SETTING_LOGOUT", @"") method:@selector(logOut) icon:[IonIcons imageWithIcon:ion_log_out size:30 color:[UIColor lightGrayColor]]]];
    
    
    [_tableView reloadData];

    
}





-(void)setupTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 90;
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 75)];
    
    UILabel *headerLabel = [UILabel new];
    headerLabel.translatesAutoresizingMaskIntoConstraints = NO;
    headerLabel.text = [UserProfile currentUser].name;
    headerLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:50];
    
    [header addSubview:headerLabel];
    
    [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[headerLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(headerLabel)]];
    
    [header addConstraint:[NSLayoutConstraint constraintWithItem:headerLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeLeft multiplier:1.0 constant:15.0f]];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:headerLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeRight multiplier:1.0 constant:-15.0f];
    rightConstraint.priority = 999;
    
    [header addConstraint:rightConstraint];

    
    _tableView.tableHeaderView = header;
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 40)];

    _tableView.tableFooterView = footerView;
    
    [_tableView registerClass:[OIHSettingTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];

}


#pragma mark - <UITableViewDelegate>

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 20;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if ([UserProfile currentUser].isMerchant) {
        return 2;
    }else return 3;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    if ([UserProfile currentUser].isMerchant) {
        switch (section) {
            case 0:
                return settingsArray.count;
            case 1:
                return logoutArray.count;
            default:
                break;
        }

    }else{
        switch (section) {
            case 0:
                return settingsArray.count;
            case 1:
                return sectionTwoArray.count;
            case 2:
                return logoutArray.count;
            default:
                break;
        }
        
    }
    return 0;
}


- (OIHSettingTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHSettingTableViewCell *cell = (OIHSettingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    OIHUserSettingObject *setting;
    
    if ([UserProfile currentUser].isMerchant) {
        switch (indexPath.section) {
            case 0:
                setting = settingsArray[indexPath.row];
                break;
            case 1:
                setting = logoutArray[indexPath.row];
                break;
            default:
                return nil;
        }

    }else{
        
        switch (indexPath.section) {
            case 0:
                setting = settingsArray[indexPath.row];
                break;
            case 1:
                setting = sectionTwoArray[indexPath.row];
                [self rowsForUserOptionsSection:cell indexPath:indexPath];
                break;
            case 2:
                setting = logoutArray[indexPath.row];
                break;
            default:
                return nil;
        }

    }
    
    cell.textLabel.text = setting.settingTitle;
    
    if (setting.settingIconImage) {
        cell.imageView.image = setting.settingIconImage;
    }else{
        cell.imageView.image = [UIImage imageFromColor:[UIColor colorWithWhite:0.8 alpha:1.0f]];
        
    }

    return cell;
}

-(void)rowsForUserOptionsSection: (OIHSettingTableViewCell *)cell indexPath: (NSIndexPath *)indexPath{

    if (indexPath.row == 2) {
        [cell setNotificationNumber:self.orderNotificationsCount];
    }

}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if ([UserProfile currentUser].isMerchant) {
        switch (section) {
            case 0:
                return NSLocalizedString(@"SETTING_HEADER_ACCOUNT", @"");
                break;
            case 1:
                return NSLocalizedString(@"SETTING_LOGOUT", @"Logout");
            default:
                break;
        }

    }else{
        switch (section) {
            case 0:
                return NSLocalizedString(@"SETTING_HEADER_ACCOUNT", @"");
                break;
            case 1:
                return @"";
                break;
            case 2:
                return NSLocalizedString(@"SETTING_LOGOUT", @"Logout");
            default:
                break;
        }
        
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

    if ([UserProfile currentUser].isMerchant) {
        switch (indexPath.section) {
            case 0:{
                OIHUserSettingObject *setting = settingsArray[indexPath.row];
                [self performSelector:setting.settingMethod];
                break;
            }
            case 1:{
                OIHUserSettingObject *setting = logoutArray[indexPath.row];
                [self performSelector:setting.settingMethod];
                break;
            }
            default:
                break;
        }
        
    }else{
        switch (indexPath.section) {
            case 0:{
                
                OIHUserSettingObject *setting = settingsArray[indexPath.row];
                [self performSelector:setting.settingMethod];
                
            }
                break;
            case 1:{
                OIHUserSettingObject *setting = sectionTwoArray[indexPath.row];
                [self performSelector:setting.settingMethod];
            }
                break;
            case 2:{
                OIHUserSettingObject *setting = logoutArray[indexPath.row];
                [self performSelector:setting.settingMethod];
                break;
            }
            default:
                break;
        }
        
#pragma clang diagnostic pop
        
    }

    
    
    
}


-(void)userSetting{
    
    OIHUserProfileViewController *vc = [OIHUserProfileViewController new];
    [self.navigationController pushViewController:vc animated:YES];

}




#pragma mark - <Facebook related>

-(void)connectToFB{
    
    if ([FBSDKAccessToken currentAccessToken]) {
        OIHUserFBFriendsTableViewController *vc = [OIHUserFBFriendsTableViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [self loginFB];
    }

}

-(void)fbUserInfo{

    
}

-(void)loginFB{
    /*
    
    NSDictionary *params = @{
                             @"true": @"installed",
                             @"access_token": @"452393414962037|zEygP_ETCm5OLok66X6bTK-SXr0"
                             };
    
//app_id: 452393414962037
//app_access_token: 452393414962037|zEygP_ETCm5OLok66X6bTK-SXr0
//test-user: 101485070233759
//test-user-3: 134752143568954
    
*/
    [[OIHSocialManager sharedInstance] loginFacebook:self];
    
}

-(void)sendSocialTokenToServer: (FBSDKLoginManagerLoginResult *)result{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"socialtokens" httpMethod:HTTP_POST];
    setting.requestData = @{@"type": @"facebook", @"userId": result.token.userID, @"access_token": result.token.tokenString, @"expiry_date": [result.token.expirationDate JSONDateFromDate]};
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (error) {
            
        }
    }];
    
}

-(void)socialAccounts{
    OIHSocialAccountsViewController *vc = [OIHSocialAccountsViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)checkOrders{

    OIHUserReviewOrdersViewController *vc = [[OIHUserReviewOrdersViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)payment{
    
    OIHUserCardsListViewController *vc = [OIHUserCardsListViewController new];
    [self.navigationController pushViewController:vc animated:YES];

}

-(void)switchMode{
    
    [UserProfile currentUser].isMerchant ^= YES;
    
    [((AppDelegate *)[UIApplication sharedApplication].delegate) switchToMerchant:[UserProfile currentUser].isMerchant];

}

-(void)logOut{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    [((AppDelegate *)[UIApplication sharedApplication].delegate) logout];
    
}

-(void)uploadProfilePic{
    
    UIImagePickerController *imagePickr = [[UIImagePickerController alloc] init];
    imagePickr.delegate = self;
    imagePickr.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePickr animated:YES completion:NULL];
    
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [self createProgressViewBar];
    
    [OIHSocialManager sharedInstance].delegate = self;

    __weak __typeof__(self) weakSelf = self;

    [[OIHSocialManager sharedInstance] uploadProfilePhoto:[chosenImage resizeImage] completionHandler:^(id result, NSError *error) {
        if (!error) {
            // boom
            JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"profilepic" httpMethod:HTTP_POST];
            setting.requestData = result;
            [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
                if (!error) {
                    //done
                    [[NSNotificationCenter defaultCenter] postNotificationName:OIHUserDidUpdateProfileNotification object:nil];
                    [weakSelf.progressViewBar removeFromSuperview];
                }
            }];
        }
    }];
    
}

#pragma mark - <OIHSocialManagerDelegate>

-(void)socialManagerUploadProgress:(float)progress{
    //handled in main thread
    [self.progressViewBar setProgress:progress animated:YES];
}

-(void)createProgressViewBar{

    progressViewBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    progressViewBar.progressTintColor = [UIColor colorWithRed:216/256.0f green:106/256.0f blue:106/256.0f alpha:1.0f];
    progressViewBar.trackTintColor = [UIColor whiteColor];
    progressViewBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.navigationController.view addSubview:progressViewBar];
    
    UIView *nav = self.navigationController.navigationBar;

    [self.navigationController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[nav][progressViewBar(==5)]" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(nav, progressViewBar)]];
    [self.navigationController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[progressViewBar]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(progressViewBar)]];
    
}


-(void)messages{
  
  OIHConversationListViewController *vc = [OIHConversationListViewController new];
  [self.navigationController pushViewController:vc animated:YES];
  
}

-(void)easterEgg{
    
    OIHSignUpIntroductionViewController *vc = [OIHSignUpIntroductionViewController new];
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(void)shoppingCart{
    
    OIHUserCartOverviewViewController *vc = [OIHUserCartOverviewViewController new];
    [self.navigationController pushViewController:vc animated:YES];
    
}



-(void)introWillDismiss:(OIHSignUpIntroductionViewController *)introViewController{
    [introViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
