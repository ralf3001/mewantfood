//
//  OIHNetworkOperation.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHNetworkOperation.h"

@interface OIHNetworkOperation () <NSURLSessionTaskDelegate, NSURLConnectionDelegate>
@property (nonatomic) NSMutableURLRequest *request;
@property (nonatomic) NSURLSession *session;
@property (nonatomic) NSURLSessionDataTask *task;

@end

@implementation OIHNetworkOperation

-(id)initWithRequest: (NSMutableURLRequest *)request setting: (JSONRequestSettings *)setting completionHandler:(void (^)(id data, BOOL isCachedResult, NSError *error))completionHandler{
    
    self = [super init];
    if (self) {
        
        executing = NO;
        finished = NO;
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        NSURLSessionConfiguration* config = [self sessionConfig];

        if ([appDelegate isNetworkReachable]) {
            if (setting.isDataTimely) {
                request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
                config.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
                
            }else{
                request.cachePolicy = NSURLRequestReloadRevalidatingCacheData;
                config.requestCachePolicy = NSURLRequestReloadRevalidatingCacheData;
                
            }
        }
        else{
            request.cachePolicy = NSURLRequestReloadRevalidatingCacheData;
            config.requestCachePolicy = NSURLRequestReloadRevalidatingCacheData;
        }
        
        NSTimer *timer;

        NSMutableDictionary *cacheInfo = [NSMutableDictionary new];
        cacheInfo[@"path"] = request.URL.absoluteString;
        
        if (completionHandler) {
            cacheInfo[@"completionHandler"] = [completionHandler copy];
        }
        
        if (setting.HTTPMethod == HTTP_GET) {
            
            switch (setting.cachePolicy) {
                case CacheFirstThenLoadData:
                    [self returnCache:cacheInfo];
                    break;
                case UseCacheIfTimeout:
                    timer = [NSTimer scheduledTimerWithTimeInterval:0.7
                                                             target:self
                                                           selector:@selector(userTimeout:)
                                                           userInfo:cacheInfo repeats:NO];
                    break;
                case CacheOnly:
                    [self returnCache:cacheInfo];
                    break;
                default:
                    break;
            }
            
        }
        
        config.URLCache = [NSURLCache sharedURLCache];

        NSURLSession* session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];

        _task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            [self setFinishStatus];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!error) {
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    
                    NSError *error = nil;
                    
                    if ([httpResponse statusCode] == 200) {
                        
                        [timer invalidate];
                        
                        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                        
                        if (setting.HTTPMethod == HTTP_GET && dict) {
                          [[OIHOnDiskCache sharedCache] cacheObject:dict withKey:request.URL.absoluteString toDisk:YES];
                        }

                        if (!dict) {
                            [[OIHOnDiskCache sharedCache] deleteCache:request.URL.path];
                        }

                        if (completionHandler) {
                            completionHandler(dict, NO, NULL);
                        }
                        
                    }else{
                        
                        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        if (dict) {
                            error = [NSError errorWithDomain:dict[@"message"] ? dict[@"message"] : @"Error" code:[httpResponse statusCode] userInfo:dict];
                        }
                        
                        if (completionHandler)
                            completionHandler(dict, NO, error);
                        else{
                            NSLog(@"[ERROR]: %@", [NSHTTPURLResponse localizedStringForStatusCode: [httpResponse statusCode]]);
                        }
                    }
                }else{
                    switch (error.code) {
                            
                        case -1004: NSLog(@"Cannot connect to the server");
                            break;
                        default: NSLog(@"[ERROR]: %@", [error localizedDescription]);
                            break;
                    }
                    
                    if (completionHandler)
                        completionHandler(nil, NO, error);
                    else{
                        NSLog(@"[ERROR]: %@", [error localizedDescription]);
                    }
                    
                }
    
            });

            
            
        }];
        
        
    }
    return self;
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler{
    
    
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);

}


-(NSURLSessionConfiguration *)sessionConfig{
    return [NSURLSessionConfiguration defaultSessionConfiguration];
}


-(void)userTimeout: (NSTimer *)timer{
    
    NSDictionary *userInfo = timer.userInfo;
    [self returnCache:userInfo];
    
}

-(void)returnCache: (NSDictionary *)info{
    
    void (^completionHandler)(id data, BOOL isCachedResult, NSError *error);
    
    if (info[@"completionHandler"]) {
        completionHandler = [info[@"completionHandler"] copy];
        
        id cache = [[OIHOnDiskCache sharedCache] loadObjectFromCache:info[@"path"]];
        completionHandler(cache, YES, nil);
    }
    
}


-(void)setFinishStatus{
    
    [self willChangeValueForKey:@"isExecuting"];
    executing = NO;
    [self didChangeValueForKey:@"isExecuting"];
    
    [self willChangeValueForKey:@"isFinished"];
    finished = YES;
    [self didChangeValueForKey:@"isFinished"];
    
    
}



-(void)start{
    
    if ([self isCancelled]){
        // Must move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"finished"];
        finished = YES;
        [self didChangeValueForKey:@"finished"];
        return;
    }
    
    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"executing"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    executing = YES;
    [self didChangeValueForKey:@"executing"];
    
}

-(void)main{
    
  [_task resume];
  [_session finishTasksAndInvalidate];

}

-(BOOL)isAsynchronous{
    return YES;
}

-(BOOL)isConcurrent{
    return YES;    //Default is NO so overriding it to return YES;
}

-(BOOL)isExecuting{
    return executing;
}

-(BOOL)isFinished{
    return finished;
}


@end
