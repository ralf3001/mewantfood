//
//  OIHHomeScreenFeedDataSource.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 27/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHHomeScreenFeedDataSource : NSObject

@property(nonatomic) NSMutableSet *feedAttributedActivities;

@end
