//
//  OIHCompetitionInfo.m
//  Shanon
//
//  Created by Ralf Cheung on 14/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCompetitionInfo.h"

@implementation OIHCompetitionInfo

-(id)initWithCompetitionInfo: (NSDictionary *)info{
    
    if (self = [super init]) {
        
        __id = info[@"_id"];
        _competitionDescription = info[@"competitionDescription"];
        _title = info[@"competitionTitle"];
        _createdDate = [NSDate dateFromJSONDate:info[@"createdAt"]];
        _endDate = [NSDate dateFromJSONDate:info[@"endAt"]];
        _info = info;
        
    }
    return self;
}

-(void)dismissCompetition{
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"competition/dismiss/%@", self._id] httpMethod:HTTP_POST];
    
    [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (error) {
            NSLog(@"[Error]: %@", [error localizedDescription]);
        }
    }];
    
}

-(NSString *)coordinatesToString: (CLLocationCoordinate2D)coordinate{
    return [NSString stringWithFormat:@"%f,%f", coordinate.longitude, coordinate.latitude];
}



@end
