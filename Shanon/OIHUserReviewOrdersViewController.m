//
//  OIHUserReviewOrdersViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserReviewOrdersViewController.h"
#import "OIHReceiptDetailViewController.h"
#import "OIHUserOrderListTableViewCell.h"
#import "OIHOrderModel.h"
#import "NSDate+JSONExtension.h"
#import "OIHRestaurantListViewController.h"

#import "OIHUserOrderInformationTableViewCell.h"


@interface OIHUserReviewOrdersViewController () <UITableViewDataSource, UITableViewDelegate, OIHEmptyViewDelegate>
@property (nonatomic) UITableView *tableView;
@property (assign) BOOL isFetching;
@property (nonatomic) NSMutableArray *orders;
@property (nonatomic) OIHUserModel *friendProfile;
@property (nonatomic) UIRefreshControl *refreshControl;

@property (nonatomic) OIHEmptyView *emptyView;

@property (assign) BOOL hasMoreStories;

@end

@implementation OIHUserReviewOrdersViewController
@synthesize tableView = _tableView;
@synthesize isFetching;
@synthesize orders;
@synthesize friendProfile;
@synthesize refreshControl;



-(id)initWithPersonProfile: (OIHUserModel *)profile{
    
    self = [super init];
    if (self) {
        friendProfile = profile;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    isFetching = NO;
    self.hasMoreStories = YES;
    
    orders = [NSMutableArray new];
    
    [self fetchOrders: nil];
    if (!friendProfile) {
        
        [self setAcknowledgement];
    }
    
    
}


-(void)setAcknowledgement{
    
    JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:@"orderread" httpMethod:HTTP_GET];
    settings.requestData = @{@"userId": [UserProfile currentUser]._id};
    
    [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        if (error) {
            //BOOM
        }else{
            
            [[NSNotificationCenter defaultCenter] postNotificationName:OIHTabBarWillDismissNotificationIcon object:@{@"tab": @4}];
            NSInteger badgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber];
            
            if (badgeNumber > 0) {
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            }
            
        }
    }];
}

-(void)createEmptyView{
    
    if (friendProfile) {
        
        _emptyView = [[OIHEmptyView alloc] initWithImage:[UIImage imageNamed:@"sadface_icon"] descriptionText:[NSString stringWithFormat:NSLocalizedString(@"ORDER_%@_HAS_NO_ORDERS_YET", @"[person] hasn't ordered anything"), friendProfile.userName]];
        
    }else{
        _emptyView = [[OIHEmptyView alloc] initWithImage:[UIImage imageNamed:@"sadface_icon"] descriptionText:NSLocalizedString(@"ORDER_USER_HAS_NO_ORDERS_YET", @"")];
        
    }
    
    _emptyView.translatesAutoresizingMaskIntoConstraints = NO;
    _emptyView.delegate = self;
    [_emptyView setReloadButtonTitle:@"why not?"];
    
    [self.view addSubview:_emptyView];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_emptyView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_emptyView)]];
    
}


-(void)setupTableView{
    
    if (!_tableView) {
        
        [_emptyView removeFromSuperview];
        _emptyView = nil;
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 90;
        _tableView.tableFooterView = [[UIView alloc] init];
        
        [_tableView registerClass:[OIHUserOrderInformationTableViewCell class] forCellReuseIdentifier:@"cell"];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"loading"];

        refreshControl = [UIRefreshControl new];
        [refreshControl addTarget:self action:@selector(fetchOrders) forControlEvents:UIControlEventValueChanged];
        [_tableView addSubview:refreshControl];
        
        [self.view addSubview:_tableView];
        
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
        
    }else{
        [_tableView reloadData];
    }
    
    
}


#pragma mark - <UITableViewDelegate>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    if (self.hasMoreStories) {
        return orders.count + 1;
    }
    return orders.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.row == orders.count) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loading" forIndexPath:indexPath];
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.contentView addSubview:indicator];
        indicator.translatesAutoresizingMaskIntoConstraints = NO;
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:indicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[indicator]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(indicator)]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:80.0f]];
        
        [indicator startAnimating];
        
        return cell;

    }else{
        
        OIHUserOrderInformationTableViewCell *cell = (OIHUserOrderInformationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        OIHOrderModel *orderModel = orders[indexPath.row];
        cell.order = orderModel;
        
//        if ([orderModel.orderRecipient._id isEqualToString:[UserProfile currentUser]._id]) {
//            [cell setAckBubble:orderModel.isAcknowledged];
//        }
        
        if (orderModel.status == Ordered && !friendProfile) {
            
            UILabel *canceledLabel = [UILabel new];
            canceledLabel.textColor = [UIColor whiteColor];
            canceledLabel.text = NSLocalizedString(@"ORDER_CANCELED", @"Canceled");
            [canceledLabel sizeToFit];
            
            
            [cell setSwipeGestureWithView:canceledLabel color:[UIColor orderCanceledColor] mode:MCSwipeTableViewCellModeSwitch state:MCSwipeTableViewCellState3 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
                
                [orderModel updateOrderStatus:OrderCanceled];
                ((OIHUserOrderListTableViewCell *)cell).order = orderModel;
                [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
            }];
        }
        
        return cell;
        
    }
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OIHReceiptDetailViewController *vc = [[OIHReceiptDetailViewController alloc] initWithOrder:orders[indexPath.row]];
    [self.navigationController pushViewController:vc animated:YES];
    
}


#pragma mark - <UIScrollViewDelegate>

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    
    if (scrollView == _tableView && bottomEdge >= scrollView.contentSize.height && isFetching == NO) {
        isFetching = YES;
        [self fetchMore];
    }
    
}


-(void)fetchOrders{
    
    orders = [NSMutableArray new];
    [self fetchOrders:nil];
    
}


-(void)fetchOrders: (NSString *)date{
    
    NSMutableDictionary *postData = [NSMutableDictionary new];
    
    JSONRequestSettings *settings = [[JSONRequestSettings alloc] initWithRoute:@"order" httpMethod:HTTP_GET];
    settings.requestData = postData;
    
    if (friendProfile) {
        postData[@"userId"] = friendProfile._id;
    }else{
        postData[@"userId"] = [UserProfile currentUser]._id;
    }
    
    if (date) {
        postData[@"orderDate"] = date;
        settings.cachePolicy = IgnoreCache;
    }else{
        settings.cachePolicy = CacheFirstThenLoadData;
    }
    
    [[JSONRequest sharedInstance] fetch:settings completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
        
        [refreshControl endRefreshing];
        isFetching = NO;
        if (!error) {
            [self populateData:data isCacheData:isCachedResult];
        }
        
    }];
    
}


-(void)populateData: (NSArray *)data isCacheData: (BOOL) isCache{
    
    NSArray *feedArray = [OIHOrderModel initFromArray:data];
    
    if(isCache){
        
        //initial fetch
        orders = [NSMutableArray arrayWithArray:feedArray];
        orders.isDataDirty = [NSNumber numberWithBool:isCache];
        
    }else{  //new data, flush cache data in the array
        
        if (orders.isDataDirty == [NSNumber numberWithBool:YES]) {
            
            //new data from network
            if (data.count > 0) {
                orders = [NSMutableArray arrayWithArray:feedArray];
                orders.isDataDirty = [NSNumber numberWithBool:NO];
            }else{
                self.hasMoreStories = NO;
                [self emptyView];
            }
            
        }else{
            
            //new data from network, load another page
            if (data.count > 0) {
                [orders addObjectsFromArray:feedArray];
                self.hasMoreStories = YES;
            }else{
                self.hasMoreStories = NO;
                
            }
        }
        
    }
    
    [self setupTableView];
    
}


-(void)fetchMore{
    
    OIHOrderModel *lastOrder = [orders lastObject];
    [self fetchOrders:[lastOrder.orderDate JSONDateFromDate]];
    
}


-(void)emptyViewDidPressReloadButton:(OIHEmptyView *)emptyView{
    
    OIHRestaurantListViewController *vc = [OIHRestaurantListViewController new];
    vc.friendProfile = friendProfile;
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
