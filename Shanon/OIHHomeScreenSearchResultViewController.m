//
//  OIHHomeScreenSearchResultViewController.m
//  Shanon
//
//  Created by Ralf Cheung on 1/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHHomeScreenSearchResultViewController.h"
#import "OIHLocationManager.h"
#import "OIHUserModel.h"
#import "OIHFeedSearchTableViewCell.h"


@interface OIHHomeScreenSearchResultViewController () <UITableViewDataSource, UITableViewDelegate, UISearchControllerDelegate>
@property (nonatomic) NSMutableArray *dataList;
@property (nonatomic) UITableView *tableView;
@property (nonatomic, weak) UISearchController *searchController;

@end

@implementation OIHHomeScreenSearchResultViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _dataList = [NSMutableArray new];
    [self createTableView];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear: animated];

}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString *query = [searchController.searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (!query || query.length == 0) {
        [self unhideTableView];
        [self fetchLatestItems];
    }
}

- (void)presentSearchController:(UISearchController *)searchController{
    _searchController = searchController;
}

-(void)unhideTableView{
    _searchController.searchResultsController.view.hidden = NO;
}


-(void)createTableView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [_tableView registerClass:[OIHFeedSearchTableViewCell class] forCellReuseIdentifier:@"cell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 60.0f;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:_tableView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_tableView)]];
    
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    //[self searchFriend: searchBar.text];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _dataList.count;
}


- (OIHFeedSearchTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OIHFeedSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary *rest = _dataList[indexPath.row];
    
    cell.titleLabel.text = rest[@"restaurantName"];
    cell.detailLabel.text = rest[@"latestItem"][@"name"];
    
    return cell;
}

#pragma mark - <FriendTableViewCellDelegate>
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (section == 0 && _searchController.searchBar.text.length == 0 && _dataList.count > 0) {
        return @" ";
    }
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
 
    if (section == 0 && _searchController.searchBar.text.length == 0 && _dataList.count > 0) {
        
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(8, 0, CGRectGetWidth(self.view.frame), 25);
        label.font = [UIFont systemFontOfSize:14];
        label.text = @"Latest Items Around You";
        
        UIView *headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor whiteColor];
        [headerView addSubview:label];
        return headerView;

    }
    
    return nil;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIViewController *vc = [UIViewController new];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([self.searchController.searchBar isFirstResponder]) {
        [self.searchController.searchBar resignFirstResponder];
    }
}



#pragma mark - Fetch latest items
-(NSString *)coordinatesToString: (CLLocationCoordinate2D)coordinate{
    return [NSString stringWithFormat:@"%f,%f", coordinate.longitude, coordinate.latitude];
}

-(void)fetchLatestItems{
    
    CLLocation *lastLocation = [[OIHLocationManager sharedInstance] lastLocation];
    
    if (lastLocation) {
        
        JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"latestitems" httpMethod:HTTP_GET];
        setting.requestData = @{@"coordinates": [self coordinatesToString:lastLocation.coordinate]};
        
        [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
            if (!error) {
                _dataList = [NSMutableArray arrayWithArray:data];
                [self.tableView reloadData];
            }else{
                NSLog(@"%@", [error localizedDescription]);
            }
        }];
        
    }
    
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if (object == self.searchController.searchBar) {
        if ([keyPath isEqualToString:@"isFirstResponder"]) {
            BOOL isFirstResponder = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
            if (isFirstResponder) {
                [self fetchLatestItems];
            }
        }

    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
