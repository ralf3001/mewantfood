//
//  OIHNetworkOperation.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHNetworkOperation : NSOperation{
    BOOL executing;
    BOOL finished;
}

-(id)initWithRequest: (NSMutableURLRequest *)request setting: (JSONRequestSettings *)setting completionHandler:(void (^)(id data, BOOL isCachedResult, NSError *error))completionHandler;


@end
