//
//  OIHFloatingView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHFloatingView.h"

@implementation OIHFloatingView


-(id)init{
    
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    
    self.layer.shadowRadius = 5.0f;
    self.layer.shadowOpacity = 0.3f;
    self.layer.shadowOffset = CGSizeMake(-5, 5);
    
    self.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0f].CGColor;
    self.layer.borderWidth = 0.5f;
    self.layer.cornerRadius = 3.0f;
    self.backgroundColor = [UIColor whiteColor];
    
}

-(void)setBorderColor:(UIColor *)borderColor{
    self.layer.borderColor = borderColor.CGColor;
}

-(void)setShadowRadius:(CGFloat)shadowRadius{
    
    if (shadowRadius < 0.0f) {
        self.layer.shadowRadius = 5.0f;
    }else{
        self.layer.shadowRadius = shadowRadius;
    }
    
}

-(void)setShadowOpacity:(CGFloat)shadowOpacity{
    
    if (shadowOpacity < 0.0f) {
        self.layer.shadowOpacity = 0.3f;
    }else{
        self.layer.shadowOpacity = shadowOpacity;
    }
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    self.layer.cornerRadius = cornerRadius;
}

-(void)setHighlight: (BOOL)highlighted{
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"shadowOffset"];
    animation.duration = 0.08;
    
    if (highlighted) {
        animation.fromValue = [NSValue valueWithCGSize:CGSizeMake(-5, 5)];
        animation.toValue = [NSValue valueWithCGSize:CGSizeMake(-2, 2)];
        self.layer.shadowOffset = CGSizeMake(-2, 2);
        
    }else{
        
        animation.toValue = [NSValue valueWithCGSize:CGSizeMake(-5, 5)];
        animation.fromValue = [NSValue valueWithCGSize:CGSizeMake(-2, 2)];
        self.layer.shadowOffset = CGSizeMake(-5, 5);
    }
    
    [self.layer addAnimation:animation forKey:@"shadow"];
    
    
}



@end
