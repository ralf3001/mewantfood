//
//  OIHModalPresentationController.m
//  Shanon
//
//  Created by Ralf Cheung on 9/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHModalPresentationController.h"

@implementation OIHModalPresentationController

-(id)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController{
    
    if (self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController]) {
        _dimmingView = [UIView new];
        _dimmingView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        [_dimmingView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimmingViewTapped:)]];
    }
    return self;
}

-(CGRect)frameOfPresentedViewInContainerView{
    CGRect frame = self.containerView.frame;
    return CGRectInset(frame, 30, 80);
}

-(void)presentationTransitionWillBegin{
    
    _dimmingView.frame = self.containerView.frame;
    _dimmingView.alpha = 0.0f;
    
    [self.containerView addSubview:_dimmingView];
    [self.containerView addSubview:[self presentedView]];
    
    id transitionCoordinator = self.presentingViewController.transitionCoordinator;
    [transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        _dimmingView.alpha = 1.0f;
    } completion:nil];
    
}

-(void)presentationTransitionDidEnd:(BOOL)completed{
    if (!completed) {
        [_dimmingView removeFromSuperview];
    }
}

-(void)dismissalTransitionDidEnd:(BOOL)completed{
    if (completed) {
        [_dimmingView removeFromSuperview];
    }
}

-(void)dismissalTransitionWillBegin{
    id transitionCoordinator = self.presentingViewController.transitionCoordinator;
    [transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        _dimmingView.alpha = 0.0f;
    } completion:nil];

}


-(void)dimmingViewTapped: (UITapGestureRecognizer *)gesture{

    if (gesture.state == UIGestureRecognizerStateEnded) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        
    }
}

@end
