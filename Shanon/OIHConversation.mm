//
//  OIHConversation.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHConversation.h"
#import "OIHMessageModel.h"


@implementation OIHConversation

-(id)initWithDictionary: (NSDictionary *)info{
  
  self = [super init];
  if (self) {
    
    self._id = info[@"_id"];
    self.conversationName = info[@"conversationName"];
    
    if ([info[@"users"] isKindOfClass:[NSArray class]]) {
      self.users = [OIHUserModel profilesFromArray:info[@"users"]];
    }
    
    self.lastMessage = [[OIHMessageModel alloc] initWithDictionary:info[@"lastMessage"]];
  
  }
  return self;
}

-(id)initWithLastMessage: (NSDictionary *)info{
  
  self = [super init];
  if (self) {
    
    self._id = info[@"conversation"][@"_id"];
    self.conversationName = info[@"conversation"][@"conversationName"];
    
    if ([info[@"conversation"][@"users"] isKindOfClass:[NSArray class]]) {
      self.users = [OIHUserModel profilesFromArray:info[@"conversation"][@"users"]];
    }
    
    self.lastMessage = [[OIHMessageModel alloc] initWithDictionary:info[@"conversation"][@"lastMessage"]];
    
  }
  return self;

  
}


+(NSArray *)conversationsFromArray: (id)list{
  
  NSMutableArray *array = [NSMutableArray new];
  for (NSDictionary *info in list) {
    [array addObject:[[OIHConversation alloc] initWithDictionary:info]];
  }
  return array;
}

-(void)startConversationWithMessage: (OIHMessageModel *)message user: (OIHUserModel *)user completionHandler:(void (^)(id data, NSError *error))completionHandler{
  
  JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"conversation" httpMethod:HTTP_POST];
  
  setting.requestData = @{@"message": [message toDictionary], @"userId": user._id};
  
  [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
    if (completionHandler) {
      completionHandler(data, error);
    }
  }];
  
}

@end

