//
//  OIHAddressModel.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "OIHModelProtocol.h"


@interface OIHAddressModel : NSObject <OIHModelProtocol>

@property (nonatomic) NSArray *addressComponents;
@property (nonatomic) NSString *userEnteredAddress;
@property (nonatomic) NSString *formattedAddress;

@property (nonatomic) NSString *city;
@property (nonatomic) NSString *postalCode;
@property (nonatomic) NSString *ISOCountryCode;
@property (nonatomic) NSString *country;

@property (nonatomic) NSString *_id;
@property (nonatomic) CLLocationCoordinate2D coordinates;
@property (nonatomic) NSDictionary *addressDictionary;
@property (nonatomic) NSString *addressTag;

-(NSDictionary *)toDictionary;
-(id)initWithAddressInfo: (NSDictionary *)addressInfo;

+(id)addressFromArray: (NSArray *)addresses;

@end
