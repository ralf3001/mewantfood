//
//  OIHCacheObject.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHCacheObject.h"

@implementation OIHCacheObject

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self != nil) {
        self.object = [decoder decodeObjectForKey:@"object"];
        self.key = [decoder decodeObjectForKey:@"key"];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.object forKey:@"object"];
    [encoder encodeObject:self.key forKey:@"key"];

}


@end
