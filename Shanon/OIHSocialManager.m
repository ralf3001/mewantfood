//
//  OIHSocialManager.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 13/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSocialManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "NSDate+JSONExtension.h"
#import "NSString+extraBits.h"
#import <AWSS3/AWSS3.h>

#import "OIHFBCache.h"
#import "OIHFBProfile.h"

@implementation OIHSocialManager

+ (OIHSocialManager *)sharedInstance {
    
    static OIHSocialManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[OIHSocialManager alloc] init];
        [[_sharedInstance notificationCenter]
         addObserver:self
         selector:@selector(_currentProfileChanged:)
         name:FBSDKProfileDidChangeNotification
         object:nil];
        
    });
    return _sharedInstance;
}

- (NSNotificationCenter *)notificationCenter {
    
    return [NSNotificationCenter defaultCenter];
}

-(id)init{
    
    if (self = [super init]) {

        AWSCognitoCredentialsProvider *credentialsProvider =
        [[AWSCognitoCredentialsProvider alloc]
         initWithRegionType:AWSRegionUSEast1
         identityPoolId:@"us-east-1:25e9e557-7b39-41f6-8e83-ff7314b3cbdb"];
        
        AWSServiceConfiguration *configuration =
        [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1
                                    credentialsProvider:credentialsProvider];
        
        [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;

    }
    return self;
}

- (void)loginFacebook:(UIViewController *)vc {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    __weak typeof(self) weakSelf = self;
    
    [login
     logInWithReadPermissions:@[ @"public_profile", @"user_friends" ]
     fromViewController:vc
     handler:^(FBSDKLoginManagerLoginResult *result,
               NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             
             [login logInWithPublishPermissions:@[@"public_actions"] fromViewController:vc handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                
                 if (error) {
                     NSLog(@"Process error");
                 } else if (result.isCancelled) {

                     [self sendSocialTokenToServer:result];
                     
                     if ([weakSelf.delegate
                          respondsToSelector:
                          @selector(
                                    socialManagerDidLogInFacebook)]) {
                              [weakSelf.delegate socialManagerDidLogInFacebook];
                          }

                 } else {
                 
                     [self sendSocialTokenToServer:result];
                     
                     if ([weakSelf.delegate
                          respondsToSelector:
                          @selector(
                                    socialManagerDidLogInFacebook)]) {
                              [weakSelf.delegate socialManagerDidLogInFacebook];
                          }

                 }
                 
                 
             }];
             
         }
     }];
}

+ (void)_currentProfileChanged:(NSNotification *)notification {
    
    FBSDKProfile *profile = notification.userInfo[FBSDKProfileChangeNewKey];
    if (profile) {
        OIHFBProfile *cachedProfile = [[OIHFBProfile alloc] init];
        cachedProfile.profile = profile;
        [OIHFBCache saveItem:cachedProfile];
    }
}

- (void)sendSocialTokenToServer:(FBSDKLoginManagerLoginResult *)result {
    
    [self socialToken:result.token];
}

- (void)socialToken:(FBSDKAccessToken *)token {
    
    JSONRequestSettings *setting =
    [[JSONRequestSettings alloc] initWithRoute:@"socialtokens"
                                    httpMethod:HTTP_POST];
    setting.requestData = @{
                            @"type" : @"facebook",
                            @"userId" : token.userID,
                            @"access_token" : token.tokenString,
                            @"expiry_date" : [token.expirationDate JSONDateFromDate]
                            };
    
    [[JSONRequest sharedInstance]
     fetch:setting
     completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
         if (error) {
             //[[self notificationCenter]
             //postNotificationName:@"BNRBitbucketFetchCompletedNotification"
             //object:self userInfo:userInfo];
         }
     }];
}

- (void)renewFacebookCredentials {
    
    if ([FBSDKAccessToken currentAccessToken]) {
        [self socialToken:[FBSDKAccessToken currentAccessToken]];
    }
}

- (void)fetchFacebookFriends:(OIHRequestHandler)handler {
    
    FBSDKAccessToken *token = [FBSDKAccessToken currentAccessToken];
    
    JSONRequestSettings *settings =
    [[JSONRequestSettings alloc] initWithRoute:@"fbfriends"
                                    httpMethod:HTTP_GET];
    settings.requestData = @{ @"access_token" : token.tokenString };
    settings.cachePolicy = IgnoreCache;
    
    [[JSONRequest sharedInstance]
     fetch:settings
     completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
         handler(data, error);
     }];
}

- (void)facebookLogout {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    [OIHFBCache deleteProfile];
}

#pragma mark - <Upload Pictures to S3>

- (void)uploadStoreImage:(UIImage *)image
       completionHandler:(OIHRequestHandler)handler{
    [self uploadPicture:image bucket:kOIHS3BucketStorefrontImage completionHandler:handler];
}


- (void)uploadProfilePhoto:(UIImage *)image
         completionHandler:(OIHRequestHandler)handler {
    [self uploadPicture:image bucket:kOIHS3BucketName completionHandler:handler];
}

- (void)uploadActivityImage:(UIImage *)image
          completionHandler:(OIHRequestHandler)handler {
    [self uploadPicture:image
                 bucket:kOIHS3BucketActivityImage
      completionHandler:handler];
}

- (void)uploadProductImage:(UIImage *)image
         completionHandler:(OIHRequestHandler)handler {
    [self uploadPicture:image
                 bucket:kOIHS3BucketProductImage
      completionHandler:handler];
}

- (void)uploadOrderAudioFile:(NSData *)audioFile
           completionHandler:(OIHRequestHandler)handler {
    [self uploadAudioFile:audioFile
                   bucket:kOIHS3BucketOrderAudio
        completionHandler:handler];
}

- (void)uploadAudioFile:(NSData *)audio
                 bucket:(NSString *)bucket
      completionHandler:(OIHRequestHandler)handler {
    
    NSURL *fileURL =
    [NSURL fileURLWithPath:[NSTemporaryDirectory()
                            stringByAppendingPathComponent:@"audio"]];
    [audio writeToURL:fileURL atomically:YES];
    
    AWSS3TransferManager *transferManager =
    [AWSS3TransferManager defaultS3TransferManager];
    AWSS3TransferManagerUploadRequest *uploadRequest =
    [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = bucket;
    
    uploadRequest.key = [NSString
                         stringWithFormat:@"%@_%@.aac", [[UserProfile currentUser].email md5],
                         [[[NSDate date] JSONDateFromDate] md5]];
    uploadRequest.body = fileURL;
    
    uploadRequest.ACL = AWSS3BucketCannedACLPublicReadWrite;
    
    __weak typeof(self) weakSelf = self;
    
    uploadRequest.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent,
                                     int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update progress.
            if ([weakSelf.delegate
                 respondsToSelector:@selector(socialManagerUploadProgress:)]) {
                [weakSelf.delegate
                 socialManagerUploadProgress:(double)totalBytesSent /
                 totalBytesExpectedToSend];
            }
        });
    };
    
    [self executeTransferManager:transferManager
               withUploadRequest:uploadRequest
               completionHandler:handler];
}

- (void)uploadPicture:(UIImage *)image
               bucket:(NSString *)bucket
    completionHandler:(OIHRequestHandler)handler {
    
    NSURL *fileURL = [NSURL
                      fileURLWithPath:[NSTemporaryDirectory()
                                       stringByAppendingPathComponent:@"imagetoupload"]];
    NSData *data = UIImagePNGRepresentation(image);
    [data writeToURL:fileURL atomically:YES];
    
    AWSS3TransferManager *transferManager =
    [AWSS3TransferManager defaultS3TransferManager];
    AWSS3TransferManagerUploadRequest *uploadRequest =
    [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = bucket;
    
    uploadRequest.key = [NSString
                         stringWithFormat:@"%@-%@.png", [[UserProfile currentUser].email md5],
                         [[[NSDate date] JSONDateFromDate] md5]];
    uploadRequest.body = fileURL;
    
    uploadRequest.ACL = AWSS3BucketCannedACLPublicReadWrite;
    
    __weak typeof(self) weakSelf = self;
    
    uploadRequest.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent,
                                     int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update progress.
            if ([weakSelf.delegate
                 respondsToSelector:@selector(socialManagerUploadProgress:)]) {
                [weakSelf.delegate
                 socialManagerUploadProgress:(double)totalBytesSent /
                 totalBytesExpectedToSend];
            }
        });
    };
    
    [self executeTransferManager:transferManager
               withUploadRequest:uploadRequest
               completionHandler:handler];
}

- (void)executeTransferManager:(AWSS3TransferManager *)transferManager
             withUploadRequest:
(AWSS3TransferManagerUploadRequest *)uploadRequest
             completionHandler:(OIHRequestHandler)handler {
    
    __weak typeof(self) weakSelf = self;
    
    uploadRequest.ACL = AWSS3BucketCannedACLPublicReadWrite;
    
    uploadRequest.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent,
                                     int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update progress.
            if ([weakSelf.delegate
                 respondsToSelector:@selector(socialManagerUploadProgress:)]) {
                [weakSelf.delegate
                 socialManagerUploadProgress:(double)totalBytesSent /
                 totalBytesExpectedToSend];
            }
        });
    };
    
    [[transferManager upload:uploadRequest]
     continueWithExecutor:[AWSExecutor mainThreadExecutor]
     withBlock:^id(AWSTask *task) {
         if (task.error) {
             if ([task.error.domain
                  isEqualToString:AWSS3TransferManagerErrorDomain]) {
                 switch (task.error.code) {
                     case AWSS3TransferManagerErrorCancelled:
                     case AWSS3TransferManagerErrorPaused:
                         break;
                         
                     default:
                         NSLog(@"Error: %@", task.error);
                         break;
                 }
             } else {
                 // Unknown error.
                 NSLog(@"Error: %@", task.error);
             }
             handler(NULL, task.error);
         }
         
         if (task.result) {
             NSString *url = [NSString
                              stringWithFormat:@"https://s3.amazonaws.com/%@/%@",
                              uploadRequest.bucket,
                              uploadRequest.key];
             
             if ([weakSelf.delegate
                  respondsToSelector:
                  @selector(
                            socialManagerProfilePicDidFinishUploading:
                            )]) {
                      [weakSelf.delegate
                       socialManagerProfilePicDidFinishUploading:@{
                                                                   @"url" : url
                                                                   }];
                  }
             [[NSNotificationCenter defaultCenter]
              postNotificationName:
              OIHSocialManagerUploadDidCompleteNotification
              object:nil];
             handler(@{ @"url" : url }, NULL);
         }
         return nil;
     }];
}

@end
