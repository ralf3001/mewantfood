//
//  UIFont+OIHSystemFonts.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 18/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (OIHSystemFonts)

+(UIFont *)tableViewCellDescriptionFont;
+(UIFont *)tableViewCellContentFont;
+(UIFont *)buttonTextNormal;
+(UIFont *)buttonTextBold;
+(UIFont *)activityButtonNormal;

@end
