//
//  OIHUserRestaurantInfoViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHAddMealViewController.h"
#import "OIHRestaurantEditInfoViewController.h"
#import "OIHRestaurantInfoButton.h"
#import "OIHRestaurantManagementStatisticsViewController.h"
#import "OIHRestaurantMealListViewController.h"
#import "OIHUserFriendsListTableViewController.h"
#import "OIHUserRestaurantInfoViewController.h"
#import "OIHUserRestaurantOrdersViewController.h"
#import "RSA.h"
#import "OIHActivityFeedViewController.h"
#import "OIHImageViewViewController.h"
#import "OIHSocialManager.h"

#define kRestaurantNameTag 0
#define kRestaurantAddressTag 1

@interface OIHUserRestaurantInfoViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, ModalViewControllerDismissDelegate, OIHSocialManagerDelegate>

@property(nonatomic) UIImageView *restaurantLogoImageView;
@property(nonatomic) OIHRestaurantInfoButton *recentOrderButton;
@property(nonatomic) OIHRestaurantInfoButton *editRestaurantDetailsButton;
@property(nonatomic) OIHRestaurantInfoButton *addItemButton;

@property(nonatomic) OIHRestaurantInfoButton *deleteRestaurantButton;

@property(nonatomic) OIHRestaurantInfoButton *addManagingUserButton;

@property(nonatomic) OIHRestaurantInfoButton *restaurantSalesInfoButton;

@property(nonatomic) UILabel *restaurantNameLabel;
@property(nonatomic) UILabel *restaurantAddressLabel;
@property(nonatomic) UIImage *chosenImage;

@property(nonatomic) UIScrollView *scrollView;

@end

@implementation OIHUserRestaurantInfoViewController
@synthesize restaurantInfo;
@synthesize restaurantLogoImageView;
@synthesize recentOrderButton;
@synthesize editRestaurantDetailsButton;
@synthesize addItemButton;
@synthesize deleteRestaurantButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _scrollView = [UIScrollView new];
    _scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_scrollView];
    
    [self.view addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"H:|[_scrollView]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           _scrollView)]];
    
    [self.view addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"V:|[_scrollView]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           _scrollView)]];
    
    _restaurantNameLabel = [UILabel new];
    _restaurantNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _restaurantNameLabel.text = restaurantInfo.restaurantName;
    _restaurantNameLabel.textColor = [UIColor darkGrayColor];
    _restaurantNameLabel.textAlignment = NSTextAlignmentCenter;
    _restaurantNameLabel.font =
    [UIFont preferredFontForTextStyle:UIFontTextStyleTitle2];
    _restaurantNameLabel.numberOfLines = 0;
    [_scrollView addSubview:_restaurantNameLabel];
    
    _restaurantAddressLabel = [UILabel new];
    _restaurantAddressLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _restaurantAddressLabel.text = restaurantInfo.addressInfo;
    _restaurantAddressLabel.textColor = [UIColor lightGrayColor];
    _restaurantAddressLabel.textAlignment = NSTextAlignmentCenter;
    _restaurantAddressLabel.numberOfLines = 0;
    _restaurantAddressLabel.font =
    [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
    [_scrollView addSubview:_restaurantAddressLabel];
    
    restaurantLogoImageView = [[UIImageView alloc] init];
    [restaurantLogoImageView sd_setImageWithURL:[NSURL URLWithString:[restaurantInfo.restaurantPictures firstObject]] placeholderImage:[UIImage imageFromColor:[UIColor highlightColor]]];
    restaurantLogoImageView.contentMode = UIViewContentModeScaleAspectFill;
    restaurantLogoImageView.layer.cornerRadius = 75.0f;
    restaurantLogoImageView.layer.borderWidth = 3.0f;
    restaurantLogoImageView.layer.masksToBounds = YES;
    restaurantLogoImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    restaurantLogoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    restaurantLogoImageView.userInteractionEnabled = YES;
    [_scrollView addSubview:restaurantLogoImageView];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newRestaurantImage)];
    [restaurantLogoImageView addGestureRecognizer:tap];
    
    UIView *container = [UIView new];
    container.translatesAutoresizingMaskIntoConstraints = NO;
    
    recentOrderButton = [[OIHRestaurantInfoButton alloc]
                         initWithTitle:NSLocalizedString(
                                                         @"MANAGEMENT_RESTAURANT_INFO_RECENT_ORDERS",
                                                         @"Review Recent Orders")
                         color:[UIColor colorWithRed:46 / 256.0f
                                               green:65 / 256.0f
                                                blue:114 / 256.0f
                                               alpha:1.0f]
                         buttonImage:[IonIcons imageWithIcon:ion_ios_filing_outline
                                                        size:35
                                                       color:[UIColor whiteColor]]];
    recentOrderButton.translatesAutoresizingMaskIntoConstraints = NO;
    [recentOrderButton.button addTarget:self
                                 action:@selector(recentOrderButtonDidPress)
                       forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:recentOrderButton];
    
    editRestaurantDetailsButton = [[OIHRestaurantInfoButton alloc]
                                   initWithTitle:NSLocalizedString(
                                                                   @"MANAGEMENT_RESTAURANT_INFO_EDIT_DETAILS",
                                                                   @"Edit Details")
                                   color:[UIColor colorWithRed:46 / 256.0f
                                                         green:65 / 256.0f
                                                          blue:114 / 256.0f
                                                         alpha:1.0f]
                                   buttonImage:[IonIcons imageWithIcon:ion_ios_compose_outline
                                                                  size:35
                                                                 color:[UIColor whiteColor]]];
    editRestaurantDetailsButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [editRestaurantDetailsButton.button addTarget:self
                                           action:@selector(editDetail)
                                 forControlEvents:UIControlEventTouchUpInside];
    
    [container addSubview:editRestaurantDetailsButton];
    
    addItemButton = [[OIHRestaurantInfoButton alloc]
                     initWithTitle:NSLocalizedString(@"MANAGEMENT_RESTAURANT_INFO_ADD_DISH",
                                                     @"Add item")
                     color:[UIColor colorWithRed:46 / 256.0f
                                           green:65 / 256.0f
                                            blue:114 / 256.0f
                                           alpha:1.0f]
                     buttonImage:[IonIcons imageWithIcon:ion_ios_plus_empty
                                                    size:35
                                                   color:[UIColor whiteColor]]];
    addItemButton.translatesAutoresizingMaskIntoConstraints = NO;
    [addItemButton.button addTarget:self
                             action:@selector(addMeal)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [container addSubview:addItemButton];
    
    UIView *bottomContainer = [UIView new];
    bottomContainer.translatesAutoresizingMaskIntoConstraints = NO;
    
    _addManagingUserButton = [[OIHRestaurantInfoButton alloc]
                              initWithTitle:NSLocalizedString(
                                                              @"MANAGEMENT_RESTAURANT_INFO_FOOD_CALENDAR",
                                                              @"Upcoming Food Calendar")
                              color:[UIColor colorWithRed:51 / 256.0f
                                                    green:138 / 256.0f
                                                     blue:46 / 256.0f
                                                    alpha:1.0f]
                              buttonImage:[IonIcons imageWithIcon:ion_ios_calendar_outline
                                                             size:35
                                                            color:[UIColor whiteColor]]];
    [_addManagingUserButton.button addTarget:self
                                      action:@selector(addManagingUser)
                            forControlEvents:UIControlEventTouchUpInside];
    _addManagingUserButton.translatesAutoresizingMaskIntoConstraints = NO;
    [bottomContainer addSubview:_addManagingUserButton];
    
    deleteRestaurantButton = [[OIHRestaurantInfoButton alloc]
                              initWithTitle:NSLocalizedString(
                                                              @"MANAGEMENT_RESTAURANT_INFO_DELETE_RESTAURANT",
                                                              @"Delete Restaurant")
                              color:[UIColor colorWithRed:224 / 256.0f
                                                    green:80 / 256.0f
                                                     blue:76 / 256.0f
                                                    alpha:1.0f]
                              buttonImage:[IonIcons imageWithIcon:ion_ios_trash_outline
                                                             size:35
                                                            color:[UIColor whiteColor]]];
    deleteRestaurantButton.translatesAutoresizingMaskIntoConstraints = NO;
    [deleteRestaurantButton.button addTarget:self
                                      action:@selector(deleteRestaurant)
                            forControlEvents:UIControlEventTouchUpInside];
    [bottomContainer addSubview:deleteRestaurantButton];
    
    _restaurantSalesInfoButton = [[OIHRestaurantInfoButton alloc]
                                  initWithTitle:NSLocalizedString(
                                                                  @"MANAGEMENT_RESTAURANT_INFO_SALES_REPORT",
                                                                  @"Sales Report")
                                  color:[UIColor colorWithRed:51 / 256.0f
                                                        green:138 / 256.0f
                                                         blue:46 / 256.0f
                                                        alpha:1.0f]
                                  buttonImage:[IonIcons imageWithIcon:ion_ios_pie_outline
                                                                 size:35
                                                                color:[UIColor whiteColor]]];
    _restaurantSalesInfoButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_restaurantSalesInfoButton.button
     addTarget:self
     action:@selector(didPressSalesInfoButton)
     forControlEvents:UIControlEventTouchUpInside];
    
    [bottomContainer addSubview:_restaurantSalesInfoButton];
    
    UIView *bottomLeftDivider = [UIView new];
    bottomLeftDivider.translatesAutoresizingMaskIntoConstraints = NO;
    [bottomContainer addSubview:bottomLeftDivider];
    
    UIView *bottomFirstSecondDivider = [UIView new];
    bottomFirstSecondDivider.translatesAutoresizingMaskIntoConstraints = NO;
    [bottomContainer addSubview:bottomFirstSecondDivider];
    
    UIView *bottomSecondThirdDivider = [UIView new];
    bottomSecondThirdDivider.translatesAutoresizingMaskIntoConstraints = NO;
    [bottomContainer addSubview:bottomSecondThirdDivider];
    
    UIView *bottomRightDivider = [UIView new];
    bottomRightDivider.translatesAutoresizingMaskIntoConstraints = NO;
    [bottomContainer addSubview:bottomRightDivider];
    
    [_scrollView addSubview:bottomContainer];
    
    UIView *leftDivider = [UIView new];
    leftDivider.translatesAutoresizingMaskIntoConstraints = NO;
    [container addSubview:leftDivider];
    
    UIView *firstSecondDivider = [UIView new];
    firstSecondDivider.translatesAutoresizingMaskIntoConstraints = NO;
    [container addSubview:firstSecondDivider];
    
    UIView *secondThirdDivider = [UIView new];
    secondThirdDivider.translatesAutoresizingMaskIntoConstraints = NO;
    [container addSubview:secondThirdDivider];
    
    UIView *rightDivider = [UIView new];
    rightDivider.translatesAutoresizingMaskIntoConstraints = NO;
    [container addSubview:rightDivider];
    
    [_scrollView addSubview:container];
    
    [_scrollView addConstraint:[NSLayoutConstraint
                                constraintWithItem:restaurantLogoImageView
                                attribute:NSLayoutAttributeWidth
                                relatedBy:NSLayoutRelationEqual
                                toItem:nil
                                attribute:0
                                multiplier:1.0f
                                constant:150.0f]];
    
    [_scrollView addConstraint:[NSLayoutConstraint
                                constraintWithItem:restaurantLogoImageView
                                attribute:NSLayoutAttributeHeight
                                relatedBy:NSLayoutRelationEqual
                                toItem:restaurantLogoImageView
                                attribute:NSLayoutAttributeWidth
                                multiplier:1.0
                                constant:0.0f]];
    
    [_scrollView
     addConstraint:[NSLayoutConstraint
                    constraintWithItem:_restaurantAddressLabel
                    attribute:NSLayoutAttributeWidth
                    relatedBy:NSLayoutRelationEqual
                    toItem:nil
                    attribute:0
                    multiplier:1.0f
                    constant:self.view.frame.size.width * 6 / 8]];
    
    [_scrollView
     addConstraint:[NSLayoutConstraint
                    constraintWithItem:_restaurantNameLabel
                    attribute:NSLayoutAttributeWidth
                    relatedBy:NSLayoutRelationEqual
                    toItem:nil
                    attribute:0
                    multiplier:1.0f
                    constant:self.view.frame.size.width * 6 / 8]];
    
    [_scrollView
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"V:|-20-[_restaurantNameLabel]-[_"
      @"restaurantAddressLabel]-25-["
      @"restaurantLogoImageView]-25-["
      @"container]-25-[bottomContainer]-"
      @"20-|"
      options:NSLayoutFormatAlignAllCenterX
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           _restaurantAddressLabel,
                                           _restaurantNameLabel,
                                           restaurantLogoImageView,
                                           container, deleteRestaurantButton,
                                           bottomContainer)]];
    
    [_scrollView
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"H:|[container(==_scrollView)]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           container, _scrollView)]];
    
    [_scrollView
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:
      @"H:|[bottomContainer(==_scrollView)]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           bottomContainer, _scrollView)]];
    
    [bottomContainer
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"V:|[deleteRestaurantButton]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           deleteRestaurantButton)]];
    
    [bottomContainer
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"H:|[bottomLeftDivider][_"
      @"addManagingUserButton]["
      @"bottomFirstSecondDivider(=="
      @"bottomLeftDivider)]["
      @"deleteRestaurantButton(==_"
      @"addManagingUserButton)]["
      @"bottomSecondThirdDivider(=="
      @"bottomLeftDivider)][_"
      @"restaurantSalesInfoButton]["
      @"bottomRightDivider(=="
      @"bottomLeftDivider)]|"
      options:NSLayoutFormatAlignAllTop |
      NSLayoutFormatAlignAllBottom
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           bottomLeftDivider,
                                           bottomSecondThirdDivider,
                                           bottomFirstSecondDivider,
                                           bottomRightDivider,
                                           _addManagingUserButton,
                                           deleteRestaurantButton,
                                           _restaurantSalesInfoButton)]];
    
    [container addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"V:|[recentOrderButton]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           recentOrderButton)]];
    
    [container
     addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"H:|[leftDivider][recentOrderButton]"
      @"[firstSecondDivider(==leftDivider)"
      @"][editRestaurantDetailsButton(=="
      @"recentOrderButton)]["
      @"secondThirdDivider(==leftDivider)]"
      @"[addItemButton(=="
      @"recentOrderButton)][rightDivider(="
      @"=leftDivider)]|"
      options:NSLayoutFormatAlignAllTop |
      NSLayoutFormatAlignAllBottom
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           recentOrderButton,
                                           editRestaurantDetailsButton,
                                           addItemButton, leftDivider,
                                           firstSecondDivider,
                                           secondThirdDivider,
                                           rightDivider)]];
    
    
}



- (void)addManagingUser {
    
    OIHRestaurantMealListViewController *vc =
    [OIHRestaurantMealListViewController new];
    vc.restaurantInfo = restaurantInfo;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)recentOrderButtonDidPress {
    
    OIHUserRestaurantOrdersViewController *vc =
    [[OIHUserRestaurantOrdersViewController alloc] init];
    vc.restaurantInfo = restaurantInfo;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)editDetail {
    
    OIHRestaurantEditInfoViewController *vc =
    [OIHRestaurantEditInfoViewController new];
    vc.restaurantModel = restaurantInfo;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)addMeal {
    
    OIHAddMealViewController *vc = [OIHAddMealViewController new];
    vc.restaurantInfo = restaurantInfo;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didPressSalesInfoButton {
    
    OIHActivityFeedViewController *vc = [[OIHActivityFeedViewController alloc] initWithRestaurant:restaurantInfo];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)deleteRestaurant {
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Delete?"
                                message:@"Are you sure you want to delete this"
                                preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction
                      actionWithTitle:@"NO"
                      style:UIAlertActionStyleCancel
                      handler:^(UIAlertAction *_Nonnull action){
                          
                      }]];
    
    [alert addAction:[UIAlertAction
                      actionWithTitle:@"YES"
                      style:UIAlertActionStyleDefault
                      handler:^(UIAlertAction *_Nonnull action) {
                          
                          [self confirmDeleteRestaurant];
                          
                      }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)confirmDeleteRestaurant {
    
    JSONRequestSettings *setting = [[JSONRequestSettings alloc]
                                    initWithRoute:[NSString stringWithFormat:@"restaurant/%@",
                                                   self.restaurantInfo._id]
                                    httpMethod:HTTP_DELETE];
    
    [[JSONRequest sharedInstance]
     fetch:setting
     completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
         if (!error) {
             
         } else {
             [SVProgressHUD
              showErrorWithStatus:@"Something's gone wrong, try again."
              maskType:SVProgressHUDMaskTypeBlack];
         }
     }];
}

-(void)newRestaurantImage{
    
    
    UIImagePickerController *imagePickr = [[UIImagePickerController alloc] init];
   
    if ([UIImagePickerController availableMediaTypesForSourceType:
         UIImagePickerControllerSourceTypeCamera]
        .count > 0) {
        
        __weak typeof(self) weakSelf = self;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Pick your image source" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            imagePickr.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePickr.delegate = weakSelf;
            
            [weakSelf presentViewController:imagePickr animated:YES completion:nil];

        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            imagePickr.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imagePickr.delegate = weakSelf;
            
            [weakSelf presentViewController:imagePickr animated:YES completion:nil];

        }]];

        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"CANCEL", @"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        imagePickr.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickr.delegate = self;
        
        [self presentViewController:imagePickr animated:YES completion:nil];

    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    _chosenImage = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    OIHImageViewViewController *vc = [[OIHImageViewViewController alloc] initWithImage:_chosenImage];
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(void)modalViewControllerWillDismissWithCancelOption{
    
}

-(void)modalViewControllerWillDismissWithDoneOption{
    
    [OIHSocialManager sharedInstance].delegate = self;
    [[OIHSocialManager sharedInstance] uploadStoreImage:[_chosenImage resizeImage] completionHandler:^(id result, NSError *error) {
        if (!error) {
            JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"restaurant/%@", restaurantInfo._id] httpMethod:HTTP_PUT];
            setting.requestData = @{@"restaurantPictures": result[@"url"]};
            [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
                [SVProgressHUD dismiss];
                restaurantLogoImageView.image = _chosenImage;
            }];
            
        }else{
            [SVProgressHUD dismiss];
            [SVProgressHUD showErrorWithStatus:@"Error uploading to the server, try again later"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}

#pragma mark - OIHSocialManagerDelegate
- (void)socialManagerUploadProgress:(float)progress{
    [SVProgressHUD showProgress:progress];
}


- (void)authenticateWithTouchID {
    
    NSDictionary *query = @{
                            (__bridge id)kSecClass : (__bridge id)kSecClassGenericPassword,
                            (__bridge id)kSecAttrService :
                                NSLocalizedString(@"USER_PAYMENT_CONFIRMATION", @"User Confirmation"),
                            (__bridge id)kSecReturnData : @YES,
                            (__bridge id)kSecUseOperationPrompt :
                                NSLocalizedString(@"TOUCH_ID_CONFIRM_IDENTITY", nil)
                            };
    
    dispatch_async(
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                       CFTypeRef dataTypeRef = NULL;
                       
                       OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)(query),
                                                             &dataTypeRef);
                       
                       NSData *resultData = (__bridge NSData *)dataTypeRef;
                       NSString *result = [[NSString alloc] initWithData:resultData
                                                                encoding:NSUTF8StringEncoding];
                       NSString *msg = [NSString
                                        stringWithFormat:NSLocalizedString(@"SEC_ITEM_COPY_MATCHING_STATUS",
                                                                           nil),
                                        [self keychainErrorToString:status]];
                       if (resultData)
                           msg = [msg
                                  stringByAppendingString:[NSString
                                                           stringWithFormat:NSLocalizedString(
                                                                                              @"RESULT", nil),
                                                           result]];
                       
                       if (status == noErr) {
                           //[self.delegate passwordRetrieved:result];
                       } else {
                           //[self.delegate passwordRetrieved:nil];
                       }
                       
                   });
}

- (NSString *)keychainErrorToString:(NSInteger)error {
    
    NSString *msg = [NSString stringWithFormat:@"%ld", (long)error];
    
    switch (error) {
        case errSecSuccess:
            msg = NSLocalizedString(@"SUCCESS", nil);
            break;
        case errSecDuplicateItem:
            msg = NSLocalizedString(@"ERROR_ITEM_ALREADY_EXISTS", nil);
            break;
        case errSecItemNotFound:
            msg = NSLocalizedString(@"ERROR_ITEM_NOT_FOUND", nil);
            break;
        case errSecAuthFailed: // this error will be replaced by errSecAuthFailed
            msg = NSLocalizedString(@"ERROR_ITEM_AUTHENTICATION_FAILED", nil);
            
        default:
            break;
    }
    
    return msg;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little
 preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
