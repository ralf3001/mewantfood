//
//  OIHRestaurantInfoRestaurantNameCollectionViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHRestaurantInfoRestaurantNameCollectionViewCell : UICollectionViewCell
@property (nonatomic) UILabel *nameLabel;
@property (nonatomic) UILabel *titleLabel;

@end
