//
//  OIHUserProfileCollectionViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 14/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserProfileCollectionViewCell.h"

@implementation OIHUserProfileCollectionViewCell
@synthesize descriptionContent, descriptionField;
@synthesize moreButton;

-(void)setHighlighted:(BOOL)highlighted{
  
  if (highlighted) {
    self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0f];
  }else{
    self.backgroundColor = [UIColor whiteColor];
  }
  
}


- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        
        descriptionField = [UILabel new];
        descriptionField.translatesAutoresizingMaskIntoConstraints = NO;
        descriptionField.backgroundColor = [UIColor clearColor];
        descriptionField.numberOfLines = 0;
        descriptionField.textColor = [UIColor lightGrayColor];
//        descriptionField.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        descriptionField.textAlignment = NSTextAlignmentLeft;
        descriptionField.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
        [self.contentView addSubview:descriptionField];
        
        descriptionContent = [UILabel new];
        descriptionContent.translatesAutoresizingMaskIntoConstraints = NO;
        descriptionContent.backgroundColor = [UIColor clearColor];
        descriptionContent.numberOfLines = 0;
        descriptionContent.textAlignment = NSTextAlignmentLeft;
//        descriptionContent.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;

        [self.contentView addSubview:descriptionContent];
        

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[descriptionField]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionField)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[descriptionField(==30)][descriptionContent]-20-|" options:NSLayoutFormatAlignAllLeading | NSLayoutFormatAlignAllTrailing metrics:0 views:NSDictionaryOfVariableBindings(descriptionContent, descriptionField)]];

        
    }
    
    return self;
}



-(void)setDescriptionContentText: (NSString *)content{
    
    content = [content stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                (id)[UIFont fontWithName:@"HelveticaNeue-Thin" size:24], NSFontAttributeName,
                                (id)[UIColor lightGrayColor], NSForegroundColorAttributeName, nil];
    
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:content
                                               attributes:attributes];
    
    descriptionContent.attributedText = attrText;
    
}



-(void)setMoreButton{
    
    moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.translatesAutoresizingMaskIntoConstraints = NO;
    [moreButton addTarget:self action:@selector(actionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [moreButton setBackgroundImage:[IonIcons imageWithIcon:ion_ios_more size:20 color:[UIColor grayColor]] forState:UIControlStateNormal];
    [self.contentView addSubview:moreButton];

    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:moreButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:descriptionField attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    

    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:moreButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0f constant:-15.0f]];
    
}


-(void)actionButtonPressed: (UIButton *)button{
    
    if ([self.delegate respondsToSelector:@selector(userDidPressActionButton:cell:)]) {
        [self.delegate userDidPressActionButton:button cell:self];
    }
    
}


-(AddressType)typeEnumFromString: (NSString *)type{
        
    NSDictionary<NSString *, NSNumber *> *types = @{
                                                    @"default": @(UserDefaultAddress),
                                                    @"home": @(UserHomeAddress),
                                                    @"other": @(UserOtherTypes)
                                                    };
    
    return types[[type lowercaseString]].integerValue;
}




@end
