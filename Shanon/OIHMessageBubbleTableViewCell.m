//
//  OIHMessageBubbleTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMessageBubbleTableViewCell.h"

@interface OIHMessageBubbleTableViewCell ()
@property (nonatomic) NSLayoutConstraint *dateLabelBottomConstraint;
@property (nonatomic) NSLayoutConstraint *contentBottomConstraint;
@property (nonatomic) NSLayoutConstraint *leftAlignmentConstraint;
@property (nonatomic) NSLayoutConstraint *rightAlignmentConstraint;

@end

@implementation OIHMessageBubbleTableViewCell
@synthesize contentLabel;


- (void)awakeFromNib {
  [super awakeFromNib];
  // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];
  
  // Configure the view for the selected state
}


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
  
  //    [super setHighlighted:highlighted animated:animated];
  
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
  
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    
    contentLabel = [OIHMessageFooterLabel new];
    contentLabel.translatesAutoresizingMaskIntoConstraints = NO;
    contentLabel.numberOfLines = 0;
    contentLabel.backgroundColor = [UIColor highlightColor];
    contentLabel.layer.masksToBounds = YES;
    contentLabel.clipsToBounds = YES;
    contentLabel.textAlignment = NSTextAlignmentLeft;
    contentLabel.layer.cornerRadius = 10.0f;
    contentLabel.textColor = [UIColor whiteColor];
    
    [self.contentView addSubview:contentLabel];
    
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[contentLabel]" options:0 metrics:0 views:NSDictionaryOfVariableBindings(contentLabel)]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:contentLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationLessThanOrEqual toItem:nil attribute:0 multiplier:1.0f constant:200.0f]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:contentLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:0 multiplier:1.0f constant:80.0f]];
    
    _contentBottomConstraint = [NSLayoutConstraint constraintWithItem:contentLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-10.0f];
    
    [self.contentView addConstraint:_contentBottomConstraint];
    
    [contentLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellTapped)]];
    
    _leftAlignmentConstraint = [NSLayoutConstraint constraintWithItem:contentLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0f constant:15.0f];
    
    _rightAlignmentConstraint = [NSLayoutConstraint constraintWithItem:contentLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0f constant:-15.0f];
    
  }
  
  return self;
}


-(void)setMessageRecevierType: (AuthorType) type{
  
  if (type == MessageFromSender) {
    
    [self.contentView removeConstraint:_rightAlignmentConstraint];
    [self.contentView addConstraint:_leftAlignmentConstraint];
    contentLabel.backgroundColor = [UIColor highlightColor];
    contentLabel.textColor = [UIColor blackColor];
    
    contentLabel.leftInset = 15.0f;
    contentLabel.rightInset = 0;
    
  }else{
    
    [self.contentView removeConstraint:_leftAlignmentConstraint];
    [self.contentView addConstraint:_rightAlignmentConstraint];
    
    contentLabel.backgroundColor = [UIColor bc_colorForBrand:@"facebook"];  //dummy color
    contentLabel.textColor = [UIColor whiteColor];
    
    contentLabel.leftInset = 0;
    contentLabel.rightInset = -15.0f;
    
  }
  
  
}

-(void)setMessage:(OIHMessageModel *)message{
  
  _message = message;
  self.contentLabel.text = message.content;
  [self setMessageRecevierType:message.isSentFromUser];
  
}

-(void)cellTapped{
  
  [_tableView beginUpdates];
  
  if (!_dateLabel) {
    
    _dateLabel = [UILabel new];
    _dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _dateLabel.textColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateStyle = NSDateFormatterShortStyle;
    
    _dateLabel.text = [formatter stringFromDate:_message.createdDate];
    
    [self.contentView addSubview:_dateLabel];
    
    [self.contentView removeConstraint:_contentBottomConstraint];
    
    _contentBottomConstraint = [NSLayoutConstraint constraintWithItem:contentLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_dateLabel attribute:NSLayoutAttributeTop multiplier:1.0f constant:-10.0f];
    
    [self.contentView addConstraint:_contentBottomConstraint];
    
    _dateLabelBottomConstraint = [NSLayoutConstraint constraintWithItem:_dateLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-10.0f];
    
    [self.contentView addConstraint:_dateLabelBottomConstraint];
    
    
    if (_authorType == MessageFromSender) {
      [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_dateLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:contentLabel attribute:NSLayoutAttributeLeft multiplier:1.0f constant:0.0f]];
    }else{
      
      [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_dateLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:contentLabel attribute:NSLayoutAttributeRight multiplier:1.0f constant:0.0f]];
      
    }
    
  }else{
    
    [_dateLabel removeFromSuperview];
    _dateLabel = nil;
    
    _contentBottomConstraint = [NSLayoutConstraint constraintWithItem:contentLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-10.0f];
    
    [self.contentView addConstraint:_contentBottomConstraint];
    
  }
  
  [_tableView endUpdates];
  
  
  
}


@end
