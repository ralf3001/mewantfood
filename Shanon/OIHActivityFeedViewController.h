//
//  OIHActivityFeedViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHRestaurantModel.h"


@interface OIHActivityFeedViewController : UIViewController
@property (nonatomic) OIHRestaurantModel *restaurantInfo;

-(id)initWithRestaurant: (OIHRestaurantModel *)restaurantInfo;

@end
