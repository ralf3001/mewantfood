//
//  UIApplication+EvenAutomator.m
//  Einer
//
//  Created by Ralf Cheung on 6/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "UIApplication+EvenAutomator.h"
#import <objc/runtime.h>

@implementation UIApplication (EvenAutomator)


+ (void)load {

    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        SEL originalSelector = @selector(sendAction:to:from:forEvent:);
        SEL replacementSelector = @selector(heap_sendAction:to:from:forEvent:);
        
        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method replacementMethod = class_getInstanceMethod(class, replacementSelector);
        //    method_exchangeImplementations(originalMethod, replacementMethod);
        
        BOOL didAddMethod = class_addMethod(class, originalSelector, method_getImplementation(replacementMethod), method_getTypeEncoding(replacementMethod));
        
        if (didAddMethod) {
            class_replaceMethod(class, replacementSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
        }

    });
    
    
    
}

- (BOOL)heap_sendAction:(SEL)action to:(id)target from:(id)sender forEvent:(UIEvent *)event {
    NSString *selectorName = NSStringFromSelector(action);
    
    printf("Selector %s occurred.\n", [selectorName UTF8String]);
    return [self heap_sendAction:action to:target from:sender forEvent:event];
}





@end
