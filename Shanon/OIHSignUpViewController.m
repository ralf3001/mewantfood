//
//  OIHSignUpViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHSignUpViewController.h"
#import "NSString+extraBits.h"
#import "UITextField+HighlightFrame.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface OIHSignUpViewController () <UITextFieldDelegate, OIHSignupProtocol>

@property (nonatomic) UIButton *signUpButton;
@property (nonatomic) UITextField *nameField;
@property (nonatomic) UITextField *lastNameField;
@property (nonatomic) UITextField *usernameField;
@property (nonatomic) UITextField *passwordField;

@property CGFloat KEYBOARD_HEIGHT;
@property (nonatomic) UIButton *exitButton;
@property (nonatomic) UILabel *warningMessageLabel;
@property (nonatomic) UIScrollView *scrollView;
@end

@implementation OIHSignUpViewController
@synthesize signUpButton;
@synthesize nameField;
@synthesize usernameField, passwordField, lastNameField;
@synthesize KEYBOARD_HEIGHT;
@synthesize exitButton;
@synthesize warningMessageLabel;
@synthesize scrollView;


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    scrollView = [UIScrollView new];
    scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:scrollView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scrollView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(scrollView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(scrollView)]];
    
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:40];
    [scrollView addSubview:titleLabel];

    
    
    UITapGestureRecognizer *dismissKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [scrollView addGestureRecognizer:dismissKeyboard];
    
    exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exitButton.translatesAutoresizingMaskIntoConstraints = NO;
    [exitButton setImage:[IonIcons imageWithIcon:ion_ios_close_empty size:50 color:[UIColor lightGrayColor]] forState:UIControlStateNormal];
    [exitButton addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:exitButton];
    
    
    
    [UserProfile currentUser].signupDelegate = self;
    
    nameField = [UITextField new];
    nameField.delegate = self;
    nameField.placeholder = NSLocalizedString(@"LOGIN_NAME_PLACEHOLDER", @"");
    nameField.layer.borderWidth = 0.6;
    nameField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    nameField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    nameField.translatesAutoresizingMaskIntoConstraints = NO;
    [scrollView addSubview:nameField];

    
    lastNameField = [UITextField new];
    lastNameField.delegate = self;
    lastNameField.placeholder = NSLocalizedString(@"LOGIN_LAST_NAME_PLACEHOLDER", @"");
    lastNameField.layer.borderWidth = 0.6;
    lastNameField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    lastNameField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    lastNameField.translatesAutoresizingMaskIntoConstraints = NO;
    [scrollView addSubview:lastNameField];

    
    
    usernameField = [UITextField new];
    usernameField.delegate = self;
    usernameField.placeholder = NSLocalizedString(@"LOGIN_EMAIL_PLACEHOLDER", @"");
    usernameField.layer.borderWidth = 0.6;
    usernameField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    usernameField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    usernameField.translatesAutoresizingMaskIntoConstraints = NO;
    usernameField.keyboardType = UIKeyboardTypeEmailAddress;
    [scrollView addSubview:usernameField];
    
    
    passwordField = [UITextField new];
    passwordField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    passwordField.placeholder = NSLocalizedString(@"LOGIN_PASSWORD_PLACEHOLDER", @"");
    passwordField.layer.borderWidth = 0.6;
    passwordField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    passwordField.delegate = self;
    passwordField.secureTextEntry = YES;
    passwordField.translatesAutoresizingMaskIntoConstraints = NO;
    [scrollView addSubview:passwordField];
    
    
  nameField.text = @"Real";
  lastNameField.text = @"Name";
  usernameField.text = @"real@gmail.com";
  passwordField.text = @"12345678";
  
    
  
    warningMessageLabel = [UILabel new];
    warningMessageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    warningMessageLabel.alpha = 0.0f;
    warningMessageLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    warningMessageLabel.textColor = [UIColor redColor];
    [scrollView addSubview:warningMessageLabel];

    
    
    signUpButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [signUpButton setTitle:NSLocalizedString(@"SIGNUP", @"") forState:UIControlStateNormal];
    signUpButton.translatesAutoresizingMaskIntoConstraints = NO;
    [signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signUpButton addTarget:self action:@selector(signup) forControlEvents:UIControlEventTouchUpInside];
    signUpButton.backgroundColor = [UIColor colorWithRed:224/256.0f green:80/256.0f blue:76/256.0f alpha:1.0f];
    [scrollView addSubview:signUpButton];

    
    
    
    [scrollView addConstraint:[NSLayoutConstraint constraintWithItem:exitButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:scrollView attribute:NSLayoutAttributeTop multiplier:1.0f constant:20]];
    
    [scrollView addConstraint:[NSLayoutConstraint constraintWithItem:exitButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:scrollView attribute:NSLayoutAttributeLeft multiplier:1.0f constant:10]];
    [scrollView addConstraint:[NSLayoutConstraint constraintWithItem:exitButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:40.0f]];
    [scrollView addConstraint:[NSLayoutConstraint constraintWithItem:exitButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:40.0f]];

    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[titleLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(titleLabel)]];
    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[lastNameField]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(lastNameField)]];
    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[nameField]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(nameField)]];
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[usernameField]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(usernameField)]];
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[passwordField]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(passwordField)]];
    

    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[warningMessageLabel]-10-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(warningMessageLabel)]];
    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[signUpButton(==scrollView)]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(signUpButton, scrollView)]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:signUpButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]];

    [scrollView addConstraint:[NSLayoutConstraint constraintWithItem:signUpButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:self.view.frame.size.height / 9]];

    
    
    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==titleLabelTop)-[titleLabel]-50-[nameField(==60)][lastNameField(==nameField)][usernameField(==nameField)][passwordField(==nameField)]-[warningMessageLabel]" options:0 metrics:@{@"titleLabelTop": @(self.view.frame.size.height / 5)} views:NSDictionaryOfVariableBindings(titleLabel, lastNameField, nameField, usernameField, passwordField, warningMessageLabel)]];
    
    
}

-(void)dismissKeyboard{
    
    if (usernameField.isFirstResponder) {
        [usernameField resignFirstResponder];
        return;
    }
    
    if (passwordField.isFirstResponder) {
        [passwordField resignFirstResponder];
        return;
    }
 
    if (lastNameField.isFirstResponder) {
        [lastNameField resignFirstResponder];
        return;
    }
    
    if (nameField.isFirstResponder) {
        [nameField resignFirstResponder];
    }

}


-(void) signup{
    
    
    AudioServicesPlaySystemSound (1105);
    //AudioServicesPlaySystemSound (1304);
    
    if (nameField.text.length <= 0) {
        [nameField hightlightBorder:[UIColor redColor]];
        [self setWarningText:NSLocalizedString(@"SIGN_UP_NAME_EMPTY", @"")];
        return;
    }
    
    
    if (lastNameField.text.length <= 0) {
        [lastNameField hightlightBorder:[UIColor redColor]];
        [self setWarningText:NSLocalizedString(@"SIGN_UP_LAST_NAME_EMPTY", @"")];
        return;
    }

    if (usernameField.text.length <= 0) {

        if (![usernameField.text isValidEmail]) {
            [self setWarningText:NSLocalizedString(@"NOT_AN_EMAIL", @"")];
        }else{
            [self setWarningText:NSLocalizedString(@"SIGNUP_EMAIL_FIELD_EMPTY", @"")];
        }

        [usernameField hightlightBorder:[UIColor redColor]];
        
        return;
    }
    
    if (passwordField.text.length < 8) {
        
        [passwordField hightlightBorder:[UIColor redColor]];
        [self setWarningText:NSLocalizedString(@"PASSWORD_LENGTH_FAIL", @"")];
        
        return;
    }

    [SVProgressHUD showWithStatus:NSLocalizedString(@"SIGN_IN", @"Signing in")];
    [[UserProfile currentUser] signup: @{@"email": usernameField.text, @"password": passwordField.text, @"name": nameField.text, @"lastName": lastNameField.text}];
  
}


-(void)exit{
    
    [self dismissViewControllerAnimated:YES completion:nil];

}



-(void)userDidSignUp:(UserProfile *)profile{
  
  [SVProgressHUD dismiss];
  [self.delegate userDidSignUp:profile withSignUpController:self];
  
}

-(void)userSignupFailed:(NSString *)reason{

    dispatch_async(dispatch_get_main_queue(), ^{
        [self setWarningText: reason];
    });

}


-(void)setWarningText: (NSString *)reason{
    
    warningMessageLabel.alpha = 0.0f;
    
    warningMessageLabel.text = reason;
    [UIView animateWithDuration:0.2 animations:^{
        warningMessageLabel.alpha = 1.0f;
    }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)keyboardWillHide:(__unused NSNotification *)inputViewNotification {
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentOffset = CGPointMake(0.0, 0.0);
    }];
}

- (void)keyboardWillShow:(__unused NSNotification *)inputViewNotification {
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentOffset = CGPointMake(0.0, intersection.size.height - 100);
}

-(void)textFieldDidBeginEditing:(UITextField *)sender{
    
    //move the main view, so that the keyboard does not hide it.
    if  (self.view.frame.origin.y >= 0){
        [self setViewMovedUp:YES];
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    if(textField == usernameField && ![textField.text isValidEmail]){
        [textField hightlightBorder:[UIColor redColor]];
        [self setWarningText:NSLocalizedString(@"NOT_AN_EMAIL", @"")];
    
    }

}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    if (textField == passwordField) {
        [self signup];
    }else if(textField == usernameField && ![textField.text isValidEmail]){
        
        [textField hightlightBorder:[UIColor redColor]];
        [self setWarningText:NSLocalizedString(@"NOT_AN_EMAIL", @"")];
    
    }
    
    return YES;
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp){
        
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= KEYBOARD_HEIGHT;
        //rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else{
        // revert back to the normal state.
        rect.origin.y += KEYBOARD_HEIGHT;
        //        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


@end
