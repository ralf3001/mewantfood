//
//  OIHCalendarDay.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHCalendarMealView.h"
#import <UIKit/UIKit.h>
#import "OIHCalendarMealView.h"

@interface OIHCalendarDay : UIView
@property(nonatomic) NSDate *date;
@property(nonatomic) UILabel *dateLabel;
@property(nonatomic) NSMutableArray *mealViewList;

@end
