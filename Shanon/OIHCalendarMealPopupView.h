//
//  OIHCalendarMealPopupView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 6/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHMealModel.h"


@interface OIHCalendarMealPopupView : UIViewController <UIViewControllerTransitioningDelegate>
@property (nonatomic) OIHMealModel *mealInfo;

-(id)initWithMeal: (OIHMealModel *)meal;

@end


@interface CalendarMealPopupAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@property (assign) BOOL presenting;
@property (nonatomic) UIView *snapShot;
@property (nonatomic) UIView *backgroundView;
@property (nonatomic) NSInteger indexOfBackgroundView;

@property (assign) BOOL dismissUp;

@end
