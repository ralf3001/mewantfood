//
//  OIHTabBarViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHTabBarViewController.h"

#define kTabBarHeight 49

@interface OIHTabBarViewController ()
@property(nonatomic) OIHTabBar *tabBar;
@property(nonatomic) NSInteger tabBarHeight;
@end

@implementation OIHTabBarViewController
@synthesize tabBar;

- (id)initWithViewControllers:(NSArray *)viewControllers {
    self = [super init];
    if (self) {
        self.viewControllers = viewControllers;
        [self commonInit];
    }
    return self;
}

-(id)init{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedTab:) name:OIHTabBarWillSelectTabAtIndexNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tabBarHeight = 40;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    tabBar = [[OIHTabBar alloc] initWithTabBarItems:self.viewControllers];
    tabBar.translatesAutoresizingMaskIntoConstraints = NO;
    tabBar.delegate = self;
    [self.view addSubview:tabBar];
    
    [self.view addConstraints:
     [NSLayoutConstraint
      constraintsWithVisualFormat:@"|[tabBar]|"
      options:0
      metrics:0
      views:NSDictionaryOfVariableBindings(
                                           tabBar)]];
    [self.view addConstraint:[NSLayoutConstraint
                              constraintWithItem:tabBar
                              attribute:NSLayoutAttributeHeight
                              relatedBy:NSLayoutRelationEqual
                              toItem:nil
                              attribute:0
                              multiplier:1.0f
                              constant:[self tabBarHeight]]];
    
    [self.view addConstraint:[NSLayoutConstraint
                              constraintWithItem:tabBar
                              attribute:NSLayoutAttributeBottom
                              relatedBy:NSLayoutRelationEqual
                              toItem:self.view
                              attribute:NSLayoutAttributeBottom
                              multiplier:1.0f
                              constant:0.0f]];
    
    self.selectedIndex = self.viewControllers.count - 1;
    [self barItemSelected:0];
}

- (NSInteger)tabBarHeight {
    return _tabBarHeight;
}

- (void)barItemSelected:(NSInteger)index {
    
    if (self.selectedIndex == index) {
        
        UIViewController *vc = self.viewControllers[index];
        if ([vc isKindOfClass:[UINavigationController class]]) {
            [(UINavigationController *)vc popToRootViewControllerAnimated:YES];
        }
        
    } else {
        
        UIViewController *vc = self.viewControllers[index];
        UIViewController *fromVC = self.viewControllers[self.selectedIndex];
        [fromVC.view removeFromSuperview];
        
        CGRect l_RectFrame = self.view.frame;
        l_RectFrame.size.height -=
        [self tabBarHeight]; // so that tab bar doesn't overlap the VC
        vc.view.frame = l_RectFrame;
        
        [self.view insertSubview:vc.view belowSubview:tabBar];
        
        self.selectedIndex = index;
    }
}

- (void)setSelectedTabBarIndex:(NSInteger)index {
    [self barItemSelected:index];
}

- (void)setNotificationForTabBarItem:(NSInteger)tab {
    [tabBar setNotificationForBarItem:tab];
}

- (void)removeNotificationForTabBarItem:(NSInteger)tab {
    [tabBar removeNotificationFroBarItem:tab];
}

-(void)selectedTab:(NSNotification *)notification{
    
    [self setSelectedTabBarIndex:[notification.object[@"tab"] integerValue]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
