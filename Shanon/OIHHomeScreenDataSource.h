//
//  OIHHomeScreenDataSource.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 3/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHHomeScreenDataSource : NSObject <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) NSMutableArray *feedData;
@end
