//
//  EinerCanvasContentCollectionViewCell.m
//  Einer
//
//  Created by Ralf Cheung on 24/8/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "EinerCanvasContentCollectionViewCell.h"

@implementation EinerCanvasContentCollectionViewCell
@synthesize imageView, nameLabel;

- (id)initWithFrame:(CGRect)aRect{
    
    self = [super initWithFrame:aRect];
    
    if (self) {

//        self.layer.borderColor = [UIColor whiteColor].CGColor;
//        self.layer.borderWidth =  1.0f;
        imageView = [[UIImageView alloc] init];
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [imageView setClipsToBounds:YES];
        [self.contentView addSubview:imageView];
        
        
        nameLabel = [UILabel new];
        nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        nameLabel.textColor = [UIColor whiteColor];
        nameLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:nameLabel];

        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:nameLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-8]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[nameLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(nameLabel)]];

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                                    attribute:NSLayoutAttributeHeight
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:imageView
                                                                    attribute:NSLayoutAttributeWidth
                                                                    multiplier:1.0f constant:0.0f]];
    
    }
    
    return self;
}
-(void)setImage: (id)url withType: (ImageType) type{
    switch (type) {
        case URL:
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageFromColor:[UIColor highlightColor]]];
            break;
        case ImageName:
            self.imageView.image = [UIImage imageNamed:url];
            break;
        case Image:
            self.imageView.image = (UIImage *)url;
            break;
        default:
            break;
    }
}

-(void)setImage: (NSString *)url{
    if (url) {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageFromColor:[UIColor highlightColor]]];
    }else{
        [LoremIpsum asyncPlaceholderImageWithSize:CGSizeMake(300, 300)
                                       completion:^(UIImage *image) {
                                           self.imageView.image = image;
                                       }];
    }
  
}

@end
