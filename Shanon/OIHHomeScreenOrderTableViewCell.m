//
//  OIHHomeScreenOrderTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 24/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHHomeScreenOrderTableViewCell.h"
#include "EZAudio.h"
#import "OIHAudioPlayer.h"
#import "AppDelegate.h"

@interface OIHHomeScreenOrderTableViewCell () <EZAudioPlayerDelegate>
@property (nonatomic) AVPlayer *player;

@end

@implementation OIHHomeScreenOrderTableViewCell
@synthesize thanksButton;
@synthesize commentButton;
@synthesize imageView;

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    
    [_floatingView setHighlight:highlighted];
    
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
    
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        _floatingView = [OIHFloatingView new];
        _floatingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_floatingView];
        
        imageView = [UIImageView new];
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.layer.borderColor = [UIColor colorWithWhite:0.95 alpha:1.0f].CGColor;
        imageView.layer.borderWidth = 2.0f;
        imageView.layer.masksToBounds = YES;
        
        [_floatingView addSubview:imageView];
        
        _timeLabel = [UILabel new];
        _timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _timeLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
        _timeLabel.textColor = [UIColor darkGrayColor];
        [_floatingView addSubview:_timeLabel];
        
        
        _descriptionLabel = [UILabel new];
        _descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _descriptionLabel.numberOfLines = 0;
        _descriptionLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
        
        [_floatingView addSubview:_descriptionLabel];
        
        
        _audioButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _audioButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_audioButton setImage:[IonIcons imageWithIcon:ion_ios_volume_high size:30.0f color:[UIColor colorWithWhite:0.8 alpha:1.0f]] forState:UIControlStateNormal];
        [_audioButton addTarget:self action:@selector(playAudio) forControlEvents:UIControlEventTouchUpInside];
        
        [_floatingView addSubview:_audioButton];
        
        
        thanksButton = [UIButton buttonWithType:UIButtonTypeSystem];
        thanksButton.titleLabel.font = [UIFont activityButtonNormal];
        [thanksButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        thanksButton.translatesAutoresizingMaskIntoConstraints = NO;
        [thanksButton setTitle:NSLocalizedString(@"ACTIVITY_BUTTON_THANKS", @"Thanks!") forState:UIControlStateNormal];
        [thanksButton addTarget:self action:@selector(thanksButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_floatingView addSubview:thanksButton];
        
        
        commentButton = [UIButton buttonWithType:UIButtonTypeSystem];
        commentButton.translatesAutoresizingMaskIntoConstraints = NO;
        commentButton.titleLabel.font = [UIFont activityButtonNormal];
        [commentButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [commentButton setTitle:NSLocalizedString(@"ACTIVITY_BUTTON_COMMENT", @"Comment") forState:UIControlStateNormal];
        [commentButton addTarget:self action:@selector(commentButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_floatingView addSubview:commentButton];
        
        
        UIView *buttonHorizontalDivider = [UIView new];
        buttonHorizontalDivider.translatesAutoresizingMaskIntoConstraints = NO;
        buttonHorizontalDivider.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0f].CGColor;
        buttonHorizontalDivider.layer.borderWidth = 0.3f;
        [_floatingView addSubview:buttonHorizontalDivider];
        
        
        UIView *verticalDivider = [UIView new];
        verticalDivider.translatesAutoresizingMaskIntoConstraints = NO;
        verticalDivider.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0f].CGColor;
        verticalDivider.layer.borderWidth = 0.3f;
        [_floatingView addSubview:verticalDivider];
        
        UIView *buttonBottomHorizontalDivider = [UIView new];
        buttonBottomHorizontalDivider.translatesAutoresizingMaskIntoConstraints = NO;
        buttonBottomHorizontalDivider.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0f].CGColor;
        buttonBottomHorizontalDivider.layer.borderWidth = 0.3f;
        [_floatingView addSubview:buttonBottomHorizontalDivider];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_floatingView]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_floatingView)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_floatingView]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_floatingView)]];
        
        [_floatingView addConstraint:[NSLayoutConstraint constraintWithItem:_audioButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_audioButton attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView)]];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_descriptionLabel]-[_audioButton(==30)]-|" options:NSLayoutFormatAlignAllTop metrics:0 views:NSDictionaryOfVariableBindings(_descriptionLabel, _audioButton)]];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_timeLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_timeLabel)]];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[buttonHorizontalDivider]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(buttonHorizontalDivider)]];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[buttonBottomHorizontalDivider]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(buttonBottomHorizontalDivider)]];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[thanksButton][verticalDivider(==1)][commentButton(==thanksButton)]-|" options:NSLayoutFormatAlignAllCenterY | NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(thanksButton, commentButton, verticalDivider)]];
        
        [_floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_descriptionLabel]-[_timeLabel]-[imageView(==150)]-[buttonHorizontalDivider(==1)]-[thanksButton]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(imageView, _descriptionLabel, _timeLabel, buttonBottomHorizontalDivider, buttonHorizontalDivider, thanksButton)]];
        
    }
    
    return self;
}

-(void)setFeed:(OIHActivityFeed *)feed{
    
    _feed = feed;
    
    switch (feed.type) {
            
        case UserOrder:{
            
            if ([feed.fromUser._id isEqualToString:[UserProfile currentUser]._id]) {
                if (feed.order.quantity > 1) {
                    
                    _descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"YOU_ORDERED_%@_%@", @"You order [number] [dish name]"),
                                              [NSString stringWithFormat:@"%ld", (long)feed.order.quantity],
                                              feed.order.orderMeal.name
                                              ];
                    
                }else{
                    _descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"YOU_ORDERED_A_%@", @"You order a [dish name]"),
                                              feed.order.orderMeal.name];
                }
                
            }else{
                
                if (feed.order.quantity > 1) {
                    
                    _descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_ORDERED_%@_%@", @"[person] order [number] [dish name]"),
                                              feed.fromUser.userName,
                                              [NSString stringWithFormat:@"%ld", (long)feed.order.quantity],
                                              feed.order.orderMeal.name
                                              ];
                    
                }else{
                    _descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_ORDERED_A_%@", @"[person] order a [dish name]"),
                                              feed.fromUser.userName,
                                              feed.order.orderMeal.name];
                }
                
                
            }
            
            
            if(feed.order.orderMeal.productImage){
                [self.imageView sd_setImageWithURL:[NSURL URLWithString:feed.order.orderMeal.productImage] placeholderImage:[UIImage imageFromColor:[UIColor colorWithWhite:0.8 alpha:1.0f]]];
            }else{
                self.imageView.image = [UIImage defaultImage];
            }
            
            
            break;
        }
        case NewItem:{
            
            _descriptionLabel.text = [NSString stringWithFormat: NSLocalizedString(@"%@_POSTED_A_NEW_ITEM_%@_%@", @""), feed.fromUser.userName, feed.meal.name, feed.meal.restaurant.restaurantName];
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:feed.postPicture] placeholderImage:[UIImage imageFromColor:[UIColor colorWithWhite:0.8 alpha:1.0f]]];
            
            break;
        }
        default:
            break;
    }
    
    _audioButton.enabled = self.feed.order.audioNoteURL ? YES : NO;
    
    if (!self.feed.order.audioNoteURL) {
        _audioButton.alpha = 0.0f;
    }else{
        _audioButton.alpha = 1.0f;
    }
    
    self.timeLabel.text = [NSDate daysBetweenDate:feed.createdAt andDate:[NSDate date]];
    
    if (feed.isPostThanked) {
        thanksButton.titleLabel.font = [UIFont buttonTextBold];
    }else{
        thanksButton.titleLabel.font = [UIFont activityButtonNormal];
        
    }
    
    
}


-(void)playAudio{
    
    if (self.feed.order.audioNoteURL) {
        
        
        if (([[[OIHAudioPlayer audioPlayer] currentPlaying] isEqualToString:self.feed.order.audioNoteURL] && [[OIHAudioPlayer audioPlayer] isPlaying]) || (![(AppDelegate *)[[UIApplication sharedApplication] delegate] isNetworkReachable])) {
            
            [[OIHAudioPlayer audioPlayer] stop];
            [_audioButton setImage:[IonIcons imageWithIcon:ion_ios_volume_high size:30.0f color:[UIColor colorWithWhite:0.8 alpha:1.0f]] forState:UIControlStateNormal];
            [self stopFlashAnimation:_audioButton];
            
        }else{
            
            [[OIHAudioPlayer audioPlayer] playAudioWithURLString:self.feed.order.audioNoteURL];
            [_audioButton setImage:[IonIcons imageWithIcon:ion_ios_volume_high size:30.0f color:[UIColor colorWithWhite:0.3 alpha:1.0f]] forState:UIControlStateNormal];
            [self flashAnimationForView:_audioButton];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(playerItemDidReachEnd:)
                                                         name:AVPlayerItemDidPlayToEndTimeNotification
                                                       object:[_player currentItem]];
            
        }
        
        
    }
    
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (object == [OIHAudioPlayer audioPlayer]) {
        
        [keyPath isEqualToString:@"status"];
        
    }
}


-(void)playerItemDidReachEnd: (NSNotification *)notification{
    
    [_audioButton setImage:[IonIcons imageWithIcon:ion_ios_volume_high size:30.0f color:[UIColor colorWithWhite:0.8 alpha:1.0f]] forState:UIControlStateNormal];
    [self stopFlashAnimation:_audioButton];
    
}

-(void)flashAnimationForView: (UIView *)view{
    
    CABasicAnimation *flash = [CABasicAnimation animationWithKeyPath:@"opacity"];
    flash.fromValue = @(0.0f);
    flash.toValue = @(1.0f);
    flash.duration = 1.0f;
    flash.autoreverses = YES;
    flash.repeatCount = FLT_MAX;
    [view.layer addAnimation:flash forKey:@"flashAnimation"];
    
}

-(void)stopFlashAnimation: (UIView *)view{
    [view.layer removeAnimationForKey:@"flashAnimation"];
}


-(void)commentButtonPressed: (UIButton *)button{
    
    if ([self.delegate respondsToSelector:@selector(userDidTapCommentButton:)]) {
        [self.delegate userDidTapCommentButton:self.feed];
    }
    
}

-(void)thanksButtonPressed: (UIButton *)button{
    
    [self animate];
    
    if (self.feed.isPostThanked == YES) {
        
        thanksButton.titleLabel.font = [UIFont activityButtonNormal];
        
        if ([self.delegate respondsToSelector:@selector(userDidTapThanksButton:isPostThanked:)]) {
            [self.delegate userDidTapThanksButton:self.feed isPostThanked:NO];
        }
        self.feed.isPostThanked = NO;
        
    }else{
        
        thanksButton.titleLabel.font = [UIFont buttonTextBold];
        
        if ([self.delegate respondsToSelector:@selector(userDidTapThanksButton:isPostThanked:)]) {
            [self.delegate userDidTapThanksButton:self.feed isPostThanked:YES];
        }
        self.feed.isPostThanked = YES;
        
    }
    
}


-(void)animate{
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.autoreverses = YES;
    animation.duration = 0.1;
    animation.fromValue = @(1.0f);
    animation.toValue = @(1.5f);
    animation.fillMode = kCAFillModeForwards;
    
    [thanksButton.layer addAnimation:animation forKey:@"scale"];
    
}


@end
