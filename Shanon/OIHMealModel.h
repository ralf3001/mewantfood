//
//  OIHMealModel.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHRestaurantModel.h"
#import "OIHModelProtocol.h"

typedef NS_ENUM(NSInteger, MealType) {
    
    Breakfast = 0,
    Lunch = 1,
    Dinner = 2,
    Snack = 3,
    MealInvalid = 1 << 4
};


@interface OIHMealModel : NSObject <OIHModelProtocol>

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *name;
@property (nonatomic) NSDate *availableDate;
@property (nonatomic) NSDate *availableEndDate;
@property (nonatomic) NSString *restaurantId;
@property (nonatomic) NSNumber *priceInLocalCurrency;
@property (nonatomic) NSString *priceInString;
@property (nonatomic) OIHRestaurantModel *restaurant;
@property (nonatomic) NSDictionary *infoDictionary;
@property (nonatomic) NSString *productImage;
@property (nonatomic) MealType mealType;
@property (nonatomic) NSNumber *chargeRate;

@property (nonatomic) NSString *productIngredients;
@property (nonatomic) NSString *productNote;
@property (nonatomic) NSString *productNetWeight;
@property (nonatomic) NSNumber *productQuantity;

@property (nonatomic) BOOL isFavourited;

-(id)initWithInfoDictionary: (NSDictionary *)info;

-(void)setAttribute: (NSString *) attribute data: (id)data;

+(NSMutableArray <OIHMealModel *>*) mealsFromArray: (id)data;

-(NSString *)mealTypeString;

@end
