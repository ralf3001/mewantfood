//
//  OIHDropDownMenuTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 12/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHDropDownMenuTableViewCell.h"
#import "UIImage+Color.h"

@implementation OIHDropDownMenuTableViewCell
@synthesize notificationCount, notificationDot;
@synthesize menuLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor blackColor];
        
        menuLabel = [UILabel new];
        menuLabel.translatesAutoresizingMaskIntoConstraints = NO;
        menuLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        [self.contentView addSubview:menuLabel];
        
        notificationDot = [UIView new];
        notificationDot.translatesAutoresizingMaskIntoConstraints = NO;
        notificationDot.backgroundColor = [UIColor redColor];
        notificationDot.layer.cornerRadius = 4.0f;
        notificationDot.layer.masksToBounds = YES;
        [self.contentView addSubview:notificationDot];

        
        notificationCount = [UILabel new];
        notificationCount.translatesAutoresizingMaskIntoConstraints = NO;
        notificationCount.layer.cornerRadius = 13;
        notificationCount.layer.masksToBounds = YES;
        notificationCount.textColor = [UIColor whiteColor];
        notificationCount.backgroundColor = [UIColor redColor];
        notificationCount.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:notificationCount];

        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:notificationDot attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:8]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:notificationDot attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:notificationDot attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:notificationDot attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
        
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:notificationCount attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];

        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[notificationDot(==8)]-10-[menuLabel]-[notificationCount(==26)]-|" options:NSLayoutFormatAlignAllCenterY metrics:0 views:NSDictionaryOfVariableBindings(notificationDot, menuLabel, notificationCount)]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:notificationCount attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:notificationCount attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0]];

        
    }
    return self;
}


-(void)setNotification: (NSInteger)count notificationDot: (BOOL)displayDot{
    
    if (count > 0) {
      
        notificationCount.text = [NSString abbreviateNumber:count];

        if (displayDot == NO) {
            notificationDot.backgroundColor = [UIColor clearColor];
            notificationCount.backgroundColor = [UIColor clearColor];
        }else{
            notificationCount.backgroundColor = [UIColor redColor];
        }
        
    }else{
        notificationDot.backgroundColor = [UIColor clearColor];
        notificationCount.backgroundColor = [UIColor clearColor];
    }
    
}


@end
