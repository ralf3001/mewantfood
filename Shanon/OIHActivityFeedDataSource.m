//
//  OIHActivityFeedDataSource.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 8/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHActivityFeedDataSource.h"
#import "OIHActivityFeed.h"
#import "OIHHomeScreenReviewTableViewCell.h"
#import "OIHEmptyView.h"


@interface OIHActivityFeedDataSource ()
@property (nonatomic) NSMutableArray <OIHActivityFeed *> *activityFeed;
@property (nonatomic, assign) BOOL isFetching;
@property (assign) BOOL hasMoreStories;
@property (nonatomic) OIHEmptyView *emptyView;

@end

@implementation OIHActivityFeedDataSource
@synthesize emptyView;

-(id)initWithRestaurant: (OIHRestaurantModel *)restaurantInfo{
  
  self = [super init];
  if (self) {
    _restaurantInfo = restaurantInfo;
    _activityFeed = [NSMutableArray new];
    _hasMoreStories = YES;

  }
  return self;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  
  // Return the number of sections.
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

  // Return the number of rows in the section.
  NSInteger rows = _hasMoreStories ? _activityFeed.count : (_activityFeed.count + 1);
  return rows;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  if (indexPath.row == _activityFeed.count) {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoMoreDataCell" forIndexPath:indexPath];
    
    cell.textLabel.text = NSLocalizedString(@"ACTIVITY_NO_MORE_STORIES", @"no more stories");
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1.0f];
    cell.userInteractionEnabled = NO;
    
    return cell;
    
  }else{

    OIHHomeScreenReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reviewCell" forIndexPath:indexPath];
    OIHActivityFeed *activity = _activityFeed[indexPath.row];
    cell.feed = activity;

    return cell;
  
  }
  
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
  if (indexPath.row != _activityFeed.count) {
    if ([self.delegate respondsToSelector:@selector(tableViewDidSelectRowForReview:)]) {
      [self.delegate tableViewDidSelectRowForReview: _activityFeed[indexPath.row]];
    }
  }
  
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
  
  CGFloat actualPosition = scrollView.contentOffset.y;
  CGFloat contentHeight = scrollView.contentSize.height - (scrollView.frame.size.height);
  
  if (_isFetching == NO && actualPosition >= contentHeight && contentHeight > scrollView.frame.size.height) {
    _isFetching = YES;
    [self fetchMoreActivities];
  }
  
}

-(void)fetchMoreActivities{
  
  OIHActivityFeed *lastActivity = [_activityFeed lastObject];
  [self fetchActivities:[lastActivity.createdAt JSONDateFromDate]];
  
}


-(void)fetchActivities: (NSString *)date{
  
  NSMutableDictionary *getData = [NSMutableDictionary new];
  JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:@"restaurantfeed" httpMethod:HTTP_GET];
  
  if (date) {
    getData[@"feedDate"] = date;
    setting.cachePolicy = IgnoreCache;
  }else{
    setting.cachePolicy = CacheFirstThenLoadData;
  }
  
  setting.requestData = getData;
  
  
  [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
    
    _isFetching = NO;
    if (!error) {
      [self populateData:data isCacheData:isCachedResult];
    }
  }];
  
}

-(void)populateData: (NSArray *)data isCacheData: (BOOL) isCache{
  
  NSArray *feedArray = [OIHActivityFeed activitiesFromArray:data];
  
  if(isCache){
    
    //initial fetch
    _activityFeed = [NSMutableArray arrayWithArray:feedArray];
    _activityFeed.isDataDirty = [NSNumber numberWithBool:isCache];
    
  }else{  //new data, flush cache data in the array
    
    if (_activityFeed.isDataDirty == [NSNumber numberWithBool:YES]) {
      
      //new data from network
      if (data.count > 0) {
        _activityFeed = [NSMutableArray arrayWithArray:feedArray];
        _activityFeed.isDataDirty = [NSNumber numberWithBool:NO];
      }else{
        _hasMoreStories = NO;
      }
      
    }else{
      
      //new data from network, load another page
      if (data.count > 0) {
        [_activityFeed addObjectsFromArray:feedArray];
        
      }else{
        _hasMoreStories = NO;
      }
    }
    
  }

  [_tableView reloadData];
  
}


-(void)createEmptyView{
  
  if (emptyView) {
    [emptyView removeFromSuperview];
    emptyView = nil;
  }
  
  [_tableView removeFromSuperview];
  _tableView = nil;
  
  emptyView = [[OIHEmptyView alloc] initWithImage:[IonIcons imageWithIcon:ion_ios_list_outline size:90 color:[UIColor lightGrayColor]] descriptionText:NSLocalizedString(@"HOME_SCREEN_HAS_NO_STORIES", @"")];
  
  emptyView.translatesAutoresizingMaskIntoConstraints = NO;
//  emptyView.delegate = self;
//  [self.view addSubview:emptyView];
//  
//  [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
//  [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[emptyView]|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(emptyView)]];
  
  
}



@end
