//
//  OIHRestaurantListViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 13/11/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHUserModel.h"


@interface OIHRestaurantListViewController : UIViewController
@property (nonatomic) NSString *userId;
//if buying for a friend, this will be his/her userid, if not it will be null (buying for user him/herself)
@property (nonatomic) OIHUserModel *friendProfile;

@end
