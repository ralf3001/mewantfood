//
//  PAPConstants.h
//  Anypic
//
//  Created by Mattieu Gamache-Asselin on 5/25/12.
//  Copyright (c) 2013 Parse. All rights reserved.
//
#import <UIKit/UIKit.h>
typedef enum {
	PAPHomeTabBarItemIndex = 0,
	PAPEmptyTabBarItemIndex = 1,
	PAPActivityTabBarItemIndex = 2
} PAPTabBarControllerViewControllerIndex;


#define kEinerUserInfoViewWidth 90


extern NSString *const OIHFacebookAppId;

extern NSString *const kOIHS3BucketName;
extern NSString *const kOIHS3BucketActivityImage;
extern NSString *const kOIHS3BucketProductImage;
extern NSString *const kOIHS3BucketOrderAudio;
extern NSString *const kOIHS3BucketStorefrontImage;

extern NSString *const kOIHRSAPublicKey;

extern NSString *const kOIHPublicKey;

extern NSString *const kEinerApplicationID;
extern NSString *const kEinerClientKey;
extern NSString *const kEinerTwitterConsumerKey;
extern NSString *const kEinerTwitterConsumerSecret;
extern NSString *const kOIHTwitterConsumerKey;
extern NSString *const kOIHTwitterConsumerSecret;


#pragma mark - NSUserDefaults
extern NSString *const kPAPUserDefaultsActivityFeedViewControllerLastRefreshKey;
extern NSString *const kPAPUserDefaultsCacheFacebookFriendsKey;

#pragma mark - Launch URLs

extern NSString *const kPAPLaunchURLHostTakePicture;


#pragma mark - NSNotification


extern NSString *const EinerUserDidUpdateProfilePictureNotification;

extern NSString *const OIHUpdateOrderNotification;
extern NSString *const OIHUserDidReceiveANewGiftOrder;
extern NSString *const OIHTabBarWillCreateNotificationIcon;
extern NSString *const OIHTabBarWillDismissNotificationIcon;
extern NSString *const OIHTabBarWillSelectTabAtIndexNotification;

extern NSString *const OIHViewControllerPresentModalViewControllerNotification;


extern NSString *const OIHUserDidReceiveFriendRequestsNotification;

extern NSString *const OIHUserDidUpdateProfileNotification;

extern NSString *const OIHSocialManagerUploadDidCompleteNotification;

extern NSString *const OIHMessageUserIsTypingNotification;
extern NSString *const OIHMessageUserWillFetchMessage;

extern NSString *const OIHAudioPlayerAVPlayerStatusDidFail;
extern NSString *const OIHAudioPlayerAVPlayerStatusDidPlay;
extern NSString *const OIHAudioPlayerAVPlayerStatusDidFail;

#pragma mark - Installation Class

// Field keys
extern NSString *const kPAPInstallationUserKey;


#pragma mark - PayPal API Keys
extern NSString *const kOIHPayPalClientIDSandbox;
extern NSString *const kOIHPayPalClientIDProduction;


#pragma mark - Restaurant Class

extern NSString *const kOIHRestaurantName;
extern NSString *const kOIHRestaurantCurrency;
extern NSString *const kOIHRestaurantAddress;
extern NSString *const kOIHRestaurantId;
extern NSString *const kOIHRestaurantPhoneNumber;
extern NSString *const kOIHRestaurantOperatingHours;


#pragma mark - Meal Class

extern NSString *const kOIHMealName;
extern NSString *const kOIHMealAvailableDate;
extern NSString *const kOIHMealPriceInLocalCurrency;
extern NSString *const kOIHMealType;
extern NSString *const kOIHMealRestaurant;
extern NSString *const kOIHMealIngredients;
extern NSString *const kOIHMealNetWeight;
extern NSString *const kOIHMealQuantity;
extern NSString *const kOIHMealNote;


#pragma mark - Image Files

extern NSString *const kOIHImageFBIcon;
extern NSString *const kOIHImageFeedIcon;
extern NSString *const kOIHImageProfileIcon;


#pragma mark - User Class

extern NSString *const kOIHUserEmail;
extern NSString *const kOIHUserLocalProfile;
extern NSString *const kOIHUserPhoneNumber;
extern NSString *const kOIHUserName;
extern NSString *const kOIHUserLastName;
extern NSString *const kOIHUserPastOrders;
extern NSString *const kOIHUserFriends;
extern NSString *const kOIHUserAddress;
extern NSString *const kOIHUserId;
extern NSString *const kOIHUserAllowToBeAddedAsFriend;
extern NSString *const kOIHUserIsFriend;
extern NSString *const kOIHUserIsFollowing;
extern NSString *const kOIHUserPendingRequest;
extern NSString *const kOIHUserIsWaitingForConfirmation;
extern NSString *const kOIHUserProfilePic;

#pragma mark - Order Class

extern NSString *const kOIHOrderId;
extern NSString *const kOIHOrderRestaurant;
extern NSString *const kOIHOrderUser;
extern NSString *const kOIHOrderRecipient;
extern NSString *const kOIHOrderStatus;
extern NSString *const kOIHOrderReceipt;
extern NSString *const kOIHOrderDate;
extern NSString *const kOIHOrderMeal;
extern NSString *const kOIHOrderIsDelivered;
extern NSString *const kOIHOrderPaymentMethod;
extern NSString *const kOIHOrderQuantity;

extern NSString *const kOIHOrderStatusOrdered;
extern NSString *const kOIHOrderStatusAccepted;
extern NSString *const kOIHOrderStatusDelivered;
extern NSString *const kOIHOrderStatusCanceled;


#pragma mark - Address
extern NSString *const kOIHAddressUserEnteredAddress;
extern NSString *const kOIHAddressFormattedAddress;
extern NSString *const kOIHAddressLatitude;
extern NSString *const kOIHAddressLongitude;
extern NSString *const kOIHAddressTag;

#pragma mark - Activity
extern NSString *const kOIHActivityId;
extern NSString *const kOIHActivityFromUser;
extern NSString *const kOIHActivityToUser;
extern NSString *const kOIHActivityType;
extern NSString *const kOIHActivityOrder;
extern NSString *const kOIHActivityCreatedAt;

extern NSString *const kOIHActivityTypeLike;
extern NSString *const kOIHActivityTypeUserOrder;
extern NSString *const kOIHActivityTypeThank;


extern NSDictionary *const kOIHRouteClassMapping;




