//
//  OIHUserOrderReviewViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 19/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHActivityFeed.h"
@interface OIHUserOrderReviewViewController : UIViewController <UIViewControllerTransitioningDelegate>
@property (nonatomic) OIHActivityFeed *feed;

-(id)initWithReview: (OIHActivityFeed *)feed;
-(BOOL)dismissUp;

@end


@interface OrderReviewAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@property (assign) BOOL presenting;
@property (nonatomic) UIView *snapShot;
@property (nonatomic) UIVisualEffectView *backgroundView;
@property (nonatomic) NSInteger indexOfBackgroundView;

@property (assign) BOOL dismissUp;

@end
