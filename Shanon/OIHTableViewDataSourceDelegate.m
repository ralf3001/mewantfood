//
//  OIHTableViewDataSourceDelegate.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 2/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHTableViewDataSourceDelegate.h"

@interface OIHTableViewDataSourceDelegate ()
@property(nonatomic) NSMutableArray *data;
@end

@implementation OIHTableViewDataSourceDelegate

- (id)initWithQuery:(id)data {

  self = [super init];
  if (self) {

    self.data = [NSMutableArray arrayWithArray:data];
  }
  return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  // Return the number of sections.
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
  // Return the number of rows in the section.
  return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:@"cell"
                                      forIndexPath:indexPath];

  if ([self.delegate
          respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)]) {
    cell = [self.delegate tableView:tableView cellForRowAtIndexPath:indexPath];
  }

  return cell;
}

@end
