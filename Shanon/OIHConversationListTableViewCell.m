//
//  OIHConversationListTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 15/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHConversationListTableViewCell.h"
#import "OIHMessageModel.h"

@implementation OIHConversationListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
  
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
  
    _userLabel = [UILabel new];
    _userLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:_userLabel];
    
    _lastMessageLabel = [UILabel new];
    _lastMessageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:_lastMessageLabel];
    
    _userProfileImageView = [UIImageView new];
    _userProfileImageView.translatesAutoresizingMaskIntoConstraints = NO;
    _userProfileImageView.layer.cornerRadius = 30.0f;
    _userProfileImageView.layer.borderWidth = 3.0f;
    _userProfileImageView.layer.borderColor = [UIColor highlightColor].CGColor;
    _userProfileImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:_userProfileImageView];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_userProfileImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_userProfileImageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_userLabel]-[_lastMessageLabel]-|" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:0 views:NSDictionaryOfVariableBindings(_userLabel, _lastMessageLabel)]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_userProfileImageView]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_userProfileImageView)]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_userProfileImageView]-[_userLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(_userProfileImageView, _userLabel)]];
    
  }
  
  return self;
}


-(void)setConversation:(OIHConversation *)conversation{
  
  _conversation = conversation;
  
  NSMutableString *string = [NSMutableString new];
  
  OIHUserModel *chattingWithUser = nil;
  
  for (OIHUserModel *user in _conversation.users) {
    if (![user._id isEqualToString:[UserProfile currentUser]._id]) {
      [string appendFormat:@"%@ ", user.userName];
      chattingWithUser = user;
    }
  }
  
  self.userLabel.text = string;
  self.lastMessageLabel.text = _conversation.lastMessage.content;

  [self setAccessibilityLabel:[NSString stringWithFormat:NSLocalizedString(@"CONVERSATION_WITH_%@_LAST_MESSAGE_%@", @""), string, _conversation.lastMessage.content]];
  
  [self.userProfileImageView sd_setImageWithURL:[NSURL URLWithString:chattingWithUser.profilePicURL] placeholderImage:[UIImage imageFromColor: [UIColor highlightColor]]];
  
}

@end
