//
//  OIHAnimatedTextTableViewCell.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 19/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICountingLabel.h"
#import "OIHPriceWithAnimationLabel.h"


@interface OIHAnimatedTextTableViewCell : UITableViewCell
@property (nonatomic) UILabel *descriptionTitleLabel;
@property (nonatomic) OIHPriceWithAnimationLabel *descriptionContentLabel;

@end
