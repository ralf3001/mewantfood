//
//  OIHFloatingTableViewCell.h
//  Shanon
//
//  Created by Ralf Cheung on 23/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHFloatingView.h"

@interface OIHFloatingTableViewCell : UITableViewCell
@property (nonatomic) OIHFloatingView *floatingView;
@property (nonatomic) UILongPressGestureRecognizer *longPressGesture;

@end
