//
//  UIFont+HelveticaNeueLight_DynamicText.m
//  Shanon
//
//  Created by Ralf Cheung on 8/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "UIFont+HelveticaNeueLight_DynamicText.h"

@implementation UIFont (HelveticaNeueLight_DynamicText)

+ (UIFont *)preferredHelveticaNeueLightForTextStyle:(NSString *)textStyle {
    // choose the font size
    CGFloat fontSize = 16.0;
    NSString *contentSize = [UIApplication sharedApplication].preferredContentSizeCategory;
    
    if ([contentSize isEqualToString:UIContentSizeCategoryExtraSmall]) {
        fontSize = 13.0;
        
    } else if ([contentSize isEqualToString:UIContentSizeCategorySmall]) {
        fontSize = 14.0;
        
    } else if ([contentSize isEqualToString:UIContentSizeCategoryMedium]) {
        fontSize = 15.0;
        
    } else if ([contentSize isEqualToString:UIContentSizeCategoryLarge]) {
        fontSize = 17.0;
        
    } else if ([contentSize isEqualToString:UIContentSizeCategoryExtraLarge]) {
        fontSize = 19.0;
        
    } else if ([contentSize isEqualToString:UIContentSizeCategoryExtraExtraLarge]) {
        fontSize = 21.0;
        
    } else if ([contentSize isEqualToString:UIContentSizeCategoryExtraExtraExtraLarge]) {
        fontSize = 23.0;
    }
    
    return [UIFont fontWithName:textStyle size:fontSize];
    
}

@end
