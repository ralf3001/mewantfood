//
//  OIHMapLocationDetailViewController.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 17/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHMapLocationDetailViewController.h"

@interface OIHMapLocationDetailViewController ()
@property (nonatomic) UIView *contentView;
@property (nonatomic) UILabel *addressTagLabel;
@property (nonatomic) UILabel *formattedAddressLabel;
@property (nonatomic) UILabel *isAddressDefaultLabel;

@end

@implementation OIHMapLocationDetailViewController
@synthesize addressTagLabel, formattedAddressLabel, isAddressDefaultLabel;
@synthesize contentView;

-(id)initWithLocationDetail: (OIHMapViewAddressPin *)location{
    
    self = [super init];
    if (self) {
        self.locationDetail = location;
        self.transitioningDelegate = self;
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViews];
}


-(void)setupViews{
    

    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)]];
    
    [self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)]];

    NSString *addressTag = self.locationDetail.title;
    NSString *formattedAddress = self.locationDetail.formattedAddress;
    
    contentView = [UIView new];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    contentView.layer.shadowColor = [UIColor blackColor].CGColor;
    contentView.layer.shadowRadius = 30.0f;
    contentView.layer.shadowOpacity = 0.4f;
    contentView.layer.borderColor = [UIColor grayColor].CGColor;
    contentView.layer.borderWidth = 0.5f;
    contentView.layer.cornerRadius = 8.0f;
    contentView.layer.shadowOffset = CGSizeMake(-15, 20);
    contentView.layer.masksToBounds = NO;
    
    
    
    [contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(contentViewTapped)]];
    
    
    [self.view addSubview:contentView];
    
    
    addressTagLabel = [UILabel new];
    addressTagLabel.translatesAutoresizingMaskIntoConstraints = NO;
    addressTagLabel.text = [addressTag stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    addressTagLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:20];
    [contentView addSubview:addressTagLabel];
    
    
    formattedAddressLabel = [UILabel new];
    formattedAddressLabel.textColor = [UIColor grayColor];
    formattedAddressLabel.translatesAutoresizingMaskIntoConstraints = NO;
    formattedAddressLabel.text = [formattedAddress stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    formattedAddressLabel.numberOfLines = 0;
    [contentView addSubview:formattedAddressLabel];
    
    
    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[addressTagLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(addressTagLabel)]];

    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[formattedAddressLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(formattedAddressLabel)]];
    
    [contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[addressTagLabel(==addressTagHeight)]-[formattedAddressLabel]-|" options:0 metrics:@{@"addressTagHeight":@30} views:NSDictionaryOfVariableBindings(addressTagLabel, formattedAddressLabel)]];
    
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[contentView]-15-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(contentView)]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:contentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:40.0f]];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0 constant:120.0f]];

    
}
-(void)contentViewTapped{
    

}

-(void)dismiss{
    
    if ([self.delegate respondsToSelector:@selector(mapLocationDetailControllerWillDismiss:)]) {
        [self.delegate mapLocationDetailControllerWillDismiss:self];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.delegate respondsToSelector:@selector(mapLocationDetailControllerDidDismiss:)]) {
            [self.delegate mapLocationDetailControllerDidDismiss:self];
        }
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    MapLocationAnimatedTransitioning *transitioning = [MapLocationAnimatedTransitioning new];
    transitioning.presenting = YES;
    return transitioning;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    MapLocationAnimatedTransitioning * transitioning = [MapLocationAnimatedTransitioning new];
    transitioning.presenting = NO;
    return transitioning;
}


@end


@implementation MapLocationAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext{
    return 0.2;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext{
    
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *containerView = [transitionContext containerView];

    UIViewController *animatingVC = self.presenting? toViewController : fromViewController;
    UIView *animatingView = [animatingVC view];
    
    
    
    CGRect appearedFrame = [transitionContext finalFrameForViewController:animatingVC];
    CGRect dismissedFrame = appearedFrame;
    dismissedFrame.origin.y = self.presenting ? 50 : -300;
    
    CGRect initialFrame = self.presenting ? dismissedFrame : appearedFrame;
    CGRect finalFrame = self.presenting ? appearedFrame : dismissedFrame;
    
    [animatingView setFrame:initialFrame];
    
    
    if (self.presenting) {
        
        [containerView insertSubview:toViewController.view aboveSubview:fromViewController.view];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0 usingSpringWithDamping:0.39 initialSpringVelocity:13 options:UIViewAnimationOptionCurveLinear animations:^{
            [animatingView setFrame:finalFrame];
            toViewController.view.alpha = 1.0f;
            
        } completion:^(BOOL finished) {
            if (finished) {
                [transitionContext completeTransition:YES];
            }
        }];
        
    }
    else {
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            
            [animatingView setFrame:finalFrame];
            
        } completion:^(BOOL finished) {
            if (finished) {
                [transitionContext completeTransition:YES];
                
            }
        }];
        
    }
}


@end