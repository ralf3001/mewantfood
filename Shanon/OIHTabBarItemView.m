//
//  OIHTabBarItemView.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 4/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHTabBarItemView.h"


@interface OIHTabBarItemView ()
@property (nonatomic) UIButton *itemButton;
@end

@implementation OIHTabBarItemView
@synthesize redDotView;
@synthesize itemButton;

-(id)initWithButton: (UIButton *)button{
    
    self = [super init];
    if (self) {
        self.itemButton = button;
        [self addSubview:self.itemButton];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:itemButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0f]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:itemButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:itemButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
     
        
    }
    return self;
}

-(void)createNotificationView{
    
    if (!redDotView) {
        redDotView = [UIView new];
        redDotView.translatesAutoresizingMaskIntoConstraints = NO;
        redDotView.layer.cornerRadius = 3.0f;
        redDotView.backgroundColor = [UIColor redColor];
        [self addSubview:redDotView];

        [self addConstraint:[NSLayoutConstraint constraintWithItem:redDotView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:6.0f]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:redDotView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0f constant:6.0f]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:redDotView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:itemButton attribute:NSLayoutAttributeRight multiplier:1.0f constant:0.0f]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:redDotView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:itemButton attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:-5]];
        
    }
    
}


-(void)setNotificationCountForView: (NSInteger )notifications{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (notifications > 0){
            [self createNotificationView];
        }else{
            [redDotView removeFromSuperview];
        }
    });
    
}



@end
