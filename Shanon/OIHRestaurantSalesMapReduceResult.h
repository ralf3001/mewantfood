//
//  OIHRestaurantSalesMapReduceResult.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 30/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OIHRestaurantSalesMapReduceResult : NSObject

@property (nonatomic) NSString *restaurantName;
@property (nonatomic) double value;
@property (nonatomic) NSArray *salesInRange;

-(id)initWithInfo: (NSDictionary *)info;
+(NSArray *)salesResultsFromArray: (id)data;

@end
