//
//  OIHRestaurantMealDetailViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 28/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OIHMealModel.h"
#import "OIHRestaurantModel.h"

@class OIHRestaurantMealDetailViewController;

@protocol OIHMealDetailDelegate <NSObject>

@optional

-(void)userDidPressConfirmOrder: (OIHMealModel *)meal  viewController: (OIHRestaurantMealDetailViewController *)viewController;
-(void)userDidCancelOrder: (OIHRestaurantMealDetailViewController *)viewController;

@end


@interface OIHRestaurantMealDetailViewController : UIViewController <UIViewControllerTransitioningDelegate>
//@property (nonatomic) OIHRestaurantModel *restaurantInfo;
@property (nonatomic) OIHMealModel *mealInfo;

@property (nonatomic, weak) id<OIHMealDetailDelegate> delegate;
@property (nonatomic) CGRect originalCellFrame;
@end


@interface MealDetailAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (assign) BOOL isPresenting;
@property (nonatomic) CGRect fromFrame;
@property (weak) id<UIViewControllerContextTransitioning> transitionContext;

@end