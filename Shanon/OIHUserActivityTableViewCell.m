//
//  OIHUserActivityTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 3/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import "OIHUserActivityTableViewCell.h"
#import "OIHFloatingView.h"


#define kImageViewWidth 80.0


@interface OIHUserActivityTableViewCell ()
@property (nonatomic) UIImageView *arrowImageView;
@property (nonatomic) OIHFloatingView *floatingView;
@property (nonatomic) UILabel *debugLabel;
@property (nonatomic) UIView *buttonHorizontalDivider;
@property (nonatomic) UILabel *timeLabel;

@end


@implementation OIHUserActivityTableViewCell

@synthesize fromUserImageView, toUserImageView;
@synthesize descriptionLabel;
@synthesize arrowImageView;
@synthesize floatingView;
@synthesize likeButton;
@synthesize debugLabel;
@synthesize isPostLiked;
@synthesize buttonHorizontalDivider;
@synthesize timeLabel;
@synthesize thanksButton;
@synthesize commentButton;
@synthesize activityInfo = _activityInfo;


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    
    [floatingView setHighlight:highlighted];
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        floatingView = [OIHFloatingView new];
        floatingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:floatingView];
        
        fromUserImageView = [[UIImageView alloc] init];
        fromUserImageView.translatesAutoresizingMaskIntoConstraints = NO;
        fromUserImageView.layer.cornerRadius = kImageViewWidth / 2;
        fromUserImageView.layer.borderColor = [UIColor colorWithWhite:0.95 alpha:1.0f].CGColor;
        fromUserImageView.layer.borderWidth = 2.0f;
        fromUserImageView.contentMode = UIViewContentModeScaleAspectFill;
        fromUserImageView.layer.masksToBounds = YES;
        fromUserImageView.image = [IonIcons imageWithIcon:ion_ios_person size:60 color:[UIColor lightGrayColor]];
        [floatingView addSubview:fromUserImageView];
        
        toUserImageView = [[UIImageView alloc] init];
        toUserImageView.translatesAutoresizingMaskIntoConstraints = NO;
        toUserImageView.layer.cornerRadius = kImageViewWidth / 2;
        toUserImageView.layer.borderColor = [UIColor colorWithWhite:0.95 alpha:1.0f].CGColor;
        toUserImageView.layer.borderWidth = 2.0f;
        toUserImageView.contentMode = UIViewContentModeScaleAspectFill;
        toUserImageView.layer.masksToBounds = YES;
        toUserImageView.image = [IonIcons imageWithIcon:ion_ios_person size:60 color:[UIColor lightGrayColor]];
        [floatingView addSubview:toUserImageView];
        
        descriptionLabel = [UILabel new];
        descriptionLabel.numberOfLines = 0;
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        descriptionLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        [floatingView addSubview:descriptionLabel];
        
        debugLabel = [UILabel new];
        debugLabel.numberOfLines = 0;
        debugLabel.translatesAutoresizingMaskIntoConstraints = NO;
        debugLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCallout];
        
        
        timeLabel = [UILabel new];
        timeLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
        timeLabel.textColor = [UIColor lightGrayColor];
        timeLabel.numberOfLines = 0;
        timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [floatingView addSubview:timeLabel];
        
        
        arrowImageView = [[UIImageView alloc] init];
        arrowImageView.translatesAutoresizingMaskIntoConstraints = NO;
        arrowImageView.image = [IonIcons imageWithIcon:ion_ios_arrow_thin_right iconColor:[UIColor grayColor] iconSize:40 imageSize:CGSizeMake(100, 100)];
        [floatingView addSubview:arrowImageView];
        
        
        thanksButton = [UIButton buttonWithType:UIButtonTypeSystem];
        thanksButton.titleLabel.font = [UIFont activityButtonNormal];
        [thanksButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        thanksButton.translatesAutoresizingMaskIntoConstraints = NO;
        [thanksButton setTitle:NSLocalizedString(@"ACTIVITY_BUTTON_THANKS", @"Thanks!") forState:UIControlStateNormal];
        [thanksButton addTarget:self action:@selector(thanksButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [floatingView addSubview:thanksButton];
        
        
        commentButton = [UIButton buttonWithType:UIButtonTypeSystem];
        commentButton.translatesAutoresizingMaskIntoConstraints = NO;
        commentButton.titleLabel.font = [UIFont activityButtonNormal];
        [commentButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [commentButton setTitle:NSLocalizedString(@"ACTIVITY_BUTTON_COMMENT", @"Comment") forState:UIControlStateNormal];
        [commentButton addTarget:self action:@selector(commentButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [floatingView addSubview:commentButton];
        
        
        buttonHorizontalDivider = [UIView new];
        buttonHorizontalDivider.translatesAutoresizingMaskIntoConstraints = NO;
        buttonHorizontalDivider.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0f].CGColor;
        buttonHorizontalDivider.layer.borderWidth = 0.3f;
        [floatingView addSubview:buttonHorizontalDivider];
        
        
        UIView *verticalDivider = [UIView new];
        verticalDivider.translatesAutoresizingMaskIntoConstraints = NO;
        verticalDivider.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0f].CGColor;
        verticalDivider.layer.borderWidth = 0.3f;
        [floatingView addSubview:verticalDivider];
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[floatingView]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(floatingView)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[floatingView]-20-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(floatingView)]];
        
        
        UIView *arrowLeftDivider = [UIView new];
        arrowLeftDivider.translatesAutoresizingMaskIntoConstraints = NO;
        [floatingView addSubview:arrowLeftDivider];
        
        UIView *arrowRightDivider = [UIView new];
        arrowRightDivider.translatesAutoresizingMaskIntoConstraints = NO;
        [floatingView addSubview:arrowRightDivider];
        
        
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[fromUserImageView(==iconWidth)]-[arrowLeftDivider]-[arrowImageView]-[arrowRightDivider]-[toUserImageView(==iconWidth)]-|" options:NSLayoutFormatAlignAllCenterY  metrics:@{@"iconWidth": @kImageViewWidth} views:NSDictionaryOfVariableBindings(fromUserImageView, toUserImageView, arrowImageView, arrowRightDivider, arrowLeftDivider)]];
        
        [floatingView addConstraint:[NSLayoutConstraint constraintWithItem:arrowImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:floatingView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        
        
        [floatingView addConstraint:[NSLayoutConstraint constraintWithItem:arrowImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:arrowImageView attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
        
        [floatingView addConstraint:[NSLayoutConstraint constraintWithItem:fromUserImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:toUserImageView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0f]];
        
        [floatingView addConstraint:[NSLayoutConstraint constraintWithItem:fromUserImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:toUserImageView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0f]];
        
        
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[descriptionLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(descriptionLabel)]];
        
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[buttonHorizontalDivider]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(buttonHorizontalDivider)]];
        
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[timeLabel]-|" options:0 metrics:0 views:NSDictionaryOfVariableBindings(timeLabel)]];
        
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[thanksButton][verticalDivider(==1)][commentButton(==thanksButton)]-|" options:NSLayoutFormatAlignAllCenterY | NSLayoutFormatAlignAllTop | NSLayoutFormatAlignAllBottom metrics:0 views:NSDictionaryOfVariableBindings(thanksButton, commentButton, verticalDivider)]];
        
        
        
        [floatingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[descriptionLabel]-[timeLabel]-15-[fromUserImageView(==iconHeight)]-[buttonHorizontalDivider(==1)]-[thanksButton]-|" options:0 metrics:@{@"iconHeight": @kImageViewWidth} views:NSDictionaryOfVariableBindings(descriptionLabel, fromUserImageView, timeLabel, thanksButton, buttonHorizontalDivider)]];
        
        
    }
    
    return self;
}

-(void) setDescriptionLabelText: (NSString *) text{
    
    descriptionLabel.text = text;
    
}


-(void)setIsPostLiked:(BOOL)isPostLiked{
    
    if (_activityInfo.isPostThanked) {
        [likeButton setBackgroundImage:[IonIcons imageWithIcon:ion_ios_heart size:30 color:[UIColor redColor]] forState:UIControlStateNormal];
    }else{
        [likeButton setBackgroundImage:[IonIcons imageWithIcon:ion_ios_heart_outline size:30 color:[UIColor grayColor]] forState:UIControlStateNormal];
    }
    
}

-(void)activityLiked: (UIButton *)button{
    
    
    [button setBackgroundImage:[IonIcons imageWithIcon:ion_ios_heart size:30 color:[UIColor redColor]] forState:UIControlStateNormal];
    
    if (!isPostLiked) {
        if ([self.delegate respondsToSelector:@selector(userDidTapLikeButton:isPostLiked:)]) {
            [self.delegate userDidTapLikeButton:self.activityInfo isPostLiked:YES];
        }
        [self setIsPostLiked:YES];
    }else{
        if ([self.delegate respondsToSelector:@selector(userDidTapLikeButton:isPostLiked:)]) {
            [self.delegate userDidTapLikeButton:self.activityInfo isPostLiked:NO];
        }
        [self setIsPostLiked:NO];
    }
    
}

-(void)thanksButtonPressed: (UIButton *)button{
    
    if (_activityInfo.isPostThanked == YES) {
        
        thanksButton.titleLabel.font = [UIFont activityButtonNormal];
        
        _activityInfo.isPostThanked = NO;
        
        if ([self.delegate respondsToSelector:@selector(userDidTapThanksButton:isPostThanked:)]) {
            [self.delegate userDidTapThanksButton:self.activityInfo isPostThanked:NO];
        }
        
    }else{
        
        thanksButton.titleLabel.font = [UIFont buttonTextBold];
        _activityInfo.isPostThanked = YES;
        
        if ([self.delegate respondsToSelector:@selector(userDidTapThanksButton:isPostThanked:)]) {
            [self.delegate userDidTapThanksButton:self.activityInfo isPostThanked:YES];
        }
        
    }
    
}


-(void)commentButtonPressed: (UIButton *)button{
    
    if ([self.delegate respondsToSelector:@selector(userDidTapCommentButton:)]) {
        [self.delegate userDidTapCommentButton:self.activityInfo];
    }
    
}


-(void)setActivityInfo:(OIHActivityFeed *)activityInfo{
    
    _activityInfo = activityInfo;
    [self activityInfoDescription];
    
}


-(void)activityInfoDescription{
    
    
    switch (_activityInfo.type) {
            
        case UserOrder:{
            
            if ([_activityInfo.fromUser._id isEqualToString:[UserProfile currentUser]._id] && [_activityInfo.toUser._id isEqualToString:[UserProfile currentUser]._id]) {
                if (_activityInfo.order.quantity > 1) {
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"YOU_ORDERED_%@_%@", @"You order [number] [dish name]"),
                                             [NSString stringWithFormat:@"%ld", (long)_activityInfo.order.quantity],
                                             _activityInfo.order.orderMeal.name
                                             ];
                    
                }else{
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"YOU_ORDERED_A_%@", @"You order a [dish name]"),
                                             _activityInfo.order.orderMeal.name];
                }
                
                if(_activityInfo.order.orderMeal.productImage){
                    [arrowImageView sd_setImageWithURL:[NSURL URLWithString:_activityInfo.order.orderMeal.productImage] placeholderImage:[UIImage imageFromColor:[UIColor lightGrayColor]]];
                    fromUserImageView.image = nil;
                    toUserImageView.image = nil;
                }
                
                //Current user ordered something for him/herself
            }
            else if ([_activityInfo.toUser._id isEqualToString:_activityInfo.fromUser._id]) {
                
                if (_activityInfo.order.quantity > 1) {
                    
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_ORDERED_%@_%@", @"[person] ordered [quantity] [dish name]"),
                                             _activityInfo.fromUser.userName,
                                             [NSString stringWithFormat:@"%ld", (long)_activityInfo.order.quantity],
                                             _activityInfo.order.orderMeal.name
                                             ];
                    
                }else{
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_ORDERED_A_%@", @"[person] ordered a [dish name]"),
                                             _activityInfo.fromUser.userName,
                                             _activityInfo.order.orderMeal.name
                                             ];
                }
                
            }
            else if ([[UserProfile currentUser]._id isEqualToString:_activityInfo.fromUser._id]){
                
                if (_activityInfo.order.quantity > 1) {
                    
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"YOU_ORDERED_%@_%@_FOR_%@", @"You ordered a [dish name] for [person]"),
                                             [NSString stringWithFormat:@"%ld", (long)_activityInfo.order.quantity],
                                             _activityInfo.order.orderMeal.name,
                                             _activityInfo.toUser.userName
                                             ];
                    
                }else{
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"YOU_ORDERED_A_%@_FOR_%@", @"You ordered a [dish name] for [person]"),
                                             _activityInfo.order.orderMeal.name,
                                             _activityInfo.toUser.userName
                                             ];
                    
                }
                
            }
            else if([[UserProfile currentUser]._id isEqualToString:_activityInfo.toUser._id]){
                
                if (_activityInfo.order.quantity > 1) {
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_ORDERED_YOU_%@_%@", @"[person] ordered you [number] [dish name]"),
                                             _activityInfo.fromUser.userName,
                                             [NSString stringWithFormat:@"%ld", (long)_activityInfo.order.quantity],
                                             _activityInfo.order.orderMeal.name
                                             ];
                    
                }else{
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_ORDERED_YOU_A_%@", @"[person] ordered you a [dish name]"),
                                             _activityInfo.fromUser.userName,
                                             _activityInfo.order.orderMeal.name
                                             ];
                    
                }
                
            }
            else {
                if (_activityInfo.order.quantity > 1) {
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_ORDERED_%@_%@_FOR_%@", @"[person A name] ordered a [dish name] for [person B name]"),
                                             _activityInfo.fromUser.userName,
                                             [NSString stringWithFormat:@"%ld", (long)_activityInfo.order.quantity],
                                             _activityInfo.order.orderMeal.name,
                                             _activityInfo.toUser.userName
                                             ];
                    
                }else{
                    descriptionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@_ORDERED_A_%@_FOR_%@", @"[person A name] ordered a [dish name] for [person B name]"),
                                             _activityInfo.fromUser.userName,
                                             _activityInfo.order.orderMeal.name,
                                             _activityInfo.toUser.userName
                                             ];
                    
                }
                
            }
            
            break;
            
        }
            
        default:
            break;
            
    }
    
    if (_activityInfo.fromUser) {
        [self.fromUserImageView sd_setImageWithURL:[NSURL URLWithString:_activityInfo.fromUser.profilePicURL] placeholderImage:[IonIcons imageWithIcon:ion_ios_person size:60 color:[UIColor lightGrayColor]]];
    }
    if (_activityInfo.toUser) {
        
        [self.toUserImageView sd_setImageWithURL:[NSURL URLWithString:_activityInfo.toUser.profilePicURL] placeholderImage:[IonIcons imageWithIcon:ion_ios_person size:60 color:[UIColor lightGrayColor]]];
    }
    
    debugLabel.text = [NSString stringWithFormat:@"DEBUG_INFO: %@", _activityInfo._id];
    debugLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    timeLabel.text = [NSDate daysBetweenDate:_activityInfo.createdAt andDate:[NSDate date]];
    
    if (_activityInfo.isPostThanked) {
        thanksButton.titleLabel.font = [UIFont buttonTextBold];
    }else{
        thanksButton.titleLabel.font = [UIFont activityButtonNormal];
        
    }
    
}



-(NSString *)compareDate: (NSDate *)postDate{
    
    NSCalendar *gregorianCal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComps = [gregorianCal components: (NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:postDate];
    
    NSInteger postMinutes = [dateComps minute];
    NSInteger postHours = [dateComps hour];
    NSInteger postDay = [dateComps day];
    
    NSDateComponents *curentDateComparison = [gregorianCal components: (NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:[NSDate date]];
    
    
    if (([curentDateComparison minute] - postMinutes < 60) && [curentDateComparison hour] == postHours && [curentDateComparison day] == [dateComps day]) {
        
        if ([curentDateComparison minute] - postMinutes < 1) {
            return NSLocalizedString(@"POSTED_JUST_NOW", @"");
        }
        return [NSString stringWithFormat:NSLocalizedString(@"POSTED_%ld_MINS_AGO", @"Posted [mins] mins ago"), ([curentDateComparison minute] - postMinutes)]; //minutes
    }else if (([curentDateComparison hour] - postHours < 24) && [curentDateComparison day] == [dateComps day]){
        return [NSString stringWithFormat:NSLocalizedString(@"POSTED_%ld_HRS_AGO", @"Posted [mins] mins ago"), [curentDateComparison hour] - postHours]; //hours
    }else if([curentDateComparison day] - postDay < 3){
        if ([curentDateComparison day] - postDay == 1) {
            return NSLocalizedString(@"POSTED_YESTERDAY", @""); //days
        }
        return [NSString stringWithFormat:NSLocalizedString(@"POSTED_%ld_DAYS_AGO", @""), [curentDateComparison day] - postDay]; //days
        
    }else{
        NSDateFormatter *format = [NSDateFormatter new];
        format.dateStyle = kCFDateFormatterShortStyle;
        return [format stringFromDate:postDate];
        
    }
    
    return @"";
}


@end
