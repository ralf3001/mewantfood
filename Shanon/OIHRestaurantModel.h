//
//  OIHRestaurantModel.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OIHModelProtocol.h"

@class OIHMealModel;

@interface OIHRestaurantModel : NSObject <OIHModelProtocol>
@property (nonatomic) NSString *restaurantName;
@property (nonatomic) NSString *_id;
@property (nonatomic) NSArray *operatingHours;
@property (nonatomic) NSString *phoneNumber;
@property (nonatomic) NSString *addressInfo;
@property (nonatomic) NSDictionary *restaurantInfo;
@property (nonatomic) UIImage *companyImage;
@property (nonatomic) NSString *restaurantCurrency;
@property (nonatomic) NSArray *companyLogos;
@property (nonatomic) NSArray *restaurantPictures;

@property (nonatomic) OIHMealModel *meal;
@property (nonatomic) NSArray *rating;
@property (nonatomic) CLLocationCoordinate2D coordinate;

@property (nonatomic) NSString *sellersNote;

-(id)initWithDictionary: (NSDictionary *)info;
+(NSArray *) restaurantsFromArray: (id)data;

-(void)setAttribute: (NSString *) attribute data: (id)data;

@end
