//
//  OIHModalPresentationController.h
//  Shanon
//
//  Created by Ralf Cheung on 9/4/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHModalPresentationController : UIPresentationController
@property (nonatomic) UIView *dimmingView;

@end
