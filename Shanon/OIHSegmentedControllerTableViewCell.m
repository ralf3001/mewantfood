//
//  OIHSegmentedControllerTableViewCell.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHSegmentedControllerTableViewCell.h"

@implementation OIHSegmentedControllerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
