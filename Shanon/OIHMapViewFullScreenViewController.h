//
//  OIHMapViewFullScreenViewController.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 16/12/2015.
//  Copyright © 2015 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface OIHMapViewFullScreenViewController : UIViewController
@property (nonatomic) MKMapView *mapView;
@property (nonatomic) NSArray *annotationsArray;

-(id)initWithAnnotations: (NSArray *)annotations region:(MKCoordinateRegion) region;

@end
