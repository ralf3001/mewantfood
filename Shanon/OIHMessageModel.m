//
//  OIHMessageModel.m
//  Oh It's Here
//
//  Created by Ralf Cheung on 22/2/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import "OIHMessageModel.h"

@implementation OIHMessageModel


-(OIHMessageModel *)initWithDictionary: (id)info{
    
    self = [super init];
    if (self) {
        self.content = info[@"content"];
        self.messageType = [self typeEnumFromString:info[@"type"]];
        self.createdDate = [NSDate dateFromJSONDate:info[@"createDate"]];
        self.isSentFromUser = [info[@"sender"] isEqualToString:[UserProfile currentUser]._id];
    }
    return self;
}

-(id)init{
  
  self = [super init];
  if (self) {
    self.messageType = String;
  }
  return self;
}

+(NSArray *)initWithArray: (id)list{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary *dictionary in list) {
        [array addObject:[[OIHMessageModel alloc] initWithDictionary:dictionary]];
    }
    
    return array;
    
}


- (MessageType)typeEnumFromString:(NSString *)type {
  
  NSDictionary<NSString *, NSNumber *> *types = @{
                                                  @"string" : @(String),
                                                  @"picture" : @(Picture),
                                                  @"audio" : @(Audio)
                                                  
                                                  };
  
  return types[type].integerValue;
}

+ (NSDictionary *)typeDisplayNames {
  return @{
           @(String) : @"string",
           @(Picture) : @"picture",
           @(Audio) : @"audio",
           };
}

- (NSString *)messageTypeString {
  return [[self class] typeDisplayNames][@(self.messageType)];
}


-(void)sendMessage :(void (^)(id data, NSError *error))completionHandler{
  
  JSONRequestSettings *setting = [[JSONRequestSettings alloc] initWithRoute:[NSString stringWithFormat:@"message/%@", _conversation._id] httpMethod:HTTP_POST];
  setting.requestData = self.toDictionary;
  
  [[JSONRequest sharedInstance] fetch:setting completionHandler:^(id data, BOOL isCachedResult, NSError *error) {
    if (completionHandler) {
      completionHandler(data, error);
    }
  }];
  
}


-(NSDictionary *)toDictionary{
  
  return @{
           @"content": _content ? _content : @"",
           @"_id": _conversation._id ? _conversation._id : @"",
           @"url": _url ? _url : @"",
           @"type": [self messageTypeString]
           };
  
}

@end
