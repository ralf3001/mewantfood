//
//  NSMutableArray+extraBits.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 10/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface NSMutableArray (extraBits)
@property (nonatomic) NSNumber* isDataDirty;
@end
