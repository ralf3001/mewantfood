//
//  OIHUserRatingView.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 21/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OIHUserRatingView : UIView

@property (nonatomic) double ratingScore;

@end
