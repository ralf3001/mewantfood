//
//  OIHTabBar.h
//  Oh It's Here
//
//  Created by Ralf Cheung on 1/1/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OIHTabBarDelegate <NSObject>

-(void)barItemSelected: (NSInteger )index;

@end

@interface OIHTabBar : UIView

@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, weak) id<OIHTabBarDelegate> delegate;

-(void)setNotificationForBarItem: (NSInteger)barItem;
-(void)removeNotificationFroBarItem: (NSInteger)barItem;

-(id)initWithTabBarItems: (NSArray *)viewControllers;
@end
