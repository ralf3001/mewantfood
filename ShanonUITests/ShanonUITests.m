//
//  ShanonUITests.m
//  ShanonUITests
//
//  Created by Ralf Cheung on 23/3/2016.
//  Copyright © 2016 Ralf Cheung. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OIHOrderModel.h"
#import "OIHMealModel.h"
#import "OIHReceiptDetailViewController.h"
#import "OIHRestaurantModel.h"

@interface ShanonUITests : XCTestCase
@property (nonatomic) OIHOrderModel *order;

@end

@implementation ShanonUITests

- (void)setUp {
    [super setUp];
    
    OIHRestaurantModel *rest = [[OIHRestaurantModel alloc] init];
    rest.restaurantName = @"Good Restaurant";
    rest.restaurantCurrency = @"HKD";
    
    OIHMealModel *meal = [[OIHMealModel alloc] init];
    meal.name = @"Testing meal";
    meal.priceInString = @"30.0";
    meal.priceInLocalCurrency = [NSNumber numberWithFloat:30.0f];
    meal.restaurant = rest;
    
    _order = [[OIHOrderModel alloc] init];
    _order.orderMeal = meal;
    _order.status = Ordered;
    
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testReceiptDetail{
    
    OIHReceiptDetailViewController *receiptDetail = [[OIHReceiptDetailViewController alloc] initWithOrder:_order];
    XCUIApplication *app = [XCUIApplication new];
    
}


- (void)testExample {
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

@end
